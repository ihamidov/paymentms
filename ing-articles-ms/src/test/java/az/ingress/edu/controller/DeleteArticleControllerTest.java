package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.ArticleService;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
@ActiveProfiles("test")
public class DeleteArticleControllerTest {

    public static final String ARTICLES_WITH_ID_PATH = "/articles/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String CREATED_BY = "Ejder Zekeryeyev";
    private static final String CREATED_BY_USERNAME = "ejder";
    private static final Set<String> ROLES = new HashSet<>(Arrays.asList("USER"));
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(CREATED_BY_USERNAME, CREATED_BY,
            ROLES);
    private static final String NOT_ALLOWED_OPERATION = "You don't have access for this operation";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccessToken accessToken;

    @MockBean
    private ArticleService articleService;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(CREATED_BY_USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
        when(accessToken.getRealmAccess()).thenReturn(getAccess());
    }

    @Test
    public void givenNegativeArticleIdDeleteArticleExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete(ARTICLES_WITH_ID_PATH, -1L))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroArticleIdDeleteArticleExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .delete(ARTICLES_WITH_ID_PATH, 0))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingArticleIdDeleteArticleExpectErrorMessage() throws Exception {
        long id = 1000L;
        String articleNotFoundMessage = getArticleNotFoundMessage(id);
        Mockito.doThrow(new ArticleNotFoundException(id)).when(articleService).deleteArticle(id,CREATED_BY_USERDETAILS);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(ARTICLES_WITH_ID_PATH, id))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(articleNotFoundMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(articleNotFoundMessage));

        verify(articleService).deleteArticle(id,CREATED_BY_USERDETAILS);
    }

    @Test
    public void givenExistingArticleIdAndUserWithNonOwnerUsernameAndUserRoleDeleteArticleExpectException()
            throws Exception {

        Mockito.doThrow(new AttemptToUnauthorizedOperationException())
                .when(articleService).deleteArticle(1L,CREATED_BY_USERDETAILS);

        mockMvc.perform(MockMvcRequestBuilders
                .delete(ARTICLES_WITH_ID_PATH, 1L))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(NOT_ALLOWED_OPERATION))
                .andExpect(jsonPath(USER_MESSAGE).value(NOT_ALLOWED_OPERATION));

        verify(articleService).deleteArticle(1L,CREATED_BY_USERDETAILS);
    }

    @Test
    public void givenExistingArticleIdDeleteArticleExpectOk() throws Exception {
        long id = 1L;

        mockMvc.perform(MockMvcRequestBuilders
                .delete(ARTICLES_WITH_ID_PATH, id))
                .andExpect(status().isOk());

        verify(articleService).deleteArticle(id,CREATED_BY_USERDETAILS);
    }

    private String getArticleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }

    private AccessToken.Access getAccess() {
        return new AccessToken.Access().addRole("USER");
    }

}
