package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.service.BlogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(BlogController.class)
@ActiveProfiles("test")
public class DeleteBlogControllerTest {

    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "userMessage";
    private static final String TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DELETE_BLOG_PATH = "/blog/{id}";
    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final String UNAUTHORIZED_OPERATION = "Unauthorized Operation";


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BlogService blogService;

    @Test
    public void givenExistingReviewIdExpectOk() throws Exception {
        mockMvc.perform(delete(DELETE_BLOG_PATH, 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

        verify(blogService).deleteBlogById(1L);
    }

    @Test
    public void givenNegativeBlogIdExpectErrorMessage() throws Exception {
        mockMvc.perform(delete(DELETE_BLOG_PATH, -1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingBlogIdExpectErrorMessage() throws Exception {
        doThrow(new BlogNotFountException(1L)).when(blogService).deleteBlogById(anyLong());

        mockMvc.perform(delete(DELETE_BLOG_PATH, 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(getBlogNotFoundMessage(1L)))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(getBlogNotFoundMessage(1L)));

        verify(blogService).deleteBlogById(1L);
    }

    @Test
    public void givenExistingBlogIdWithUnauhtorizedUserExpectErrorMessage() throws Exception {
        doThrow(new UnauthorizedOperationException()).when(blogService).deleteBlogById(anyLong());

        mockMvc.perform(delete(DELETE_BLOG_PATH, 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(UNAUTHORIZED_OPERATION))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(UNAUTHORIZED_OPERATION));

        verify(blogService).deleteBlogById(1L);
    }

    private String getBlogNotFoundMessage(long id) {
        return String.format("Blog with id: %d not found", id);
    }

}
