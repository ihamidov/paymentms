package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.StickNoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(StickNoteController.class)
@ActiveProfiles("test")
public class GetStickNoteControllerTest {

    private static final String STICKNOTE_WITH_ID_PATH = "/sticknote/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ID_MUST_BE_POSITIVE = "StickNote id must be positive";
    private static final String DUMMY_TEXT = "STICKNOTE title";
    private static final String ID = "$.id";
    private static final String X_COORDINATE = "$.xCoordinate";
    private static final String Y_COORDINATE = "$.yCoordinate";
    private static final String ARTICLE_ID = "$.articleId";
    private static final String TITLE = "$.title";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StickNoteService stickNoteService;

    @MockBean
    private AccessToken accessToken;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
    }

    @Test
    public void givenNegativeStickNoteIdGetStickNoteExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_WITH_ID_PATH, -1)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroStickNoteIdGetStickNoteExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_WITH_ID_PATH, 0)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingStickNoteIdGetStickNoteExpectErrorMessage() throws Exception {
        long id = 1000;
        String errorMessage = getStickNoteNotFoundMessage(id);
        when(stickNoteService.getStickNote(id)).thenThrow(new StickNoteNotFoundException(id));

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_WITH_ID_PATH, id)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(errorMessage));

        verify(stickNoteService).getStickNote(id);
    }

    @Test
    public void givenExistingStickNoteIdGetStickNote() throws Exception {
        long id = 2L;
        StickNote stickNoteWithId = new StickNote(2L, DUMMY_TEXT, 1L, 20, 10, Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteService.getStickNote(id)).thenReturn(stickNoteWithId);

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_WITH_ID_PATH, id)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ID).value(2L))
                .andExpect(MockMvcResultMatchers.jsonPath(TITLE).value(DUMMY_TEXT))
                .andExpect(MockMvcResultMatchers.jsonPath(X_COORDINATE).value(20))
                .andExpect(MockMvcResultMatchers.jsonPath(Y_COORDINATE).value(10))
                .andExpect(MockMvcResultMatchers.jsonPath(ARTICLE_ID).value(1));

        verify(stickNoteService).getStickNote(id);
    }

    private String getStickNoteNotFoundMessage(long id) {
        return "StickNote with id: " + id + " not found";
    }
}
