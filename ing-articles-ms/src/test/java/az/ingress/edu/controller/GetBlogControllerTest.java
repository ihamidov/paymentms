package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.service.BlogService;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(BlogController.class)
@ActiveProfiles("test")
public class GetBlogControllerTest {

    private static final String GET_BLOG_PATH = "/blog/{id}";
    private static final String ID = "id";
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "userMessage";
    private static final String TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DUMMY_TEXT = "This is dummy";
    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final String PHOTO = "www.dummy.com/dummy.png";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BlogService blogService;

    @Test
    public void givenExistingReviewIdExpectOk() throws Exception {
        Blog blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), null, Calendar.getInstance(), null);
        when(blogService.findBlogById(anyLong())).thenReturn(blog);

        mockMvc.perform(get(GET_BLOG_PATH, 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(blog.getId()));

        verify(blogService).findBlogById(1L);
    }

    @Test
    public void givenNegativeReviewIdExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_BLOG_PATH, -1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingReviewIdExpectErrorMessage() throws Exception {
        when(blogService.findBlogById(anyLong())).thenThrow(new BlogNotFountException(1L));

        mockMvc.perform(get(GET_BLOG_PATH, 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(getBlogNotFoundMessage(1L)))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(getBlogNotFoundMessage(1L)));

        verify(blogService).findBlogById(1L);
    }


    private String getBlogNotFoundMessage(long id) {
        return String.format("Blog with id: %d not found", id);
    }
}
