package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.service.BlogService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(BlogController.class)
@ActiveProfiles("test")
public class CreateBlogControllerTest {

    private static final String CREATE_BLOG_PATH = "/blog";
    private static final String ID = "id";
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "userMessage";
    private static final String TECHNICAL_MESSAGE = "technicalMessage";
    private static final String TITLE_CAN_T_BE_EMPTY = "Title can't be empty";
    private static final String DUMMY_TEXT = "This is dummy";
    private static final String PHOTO = "www.dummy.com/dummy.png";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BlogService blogService;

    private CreateBlogDto createBlogDto;

    private Blog blog;

    @Before
    public void setUp() {
        blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), null, Calendar.getInstance(), null);
        createBlogDto = CreateBlogDto.builder().title(DUMMY_TEXT).content(DUMMY_TEXT).category(DUMMY_TEXT)
                .photo(PHOTO).build();
    }

    @Test
    public void givenBlogCreateBlog() throws Exception {
        when(blogService.saveBlog(any(CreateBlogDto.class)))
                .thenReturn(blog);

        mockMvc.perform(post(CREATE_BLOG_PATH)
                .content(objectMapper.writeValueAsString(createBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(1));

        verify(blogService).saveBlog(createBlogDto);
    }

    @Test
    public void givenBlogWithEmptyTitleCreateBlogExpectException() throws Exception {
        when(blogService.saveBlog(any(CreateBlogDto.class)))
                .thenReturn(blog);
        createBlogDto.setTitle("");

        mockMvc.perform(post(CREATE_BLOG_PATH)
                .content(objectMapper.writeValueAsString(createBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(TITLE_CAN_T_BE_EMPTY))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(TITLE_CAN_T_BE_EMPTY));
    }
}
