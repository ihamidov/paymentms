package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.dto.UpdateBlogDto;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.service.BlogService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(BlogController.class)
@ActiveProfiles("test")
public class UpdateBlogControllerTest {

    private static final String UPDATE_BLOG_PATH = "/blog/{id}";
    private static final String ID = "id";
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "userMessage";
    private static final String TECHNICAL_MESSAGE = "technicalMessage";
    private static final String TITLE_CAN_T_BE_EMPTY = "Title can't be empty";
    private static final String DUMMY_TEXT = "This is dummy";
    private static final String STATUS_CAN_NOT_BE_EMPTY = "Status can not be empty";
    private static final String PHOTO = "www.dummy.com/dummy.png";
    private static final String UNAUTHORIZED_OPERATION = "Unauthorized Operation";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BlogService blogService;

    private UpdateBlogDto updateBlogDto;

    private Blog blog;

    @Before
    public void setUp() {
        blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), null, Calendar.getInstance(), null);
        updateBlogDto = UpdateBlogDto.builder().title(DUMMY_TEXT).content(DUMMY_TEXT).photo(PHOTO).category(DUMMY_TEXT)
                .status(BlogStatus.DRAFT).build();
    }

    @Test
    public void givenBlogUpdateBlog() throws Exception {
        when(blogService.updateBlog(any(UpdateBlogDto.class), anyLong())).thenReturn(blog);

        mockMvc.perform(put(UPDATE_BLOG_PATH, 1L)
                .content(objectMapper.writeValueAsBytes(updateBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value("1"));
    }

    @Test
    public void givenBlogWithEmptyTitleCreateBlogExpectException() throws Exception {
        when(blogService.saveBlog(any(CreateBlogDto.class)))
                .thenReturn(blog);
        updateBlogDto.setTitle("");

        mockMvc.perform(put(UPDATE_BLOG_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(TITLE_CAN_T_BE_EMPTY))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(TITLE_CAN_T_BE_EMPTY));
    }

    @Test
    public void givenBlogWithEmptyStatusUpdateBlogExpectException() throws Exception {
        when(blogService.saveBlog(any(CreateBlogDto.class)))
                .thenReturn(blog);
        updateBlogDto.setStatus(null);

        mockMvc.perform(put(UPDATE_BLOG_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(STATUS_CAN_NOT_BE_EMPTY))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(STATUS_CAN_NOT_BE_EMPTY));
    }

    @Test
    public void givenNonExistingBlogUpdateBlogExpectException() throws Exception {
        when(blogService.updateBlog(any(UpdateBlogDto.class), anyLong()))
                .thenThrow(new BlogNotFountException(1L));

        mockMvc.perform(put(UPDATE_BLOG_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(getBlogNotFoundMessage(1L)))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(getBlogNotFoundMessage(1L)));
    }

    @Test
    public void givenExistingBlogAndNonAuthorizedUserUpdateBlogExpectException() throws Exception {
        when(blogService.updateBlog(any(UpdateBlogDto.class), anyLong()))
                .thenThrow(new UnauthorizedOperationException());

        mockMvc.perform(put(UPDATE_BLOG_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateBlogDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(UNAUTHORIZED_OPERATION))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(UNAUTHORIZED_OPERATION));
    }

    private String getBlogNotFoundMessage(long id) {
        return String.format("Blog with id: %d not found", id);
    }
}
