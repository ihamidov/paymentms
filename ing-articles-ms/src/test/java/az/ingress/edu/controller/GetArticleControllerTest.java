package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.ArticleService;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
@ActiveProfiles("test")
public class GetArticleControllerTest {

    private static final String ARTICLES_ID_PATH = "/articles/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ID = "$.id";
    private static final String DUMMY_TEXT = "hello";
    private static final String ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String TITLE = "$.title";
    private static final String DESCRIPTION = "$.description";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccessToken accessToken;

    @MockBean
    private ArticleService articleService;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
    }

    @Test
    public void givenNegativeArticleIdGetArticleExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES_ID_PATH, -1L))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroArticleIdGetArticleExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES_ID_PATH, 0))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingArticleIdGetArticleExpectErrorMessage() throws Exception {
        long id = 1000L;
        String articleNotFoundMessage = articleNotFoundMessage(id);
        Mockito.doThrow(new ArticleNotFoundException(id)).when(articleService).getArticle(id);

        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES_ID_PATH, id))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(articleNotFoundMessage))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(articleNotFoundMessage));

        verify(articleService).getArticle(id);
    }

    @Test
    public void givenExistingArticleIdGetArticleExpectOk() throws Exception {
        long id = 1L;
        Article article = new Article(1L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),
                MODIFIED_BY_USERDETAILS, Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS);
        when(articleService.getArticle(id)).thenReturn(article);

        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES_ID_PATH, id))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ID).value(id))
                .andExpect(MockMvcResultMatchers.jsonPath(TITLE).value(DUMMY_TEXT))
                .andExpect(MockMvcResultMatchers.jsonPath(DESCRIPTION).value(DUMMY_TEXT));

        verify(articleService).getArticle(id);
    }

    private String articleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }
}
