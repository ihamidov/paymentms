package az.ingress.edu.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.service.CommentService;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@ActiveProfiles("test")
public class CommentListForArticleControllerTest {

    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String COMMENT_LIST_PATH = "/comments/list/article/{id}";
    private static final String PAGE = "page";
    private static final String TOTAL_PAGES = "$.totalPages";
    private static final String SIZE = "size";
    private static final String CODE = "$.code";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String TOTAL_ELEMENTS = "$.totalElements";
    private static final String CONTENT_ITEM_SUB_COMMENT = "$.content[0].subCommentList";
    private static final String INDEX_MUST_NOT_BE_NEGATIVE = "Page index must be zero or greater";
    private static final String SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String CREATED_AT = "createdAt";
    private final List<Comment> commentListWithSubComments =
            Arrays.asList(new Comment(1L, 4L, ParentType.ARTICLE, null, "Comment", 1L, false,
                    Arrays.asList(new Comment(1L, 4L, ParentType.ARTICLE, null, "Comment", 1L,
                            false, null,
                    Calendar.getInstance(), null, Calendar.getInstance(),
                    null)),
                    Calendar.getInstance(), null, Calendar.getInstance(),
                    null));


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;


    @Test
    public void givenNegativePageIndexGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "-1";
        String size = "20";
        long articleId = 3L;

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INDEX_MUST_NOT_BE_NEGATIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(INDEX_MUST_NOT_BE_NEGATIVE));
    }

    @Test
    public void givenNegativePageSizeGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "1";
        String size = "-20";
        long articleId = 3L;

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroPageSizeGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "1";
        String size = "0";
        long articleId = 3L;

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNegativeParentIdGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "0";
        String size = "1";
        long articleId = -3L;

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroParentIdGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "0";
        String size = "1";
        long articleId = 0L;

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingParentIdGetCommentsForArticleExpectErrorMessage() throws Exception {
        String page = "0";
        String size = "1";
        long articleId = 3L;
        String errorMessage = getArticleNotFoundMessage(articleId);
        when(commentService.getComments(articleId, ParentType.ARTICLE, Integer.parseInt(page), Integer.parseInt(size)))
                .thenThrow(new ArticleNotFoundException(articleId));

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, page).param(SIZE, size)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));
    }

    @Test
    public void givenValidPageAndSizeGetCommentsForArticle() throws Exception {
        int page = 0;
        int size = 3;
        long articleId = 2L;
        Pageable pageable = PageRequest.of(page, size, Sort.by(CREATED_AT).descending());
        Page<Comment> commentPage = new PageImpl<Comment>(commentListWithSubComments, pageable, 1L);
        when(commentService.getComments(articleId, ParentType.ARTICLE, page, size)).thenReturn(commentPage);

        mockMvc.perform(get(COMMENT_LIST_PATH, articleId)
                .param(PAGE, String.valueOf(page)).param(SIZE, String.valueOf(size))
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andExpect(jsonPath(TOTAL_PAGES).value(commentPage.getTotalPages()))
                .andExpect(jsonPath(TOTAL_ELEMENTS).value(commentPage.getTotalElements()))
                .andExpect(jsonPath(CONTENT_ITEM_SUB_COMMENT).isArray())
                .andExpect(jsonPath(CONTENT_ITEM_SUB_COMMENT, hasSize(1)));

        verify(commentService).getComments(articleId, ParentType.ARTICLE, page, size);
    }

    private String getArticleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }
}
