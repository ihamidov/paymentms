package az.ingress.edu.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.AttemptToDeleteCommentWithSubComment;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.service.CommentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@ActiveProfiles("test")
public class DeleteCommentControllerTest {

    private static final String CODE = "$.code";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String COMMENT_ID_MUST_BE_POZITIVE = "Comment id must be positive";
    private static final String COMMENT_WITH_ID_PATH = "/comments/{id}";
    private static final String UNAUTHORIZED_OPERATION = "You don't have access for this operation";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;

    @Test
    public void givenZeroCommentIdDeleteCommentExpectErrorMessage() throws Exception {
        long id = 0L;

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_ID_MUST_BE_POZITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_ID_MUST_BE_POZITIVE));
    }

    @Test
    public void givenNegativeCommentIdDeleteCommentExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_ID_MUST_BE_POZITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_ID_MUST_BE_POZITIVE));
    }

    @Test
    public void givenNonExistingCommentIdDeleteCommentExpectErrorMessage() throws Exception {
        long id = 1000L;
        String errorMessage = getCommentNotFoundErrorMessage(id);
        doThrow(new CommentNotFoundException(id)).when(commentService).deleteComment(id);

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id)).andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));
    }

    @Test
    public void givenUnauthorizedUserCommentIdDeleteCommentExpectErrorMessage() throws Exception {
        long id = 1000L;
        doThrow(new AttemptToUnauthorizedOperationException()).when(commentService).deleteComment(id);

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id)).andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(UNAUTHORIZED_OPERATION))
                .andExpect(jsonPath(USER_MESSAGE).value(UNAUTHORIZED_OPERATION));
    }

    @Test
    public void givenExistingCommentIdHavingSubCommentsDeleteCommentExpectErrorMessage() throws Exception {
        long id = 1000L;
        String errorMessage = getAttemtToDeleteCommentWithSubCommentErrorMessage(id);
        doThrow(new AttemptToDeleteCommentWithSubComment(id)).when(commentService)
                .deleteComment(id);

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id)).andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));

        verify(commentService).deleteComment(id);

    }

    @Test
    public void givenExistingCommentIdDeleteCommentExpectOk() throws Exception {
        long id = 1000L;

        mockMvc.perform(delete(COMMENT_WITH_ID_PATH, id)).andExpect(status().isOk());

        verify(commentService).deleteComment(id);

    }

    private String getCommentNotFoundErrorMessage(long commentId) {
        return "Comment with id: " + commentId + " not found";
    }

    private String getAttemtToDeleteCommentWithSubCommentErrorMessage(long commentId) {
        return "Comment with id: " + commentId + " has sub-comments";
    }

}
