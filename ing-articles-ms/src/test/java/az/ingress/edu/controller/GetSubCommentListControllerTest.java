package az.ingress.edu.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CommentService;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@ActiveProfiles("test")
public class GetSubCommentListControllerTest {

    private static final String LIST_SUBCOMMENT_PATH = "/comments/list/subcomment/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ID_MUST_BE_POSITIVE = "Comment id must be positive";
    private static final String ROOT = "$";
    private static final String COMMENT = "My comment";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;


    @Test
    public void givenNegativeCommentIdGetSubCommentsExpectErrorMessage() throws Exception {
        mockMvc.perform(get(LIST_SUBCOMMENT_PATH, -1L))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroCommentIdGetSubCommentsExpectErrorMessage() throws Exception {
        mockMvc.perform(get(LIST_SUBCOMMENT_PATH, 0))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingCommentIdGetSubCommentExpectErrorMessage() throws Exception {
        long id = 100L;
        when(commentService.getSubComments(id)).thenThrow(new CommentNotFoundException(id));

        mockMvc.perform(get(LIST_SUBCOMMENT_PATH, id))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(commentNotFoundMessage(id)))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(commentNotFoundMessage(id)));

        verify(commentService).getSubComments(id);
    }

    @Test
    public void givenCommentIdGetSubComments() throws Exception {
        long id = 1L;
        User user = User.builder().firstname("Admin").lastname("Admin")
                .username("user")
                .email("admin@ingress.az").build();
        List<Comment> subCommentList = Arrays.asList(
                new Comment(2L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                        Calendar.getInstance(), user, Calendar.getInstance(),
                        user),
                new Comment(3L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                        Calendar.getInstance(), user, Calendar.getInstance(),
                        user));
        when(commentService.getSubComments(id)).thenReturn(subCommentList);

        mockMvc.perform(get(LIST_SUBCOMMENT_PATH, id))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ROOT).isArray())
                .andExpect(MockMvcResultMatchers.jsonPath(ROOT).value(hasSize(2)));

        verify(commentService).getSubComments(id);
    }

    private String commentNotFoundMessage(long id) {
        return "Comment with id: " + id + " not found";
    }
}
