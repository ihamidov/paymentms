package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.model.Article;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.ArticleService;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
@ActiveProfiles("test")
public class GetListArticleControllerTestImpl {

    private static final String ARTICLES = "/articles";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String DUMMY_TEXT = "hello";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String TOTAL_PAGES = "$.totalPages";
    private static final String TOTAL_ELEMENTS = "$.totalElements";
    private static final String CONTENT = "$.content";
    private static final String INDEX_MUST_BE_ZERO_OR_GREATER = "Page index must be 0 or greater";
    private static final String MUST_NOT_BE_POSITIVE = "Page size must be positive";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccessToken accessToken;

    @MockBean
    private ArticleService articleService;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
        when(accessToken.getRealmAccess()).thenReturn(getAccess());
    }

    @Test
    public void givenPageAndSizeReturnListArticles() throws Exception {
        Page<Article> dummyPageArticles = new PageImpl<>(Arrays.asList(
                new Article(1L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), MODIFIED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS),
                new Article(2L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), MODIFIED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS),
                new Article(3L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), MODIFIED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS),
                new Article(4L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), MODIFIED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS)),
                PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "id")), 4);
        when(articleService.findAll(anyInt(),anyInt(),anyString(),anyString())).thenReturn(dummyPageArticles);

        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES).param(PAGE, "0").param(SIZE, "2")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath(TOTAL_PAGES).value(dummyPageArticles.getTotalPages()))
                .andExpect(MockMvcResultMatchers.jsonPath(TOTAL_ELEMENTS).value(dummyPageArticles.getTotalElements()))
                .andExpect(MockMvcResultMatchers.jsonPath(CONTENT).isNotEmpty());
    }

    @Test
    public void givenPageIsNegativeExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES).param(PAGE, "-1").param(SIZE, "2")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(INDEX_MUST_BE_ZERO_OR_GREATER))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(INDEX_MUST_BE_ZERO_OR_GREATER));
    }

    @Test
    public void givenSizeIsNegativeExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES).param(PAGE, "0").param(SIZE, "-2")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(MUST_NOT_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(MUST_NOT_BE_POSITIVE));
    }

    @Test
    public void givenInvalidOrderDirectionGetListArticleExpectException() throws Exception {
        when(articleService.findAll(anyInt(),anyInt(),anyString(),anyString())).thenThrow(new IllegalArgumentException(
                "test"));

        mockMvc.perform(MockMvcRequestBuilders
                .get(ARTICLES)
                .param(PAGE, "0")
                .param(SIZE, "2")
                .param("order", "id")
                .param("direction", "ascc"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers
                        .jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()));
    }

    private AccessToken.Access getAccess() {
        return new AccessToken.Access().addRole("USER");
    }
}
