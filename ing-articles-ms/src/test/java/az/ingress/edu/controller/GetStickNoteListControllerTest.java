package az.ingress.edu.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.StickNoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(StickNoteController.class)
@ActiveProfiles("test")
public class GetStickNoteListControllerTest {

    private static final String STICKNOTE_LIST_PATH = "/sticknote/list/article/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String DUMMY_TEXT = "STICKNOTE title";
    private static final String ROOT = "$";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StickNoteService stickNoteService;

    @MockBean
    private AccessToken accessToken;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
    }

    @Test
    public void givenNegativeArticleIdGetStickNotesExpectErrorMessage() throws Exception {
        long articleId = -20L;

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_LIST_PATH, articleId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroArticleIdGetStickNotesExpectErrorMessage() throws Exception {
        long articleId = 0L;

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_LIST_PATH, articleId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingArticleIdGetStickNotesExpectErrorMessage() throws Exception {
        long articleId = 1000L;
        String errorMessage = getArticleNotFoundMessage(articleId);
        when(stickNoteService.getStickNotes(articleId)).thenThrow(new ArticleNotFoundException(articleId));

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_LIST_PATH, articleId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(errorMessage));

        verify(stickNoteService).getStickNotes(articleId);
    }

    @Test
    public void givenExistingArticleIdGetStickNotes() throws Exception {
        long articleId = 10L;
        List<StickNote> stickNotes = Arrays.asList(new StickNote(1L, DUMMY_TEXT, articleId, 10, 10,
                        Calendar.getInstance().toInstant(),CREATED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS),
                new StickNote(2L, DUMMY_TEXT, articleId, 20, 10, Calendar.getInstance().toInstant(),
                        CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS),
                new StickNote(3L, DUMMY_TEXT, articleId, 10, 10,Calendar.getInstance().toInstant(),
                        CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS));
        when(stickNoteService.getStickNotes(articleId)).thenReturn(stickNotes);

        mockMvc.perform(MockMvcRequestBuilders
                .get(STICKNOTE_LIST_PATH, articleId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ROOT).isArray())
                .andExpect(MockMvcResultMatchers.jsonPath(ROOT, hasSize(3)));

        verify(stickNoteService).getStickNotes(articleId);
    }

    private static String getArticleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }
}
