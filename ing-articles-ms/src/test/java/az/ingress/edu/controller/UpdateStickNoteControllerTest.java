package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.UpdateStickNoteDto;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.StickNoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(StickNoteController.class)
@ActiveProfiles("test")
public class UpdateStickNoteControllerTest {

    private static final String STICKNOTE_WITH_ID_PATH = "/sticknote/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String COORDINATE_CANT_BE_NEGATIVE = "Coordinate must be zero or positive number";
    private static final String DUMMY_TEXT = "STICKNOTE title";
    private static final String ID = "$.id";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);
    private static final String NOT_ALLOWED_OPERATION = "You don't have access for this operation";

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StickNoteService stickNoteService;

    @MockBean
    private AccessToken accessToken;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
        when(accessToken.getRealmAccess()).thenReturn(getAccess());
    }

    @Test
    public void givenStickNoteWithNegativeXCoordinateUpdateStickNoteExpectErrorMessage() throws Exception {
        UpdateStickNoteDto updateStickNoteDto = new UpdateStickNoteDto(DUMMY_TEXT, -20, 10);

        mockMvc.perform(MockMvcRequestBuilders
                .put(STICKNOTE_WITH_ID_PATH, 2L)
                .content(objectMapper.writeValueAsString(updateStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE));
    }

    @Test
    public void givenStickNoteWithNegativeYCoordinateUpdateStickNoteExpectErrorMessage() throws Exception {
        UpdateStickNoteDto updateStickNoteDto = new UpdateStickNoteDto(DUMMY_TEXT, 20, -10);

        mockMvc.perform(MockMvcRequestBuilders
                .put(STICKNOTE_WITH_ID_PATH, 2L)
                .content(objectMapper.writeValueAsString(updateStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE));
    }

    @Test
    public void givenValidStickNoteUpdateStickNote() throws Exception {
        UpdateStickNoteDto updateStickNoteDto = new UpdateStickNoteDto(DUMMY_TEXT, 20, 10);
        StickNote updatedStickNote = new StickNote(2L, DUMMY_TEXT, 2L, 20, 10, Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteService.updateStickNote(anyLong(), any(StickNote.class),any(UserDetails.class)))
                .thenReturn(updatedStickNote);

        mockMvc.perform(MockMvcRequestBuilders
                .put(STICKNOTE_WITH_ID_PATH, 2L)
                .content(objectMapper.writeValueAsString(updateStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ID).value(2L));
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithNonOwnerUsernameAndUserRoleUpdateStickNoteExpectErrorMessage()
            throws Exception {
        UpdateStickNoteDto updateStickNoteDto = new UpdateStickNoteDto(DUMMY_TEXT, 20, 10);
        when(stickNoteService.updateStickNote(anyLong(),any(StickNote.class),any(UserDetails.class)))
                .thenThrow(new AttemptToUnauthorizedOperationException());

        mockMvc.perform(MockMvcRequestBuilders
                .put(STICKNOTE_WITH_ID_PATH, 1L)
                .content(objectMapper.writeValueAsBytes(updateStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(NOT_ALLOWED_OPERATION))
                .andExpect(jsonPath(USER_MESSAGE).value(NOT_ALLOWED_OPERATION));
    }

    private AccessToken.Access getAccess() {
        return new AccessToken.Access().addRole("USER");
    }
}
