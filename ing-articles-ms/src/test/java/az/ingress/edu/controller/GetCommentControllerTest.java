package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CommentService;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@ActiveProfiles("test")
public class GetCommentControllerTest {

    private static final String COMMENTS_ID_PATH = "/comments/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ID = "$.id";
    private static final String ID_MUST_BE_POSITIVE = "Comment id must be positive";
    private static final String COMMENT = "My comment";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;


    @Test
    public void givenNegativeCommentIdGetCommentExpectErrorMessage() throws Exception {
        mockMvc.perform(get(COMMENTS_ID_PATH, -1L))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenZeroCommentIdGetCommentExpectErrorMessage() throws Exception {
        mockMvc.perform(get(COMMENTS_ID_PATH, 0))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingCommentIdGetCommentExpectErrorMessage() throws Exception {
        long id = 100L;
        when(commentService.getComment(id)).thenThrow(new CommentNotFoundException(id));

        mockMvc.perform(get(COMMENTS_ID_PATH, id))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(commentNotFoundMessage(id)))
                .andExpect(jsonPath(USER_MESSAGE).value(commentNotFoundMessage(id)));

        verify(commentService).getComment(id);
    }

    @Test
    public void givenCommentIdGetCommentExpectComment() throws Exception {
        long id = 1L;
        User user = User.builder().firstname("Admin").lastname("Admin")
                .username("user")
                .email("admin@ingress.az").build();
        Comment comment = new Comment(1L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        when(commentService.getComment(id)).thenReturn(comment);

        mockMvc.perform(get(COMMENTS_ID_PATH, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(id));

        verify(commentService).getComment(id);
    }

    private String commentNotFoundMessage(long id) {
        return "Comment with id: " + id + " not found";
    }
}
