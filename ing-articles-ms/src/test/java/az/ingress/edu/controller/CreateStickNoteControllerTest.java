package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateStickNoteDto;
import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.StickNoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import java.util.Collections;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(StickNoteController.class)
@ActiveProfiles("test")
public class CreateStickNoteControllerTest {

    private static final String ARTICLE_ID_CANT_BE_EMPTY = "Article id can't be empty";
    private static final String STICKNOTE_PATH = "/sticknote";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String COORDINATE_CANT_BE_NEGATIVE = "Coordinate must be zero or positive number";
    private static final String TITLE_CANT_BE_EMPTY = "Title can't be empty";
    private static final String DUMMY_TEXT = "STICKNOTE title";
    private static final String ID = "$.id";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(USERNAME, CREATED_BY,ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(USERNAME, MODIFIED_BY,ROLES);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccessToken accessToken;

    @MockBean
    private StickNoteService stickNoteService;

    @Before
    public void setUp() {
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getName()).thenReturn(CREATED_BY);
        when(accessToken.getRealmAccess()).thenReturn(getAccess());
    }

    @Test
    public void givenNonExistingArticleIdCreateStickNoteExpectErrorMessage() throws Exception {
        long articleId = 1L;
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, articleId, 20L, 10L);
        StickNote stickNote = StickNote.builder()
                .title(DUMMY_TEXT)
                .articleId(articleId)
                .xCoordinate(20L)
                .yCoordinate(10L).createdBy(CREATED_BY_USERDETAILS).modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        String errorMessage = articleNotFoundMessage(articleId);
        when(stickNoteService.createStickNote(stickNote)).thenThrow(new ArticleNotFoundException(articleId));

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(errorMessage));

        verify(stickNoteService).createStickNote(stickNote);
    }

    @Test
    public void givenNullArticleIdCreateStickNoteExpectErrorMessage() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, null, 20L, 10L);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_CANT_BE_EMPTY))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ARTICLE_ID_CANT_BE_EMPTY));
    }

    @Test
    public void givenNegativeArticleIdCreateStickNoteExpectErrorMessage() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, -1L, 20L, 10L);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(ARTICLE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenEmptyTitleCreateStickNoteExpectErrorMessage() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto("", 1L, 20L, 10L);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH, 1)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(TITLE_CANT_BE_EMPTY))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(TITLE_CANT_BE_EMPTY));
    }

    @Test
    public void givenNegativeXCoordinateCreateStickNoteExpectErrorMessage() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, 1L, -20L, 10L);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH, 1)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE));
    }

    @Test
    public void givenNegativeYCoordinateCreateStickNoteExpectErrorMessage() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, 1L, 20L, -10L);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH, 1)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(MockMvcResultMatchers.jsonPath(TECHNICAL_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE))
                .andExpect(MockMvcResultMatchers.jsonPath(USER_MESSAGE).value(COORDINATE_CANT_BE_NEGATIVE));
    }

    @Test
    public void givenValidStickNoteAndArticleIdCreateStickNote() throws Exception {
        CreateStickNoteDto createStickNoteDto = new CreateStickNoteDto(DUMMY_TEXT, 1L, 20L, 10L);
        StickNote stickNoteWithId = new StickNote(2L, DUMMY_TEXT, 1L, 20, 10, Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS, Calendar.getInstance().toInstant(), MODIFIED_BY_USERDETAILS);
        StickNote stickNote = StickNote.builder()
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(20L)
                .yCoordinate(10L).createdBy(CREATED_BY_USERDETAILS)
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        when(stickNoteService.createStickNote(stickNote)).thenReturn(stickNoteWithId);

        mockMvc.perform(MockMvcRequestBuilders
                .post(STICKNOTE_PATH, 1)
                .content(objectMapper.writeValueAsString(createStickNoteDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath(ID).value(2));

        verify(stickNoteService).createStickNote(stickNote);
    }

    private String articleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }

    private AccessToken.Access getAccess() {
        return new AccessToken.Access().addRole("USER");
    }
}
