package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.BlogFilterDto;
import az.ingress.edu.dto.ListBlogDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.service.BlogService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(BlogController.class)
@ActiveProfiles("test")
public class GetListBlogControllerTest {

    private static final String ID = "id";
    private static final String GET_LIST_BLOG_URL = "/blog/list";
    private static final String GET_LIST_PUBLISHED_BLOG_URL = "/blog/list/published";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String PAGE_SIZE = "pageSize";
    private static final String PAGE_SORT = "sort";
    private static final String PAGE_DIRECTION = "direction";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String DUMMY_TEXT = "This is dummy";
    private static final String PHOTO = "www.dummy.com/dummy.png";


    private static final int PAGE_SORT_NUMBER = 1;
    private static final String PAGE_SORT_NUMBER_STRING = "1";
    private static final int PAGE_SORT_SIZE = 5;
    private static final String PAGE_SORT_SIZE_STRING = "5";
    private static final long TOTAL_ELEMENTS = 17L;
    private static final int TOTAL_PAGES = 4;

    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "userMessage";
    private static final String TECHINICAL_MESSAGE = "technicalMessage";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BlogService blogService;

    private final List<ListBlogDto> listBlog = new ArrayList<>();

    private PageDto pageDto;

    @Before
    public void setUp() {
        Blog blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), null, Calendar.getInstance(), null);
        pageDto = PageDto.builder()
                .pageNumber(PAGE_SORT_NUMBER)
                .pageSize(PAGE_SORT_SIZE)
                .sortColumn(ID)
                .sortDirection(Sort.Direction.ASC.name())
                .build();
        for (int i = 1; i <= TOTAL_ELEMENTS; i++) {
            listBlog.add(ListBlogDto.toListBlogDto(blog));
        }
    }

    @Test
    public void retrieveAllBlogPageableDefault() throws Exception {
        mockMvc.perform(get(GET_LIST_BLOG_URL))
                .andExpect(status().isOk());
    }

    @Test
    public void getBlogs() throws Exception {
        ListDto listDto = new ListDto(listBlog, TOTAL_PAGES, TOTAL_ELEMENTS);
        when(blogService.getBlogs(pageDto))
                .thenReturn(listDto);

        mockMvc.perform(get(GET_LIST_BLOG_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_DIRECTION, Sort.Direction.ASC.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(TOTAL_ELEMENTS))
                .andExpect(jsonPath("$.totalPages").value(TOTAL_PAGES));
    }


    @Test
    public void getBlogsWithZeroPageSizeBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_BLOG_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "0")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void getBlogsWithNegativePageSizeBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_BLOG_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "-1")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void getBlogsWithNegativePageNumberBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_BLOG_URL)
                .param(PAGE_NUMBER, "-1")
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_INDEX_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_INDEX_MUST_BE_POSITIVE));
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        when(blogService.getBlogs(any(PageDto.class)))
                .thenThrow(new IllegalArgumentException());
        mockMvc.perform(get(GET_LIST_BLOG_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_SORT, ID)
                .param(PAGE_DIRECTION, "ASCS"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void retrieveAllPublishedBlogPageableDefault() throws Exception {
        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL))
                .andExpect(status().isOk());
    }

    @Test
    public void getPublishedBlogs() throws Exception {
        ListDto listDto = new ListDto(listBlog, TOTAL_PAGES, TOTAL_ELEMENTS);
        when(blogService.getPublishedBlogs(any(PageDto.class), any(BlogFilterDto.class)))
                .thenReturn(listDto);

        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_DIRECTION, Sort.Direction.ASC.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements").value(TOTAL_ELEMENTS))
                .andExpect(jsonPath("$.totalPages").value(TOTAL_PAGES));
    }


    @Test
    public void getPublishedBlogsWithZeroPageSizeBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "0")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void getPublishedBlogsWithNegativePageSizeBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "-1")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void getPublishedBlogsWithNegativePageNumberBadRequest() throws Exception {
        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL)
                .param(PAGE_NUMBER, "-1")
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAGE_INDEX_MUST_BE_POSITIVE))
                .andExpect(jsonPath(TECHINICAL_MESSAGE).value(PAGE_INDEX_MUST_BE_POSITIVE));
    }

    @Test
    public void givenInvalidSortDirectionGetPublishedBlogsExceptionBadRequest() throws Exception {
        when(blogService.getPublishedBlogs(any(PageDto.class), any(BlogFilterDto.class)))
                .thenThrow(new IllegalArgumentException());
        mockMvc.perform(get(GET_LIST_PUBLISHED_BLOG_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_SORT, ID)
                .param(PAGE_DIRECTION, "ASCS"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()));
    }
}
