package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateCommentDto;
import az.ingress.edu.dto.UpdateCommentDto;
import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.exception.ParentNotSameWithParentCommentException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CommentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
@ActiveProfiles("test")
public class CommentControllerTestImpl {

    private static final String COMMENTS = "/comments";
    private static final String COMMENT_WITH_ID_PATH = "/comments/{id}";
    private static final String ID = "$.id";
    private static final String COMMENT_CAN_T_BE_EMPTY = "Comment can't be empty";
    private static final String PARENT_ID_MUST_BE_POSITIVE = "Parent id must be positive";
    private static final String COMMENT_ID_MUST_BE_POSITIVE = "Comment id must be positive";
    private static final String MUST_BE_POSITIVE = "Parent comment id must be positive";
    private static final String DUMMY_TEXT = "Dummy text";
    private static final String CODE = "$.code";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String PARENT_ID_CANT_BE_EMPTY = "Parent id can't be empty";
    private static final String UNAUTHORIZED_OPERATION = "You don't have access for this operation";
    private static final String COMMENT = "My comment";
    public static final String ADMIN = "Admin";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentService commentService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenCommentCreateCommentExpectId() throws Exception {
        User user = User.builder().firstname(ADMIN).lastname(ADMIN)
                .username("user")
                .email("admin@ingress.az").build();
        Comment comment = new Comment(2L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        CreateCommentDto givenComment = new CreateCommentDto(1L, ParentType.ARTICLE, 1L, DUMMY_TEXT);
        when(commentService.createComment(any(Comment.class))).thenReturn(comment);

        mockMvc.perform(post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(comment.getId()));
    }

    @Test
    public void givenCommentWithNonExistingParentIdForArticleCreateCommentExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(1000L, ParentType.ARTICLE, 5L, DUMMY_TEXT);
        when(commentService.createComment(any(Comment.class))).thenThrow(new ArticleNotFoundException(1000L));

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE) .value(getArticleNotFoundMessage(givenComment.getParentId())))
                .andExpect(jsonPath(USER_MESSAGE) .value(getArticleNotFoundMessage(givenComment.getParentId())));
    }

    @Test
    public void givenCommentWithNullParentIdForArticleCreateCommentExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(null, ParentType.ARTICLE, 5L, DUMMY_TEXT);

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PARENT_ID_CANT_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(PARENT_ID_CANT_BE_EMPTY));
    }

    @Test
    public void givenCommentWithNonExistingParentIdForStickNoteCreateCommentExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(1000L, ParentType.STICKNOTE, 5L, DUMMY_TEXT);
        String errorMessage = getStickNoteNoteFoundMessage(1000L);
        when(commentService.createComment(any(Comment.class))).thenThrow(new StickNoteNotFoundException(1000L));

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));
    }

    @Test
    public void givenCommentWithNullCommentCreateCommentExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(1000L, ParentType.ARTICLE, 5L, null);

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY));
    }

    @Test
    public void givenCommentWithEmptyCommentCreateCommentExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(1000L, ParentType.STICKNOTE, 5L, "");

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY));
    }

    @Test
    public void givenCommentWithNotPositiveParentIdExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(-1L, ParentType.STICKNOTE, 5L, DUMMY_TEXT);

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PARENT_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(PARENT_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenCommentWithNotPositiveParentCommentIdExpectErrorMessage() throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(1000L, ParentType.ARTICLE, -1L, DUMMY_TEXT);

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS)
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(MUST_BE_POSITIVE));
    }

    @Test
    public void givenChildCommentWhichsParentIdNotMatchParentCommentsParentIdCreateCommentExpectErrorMessage()
            throws Exception {
        CreateCommentDto givenComment = new CreateCommentDto(55L, ParentType.ARTICLE, 1L, DUMMY_TEXT);
        String errorMessage =
                parentIdNotMatchParentCommentsParentIdExceptionMessage(givenComment.getParentId(), ParentType.ARTICLE);
        when(commentService.createComment(any(Comment.class)))
                .thenThrow(new ParentNotSameWithParentCommentException(givenComment.getParentId(), ParentType.ARTICLE));

        mockMvc.perform(MockMvcRequestBuilders
                .post(COMMENTS).principal(new TestingAuthenticationToken(CREATED_BY, null))
                .content(objectMapper.writeValueAsString(givenComment))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));
    }

    @Test
    public void givenNonPositiveCommentIdUpdateCommentExceptErrorMessage() throws Exception {
        long id = -1L;
        UpdateCommentDto updateCommentDto = new UpdateCommentDto(DUMMY_TEXT);

        mockMvc.perform(MockMvcRequestBuilders.put(COMMENT_WITH_ID_PATH, id)
                .content(objectMapper.writeValueAsString(updateCommentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenEmptyCommentContentUpdateCommentExceptErrorMessage() throws Exception {
        long id = 1L;
        UpdateCommentDto updateCommentDto = new UpdateCommentDto("");

        mockMvc.perform(MockMvcRequestBuilders.put(COMMENT_WITH_ID_PATH, id)
                .content(objectMapper.writeValueAsString(updateCommentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(COMMENT_CAN_T_BE_EMPTY));
    }

    @Test
    public void givenNonExistingCommentIdAndValidDataUpdateCommentExceptErrorMessage() throws Exception {
        long id = 1L;
        String errorMessage = getCommentNotFoundMessage(id);
        UpdateCommentDto updateCommentDto = new UpdateCommentDto(DUMMY_TEXT);
        when(commentService.updateComment(anyLong(), any(Comment.class))) .thenThrow(new CommentNotFoundException(id));

        mockMvc.perform(MockMvcRequestBuilders.put(COMMENT_WITH_ID_PATH, id)
                .principal(new TestingAuthenticationToken(MODIFIED_BY, null))
                .content(objectMapper.writeValueAsString(updateCommentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(errorMessage))
                .andExpect(jsonPath(USER_MESSAGE).value(errorMessage));
    }

    @Test
    public void givenUnauthorizedUserCommentIdAndValidDataUpdateComment() throws Exception {
        UpdateCommentDto updateCommentDto = new UpdateCommentDto(DUMMY_TEXT);
        when(commentService.updateComment(anyLong(), any(Comment.class)))
                .thenThrow(new AttemptToUnauthorizedOperationException());

        mockMvc.perform(MockMvcRequestBuilders.put(COMMENT_WITH_ID_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateCommentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath(CODE).value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(UNAUTHORIZED_OPERATION))
                .andExpect(jsonPath(USER_MESSAGE).value(UNAUTHORIZED_OPERATION));
    }

    @Test
    public void givenExistingCommentIdAndValidDataUpdateComment() throws Exception {
        UpdateCommentDto updateCommentDto = new UpdateCommentDto(DUMMY_TEXT);
        User user = User.builder().firstname(ADMIN).lastname(ADMIN)
                .username("user")
                .email("admin@ingress.az").build();
        Comment comment = new Comment(2L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        when(commentService.updateComment(anyLong(), any(Comment.class))).thenReturn(comment);

        mockMvc.perform(MockMvcRequestBuilders.put(COMMENT_WITH_ID_PATH, 1L)
                .content(objectMapper.writeValueAsString(updateCommentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)).andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(1));
    }

    private String getCommentNotFoundMessage(long id) {
        return "Comment with id: " + id + " not found";
    }

    private String getArticleNotFoundMessage(long id) {
        return "Article with id: " + id + " not found";
    }

    private String parentIdNotMatchParentCommentsParentIdExceptionMessage(long id, ParentType parentType) {
        return "Subcomment parent id: " + id + " or parent type: " + parentType + " does't match with parent comment";
    }

    private String getStickNoteNoteFoundMessage(long id) {
        return "StickNote with id: " + id + " not found";
    }
}
