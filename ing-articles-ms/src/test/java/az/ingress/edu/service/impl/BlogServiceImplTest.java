package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.config.SwaggerConfiguration;
import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.dto.ListBlogDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.UpdateBlogDto;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.BlogRepository;
import az.ingress.edu.service.UserService;
import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class BlogServiceImplTest {

    private static final String DUMMY_TEXT = "This is dummy";
    private static final String RESOURCE_ID = "resource-id";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final int PAGE_SORT_SIZE = 5;
    private static final String PHOTO = "www.dummy.com/dummy.png";
    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";
    private static final String COURSE_MANAGER = "COURSE_MANAGER";

    @Mock
    private BlogRepository blogRepository;

    @Mock
    UserService userService;

    @Mock
    SwaggerConfiguration swaggerConfiguration;

    @Mock
    AccessToken accessToken;

    @InjectMocks
    private BlogServiceImpl blogService;


    private PageDto pageDto;

    private User user;

    private User nonAuthorUser;

    private CreateBlogDto createBlogDto;

    private UpdateBlogDto updateBlogDto;

    private Blog blog;

    private Blog deletedBlog;

    @Before
    public void setUp() {
        pageDto = PageDto.builder().pageNumber(PAGE_SORT_NUMBER).pageSize(PAGE_SORT_SIZE).sortColumn("id")
                .sortDirection(Sort.Direction.ASC.name()).build();
        user = User.builder().firstname("Admin").lastname("Admin")
                .username("admin")
                .email("admin@ingress.az").build();
        nonAuthorUser = User.builder().username("jhon").build();
        blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), user, Calendar.getInstance(), null);
        deletedBlog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, true, null, Calendar.getInstance(), user, Calendar.getInstance(), user);
        createBlogDto = CreateBlogDto.builder().title(DUMMY_TEXT).content(DUMMY_TEXT).photo(PHOTO).build();
        updateBlogDto = UpdateBlogDto.builder().title(DUMMY_TEXT).content(DUMMY_TEXT).photo(PHOTO)
                .status(BlogStatus.DRAFT).build();
    }

    @Test
    public void givenBlogSaveBlogReturnBlog() {
        when(blogRepository.save(any())).thenReturn(blog);
        when(userService.getCurrentUser()).thenReturn(user);

        Blog returnedBlog = blogService.saveBlog(createBlogDto);

        assertThat(returnedBlog).isEqualTo(blog);
    }

    @Test
    public void givenExistingBlogAndNonAuthorUserUpdateBlog() {
        Blog blogToUpdate = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), user, Calendar.getInstance(), null);
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blogToUpdate));
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(userService.getCurrentUser()).thenReturn(nonAuthorUser);

        assertThatThrownBy(() -> blogService.updateBlog(updateBlogDto, 1L))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(blogRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenExistingBlogAndAuhtorUserUpdateBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));
        when(blogRepository.save(any())).thenReturn(blog);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(userService.getCurrentUser()).thenReturn(user);

        Blog updatedBlog = blogService.updateBlog(updateBlogDto, 1L);

        assertThat(blog).isEqualTo(updatedBlog);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(userService).getCurrentUser();
        verify(blogRepository).save(blog);
    }

    @Test
    public void givenExistingBlogAndAdminUserUpdateBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));
        when(blogRepository.save(any())).thenReturn(blog);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(ADMIN));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        Blog updatedBlog = blogService.updateBlog(updateBlogDto, 1L);

        assertThat(blog).isEqualTo(updatedBlog);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(blogRepository).save(blog);
    }

    @Test
    public void givenExistingBlogAndManagerUserUpdateBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));
        when(blogRepository.save(any())).thenReturn(blog);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(COURSE_MANAGER));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        Blog updatedBlog = blogService.updateBlog(updateBlogDto, 1L);

        assertThat(blog).isEqualTo(updatedBlog);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(blogRepository).save(blog);
    }

    @Test
    public void givenExistingBlogWithPublishStatusUpdateBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));
        when(blogRepository.save(any())).thenReturn(blog);
        updateBlogDto.setStatus(BlogStatus.PUBLISHED);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(ADMIN));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        Blog updatedBlog = blogService.updateBlog(updateBlogDto, 1L);

        assertThat(blog).isEqualTo(updatedBlog);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(blogRepository).save(blog);
    }

    @Test
    public void givenNonExistingBlogUpdateBlogExpectException() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> blogService.updateBlog(updateBlogDto, 1L)).isInstanceOf(BlogNotFountException.class);

        verify(blogRepository).findByIdAndDeletedFalse(1L);
    }


    @Test
    public void givenExistingBlogIdAndUnauthorizedUserDeleteBlogExpectException() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(deletedBlog));
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(userService.getCurrentUser()).thenReturn(nonAuthorUser);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        assertThatThrownBy(() -> blogService.deleteBlogById(1L))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(userService).getCurrentUser();
    }

    @Test
    public void givenExistingBlogIdAndAuthorUserDeleteBlog() {
        Blog blogToDelete = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(), PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), user, Calendar.getInstance(), null);
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blogToDelete));

        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(userService.getCurrentUser()).thenReturn(user);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        ArgumentCaptor<Blog> captor = ArgumentCaptor.forClass(Blog.class);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        blogService.deleteBlogById(1L);

        verify(blogRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(userService).getCurrentUser();
        verify(blogRepository).save(captor.capture());
    }

    @Test
    public void givenExistingBlogIdAndAdminUserDeleteBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(deletedBlog));
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(ADMIN));
        ArgumentCaptor<Blog> captor = ArgumentCaptor.forClass(Blog.class);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        blogService.deleteBlogById(1L);

        verify(blogRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(blogRepository).save(captor.capture());
    }

    @Test
    public void givenExistingBlogIdAndManagerUserDeleteBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(deletedBlog));
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(COURSE_MANAGER));
        ArgumentCaptor<Blog> captor = ArgumentCaptor.forClass(Blog.class);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        blogService.deleteBlogById(1L);

        verify(blogRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
        verify(blogRepository).findByIdAndDeletedFalse(1L);
        verify(blogRepository).save(captor.capture());
    }

    @Test
    public void givenExistingBlogIdGetBlog() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));

        assertThat(blogService.findBlogById(1L)).isEqualTo(blog);

        verify(blogRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenNonExistingBlogIdGetBlogExpectException() {
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> blogService.findBlogById(1L)).isInstanceOf(BlogNotFountException.class);

        verify(blogRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void findAllForManager() {
        pageDto.setSortDirection(Sort.Direction.DESC.name());
        Page<Blog> blogs = new PageImpl<>(Collections.singletonList(blog));
        when(blogRepository.findAllByDeletedFalse(any(Pageable.class))).thenReturn(blogs);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(ADMIN));
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        ListDto listDto = new ListDto(Collections.singletonList(ListBlogDto.toListBlogDto(blog)), 1, 1L);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        ListDto expectedBlogs = blogService.getBlogs(pageDto);

        assertThat(expectedBlogs.getContent()).isEqualTo(listDto.getContent());
        assertThat(expectedBlogs.getTotalElements()).isEqualTo(1L);
        assertThat(expectedBlogs.getTotalPages()).isEqualTo(1);

        verify(blogRepository).findAllByDeletedFalse(pageRequest);
    }

    @Test
    public void findAllForAdmin() {
        pageDto.setSortDirection(Sort.Direction.DESC.name());
        Page<Blog> blogs = new PageImpl<>(Collections.singletonList(blog));
        when(blogRepository.findAllByDeletedFalse(any(Pageable.class))).thenReturn(blogs);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(COURSE_MANAGER));
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        ListDto listDto = new ListDto(Collections.singletonList(ListBlogDto.toListBlogDto(blog)), 1, 1L);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        ListDto expectedBlogs = blogService.getBlogs(pageDto);

        assertThat(expectedBlogs.getContent()).isEqualTo(listDto.getContent());
        assertThat(expectedBlogs.getTotalElements()).isEqualTo(1L);
        assertThat(expectedBlogs.getTotalPages()).isEqualTo(1);

        verify(blogRepository).findAllByDeletedFalse(pageRequest);
    }

    @Test
    public void findAllByUser() {
        pageDto.setSortDirection(Sort.Direction.DESC.name());
        Page<Blog> blogs = new PageImpl<>(Collections.singletonList(blog));
        when(blogRepository.findAllByUsername(any(Pageable.class),anyString())).thenReturn(blogs);
        when(userService.getCurrentUser()).thenReturn(user);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        ListDto listDto = new ListDto(Collections.singletonList(ListBlogDto.toListBlogDto(blog)), 1, 1L);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        ListDto expectedBlogs = blogService.getBlogs(pageDto);

        assertThat(expectedBlogs.getContent()).isEqualTo(listDto.getContent());
        assertThat(expectedBlogs.getTotalElements()).isEqualTo(1L);
        assertThat(expectedBlogs.getTotalPages()).isEqualTo(1);

        verify(blogRepository).findAllByUsername(pageRequest,user.getUsername());
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        pageDto.setSortColumn("id");
        pageDto.setSortDirection("ASCS");
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        AccessToken.Access access = new AccessToken.Access();
        access.roles(Collections.singleton(USER));
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        assertThatThrownBy(() -> blogService.getBlogs(pageDto))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void findAllPublishedPaginationAsc() {
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.asc("id")));
        Page<Blog> blogs = new PageImpl<>(Collections.singletonList(blog));
        ListDto listDto = new ListDto(Collections.singletonList(ListBlogDto.toListBlogDto(blog)), 1, 1L);
        when(blogRepository.findAll(pageRequest, null)).thenReturn(blogs);

        ListDto expectedBlogs = blogService.getPublishedBlogs(pageDto, null);

        assertThat(expectedBlogs.getContent()).isEqualTo(listDto.getContent());
        assertThat(expectedBlogs.getTotalElements()).isEqualTo(1L);
        assertThat(expectedBlogs.getTotalPages()).isEqualTo(1);

        verify(blogRepository).findAll(pageRequest, null);
    }

    @Test
    public void findAllPublishedPaginationDesc() {
        pageDto.setSortDirection(Sort.Direction.DESC.name());
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        Page<Blog> blogs = new PageImpl<>(Collections.singletonList(blog));
        ListDto listDto = new ListDto(Collections.singletonList(ListBlogDto.toListBlogDto(blog)), 1, 1L);
        when(blogRepository.findAll(pageRequest, null)).thenReturn(blogs);

        ListDto expectedBlogs = blogService.getPublishedBlogs(pageDto, null);

        assertThat(expectedBlogs.getContent()).isEqualTo(listDto.getContent());
        assertThat(expectedBlogs.getTotalElements()).isEqualTo(1L);
        assertThat(expectedBlogs.getTotalPages()).isEqualTo(1);

        verify(blogRepository).findAll(pageRequest, null);
    }

    @Test
    public void sgivenInvalidSortDirectionFindAllPublishedExceptionBadRequest() throws Exception {
        pageDto.setSortColumn("id");
        pageDto.setSortDirection("ASCS");
        assertThatThrownBy(() -> blogService.getPublishedBlogs(pageDto, null))
                .isInstanceOf(IllegalArgumentException.class);
    }

}
