package az.ingress.edu.service.impl;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToDeleteCommentWithSubComment;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.exception.ParentNotSameWithParentCommentException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.User;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.repository.ArticleRepository;
import az.ingress.edu.repository.BlogRepository;
import az.ingress.edu.repository.CommentRepository;
import az.ingress.edu.repository.StickNoteRepository;
import az.ingress.edu.service.UserService;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("PMD.TooManyFields")
public class CommentServiceImplTest {

    private static final String COMMENT = "My comment";
    private static final String CREATED_BY = "Zekeryeyev Ejder";
    private static final String MODIFIED_BY = "Zekeryeyev Ejder";
    private static final String USERNAME = "ejder";
    private static final Set<String> ROLES = Collections.singleton("USER");
    private static final String CREATED_AT = "createdAt";
    private static final String DUMMY_TEXT = "This is dummy";
    private static final String USER = "user";
    private static final String ADMIN = "Admin";
    private final Article article = new Article(1L, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),
            new UserDetails(USERNAME, CREATED_BY, ROLES), Calendar.getInstance().toInstant(),
            new UserDetails(USERNAME, MODIFIED_BY, ROLES));
    private final StickNote stickNote = new StickNote(1L, DUMMY_TEXT, 1L, 10, 10, Calendar.getInstance().toInstant(),
            new UserDetails(USERNAME, CREATED_BY, ROLES), Calendar.getInstance().toInstant(),
            new UserDetails(USERNAME, MODIFIED_BY, ROLES));
    private static final String PHOTO = "www.dummy.com/dummy.png";

    private final Blog blog = new Blog();

    @Mock
    ArticleRepository articleRepository;

    @Mock
    StickNoteRepository stickNoteRepository;

    @Mock
    BlogRepository blogRepository;

    @Mock
    CommentRepository commentRepository;

    @Mock
    UserService userService;

    @InjectMocks
    CommentServiceImpl commentService;

    private User user;
    private User otherUser;

    private Comment commentWithId;
    private Comment comment;
    private Comment parentComment;
    private Comment childComment;
    private List<Comment> commentListForArticle;
    private List<Comment> commentListForStickNote;
    private List<Comment> commentListForBlog;

    @Before
    public void setUp() {
        user = User.builder().firstname(ADMIN).lastname(ADMIN).username(USER).email("admin@ingress.az").build();
        otherUser = User.builder().firstname(ADMIN).lastname(ADMIN).username("otherUser").email("admin@ingress.az")
                .build();
        commentWithId = new Comment(1L, 4L, ParentType.ARTICLE, 5L, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        comment = new Comment(2L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        parentComment = new Comment(1L, 4L, ParentType.ARTICLE, 5L, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);

        childComment = new Comment(2L, 4L, ParentType.ARTICLE, 2L, COMMENT, 1L, false, null,
                Calendar.getInstance(), user, Calendar.getInstance(),
                user);
        commentListForArticle = Arrays.asList(comment, comment);
        commentListForStickNote = Arrays.asList(comment, comment);
        commentListForBlog = Arrays.asList(comment, comment);

    }

    @Test
    public void givenCommentCreateCommentExpectId() {
        when(articleRepository.findById(anyLong())).thenReturn(java.util.Optional.of(article));
        when(commentRepository.save(any())).thenReturn(comment);

        Comment expectedComment = commentService.createComment(comment);

        assertThat(comment).isEqualTo(expectedComment);
        verify(commentRepository).save(comment);
    }

    @Test
    public void givenCommentHaveParentCommentCreateCommentExpectId() {
        when(articleRepository.findById(anyLong())).thenReturn(java.util.Optional.of(article));
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(comment));
        when(commentRepository.save(any())).thenReturn(parentComment);
        when(commentRepository.save(any())).thenReturn(childComment);

        Comment expectedComment = commentService.createComment(childComment);

        assertThat(childComment).isEqualTo(expectedComment);
        verify(commentRepository).findByIdAndDeletedFalse(2L);
        verify(commentRepository).save(comment);
        verify(commentRepository).save(childComment);
    }

    @Test
    public void givenCommentWithNonExistingArticleIdCreateCommentExpectException() {
        comment.setParentType(ParentType.ARTICLE);
        when(articleRepository.findById(anyLong())).thenThrow(new ArticleNotFoundException(comment.getParentId()));

        assertThatThrownBy(() -> commentService.createComment(comment)).isInstanceOf(ArticleNotFoundException.class);
        verify(articleRepository).findById(comment.getParentId());
    }

    @Test
    public void givenCommentWithNonExistingStickNoteIdCreateCommentExpectException() {
        comment.setParentType(ParentType.STICKNOTE);
        when(stickNoteRepository.findById(anyLong())).thenThrow(new StickNoteNotFoundException(comment.getParentId()));

        assertThatThrownBy(() -> commentService.createComment(comment)).isInstanceOf(StickNoteNotFoundException.class);
        verify(stickNoteRepository).findById(comment.getParentId());
    }

    @Test
    public void givenCommentWithNonExistingBlogIdCreateCommentExpectException() {
        comment.setParentType(ParentType.BLOG);
        when(blogRepository.findByIdAndDeletedFalse(anyLong()))
                .thenThrow(new BlogNotFountException(comment.getParentId()));

        assertThatThrownBy(() -> commentService.createComment(comment)).isInstanceOf(BlogNotFountException.class);
        verify(blogRepository).findByIdAndDeletedFalse(comment.getParentId());
    }


    @Test
    public void givenChildCommentWhichsParentIdNotMatchParentCommentsParentIdExpectException() {
        when(articleRepository.findById(anyLong())).thenReturn(Optional.of(article));
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(parentComment));
        when(userService.getCurrentUser()).thenReturn(user);
        childComment.setParentId(10L);

        assertThatThrownBy(() -> commentService.createComment(childComment))
                .isInstanceOf(ParentNotSameWithParentCommentException.class);
        verify(commentRepository).findByIdAndDeletedFalse(2L);
    }

    @Test
    public void givenChildCommentWhichsParentTypeNotMatchParentCommentsParentTypeExpectException() {
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNote));
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(parentComment));
        childComment.setParentType(ParentType.STICKNOTE);

        assertThatThrownBy(() -> commentService.createComment(childComment))
                .isInstanceOf(ParentNotSameWithParentCommentException.class);
        verify(commentRepository).findByIdAndDeletedFalse(2L);
    }

    @Test
    public void givenChildCommentWhichParentCommentsHasParentCommentCreateComment() {
        Comment comment = new Comment(null, 4L, ParentType.ARTICLE, 2L, COMMENT, 1L, false,
                null, Calendar.getInstance(), user, Calendar.getInstance(), user);
        Comment childComment = new Comment(3L, 4L, ParentType.ARTICLE, 1L, COMMENT, 1L, false,
                null, Calendar.getInstance(), user, Calendar.getInstance(), user);
        Comment childsParentComment = new Comment(2L, 4L, ParentType.ARTICLE, 1L, COMMENT, 1L, false,
                null, Calendar.getInstance(), user, Calendar.getInstance(), user);
        Comment parentsParentComment = new Comment(1L, 4L, ParentType.ARTICLE, null, COMMENT, 1L, false,
                null, Calendar.getInstance(), user, Calendar.getInstance(), user);
        when(articleRepository.findById(anyLong())).thenReturn(Optional.of(article));
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(childsParentComment));
        when(commentRepository.save(any())).thenReturn(parentsParentComment);
        when(commentRepository.save(any())).thenReturn(childComment);

        Comment expectedComment = commentService.createComment(comment);

        assertThat(childComment).isEqualTo(expectedComment);
        verify(commentRepository).findByIdAndDeletedFalse(1L);
        verify(commentRepository).findByIdAndDeletedFalse(2L);
        verify(commentRepository).save(childsParentComment);
        verify(commentRepository).save(comment);
    }


    @Test
    public void givenCommentNotFoundParentExpectException() {
        when(commentRepository.findById(any())).thenThrow(new CommentNotFoundException(1L));

        assertThatThrownBy(() -> commentRepository.findById(1L)).isInstanceOf(CommentNotFoundException.class);
    }

    @Test
    public void givenExistingCommentIdGetCommentExpectComment() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(commentWithId));

        Comment expectedComment = commentService.getComment(1L);

        assertThat(expectedComment).isEqualTo(commentWithId);

        verify(commentRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenNonExistingCommentIdGetCommentExpectException() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenThrow(new CommentNotFoundException(1L));

        assertThatThrownBy(() -> commentRepository.findByIdAndDeletedFalse(1L))
                .isInstanceOf(CommentNotFoundException.class);

        verify(commentRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenInvalidCommentIdUpdateCommentExpectException() {
        long id = 1L;
        when(commentRepository.findByIdAndDeletedFalse(id)).thenThrow(new CommentNotFoundException(id));

        assertThatThrownBy(() -> commentService.updateComment(id, commentWithId))
                .isInstanceOf(CommentNotFoundException.class);
    }

    @Test
    public void givenExistingCommentIdWithOwnerUpdateComment() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(commentWithId));
        when(userService.getCurrentUser()).thenReturn(user);

        Comment updatedComment = commentService.updateComment(1, comment);

        assertThat(isCommentUpdatedCorrectly(updatedComment, commentWithId));
        verify(commentRepository).save(updatedComment);
    }

    @Test
    public void givenExistingCommentIdAndNonOwnerUserUpdateCommentExpectException() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(commentWithId));
        when(userService.getCurrentUser()).thenReturn(otherUser);

        assertThatThrownBy(() -> commentService.updateComment(1L, comment))
                .isInstanceOf(AttemptToUnauthorizedOperationException.class);
    }

    @Test
    public void givenNotExistingParentIdGetCommentsExpectExceptionForArticle() {
        int page = 0;
        int size = 3;
        long articleId = 2L;
        when(articleRepository.findById(articleId)).thenThrow(new ArticleNotFoundException(articleId));

        assertThatThrownBy(() -> commentService.getComments(articleId, ParentType.ARTICLE, page, size))
                .isInstanceOf(ArticleNotFoundException.class);
        verify(articleRepository).findById(articleId);
    }

    @Test
    public void givenNotExistingParentIdGetCommentsExpectExceptionForStickNote() {
        int page = 0;
        int size = 3;
        long stickNoteId = 2L;
        when(stickNoteRepository.findById(stickNoteId)).thenThrow(new StickNoteNotFoundException(stickNoteId));

        assertThatThrownBy(() -> commentService.getComments(stickNoteId, ParentType.STICKNOTE, page, size))
                .isInstanceOf(StickNoteNotFoundException.class);
        verify(stickNoteRepository).findById(stickNoteId);
    }

    @Test
    public void givenExistingParentIdAndPageAndSizeGetCommentsForArticle() {
        int page = 0;
        int size = 100;
        long articleId = 2L;
        Pageable pageable = PageRequest.of(page, size, Sort.by(CREATED_AT).ascending());
        Article article = new Article(articleId, "Test", "Content", null, null, null, null);
        Page<Comment> commentPage = new PageImpl<Comment>(commentListForArticle, pageable, 2);
        when(articleRepository.findById(articleId)).thenReturn(Optional.of(article));
        when(commentRepository.findAllByParentIdAndParentTypeAndDeletedFalse(articleId, ParentType.ARTICLE,
                pageable))
                .thenReturn(commentPage);

        Page<Comment> returnedComments = commentService.getComments(articleId, ParentType.ARTICLE, page, size);
        assertThat(returnedComments.getContent().get(0).getSubCommentList().size()).isEqualTo(0);

        verify(articleRepository).findById(articleId);
        verify(commentRepository).findAllByParentIdAndParentTypeAndDeletedFalse(articleId, ParentType.ARTICLE,
                pageable);
    }

    @Test
    public void givenExistingParentIdAndPageAndSizeGetCommentsForStickNote() {
        int page = 0;
        int size = 100;
        long stickNoteId = 2L;
        Pageable pageable = PageRequest.of(page, size, Sort.by(CREATED_AT).ascending());
        StickNote stickNote = new StickNote(stickNoteId, DUMMY_TEXT, 1L, 100, 100, Calendar.getInstance()
                .toInstant(), null, Calendar.getInstance().toInstant(), null);
        Page<Comment> commentPage = new PageImpl<Comment>(commentListForStickNote, pageable, 2);
        when(stickNoteRepository.findById(stickNoteId)).thenReturn(Optional.of(stickNote));
        when(commentRepository.findAllByParentIdAndParentTypeAndDeletedFalse(stickNoteId, ParentType.STICKNOTE,
                pageable)).thenReturn(commentPage);

        Page<Comment> returnedComments = commentService.getComments(stickNoteId, ParentType.STICKNOTE, page, size);
        assertThat(returnedComments.getContent().get(0).getSubCommentList().size()).isEqualTo(0);

        verify(stickNoteRepository).findById(stickNoteId);
        verify(commentRepository).findAllByParentIdAndParentTypeAndDeletedFalse(stickNoteId, ParentType.STICKNOTE,
                pageable);
    }

    @Test
    public void givenExistingParentIdAndPageAndSizeGetCommentsForBlog() {
        int page = 0;
        int size = 100;
        long blogId = 2L;
        Pageable pageable = PageRequest.of(page, size, Sort.by(CREATED_AT).ascending());
        Page<Comment> commentPage = new PageImpl<Comment>(commentListForBlog, pageable, 2);
        Blog blog = new Blog(1L, DUMMY_TEXT, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),PHOTO,
                BlogStatus.DRAFT, false, null, Calendar.getInstance(), null, Calendar.getInstance(), null);
        when(blogRepository.findByIdAndDeletedFalse(blogId)).thenReturn(Optional.of(blog));
        when(commentRepository.findAllByParentIdAndParentTypeAndDeletedFalse(blogId, ParentType.BLOG,
                pageable)).thenReturn(commentPage);

        Page<Comment> returnedComments = commentService.getComments(blogId, ParentType.BLOG, page, size);
        assertThat(returnedComments.getContent().get(0).getSubCommentList().size()).isEqualTo(0);

        verify(blogRepository).findByIdAndDeletedFalse(blogId);
        verify(commentRepository).findAllByParentIdAndParentTypeAndDeletedFalse(blogId, ParentType.BLOG, pageable);
    }

    @Test
    public void givenNonExistingIdDeleteCommentExpectException() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenThrow(new CommentNotFoundException(1L));

        assertThatThrownBy(() -> commentService.deleteComment(1L))
                .isInstanceOf(CommentNotFoundException.class);

        verify(commentRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenExistingCommentIdAndNonOwnerUserDeleteCommentExpectException() {
        comment.setChildCount(0L);
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(comment));
        when(userService.getCurrentUser()).thenReturn(otherUser);

        assertThatThrownBy(() -> commentService.deleteComment(1L))
                .isInstanceOf(AttemptToUnauthorizedOperationException.class);

        verify(commentRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void givenExistingCommentAndOwnerDeleteComment() {
        comment.setChildCount(0L);
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(comment));
        when(userService.getCurrentUser()).thenReturn(user);

        commentService.deleteComment(1L);

        verify(commentRepository).save(comment);
    }

    @Test
    public void givenExistingCommentIdHavingChildCommentsDeleteCommentExpectException() {
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(comment));
        when(userService.getCurrentUser()).thenReturn(user);
        assertThatThrownBy(() -> commentService.deleteComment(1L))
                .isInstanceOf(AttemptToDeleteCommentWithSubComment.class);
    }

    @Test
    public void givenExistingChildCommentIdWithoutChildDeleteComment() {
        childComment.setChildCount(0L);
        childComment.setParentCommentId(null);
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(childComment));
        when(userService.getCurrentUser()).thenReturn(user);

        commentService.deleteComment(1L);

        verify(commentRepository).save(childComment);
    }

    @Test
    public void givenExistingParentCommentWithoutChildDeleteComment() {
        comment.setChildCount(0L);
        when(commentRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(comment));
        when(userService.getCurrentUser()).thenReturn(user);

        commentService.deleteComment(1L);

        verify(commentRepository).save(comment);
    }

    @Test
    public void givenNonExistingCommentIdGetSubCommentsExpectException() {
        long id = 10L;
        when(commentRepository.findById(id)).thenThrow(new CommentNotFoundException(id));

        assertThatThrownBy(() -> commentService.getSubComments(id)).isInstanceOf(CommentNotFoundException.class);

        verify(commentRepository).findById(id);
    }

    @Test
    public void givenExistingCommentIdGetSubComments() {
        long id = 1L;
        List<Comment> subComments = Arrays.asList(comment, comment);
        when(commentRepository.findById(id)).thenReturn(Optional.of(parentComment));
        when(commentRepository.findAllByParentCommentId(id)).thenReturn(subComments);

        assertThat(commentService.getSubComments(id)).isEqualTo(subComments);

        verify(commentRepository).findById(id);
        verify(commentRepository).findAllByParentCommentId(id);
    }

    @Test
    public void givenNonExistingParentIdGetCommentsV2ForStickNoteExpectException() {
        long parentId = 10L;
        when(stickNoteRepository.findById(parentId)).thenThrow(new StickNoteNotFoundException(parentId));

        assertThatThrownBy(() -> commentService.getCommentsV2(parentId, ParentType.STICKNOTE, 0, 100))
                .isInstanceOf(StickNoteNotFoundException.class);

        verify(stickNoteRepository).findById(parentId);
    }

    @Test
    public void givenNonExistingParentIdGetCommentsV2ForArticleExpectException() {
        long parentId = 10L;
        when(articleRepository.findById(parentId)).thenThrow(new ArticleNotFoundException(parentId));

        assertThatThrownBy(() -> commentService.getCommentsV2(parentId, ParentType.ARTICLE, 0, 100))
                .isInstanceOf(ArticleNotFoundException.class);

        verify(articleRepository).findById(parentId);
    }

    @Test
    public void givenNonExistingParentIdGetCommentsV2ForBlogExpectException() {
        long parentId = 10L;
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenThrow(new BlogNotFountException(parentId));

        assertThatThrownBy(() -> commentService.getCommentsV2(parentId, ParentType.BLOG, 0, 100))
                .isInstanceOf(BlogNotFountException.class);

        verify(blogRepository).findByIdAndDeletedFalse(parentId);
    }

    @Test
    public void givenExistingParentIdGetCommentsV2ForArticleExpectException() {
        long parentId = 10L;
        List<Comment> parentCommentList = Arrays.asList(comment, comment);
        Pageable pageable = PageRequest.of(0, 100, Sort.by(CREATED_AT).ascending());
        Page<Comment> commentPage = new PageImpl<Comment>(parentCommentList, pageable, 2);
        when(articleRepository.findById(anyLong())).thenReturn(Optional.of(article));
        when(commentRepository.findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(parentId,
                ParentType.ARTICLE, pageable)).thenReturn(commentPage);

        Page<Comment> returnedPage = commentService.getCommentsV2(parentId, ParentType.ARTICLE, 0, 100);
        assertThat(returnedPage.getContent().size()).isEqualTo(2);

        verify(articleRepository).findById(parentId);
        verify(commentRepository).findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(parentId,
                ParentType.ARTICLE, pageable);
    }

    @Test
    public void givenExistingParentIdGetCommentsV2ForBlogExpectException() {
        long parentId = 10L;
        List<Comment> parentCommentList = Arrays.asList(comment, comment);
        Pageable pageable = PageRequest.of(0, 100, Sort.by(CREATED_AT).ascending());
        Page<Comment> commentPage = new PageImpl<Comment>(parentCommentList, pageable, 2);
        when(blogRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(blog));
        when(commentRepository.findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(parentId,
                ParentType.BLOG, pageable)).thenReturn(commentPage);

        Page<Comment> returnedPage = commentService.getCommentsV2(parentId, ParentType.BLOG, 0, 100);
        assertThat(returnedPage.getContent().size()).isEqualTo(2);

        verify(blogRepository).findByIdAndDeletedFalse(parentId);
        verify(commentRepository).findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(parentId,
                ParentType.BLOG, pageable);

    }

    private boolean isCommentUpdatedCorrectly(Comment updatedComment, Comment commentWithId) {
        commentWithId.setComment(updatedComment.getComment());
        return commentWithId.equals(updatedComment);
    }
}
