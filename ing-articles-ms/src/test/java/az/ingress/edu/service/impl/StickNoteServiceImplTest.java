package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.repository.ArticleRepository;
import az.ingress.edu.repository.StickNoteRepository;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StickNoteServiceImplTest {

    private static final String DUMMY_TEXT = "StickNote title";
    private static final String MODIFIED_BY = "Ichigo Kurasaki";
    private static final String CREATED_BY = "Ken Kanniki";
    private static final String CREATED_BY_USERNAME = "ejder";
    private static final String MODIFIED_BY_USERNAME = "otar";
    private static final Set<String> ROLES = new HashSet<>(Arrays.asList("USER", "OTHER"));
    private static final Set<String> ROLES_WITH_ADMIN = new HashSet<>(Arrays.asList("USER", "ADMIN"));
    private static final UserDetails CREATED_BY_USERDETAILS = new UserDetails(CREATED_BY_USERNAME, CREATED_BY, ROLES);
    private static final UserDetails MODIFIED_BY_USERDETAILS = new UserDetails(MODIFIED_BY_USERNAME,
            MODIFIED_BY,ROLES);

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private StickNoteRepository stickNoteRepository;

    @InjectMocks
    private StickNoteServiceImpl stickNoteService;

    @Test
    public void givenNonExistingArticleAndStickNoteCreateStickNoteExpectException() {
        long id = 1000L;
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, id, 0, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(articleRepository.findById(id)).thenThrow(new ArticleNotFoundException(id));

        assertThatThrownBy(() -> stickNoteService.createStickNote(stickNote))
                .isInstanceOf(ArticleNotFoundException.class);

        verify(articleRepository).findById(id);
    }

    @Test
    public void givenExistingArticleAndStickNoteCreateStickNote() {
        long articleId = 1000L;
        Article article = new Article(articleId, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),
                MODIFIED_BY_USERDETAILS, Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS);
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, articleId, 0, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        StickNote savedStickNote = new StickNote(1L, DUMMY_TEXT, articleId, 0, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(articleRepository.findById(articleId)).thenReturn(Optional.of(article));
        when(stickNoteRepository.save(any())).thenReturn(savedStickNote);

        assertThat(stickNoteService.createStickNote(stickNote)).isEqualTo(savedStickNote);

        verify(articleRepository).findById(articleId);
        verify(stickNoteRepository).save(stickNote);
    }

    @Test
    public void givenNonExistingStickNoteIdGetStickNoteExpectException() {
        long id = 1000L;
        when(stickNoteRepository.findById(id)).thenThrow(new StickNoteNotFoundException(id));

        assertThatThrownBy(() -> stickNoteService.getStickNote(id)).isInstanceOf(StickNoteNotFoundException.class);

        verify(stickNoteRepository).findById(id);
    }

    @Test
    public void givenExistingStickNoteIdGetStickNote() {
        long id = 1L;
        StickNote stickNote = new StickNote(id, DUMMY_TEXT, 100L, 0, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteRepository.findById(id)).thenReturn(Optional.of(stickNote));

        assertThat(stickNoteService.getStickNote(id)).isEqualTo(stickNote);

        verify(stickNoteRepository).findById(id);
    }

    @Test
    public void givenNonExistingStickNoteIdUpdateStickNoteExpectException() {
        long id = 1L;
        when(stickNoteRepository.findById(id)).thenThrow(new StickNoteNotFoundException(id));

        assertThatThrownBy(() -> stickNoteService.getStickNote(id)).isInstanceOf(StickNoteNotFoundException.class);

        verify(stickNoteRepository).findById(id);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithAdminRoleUpdateStickNote() {
        StickNote stickNoteForUpdate = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, null, 10, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForUpdate));

        stickNoteService.updateStickNote(1L,stickNote,
                new UserDetails(MODIFIED_BY_USERNAME, MODIFIED_BY, ROLES_WITH_ADMIN));

        verify(stickNoteRepository).findById(1L);
        verify(stickNoteRepository).save(stickNoteForUpdate);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithOwnerUsernameAndUserRoleUpdateStickNote() {
        StickNote stickNoteForUpdate = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, null, 10, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForUpdate));

        stickNoteService.updateStickNote(1L, stickNote, new UserDetails(CREATED_BY_USERNAME, CREATED_BY, ROLES));

        verify(stickNoteRepository).findById(1L);
        verify(stickNoteRepository).save(stickNoteForUpdate);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithNonOwnerUsernameAndUserRoleUpdateStickNoteExpectException() {
        StickNote stickNoteForDelete = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, null, 10, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForDelete));

        Assertions.assertThatThrownBy(() -> stickNoteService.updateStickNote(1L,stickNote,
                new UserDetails(MODIFIED_BY_USERNAME, MODIFIED_BY,ROLES)))
                .isInstanceOf(AttemptToUnauthorizedOperationException.class);
    }

    @Test
    public void givenExistingStickNoteIdAndValidStickNoteUpdateStickNote() {
        long id = 1L;
        StickNote stickNote = new StickNote(null, DUMMY_TEXT, null, 10, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        StickNote updated = new StickNote(1L, DUMMY_TEXT, 10L, 10, 10,Calendar.getInstance().toInstant(),
                CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS);
        when(stickNoteRepository.findById(id)).thenReturn(Optional.of(updated));
        when(stickNoteRepository.save(any())).thenReturn(updated);

        assertThat(stickNoteService.updateStickNote(id, stickNote,CREATED_BY_USERDETAILS)).isEqualTo(updated);

        verify(stickNoteRepository).findById(id);
        verify(stickNoteRepository).save(updated);
    }

    @Test
    public void givenNonExistingArticleIdGetStickNotesExpectException() {
        long id = 1000L;
        when(articleRepository.findById(id)).thenThrow(new ArticleNotFoundException(id));

        assertThatThrownBy(() -> stickNoteService.getStickNotes(id)).isInstanceOf(ArticleNotFoundException.class);

        verify(articleRepository).findById(id);
    }

    @Test
    public void givenExistingArticleIdGetStickNotes() {
        long articleId = 1000L;
        Article article = new Article(articleId, DUMMY_TEXT, DUMMY_TEXT, Calendar.getInstance().toInstant(),
                MODIFIED_BY_USERDETAILS, Calendar.getInstance().toInstant(), CREATED_BY_USERDETAILS);
        List<StickNote> stickNotes = Arrays.asList(new StickNote(1L, DUMMY_TEXT, articleId, 10, 10,
                        Calendar.getInstance().toInstant(),CREATED_BY_USERDETAILS,
                        Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS),
                new StickNote(2L, DUMMY_TEXT, articleId, 20, 10,Calendar.getInstance().toInstant(),
                        CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS),
                new StickNote(3L, DUMMY_TEXT, articleId, 10, 10,Calendar.getInstance().toInstant(),
                        CREATED_BY_USERDETAILS,Calendar.getInstance().toInstant(),MODIFIED_BY_USERDETAILS));
        when(articleRepository.findById(articleId)).thenReturn(Optional.of(article));
        when(stickNoteRepository.findAllByArticleId(articleId)).thenReturn(stickNotes);

        assertThat(stickNoteService.getStickNotes(articleId)).isEqualTo(stickNotes);

        verify(articleRepository).findById(articleId);
        verify(stickNoteRepository).findAllByArticleId(articleId);
    }

    @Test
    public void givenExistingStickNoteIdDeleteStickNote() {
        StickNote stickNoteForDelete = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForDelete));

        stickNoteService.deleteStickNote(1L,CREATED_BY_USERDETAILS);

        verify(stickNoteRepository).findById(1L);
        verify(stickNoteRepository).delete(stickNoteForDelete);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithAdminRoleDeleteStickNote() {
        StickNote stickNoteForDelete = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForDelete));

        stickNoteService.deleteStickNote(1L,new UserDetails(MODIFIED_BY_USERNAME, MODIFIED_BY, ROLES_WITH_ADMIN));

        verify(stickNoteRepository).findById(1L);
        verify(stickNoteRepository).delete(stickNoteForDelete);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithOwnerUsernameAndUserRoleDeleteSrickNote() {
        StickNote stickNoteForDelete = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForDelete));

        stickNoteService.deleteStickNote(1L,new UserDetails(CREATED_BY_USERNAME, CREATED_BY, ROLES));

        verify(stickNoteRepository).findById(1L);
        verify(stickNoteRepository).delete(stickNoteForDelete);
    }

    @Test
    public void givenExistingStickNoteIdAndUserWithNonOwnerUsernameAndUserRoleDeleteStickNoteExpectException() {
        StickNote stickNoteForDelete = StickNote
                .builder()
                .id(1L)
                .title(DUMMY_TEXT)
                .articleId(1L)
                .xCoordinate(150L)
                .yCoordinate(200L)
                .createdAt(Calendar.getInstance().toInstant())
                .createdBy(CREATED_BY_USERDETAILS)
                .modifiedAt(Calendar.getInstance().toInstant())
                .modifiedBy(MODIFIED_BY_USERDETAILS)
                .build();
        when(stickNoteRepository.findById(anyLong())).thenReturn(Optional.of(stickNoteForDelete));

        Assertions.assertThatThrownBy(() -> stickNoteService
                .deleteStickNote(1L,new UserDetails(MODIFIED_BY_USERNAME, MODIFIED_BY, ROLES)))
                .isInstanceOf(AttemptToUnauthorizedOperationException.class);
    }

    @Test
    public void givenNonExistingStickNoteIdExpectException() {
        when(stickNoteRepository.findById(anyLong())).thenThrow(new StickNoteNotFoundException(1L));

        assertThatThrownBy(() -> stickNoteService.deleteStickNote(1L,CREATED_BY_USERDETAILS))
                .isInstanceOf(StickNoteNotFoundException.class);

        verify(stickNoteRepository).findById(1L);
    }
}
