package az.ingress.edu.exception;

import az.ingress.edu.model.ParentType;

public class ParentNotSameWithParentCommentException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ParentNotSameWithParentCommentException(long id, ParentType parentType) {
        super("Subcomment parent id: " + id + " or parent type: " + parentType
                + " does't match with parent comment");
    }
}
