package az.ingress.edu.exception;

public class TagAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TagAlreadyExistException() {
        super("Tag already exist");
    }
}
