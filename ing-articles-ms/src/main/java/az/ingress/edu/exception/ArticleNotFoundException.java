package az.ingress.edu.exception;

public class ArticleNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ArticleNotFoundException(long id) {
        super("Article with id: " + id + " not found");
    }

}
