package az.ingress.edu.exception;

public class CommentNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CommentNotFoundException(long commentId) {
        super("Comment with id: " + commentId + " not found");
    }
}
