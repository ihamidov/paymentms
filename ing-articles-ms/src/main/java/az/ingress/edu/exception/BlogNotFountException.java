package az.ingress.edu.exception;

public class BlogNotFountException extends RuntimeException {

    private static final long serialVersionUID = 1;

    public BlogNotFountException(long id) {
        super(String.format("Blog with id: %d not found", id));
    }
}
