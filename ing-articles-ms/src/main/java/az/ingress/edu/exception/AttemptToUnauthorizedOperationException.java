package az.ingress.edu.exception;

public class AttemptToUnauthorizedOperationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AttemptToUnauthorizedOperationException() {
        super("You don't have access for this operation");
    }
}
