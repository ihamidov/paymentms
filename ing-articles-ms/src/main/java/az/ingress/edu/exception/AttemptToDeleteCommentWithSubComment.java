package az.ingress.edu.exception;

public class AttemptToDeleteCommentWithSubComment extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AttemptToDeleteCommentWithSubComment(long commentId) {
        super("Comment with id: " + commentId + " has sub-comments");
    }
}
