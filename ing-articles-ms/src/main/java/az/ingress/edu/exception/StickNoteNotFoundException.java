package az.ingress.edu.exception;

public class StickNoteNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public StickNoteNotFoundException(long id) {
        super("StickNote with id: " + id + " not found");
    }
}
