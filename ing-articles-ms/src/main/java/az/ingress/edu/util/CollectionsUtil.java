package az.ingress.edu.util;

import java.util.Collection;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CollectionsUtil {

    public static boolean isEmpty(Collection coll) {
        return coll == null || coll.isEmpty();
    }
}
