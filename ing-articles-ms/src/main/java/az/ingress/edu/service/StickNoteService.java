package az.ingress.edu.service;

import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import java.util.List;

public interface StickNoteService {

    StickNote createStickNote(StickNote stickNote);

    StickNote getStickNote(long id);

    StickNote updateStickNote(long id, StickNote stickNote, UserDetails userDetails);

    void deleteStickNote(long id,UserDetails userDetails);

    List<StickNote> getStickNotes(long articleId);
}
