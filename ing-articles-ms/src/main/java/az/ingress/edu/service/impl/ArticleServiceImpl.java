package az.ingress.edu.service.impl;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.repository.ArticleRepository;
import az.ingress.edu.service.ArticleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;

    public ArticleServiceImpl(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Article createArticle(Article article) {
        initArticleProperties(article);
        return articleRepository.save(article);
    }

    @Override
    public Article getArticle(long id) {
        return articleRepository.findById(id).orElseThrow(() -> new ArticleNotFoundException(id));
    }

    @Override
    public Article updateArticle(long articleId, Article article, UserDetails userDetails) {
        Article articleToUpdate = articleRepository.findById(articleId)
                .orElseThrow(() -> new ArticleNotFoundException(articleId));
        checkIfHasAuthorityToOperation(userDetails, articleToUpdate);
        return updateArticleAndSave(article, articleToUpdate);
    }

    @Override
    public void deleteArticle(long id, UserDetails userDetails) {
        Article article = articleRepository.findById(id).orElseThrow(() -> new ArticleNotFoundException(id));
        checkIfHasAuthorityToOperation(userDetails, article);
        articleRepository.delete(article);
    }

    @Override
    public Page<Article> findAll(int page, int size, String order, String direction) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(direction), order));
        return articleRepository.findAll(pageable);
    }

    private void checkIfHasAuthorityToOperation(UserDetails userDetails, Article article) {
        if (!(userDetails.getRoles().contains("ADMIN")
                || article.getCreatedBy().getUsername().equals(userDetails.getUsername()))) {
            throw new AttemptToUnauthorizedOperationException();
        }
    }

    private void initArticleProperties(Article article) {
        article.setId(null);
    }

    private Article updateArticleAndSave(Article article, Article articleToUpdate) {
        articleToUpdate.setTitle(article.getTitle());
        articleToUpdate.setDescription(article.getDescription());
        articleToUpdate.setModifiedBy(article.getModifiedBy());
        return articleRepository.save(articleToUpdate);
    }
}
