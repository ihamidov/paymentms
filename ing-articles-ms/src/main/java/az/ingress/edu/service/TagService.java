package az.ingress.edu.service;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.TagDto;
import az.ingress.edu.model.Tag;
import java.util.List;
import org.springframework.data.domain.Page;

public interface TagService {

    Tag findById(long id);

    Tag add(TagDto dto);

    Tag update(TagDto tagDto, long id);

    void deleteById(long id);

    Page<Tag> list(PageDto pageDto);

    List<Tag> toEntityList(List<IdDto> dtoList);

}
