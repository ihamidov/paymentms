package az.ingress.edu.service.impl;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.TagDto;
import az.ingress.edu.exception.TagAlreadyExistException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.model.Tag;
import az.ingress.edu.repository.TagRepository;
import az.ingress.edu.service.TagService;
import az.ingress.edu.util.CollectionsUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Override
    public Tag findById(long id) {
        return tagRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new TagNotFoundException(id));
    }

    @Override
    public Tag add(TagDto dto) {
        checkIfAlreadyExist(dto);
        return tagRepository.save(toEntity(dto));
    }

    @Override
    public Tag update(TagDto tagDto, long id) {
        findById(id);
        Tag tag = toEntity(tagDto);
        tag.setId(id);
        checkIfAlreadyExist(tagDto);
        return tagRepository.save(tag);
    }

    @Override
    public void deleteById(long id) {
        Tag tag = findById(id);
        tag.setDeleted(true);
        tagRepository.save(tag);
    }

    @Override
    public Page<Tag> list(PageDto pageDto) {
        return tagRepository.findAllByDeletedFalse(toPageRequest(pageDto));
    }

    @Override
    public List<Tag> toEntityList(List<IdDto> dtoList) {
        if (!CollectionsUtil.isEmpty(dtoList)) {
            return dtoList.stream().distinct().map(t -> toEntity(t.getId())).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private void checkIfAlreadyExist(TagDto dto) {
        Optional<Tag> tagByName = tagRepository.getByName(dto.getName());
        if (tagByName.isPresent()) {
            throw new TagAlreadyExistException();
        }
    }

    private PageRequest toPageRequest(PageDto pageDto) {
        return PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(),
                Sort.Direction.fromString(pageDto.getSortDirection()), pageDto.getSortColumn());
    }

    private Tag toEntity(TagDto dto) {
        Tag tag = new Tag();
        tag.setName(dto.getName());
        return tag;
    }

    private Tag toEntity(long id) {
        return Tag.builder()
                .id(id)
                .build();
    }
}
