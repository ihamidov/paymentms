package az.ingress.edu.service;

import az.ingress.edu.dto.BlogFilterDto;
import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.UpdateBlogDto;
import az.ingress.edu.model.Blog;

public interface BlogService {

    Blog saveBlog(CreateBlogDto createBlogDto);

    Blog updateBlog(UpdateBlogDto updateBlogDto, long id);

    void deleteBlogById(long id);

    Blog findBlogById(long id);

    ListDto getBlogs(PageDto pageDto);

    ListDto getPublishedBlogs(PageDto pageDto, BlogFilterDto blogFilter);
}
