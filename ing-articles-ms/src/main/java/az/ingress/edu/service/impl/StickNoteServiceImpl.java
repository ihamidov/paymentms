package az.ingress.edu.service.impl;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.repository.ArticleRepository;
import az.ingress.edu.repository.StickNoteRepository;
import az.ingress.edu.service.StickNoteService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class StickNoteServiceImpl implements StickNoteService {

    private final StickNoteRepository stickNoteRepository;
    private final ArticleRepository articleRepository;

    public StickNoteServiceImpl(StickNoteRepository stickNoteRepository, ArticleRepository articleRepository) {
        this.stickNoteRepository = stickNoteRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public StickNote createStickNote(StickNote stickNote) {
        retrieveArticle(stickNote.getArticleId());
        return stickNoteRepository.save(stickNote);
    }

    @Override
    public StickNote getStickNote(long id) {
        return retrieveStickNote(id);
    }

    @Override
    public StickNote updateStickNote(long id, StickNote stickNote, UserDetails userDetails) {
        StickNote stickNoteToUpdate = retrieveStickNote(id);
        checkIfHasAuthorityToOperation(userDetails,stickNoteToUpdate);
        return updateStickNoteAndSave(stickNote, stickNoteToUpdate);
    }

    @Override
    public List<StickNote> getStickNotes(long articleId) {
        Article article = retrieveArticle(articleId);
        return stickNoteRepository.findAllByArticleId(article.getId());
    }

    @Override
    public void deleteStickNote(long id,UserDetails userDetails) {
        StickNote stickNoteToDelete = retrieveStickNote(id);
        checkIfHasAuthorityToOperation(userDetails,stickNoteToDelete);
        stickNoteRepository.delete(stickNoteToDelete);
    }

    private StickNote updateStickNoteAndSave(StickNote stickNote, StickNote stickNoteToUpdate) {
        stickNoteToUpdate.setTitle(stickNote.getTitle());
        stickNoteToUpdate.setXCoordinate(stickNote.getXCoordinate());
        stickNoteToUpdate.setYCoordinate(stickNote.getYCoordinate());
        stickNoteToUpdate.setModifiedBy(stickNote.getModifiedBy());
        return stickNoteRepository.save(stickNoteToUpdate);
    }

    private StickNote retrieveStickNote(long id) {
        return stickNoteRepository.findById(id).orElseThrow(() -> new StickNoteNotFoundException(id));
    }

    private void checkIfHasAuthorityToOperation(UserDetails userDetails, StickNote stickNote) {
        if (!(userDetails.getRoles().contains("ADMIN")
                || stickNote.getCreatedBy().getUsername().equals(userDetails.getUsername()))) {
            throw new AttemptToUnauthorizedOperationException();
        }
    }

    private Article retrieveArticle(long id) {
        return articleRepository.findById(id).orElseThrow(() -> new ArticleNotFoundException(id));
    }
}
