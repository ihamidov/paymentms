package az.ingress.edu.service.impl;

import static az.ingress.edu.util.CollectionsUtil.isEmpty;

import az.ingress.edu.config.SwaggerConfiguration;
import az.ingress.edu.dto.BlogFilterDto;
import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListBlogDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.UpdateBlogDto;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.model.Tag;
import az.ingress.edu.repository.BlogRepository;
import az.ingress.edu.service.BlogService;
import az.ingress.edu.service.TagService;
import az.ingress.edu.service.UserService;
import az.ingress.edu.util.CollectionsUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BlogServiceImpl implements BlogService {

    private final BlogRepository blogRepository;
    private final UserService userService;
    private final TagService tagService;
    private final SwaggerConfiguration swaggerConfiguration;
    private final AccessToken accessToken;

    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_MANAGER = "COURSE_MANAGER";

    @Override
    public Blog saveBlog(CreateBlogDto createBlogDto) {
        Blog blog = toBlog(createBlogDto);
        checkIfTagsExist(blog);
        blog.setStatus(BlogStatus.DRAFT);
        blog.setCreatedBy(userService.getCurrentUser());
        blog.setModifiedBy(userService.getCurrentUser());
        return blogRepository.save(blog);
    }

    @Override
    public Blog updateBlog(UpdateBlogDto updateBlogDto, long id) {
        Blog blog = retrieveBlog(id);
        checkIfHaveAccess(blog);
        checkIfTagsExist(blog);
        setPropertiesForUpdate(updateBlogDto, blog);
        return blogRepository.save(blog);
    }

    @Override
    public void deleteBlogById(long id) {
        Blog blog = retrieveBlog(id);
        checkIfHaveAccess(blog);
        blog.setDeleted(true);
        blogRepository.save(blog);
    }

    @Override
    public Blog findBlogById(long id) {
        return retrieveBlog(id);
    }

    @Override
    public ListDto getBlogs(PageDto pageDto) {
        Page<Blog> pageBlog;
        if (hasRole(ROLE_ADMIN) || hasRole(ROLE_MANAGER)) {
            pageBlog = blogRepository.findAllByDeletedFalse(toPageRequest(pageDto));
        } else {
            pageBlog = blogRepository.findAllByUsername(toPageRequest(pageDto),
                    userService.getCurrentUser().getUsername());
        }
        return getListDtoForBlog(pageBlog);
    }

    @Override
    public ListDto getPublishedBlogs(PageDto pageDto, BlogFilterDto blogFilter) {
        Page<Blog> pageBlog = blogRepository.findAll(toPageRequest(pageDto), blogFilter);
        return getListDtoForBlog(pageBlog);
    }

    private Blog retrieveBlog(long id) {
        return blogRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new BlogNotFountException(id));
    }

    private PageRequest toPageRequest(PageDto pageDto) {
        return PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(),
                Sort.Direction.fromString(pageDto.getSortDirection()), pageDto.getSortColumn());
    }

    private ListDto getListDtoForBlog(Page<Blog> pageBlog) {
        return new ListDto(pageBlog.getContent().stream().map(blog -> ListBlogDto.toListBlogDto(blog)
        ).collect(Collectors.toList()), pageBlog.getTotalPages(), pageBlog.getTotalElements());
    }

    private void setPropertiesForUpdate(UpdateBlogDto updateBlogDto, Blog blog) {
        blog.setTitle(updateBlogDto.getTitle());
        blog.setContent(updateBlogDto.getContent());
        blog.setPhoto(updateBlogDto.getPhoto());
        blog.setStatus(updateBlogDto.getStatus());
        blog.setTags(toTagList(updateBlogDto.getTags()));
        if (updateBlogDto.getStatus().equals(BlogStatus.PUBLISHED)) {
            blog.setPublishDate(Calendar.getInstance().toInstant());
        }
    }

    private Blog toBlog(CreateBlogDto createBlogDto) {
        return Blog
                .builder()
                .title(createBlogDto.getTitle())
                .category(createBlogDto.getCategory())
                .content(createBlogDto.getContent())
                .photo(createBlogDto.getPhoto())
                .tags(toTagList(createBlogDto.getTags()))
                .build();
    }

    private List<Tag> toTagList(List<IdDto> dtoList) {
        if (!CollectionsUtil.isEmpty(dtoList)) {
            return dtoList.stream().distinct().map(t -> Tag.builder().id(t.getId()).build())
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private void checkIfTagsExist(Blog blog) {
        if (!isEmpty(blog.getTags())) {
            blog.getTags().forEach(tag -> {
                tagService.findById(tag.getId());
            });
        }
    }

    private boolean hasRole(String role) {
        String clientId = swaggerConfiguration.getClientId();
        return accessToken.getResourceAccess(clientId).getRoles().contains(role);
    }

    private void checkIfHaveAccess(Blog blog) {
        if (!hasRole(ROLE_MANAGER) && !hasRole(ROLE_ADMIN) && !blog.getCreatedBy().getUsername()
                .equals(userService.getCurrentUser().getUsername())) {
            throw new UnauthorizedOperationException();
        }
    }
}
