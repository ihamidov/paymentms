package az.ingress.edu.service.impl;

import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToDeleteCommentWithSubComment;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.exception.ParentNotSameWithParentCommentException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.ArticleRepository;
import az.ingress.edu.repository.BlogRepository;
import az.ingress.edu.repository.CommentRepository;
import az.ingress.edu.repository.StickNoteRepository;
import az.ingress.edu.service.CommentService;
import az.ingress.edu.service.UserService;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private static final String CREATED_AT = "createdAt";
    private final CommentRepository commentRepository;
    private final ArticleRepository articleRepository;
    private final StickNoteRepository stickNoteRepository;
    private final BlogRepository blogRepository;
    private final UserService userService;

    @Transactional
    @Override
    public Comment createComment(Comment comment) {
        checkIfParentIdExists(comment.getParentId(), comment.getParentType());
        setCommentProperties(comment);
        if (isSubComment(comment)) {
            Comment parentComment = retrieveParentComment(comment);

            if (isParentIdOrTypeDoesNotMatchParentCommentsParentId(comment, parentComment)) {
                throw new ParentNotSameWithParentCommentException(comment.getParentId(), comment.getParentType());
            }

            if (isSubComment(parentComment)) {
                setParentComment(comment, parentComment);
                parentComment = retrieveParentComment(comment);
            }
            updateChildCountAndSave(parentComment);
        }
        return commentRepository.save(comment);
    }

    @Override
    public Comment getComment(long id) {
        return commentRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new CommentNotFoundException(id));
    }

    @Transactional
    @Override
    public void deleteComment(long id) {
        Comment comment = commentRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new CommentNotFoundException(id));
        if (hasNoAccessGranted(userService.getCurrentUser(), comment)) {
            throw new AttemptToUnauthorizedOperationException();
        }

        if (hasSubComments(comment)) {
            throw new AttemptToDeleteCommentWithSubComment(id);
        }

        if (isSubComment(comment)) {
            updateParentChildCountForDelete(comment);
        }
        comment.setDeleted(true);
        comment.setModifiedBy(userService.getCurrentUser());
        commentRepository.save(comment);
    }

    @Override
    public Comment updateComment(long id, Comment comment) {
        Comment commentToUpdate = commentRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new CommentNotFoundException(id));
        if (hasNoAccessGranted(userService.getCurrentUser(), commentToUpdate)) {
            throw new AttemptToUnauthorizedOperationException();
        }

        commentToUpdate.setModifiedBy(userService.getCurrentUser());
        commentToUpdate.setComment(comment.getComment());
        commentRepository.save(commentToUpdate);
        return commentToUpdate;
    }

    @Override
    public Page<Comment> getCommentsV2(long parentId, ParentType parentType, int start, int size) {
        checkIfParentIdExists(parentId, parentType);
        return commentRepository.findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(parentId,
                parentType, PageRequest.of(start, size, Sort.by(CREATED_AT).ascending()));
    }

    @Override
    public List<Comment> getSubComments(long parentCommentId) {
        commentRepository.findById(parentCommentId)
                .orElseThrow(() -> new CommentNotFoundException(parentCommentId));
        return commentRepository.findAllByParentCommentId(parentCommentId);
    }

    @Override
    public Page<Comment> getComments(long parentId, ParentType parentType, int start, int size) {
        checkIfParentIdExists(parentId, parentType);
        Page<Comment> commentPage = commentRepository.findAllByParentIdAndParentTypeAndDeletedFalse(parentId,
                parentType, PageRequest.of(start, size, Sort.by(CREATED_AT).ascending()));
        List<Comment> commentList =
                convertCommentMapToList(
                        establishParentChildRelationship(convertCommentListToCommentMap(commentPage.getContent())));
        return new PageImpl<>(commentList, commentPage.getPageable(), commentList.size());
    }

    private void setCommentProperties(Comment comment) {
        comment.setId(null);
        comment.setChildCount(0L);
        comment.setCreatedBy(userService.getCurrentUser());
        comment.setModifiedBy(userService.getCurrentUser());
        comment.setDeleted(false);
    }

    private void updateChildCountAndSave(Comment comment) {
        updateParentCommentChildCount(comment);
        commentRepository.save(comment);
    }

    private Comment retrieveParentComment(Comment comment) {
        return commentRepository.findByIdAndDeletedFalse(comment.getParentCommentId())
                .orElseThrow(() -> new CommentNotFoundException(comment.getParentCommentId()));
    }

    private void setParentComment(Comment comment, Comment parentComment) {
        comment.setParentCommentId(parentComment.getParentCommentId());
    }

    private void updateParentCommentChildCount(Comment parentCommentsParent) {
        parentCommentsParent.setChildCount(parentCommentsParent.getChildCount() + 1L);
    }

    private Map<Long, Comment> convertCommentListToCommentMap(List<Comment> commentList) {
        return commentList.stream().collect(LinkedHashMap::new, (map, item) -> {
            item.setSubCommentList(new ArrayList<>());
            map.put(item.getId(), item);
        }, Map::putAll);
    }

    private Map<Long, Comment> establishParentChildRelationship(Map<Long, Comment> commentMap) {
        commentMap.forEach((commentId, comment) -> {
            Long parentCommentId = comment.getParentCommentId();
            if (isSubComment(comment)) {
                Comment parentComment = commentMap.get(parentCommentId);
                parentComment.getSubCommentList().add(comment);
            }
        });
        return commentMap;
    }

    private List<Comment> convertCommentMapToList(Map<Long, Comment> commentMap) {
        return commentMap.values().stream().filter(comment -> !isSubComment(comment)).collect(Collectors.toList());
    }

    private boolean isSubComment(Comment comment) {
        return comment.getParentCommentId() != null;
    }

    private boolean hasSubComments(Comment comment) {
        return comment.getChildCount() > 0;
    }

    private void updateParentChildCountForDelete(Comment comment) {
        Comment parentComment = retrieveParentComment(comment);
        parentComment.setChildCount(parentComment.getChildCount() - 1);
        commentRepository.save(parentComment);
    }

    private boolean isParentIdOrTypeDoesNotMatchParentCommentsParentId(Comment comment, Comment parentComment) {
        return !(parentComment.getParentId().equals(comment.getParentId())
                && parentComment.getParentType().equals(comment.getParentType()));
    }

    private void checkIfParentIdExists(long parentId, ParentType parentType) {
        if (parentType.equals(ParentType.ARTICLE)) {
            articleRepository.findById(parentId)
                    .orElseThrow(() -> new ArticleNotFoundException(parentId));
        } else if (parentType.equals(ParentType.BLOG)) {
            blogRepository.findByIdAndDeletedFalse(parentId)
                    .orElseThrow(() -> new BlogNotFountException(parentId));
        } else {
            stickNoteRepository.findById(parentId)
                    .orElseThrow(() -> new StickNoteNotFoundException(parentId));
        }
    }

    private boolean hasNoAccessGranted(User user, Comment comment) {
        return !(comment.getCreatedBy().getUsername().equals(user.getUsername()));
    }
}
