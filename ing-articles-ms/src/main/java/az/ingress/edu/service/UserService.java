package az.ingress.edu.service;

import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.model.User;

public interface UserService {

    User saveUser(User user);

    User getCurrentUser();

    void userCreated(GetUserDto user);

    void userUpdated(UpdateUserDto user);
}
