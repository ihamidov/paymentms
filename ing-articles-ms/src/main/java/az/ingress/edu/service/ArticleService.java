package az.ingress.edu.service;

import az.ingress.edu.model.Article;
import az.ingress.edu.model.UserDetails;
import org.springframework.data.domain.Page;

public interface ArticleService {

    Article createArticle(Article article);

    Article updateArticle(long articleId, Article article, UserDetails userDetails);

    Article getArticle(long id);

    void deleteArticle(long id, UserDetails userDetails);

    Page<Article> findAll(int page, int size, String order, String direction);
}
