package az.ingress.edu.service;

import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import java.util.List;
import org.springframework.data.domain.Page;

public interface CommentService {

    Comment createComment(Comment commet);

    Comment getComment(long id);

    void deleteComment(long id);

    Page<Comment> getComments(long parentId, ParentType parentType, int start, int size);

    Comment updateComment(long id, Comment comment);

    Page<Comment> getCommentsV2(long parentId,ParentType parentType,int start,int size);

    List<Comment> getSubComments(long parentCommentId);
}
