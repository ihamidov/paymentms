package az.ingress.edu.controller;

import az.ingress.edu.dto.BlogFilterDto;
import az.ingress.edu.dto.CreateBlogDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.UpdateBlogDto;
import az.ingress.edu.model.Blog;
import az.ingress.edu.service.BlogService;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/blog")
@Validated
@AllArgsConstructor
public class BlogController {

    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final String DIRECT_DEFAULT_VALUE = "ASC";
    private static final String ID = "id";
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String BLOG_ID_MUST_BE_POSITIVE = "Blog id must be positive";
    public static final String PUBLISH_DATE = "publishDate";
    public static final String DESC = "DESC";

    private final BlogService blogService;

    @PostMapping
    public IdDto createArticle(@RequestBody @Validated CreateBlogDto createBlogDto) {

        return new IdDto(blogService.saveBlog(createBlogDto).getId());
    }

    @PutMapping("/{id}")
    public IdDto updateBlog(@PathVariable("id") @Positive(message = BLOG_ID_MUST_BE_POSITIVE) long id,
                            @Validated @RequestBody UpdateBlogDto updateBlogDto) {

        return new IdDto(blogService.updateBlog(updateBlogDto, id).getId());
    }

    @DeleteMapping("/{id}")
    public void deleteBlog(@PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        blogService.deleteBlogById(id);
    }

    @GetMapping("/{id}")
    public Blog getBlog(@PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        return blogService.findBlogById(id);
    }

    @GetMapping(path = "/list")
    public ListDto list(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER)
                        @PositiveOrZero(message = PAGE_INDEX_MUST_BE_POSITIVE) int pageNumber,
                        @RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
                        @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int pageSize,
                        @RequestParam(defaultValue = ID) String sortColumn,
                        @RequestParam(defaultValue = DIRECT_DEFAULT_VALUE) String sortDirection) {
        return blogService.getBlogs(PageDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortColumn(sortColumn)
                .sortDirection(sortDirection)
                .build());
    }

    @GetMapping(path = "/list/published")
    public ListDto listPublished(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER)
                                 @PositiveOrZero(message = PAGE_INDEX_MUST_BE_POSITIVE) int pageNumber,
                                 @RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
                                 @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int pageSize,
                                 @RequestParam(defaultValue = PUBLISH_DATE) String sortColumn,
                                 @RequestParam(defaultValue = DESC) String sortDirection,
                                 @ModelAttribute BlogFilterDto blogFilterDto) {
        return blogService.getPublishedBlogs(PageDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortColumn(sortColumn)
                .sortDirection(sortDirection)
                .build(), blogFilterDto);
    }
}
