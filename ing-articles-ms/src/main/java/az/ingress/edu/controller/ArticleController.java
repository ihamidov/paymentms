package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateArticleDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.UpdateArticleDto;
import az.ingress.edu.model.Article;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.ArticleService;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/articles")
@Validated
public class ArticleController {

    private static final String INDEX_MUST_BE_ZERO_OR_GREATER = "Page index must be 0 or greater";
    private static final String SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private final ArticleService articleService;

    @Autowired
    AccessToken accessToken;

    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    public IdDto createArticle(@Validated @RequestBody CreateArticleDto createArticleDto) {
        Article article = Article.builder()
                .description(createArticleDto.getDescription())
                .title(createArticleDto.getTitle())
                .createdBy(getUserDetails())
                .modifiedBy(getUserDetails())
                .build();
        Article createdArticle = articleService.createArticle(article);
        return new IdDto(createdArticle.getId());
    }

    @PutMapping("/{id}")
    public IdDto updateArticle(@PathVariable("id") @Positive(message = ARTICLE_ID_MUST_BE_POSITIVE) long id,
                               @Validated @RequestBody UpdateArticleDto updateArticleDto) {
        Article article = Article
                .builder()
                .title(updateArticleDto.getTitle())
                .description(updateArticleDto.getDescription())
                .modifiedBy(getUserDetails())
                .build();
        Article updatedArticle = articleService.updateArticle(id, article, getUserDetails());
        return new IdDto(updatedArticle.getId());
    }

    @GetMapping("/{id}")
    public Article getArticle(@PathVariable @Positive(message = ARTICLE_ID_MUST_BE_POSITIVE) long id) {
        return articleService.getArticle(id);
    }

    @DeleteMapping("/{id}")
    public void deleteArticle(@PathVariable @Positive(message = ARTICLE_ID_MUST_BE_POSITIVE) long id) {
        articleService.deleteArticle(id, getUserDetails());
    }

    @GetMapping
    public ListDto getAllArticles(@RequestParam(name = "page", defaultValue = "0")
                                  @PositiveOrZero(message = INDEX_MUST_BE_ZERO_OR_GREATER) int page,
                                  @RequestParam(name = "size", defaultValue = "10")
                                  @Positive(message = SIZE_MUST_BE_POSITIVE) int size,
                                  @RequestParam(name = "order", defaultValue = "id") String order,
                                  @RequestParam(name = "direction", defaultValue = "desc") String direction) {
        Page<Article> listArticles = articleService.findAll(page, size, order ,direction);
        return new ListDto(listArticles.getContent(), listArticles.getTotalPages(), listArticles.getTotalElements());
    }

    private UserDetails getUserDetails() {
        return new UserDetails(accessToken.getPreferredUsername(), accessToken.getName(),
                accessToken.getRealmAccess().getRoles());
    }
}
