package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateStickNoteDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateStickNoteDto;
import az.ingress.edu.model.StickNote;
import az.ingress.edu.model.UserDetails;
import az.ingress.edu.service.StickNoteService;
import java.util.List;
import javax.validation.constraints.Positive;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("/sticknote")
public class StickNoteController {

    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String STICKNOTE_ID_MUST_BE_POSITIVE = "StickNote id must be positive";
    private final StickNoteService stickNoteService;

    @Autowired
    AccessToken accessToken;

    public StickNoteController(StickNoteService stickNoteService) {
        this.stickNoteService = stickNoteService;
    }

    @PostMapping
    public IdDto createStickNote(@Validated @RequestBody CreateStickNoteDto createStickNoteDto) {
        StickNote stickNote = StickNote.builder()
                .articleId(createStickNoteDto.getArticleId())
                .title(createStickNoteDto.getTitle())
                .xCoordinate(createStickNoteDto.getXCoordinate())
                .yCoordinate(createStickNoteDto.getYCoordinate())
                .createdBy(getUserDetails())
                .modifiedBy(getUserDetails())
                .build();

        StickNote createdStickNote = stickNoteService.createStickNote(stickNote);
        return new IdDto(createdStickNote.getId());
    }

    @GetMapping("/{id}")
    public StickNote getStickNote(@Positive(message = STICKNOTE_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return stickNoteService.getStickNote(id);
    }

    @PutMapping("/{id}")
    public IdDto updateStickNote(@Positive(message = STICKNOTE_ID_MUST_BE_POSITIVE) @PathVariable long id,
                                 @Validated @RequestBody UpdateStickNoteDto updateStickNoteDto) {
        StickNote stickNote = StickNote.builder()
                .title(updateStickNoteDto.getTitle())
                .xCoordinate(updateStickNoteDto.getXCoordinate())
                .yCoordinate(updateStickNoteDto.getYCoordinate())
                .modifiedBy(getUserDetails())
                .build();

        stickNoteService.updateStickNote(id, stickNote,getUserDetails());
        return new IdDto(id);
    }

    @GetMapping("/list/article/{articleId}")
    public List<StickNote> getStickNotes(@Positive(message = ARTICLE_ID_MUST_BE_POSITIVE)
                                             @PathVariable long articleId) {
        return stickNoteService.getStickNotes(articleId);
    }

    @DeleteMapping("/{id}")
    public void deleteStickNote(@PathVariable @Positive(message = STICKNOTE_ID_MUST_BE_POSITIVE) long id) {
        stickNoteService.deleteStickNote(id,getUserDetails());
    }

    private UserDetails getUserDetails() {
        return new UserDetails(accessToken.getPreferredUsername(),
                accessToken.getName(),
                accessToken.getRealmAccess().getRoles());
    }
}
