package az.ingress.edu.controller;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.TagDto;
import az.ingress.edu.model.Tag;
import az.ingress.edu.service.impl.TagServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("Tag Api")
@RestController
@RequestMapping("/tag")
@AllArgsConstructor
public class TagController {

    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String DIRECT_DEFAULT_VALUE = "ASC";
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";
    private static final String ID = "id";

    private final TagServiceImpl tagServiceImpl;


    @GetMapping("/{id}")
    @ApiOperation("Get course by id")
    public Tag findTagById(
            @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        return tagServiceImpl.findById(id);
    }

    @ApiOperation("Create new tag")
    @PostMapping
    public IdDto createTag(@Valid @RequestBody TagDto dto) {
        return new IdDto(tagServiceImpl.add(dto).getId());
    }

    @PutMapping("/{id}")
    @ApiOperation("Update tag")
    public IdDto update(@Validated @RequestBody TagDto tagDto,
                        @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        tagServiceImpl.findById(id);
        return new IdDto(tagServiceImpl.update(tagDto, id).getId());
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete tag")
    public void delete(@PathVariable @Positive
            (message = ID_MUST_BE_POSITIVE) long id) {
        tagServiceImpl.findById(id);
        tagServiceImpl.deleteById(id);
    }

    @GetMapping(path = "/list")
    @ApiOperation("Find all tags by page")
    public Page<Tag> list(
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) @PositiveOrZero(message = PAGE_SIZE_MUST_BE_POSITIVE)
                    int pageNumber,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE)
                    int pageSize,
            @RequestParam(defaultValue = ID) String sortColumn,
            @RequestParam(defaultValue = DIRECT_DEFAULT_VALUE) String sortDirection) {
        return tagServiceImpl.list(PageDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortColumn(sortColumn)
                .sortDirection(sortDirection)
                .build());
    }
}
