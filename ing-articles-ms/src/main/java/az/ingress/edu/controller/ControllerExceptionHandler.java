package az.ingress.edu.controller;

import az.ingress.edu.dto.ErrorDto;
import az.ingress.edu.exception.ArticleNotFoundException;
import az.ingress.edu.exception.AttemptToDeleteCommentWithSubComment;
import az.ingress.edu.exception.AttemptToUnauthorizedOperationException;
import az.ingress.edu.exception.BlogNotFountException;
import az.ingress.edu.exception.CommentNotFoundException;
import az.ingress.edu.exception.ParentNotSameWithParentCommentException;
import az.ingress.edu.exception.StickNoteNotFoundException;
import az.ingress.edu.exception.TagAlreadyExistException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.exception.UserNotFoundException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import java.util.Calendar;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ControllerExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDto> handleMethodArgumentTypeMismatch(MethodArgumentNotValidException ex) {
        ObjectError fieldError = ex.getBindingResult().getAllErrors().get(0);
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                fieldError.getDefaultMessage(), fieldError.getDefaultMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDto> handleConstraintViolation(ConstraintViolationException ex) {
        ConstraintViolation violation = ex.getConstraintViolations().iterator().next();
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                violation.getMessage(), violation.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<ErrorDto> handleInvalidFormatException(InvalidFormatException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(ArticleNotFoundException.class)
    public ResponseEntity<ErrorDto> handleArticleNotFoundException(ArticleNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<ErrorDto>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CommentNotFoundException.class)
    public ResponseEntity<ErrorDto> handleCommentNotFoundException(CommentNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<ErrorDto> handleNumberFormatException(NumberFormatException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AttemptToDeleteCommentWithSubComment.class)
    public ResponseEntity<ErrorDto> handleAttemptToDeleteCommentWithChild(AttemptToDeleteCommentWithSubComment ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.FORBIDDEN.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorDto> handleIllegalArgumentException(IllegalArgumentException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<ErrorDto> handlePropertyReferenceException(PropertyReferenceException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(ParentNotSameWithParentCommentException.class)
    public ResponseEntity<ErrorDto> handleArticleIdNotMatchParentsArticleIdException(
            ParentNotSameWithParentCommentException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StickNoteNotFoundException.class)
    public ResponseEntity<ErrorDto> handleStickNoteNotFoundException(StickNoteNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AttemptToUnauthorizedOperationException.class)
    public ResponseEntity<ErrorDto> handleAttemptToUnauthorizedOperationException(
            AttemptToUnauthorizedOperationException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.FORBIDDEN.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UnauthorizedOperationException.class)
    public ResponseEntity<ErrorDto> handleUnauthorizedOperationException(
            UnauthorizedOperationException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.FORBIDDEN.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(BlogNotFountException.class)
    public ResponseEntity<ErrorDto> handleBlogNotFountException(BlogNotFountException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TagNotFoundException.class)
    public ResponseEntity<ErrorDto> handleTagNotFoundException(TagNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorDto> handleTagNotFoundException(UserNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TagAlreadyExistException.class)
    public ResponseEntity<ErrorDto> handleTagAlreadyExistException(TagAlreadyExistException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }
}
