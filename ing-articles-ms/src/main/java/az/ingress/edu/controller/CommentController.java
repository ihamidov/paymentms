package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateCommentDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.UpdateCommentDto;
import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import az.ingress.edu.service.CommentService;
import java.util.List;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comments")
@Validated
public class CommentController {

    private static final String COMMENT_ID_MUST_BE_POSITIVE = "Comment id must be positive";
    private static final String INDEX_MUST_NOT_BE_NEGATIVE = "Page index must be zero or greater";
    private static final String SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String ARTICLE_ID_MUST_BE_POSITIVE = "Article id must be positive";
    private static final String STICKNOTE_ID_MUST_BE_POSITIVE = "StickNote id must be positive";
    private static final String BLOG_ID_MUST_BE_POSITIVE = "Blog id must be positive";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String DEFAULT_PAGE_INDEX = "0";
    private static final String DEFAULT_PAGE_SIZE = "100";
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    public IdDto createComment(@Validated @RequestBody CreateCommentDto createCommentDto) {
        Comment comment = Comment
                .builder()
                .parentId(createCommentDto.getParentId())
                .parentType(createCommentDto.getParentType())
                .parentCommentId(createCommentDto.getParentCommentId())
                .comment(createCommentDto.getComment())
                .parentCommentId(createCommentDto.getParentCommentId())
                .build();
        Comment createdComment = commentService.createComment(comment);
        return new IdDto(createdComment.getId());
    }

    @GetMapping("/{id}")
    public Comment getComment(@PathVariable @Positive(message = COMMENT_ID_MUST_BE_POSITIVE) long id) {
        return commentService.getComment(id);
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable @Positive(message = COMMENT_ID_MUST_BE_POSITIVE) long id) {
        commentService.deleteComment(id);
    }

    @PutMapping("/{id}")
    public IdDto updateComment(@PathVariable @Positive(message = COMMENT_ID_MUST_BE_POSITIVE) long id,
                               @Validated @RequestBody UpdateCommentDto updateCommentDto) {
        Comment comment = Comment
                .builder()
                .comment(updateCommentDto.getComment())
                .comment(updateCommentDto.getComment())
                .build();
        commentService.updateComment(id, comment);
        return new IdDto(id);
    }

    @GetMapping("/list/article/{id}")
    public ListDto getCommentsForArticle(
            @PathVariable("id") @Positive(message = ARTICLE_ID_MUST_BE_POSITIVE) long articleId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getComments(articleId, ParentType.ARTICLE, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("/list/sticknote/{id}")
    public ListDto getCommentsForStickNote(
            @PathVariable("id") @Positive(message = STICKNOTE_ID_MUST_BE_POSITIVE) long stickNoteId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getComments(stickNoteId, ParentType.STICKNOTE, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("/list/blog/{id}")
    public ListDto getCommentsForBlog(
            @PathVariable("id") @Positive(message = BLOG_ID_MUST_BE_POSITIVE) long stickNoteId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getComments(stickNoteId, ParentType.BLOG, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("/list/article/v2/{id}")
    public ListDto getCommentsForArticleV2(
            @PathVariable("id") @Positive(message = ARTICLE_ID_MUST_BE_POSITIVE) long articleId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getCommentsV2(articleId, ParentType.ARTICLE, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("/list/sticknote/v2/{id}")
    public ListDto getCommentsForStickNoteV2(
            @PathVariable("id") @Positive(message = STICKNOTE_ID_MUST_BE_POSITIVE) long stickNoteId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getCommentsV2(stickNoteId, ParentType.STICKNOTE, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("/list/blog/v2/{id}")
    public ListDto getCommentsForBlogV2(
            @PathVariable("id") @Positive(message = BLOG_ID_MUST_BE_POSITIVE) long stickNoteId,
            @PositiveOrZero(message = INDEX_MUST_NOT_BE_NEGATIVE)
            @RequestParam(value = PAGE, required = false, defaultValue = DEFAULT_PAGE_INDEX) int page,
            @Positive(message = SIZE_MUST_BE_POSITIVE)
            @RequestParam(value = SIZE, required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {
        Page<Comment> commentPage = commentService.getCommentsV2(stickNoteId, ParentType.BLOG, page, size);
        return new ListDto(commentPage.getContent(),
                commentPage.getTotalPages(), commentPage.getTotalElements());
    }

    @GetMapping("list/subcomment/{id}")
    public List<Comment> getSubComments(@PathVariable("id") @Positive(message = COMMENT_ID_MUST_BE_POSITIVE)
                                                long parentCommentId) {
        return commentService.getSubComments(parentCommentId);
    }
}
