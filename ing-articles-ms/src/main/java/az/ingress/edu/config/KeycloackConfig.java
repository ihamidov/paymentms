package az.ingress.edu.config;

import az.ingress.edu.exception.IngressAccessDeniedHandler;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Profile("!test")
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class KeycloackConfig extends KeycloakWebSecurityConfigurerAdapter {

    public static final String COMMENTS = "/comments/**";
    public static final String BLOGS = "/blog/**";
    public static final String ARTICLES = "/articles/**";
    public static final String STICKNOTE = "/sticknote/**";
    public static final String ROLE_USER = "USER";

    @Autowired
    public void configureGlobal(
            AuthenticationManagerBuilder auth) {

        KeycloakAuthenticationProvider keycloakAuthenticationProvider
                = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(
                new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(
                new SessionRegistryImpl());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/blog/list")
                .hasRole(ROLE_USER)
                .antMatchers(HttpMethod.POST, COMMENTS, BLOGS, ARTICLES, STICKNOTE)
                .hasRole(ROLE_USER)
                .antMatchers(HttpMethod.DELETE, COMMENTS, BLOGS, ARTICLES, STICKNOTE)
                .hasRole(ROLE_USER)
                .antMatchers(HttpMethod.PUT, COMMENTS, BLOGS, ARTICLES, STICKNOTE)
                .hasRole(ROLE_USER)
                .antMatchers(HttpMethod.GET, COMMENTS, BLOGS, ARTICLES, STICKNOTE)
                .permitAll()
                .and().csrf().disable();
    }

    @Bean
    public IngressAccessDeniedHandler accessDeniedHandler() {
        return new IngressAccessDeniedHandler();
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AccessToken getAccessToken() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return ((KeycloakAuthenticationToken) request.getUserPrincipal())
                .getAccount().getKeycloakSecurityContext().getToken();
    }
}
