package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("timestamp-configuration")
@EnableConfigurationProperties(TimestampConfiguration.class)
public class TimestampConfiguration {

    private String timestampFormat;
    private String timeZone;
}
