package az.ingress.edu.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateSerializer extends JsonSerializer<Instant> {

    private final TimestampConfiguration timestampConfiguration;

    public DateSerializer(TimestampConfiguration timestampConfiguration) {
        this.timestampConfiguration = timestampConfiguration;
    }

    @Override
    public void serialize(final Instant instant, final JsonGenerator jsonGenerator,
                          final SerializerProvider serializerProvider) throws IOException {
        final String serializedInstant = DateTimeFormatter
                .ofPattern(timestampConfiguration.getTimestampFormat())
                .withZone(ZoneId.of(timestampConfiguration.getTimeZone())).format(instant);
        jsonGenerator.writeString(serializedInstant);
    }
}
