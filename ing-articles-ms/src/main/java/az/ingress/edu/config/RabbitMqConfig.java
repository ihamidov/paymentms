package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("rabbit")
@EnableConfigurationProperties(RabbitMqConfig.class)
@Setter
@Getter
public class RabbitMqConfig {

    private String articleMsUserCreateRoutingKey;
    private String articleMsExchange;
    private String articleMsUserCreateQueue;
    private String articleMsUserUpdateQueue;
    private String articleMsUserUpdateRoutingKey;

    @Bean
    public TopicExchange articleMsExchange() {
        return new TopicExchange(articleMsExchange);
    }

    @Bean
    public Queue articleMsUserCreateQueue() {
        return new Queue(articleMsUserCreateQueue);
    }

    @Bean
    public Binding articleMsUserCreateBinding() {
        return BindingBuilder.bind(articleMsUserCreateQueue())
                .to(articleMsExchange()).with(articleMsUserCreateRoutingKey);
    }

    @Bean
    public Queue articleMsUserUpdateQueue() {
        return new Queue(articleMsUserUpdateQueue);
    }

    @Bean
    public Binding articleMsUserUpdateBinding() {
        return BindingBuilder.bind(articleMsUserUpdateQueue())
                .to(articleMsExchange()).with(articleMsUserUpdateRoutingKey);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
