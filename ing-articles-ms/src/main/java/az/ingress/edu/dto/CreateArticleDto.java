package az.ingress.edu.dto;

import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateArticleDto {

    @NotEmpty(message = "Title can't be empty")
    private String title;

    @NotEmpty(message = "Description can't be empty")
    private String description;
}
