package az.ingress.edu.dto;

import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.model.Tag;
import az.ingress.edu.model.User;
import java.time.Instant;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListBlogDto {

    private long id;
    private String title;
    private String category;
    private String content;
    private List<Tag> tags;
    private Instant publishDate;
    private String photo;
    private BlogStatus status;
    private User createdBy;

    public static ListBlogDto toListBlogDto(Blog blog) {
        return ListBlogDto
                .builder()
                .id(blog.getId())
                .title(blog.getTitle())
                .category(blog.getCategory())
                .tags(blog.getTags())
                .content(blog.getContent())
                .publishDate(blog.getPublishDate())
                .photo(blog.getPhoto())
                .status(blog.getStatus())
                .createdBy(blog.getCreatedBy())
                .build();
    }
}
