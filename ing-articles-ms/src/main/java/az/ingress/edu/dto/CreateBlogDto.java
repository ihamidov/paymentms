package az.ingress.edu.dto;

import java.util.List;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateBlogDto {

    @NotEmpty(message = "Title can't be empty")
    private String title;

    @NotEmpty(message = "Category can't be empty")
    private String category;

    @Lob
    private String content;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;

    private List<IdDto> tags;
}
