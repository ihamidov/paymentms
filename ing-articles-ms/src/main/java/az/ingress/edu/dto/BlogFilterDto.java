package az.ingress.edu.dto;

import lombok.Data;

@Data
public class BlogFilterDto {

    private String title;
    private String category;
    private long tag;
}
