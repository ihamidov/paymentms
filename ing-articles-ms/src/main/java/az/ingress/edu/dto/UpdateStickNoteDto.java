package az.ingress.edu.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateStickNoteDto {

    @NotEmpty(message = "Title can't be empty")
    private String title;

    @PositiveOrZero(message = "Coordinate must be zero or positive number")
    @JsonProperty("xCoordinate")
    private long xCoordinate;

    @PositiveOrZero(message = "Coordinate must be zero or positive number")
    @JsonProperty("yCoordinate")
    private long yCoordinate;
}
