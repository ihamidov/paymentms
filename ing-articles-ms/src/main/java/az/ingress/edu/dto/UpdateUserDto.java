package az.ingress.edu.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserDto {

    private String username;

    private String firstname;

    private String lastname;

    private String photo;

    private UserContactInfoDto contactDetails;

    private String description;
}
