package az.ingress.edu.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetUserDto {

    private long id;

    private String username;

    private String firstname;

    private String lastname;

    private UserContactInfoDto contactDetails;

    private String photo;

    private String description;
}
