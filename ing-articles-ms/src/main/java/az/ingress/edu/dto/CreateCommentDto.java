package az.ingress.edu.dto;

import az.ingress.edu.model.ParentType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateCommentDto {

    @NotNull(message = "Parent id can't be empty")
    @Positive(message = "Parent id must be positive")
    private Long parentId;

    @NotNull(message = "Parent type can't be empty")
    @Enumerated(EnumType.STRING)
    private ParentType parentType;

    @Positive(message = "Parent comment id must be positive")
    private Long parentCommentId;

    @NotEmpty(message = "Comment can't be empty")
    private String comment;
}
