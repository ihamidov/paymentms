package az.ingress.edu.model;

public enum ParentType {
    ARTICLE, STICKNOTE, BLOG
}
