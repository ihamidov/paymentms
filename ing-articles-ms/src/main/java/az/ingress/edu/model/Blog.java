package az.ingress.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners({AuditingEntityListener.class})
public class Blog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Title can't be empty")
    private String title;

    @NotEmpty(message = "Category can't be empty")
    private String category;

    @Lob
    private String content;

    private Instant publishDate;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;

    @NotNull(message = "Status can not be empty")
    @Enumerated(EnumType.STRING)
    BlogStatus status;

    @JsonIgnore
    private boolean deleted;

    @ManyToMany
    private List<Tag> tags;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Calendar createdAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    private User createdBy;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar modifiedAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "modified_by")
    private User modifiedBy;
}
