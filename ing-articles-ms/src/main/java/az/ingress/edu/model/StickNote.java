package az.ingress.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@EntityListeners(AuditingEntityListener.class)
public class StickNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Title can't be empty")
    private String title;

    @NotNull(message = "Article id can't be empty")
    @Positive(message = "Article id must be positive")
    private Long articleId;

    @PositiveOrZero(message = "Coordinate must be zero or positive number")
    @JsonProperty("xCoordinate")
    private long xCoordinate;

    @PositiveOrZero(message = "Coordinate must be zero or positive number")
    @JsonProperty("yCoordinate")
    private long yCoordinate;

    @CreatedDate
    @Column(updatable = false)
    private Instant createdAt;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "username", column = @Column(name = "created_by")),
            @AttributeOverride(name = "displayName", column = @Column(name = "created_by_display_name"))})
    private UserDetails createdBy;

    @LastModifiedDate
    private Instant modifiedAt;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "username", column = @Column(name = "modified_by")),
            @AttributeOverride(name = "displayName", column = @Column(name = "modified_by_display_name"))})
    private UserDetails modifiedBy;
}
