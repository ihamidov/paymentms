package az.ingress.edu.model;

public enum BlogStatus {
    DRAFT, PUBLISHED
}

