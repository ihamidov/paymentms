package az.ingress.edu.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {

    @Id
    @NotEmpty(message = "Username can't be empty")
    @NotNull
    private String username;

    @NotEmpty(message = "Firstname can't be empty")
    @NotNull
    private String firstname;

    @NotEmpty(message = "Lastname can't be empty")
    @NotNull
    private String lastname;

    @NotEmpty(message = "Email can't be empty")
    @NotNull
    private String email;

    private String phone;

    private String photo;
}
