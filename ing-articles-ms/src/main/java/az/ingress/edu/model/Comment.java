package az.ingress.edu.model;

import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@EntityListeners({AuditingEntityListener.class})
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Parent id can't be empty")
    @Positive(message = "Parent id must be positive")
    private Long parentId;

    @NotNull(message = "Parent type can't be empty")
    @Enumerated(EnumType.STRING)
    private ParentType parentType;

    @Positive(message = "Parent comment id must be positive")
    private Long parentCommentId;

    @NotEmpty(message = "Comment can't be empty")
    private String comment;

    private Long childCount;

    private boolean deleted;

    @Transient
    private List<Comment> subCommentList;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Calendar createdAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    private User createdBy;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar modifiedAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "modified_by")
    private User modifiedBy;
}
