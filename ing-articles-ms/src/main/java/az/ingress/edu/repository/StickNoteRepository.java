package az.ingress.edu.repository;

import az.ingress.edu.model.StickNote;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StickNoteRepository extends CrudRepository<StickNote,Long> {
    List<StickNote> findAllByArticleId(long articleId);
}
