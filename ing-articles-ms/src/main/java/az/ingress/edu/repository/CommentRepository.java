package az.ingress.edu.repository;

import az.ingress.edu.model.Comment;
import az.ingress.edu.model.ParentType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long>, PagingAndSortingRepository<Comment, Long> {

    Page<Comment> findAllByParentIdAndParentTypeAndDeletedFalse(long parentId, ParentType parentType,
                                                                Pageable pageable);

    Page<Comment> findAllByParentIdAndParentTypeAndParentCommentIdNullAndDeletedFalse(long parentId,
                                                                       ParentType parentType, Pageable pageable);

    List<Comment> findAllByParentCommentId(long parentCommentId);

    Optional<Comment> findByIdAndDeletedFalse(long id);
}
