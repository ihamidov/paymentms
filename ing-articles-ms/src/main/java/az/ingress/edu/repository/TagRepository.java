package az.ingress.edu.repository;

import az.ingress.edu.model.Tag;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends PagingAndSortingRepository<Tag, Long> {

    Optional<Tag> findByIdAndDeletedFalse(Long id);

    Page<Tag> findAllByDeletedFalse(Pageable pageable);

    Optional<Tag> getByName(@Param("name") String name);

}
