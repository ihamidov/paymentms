package az.ingress.edu.repository;

import az.ingress.edu.dto.BlogFilterDto;
import az.ingress.edu.model.Blog;
import az.ingress.edu.model.BlogStatus;
import az.ingress.edu.model.Tag;
import az.ingress.edu.model.User;
import java.util.Arrays;
import java.util.Optional;
import javax.persistence.criteria.Join;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends CrudRepository<Blog, Long>, PagingAndSortingRepository<Blog, Long>,
        JpaSpecificationExecutor<Blog> {

    Optional<Blog> findByIdAndDeletedFalse(long id);

    Page<Blog> findAllByDeletedFalse(Pageable pageable);

    default Page<Blog> findAllByUsername(Pageable pageable, String username) {
        return findAll((root, query, cb) -> {
            Join<Blog, User> joinUser = root.join("createdBy");
            return cb.and(cb.equal(root.get("deleted"), false),
                    cb.equal(joinUser.get("username"), username));
        }, pageable);
    }

    default Page<Blog> findAll(Pageable pageable, BlogFilterDto blogFilter) {
        return findAll((root, query, cb) -> {
            return cb.and(cb.equal(root.get("deleted"), false),
                    cb.equal(root.get("status"), BlogStatus.PUBLISHED),
                    blogFilter.getTitle() != null && !blogFilter.getTitle().trim().equals("")
                            ? cb.like(cb.lower(root.get("title")), "%" + blogFilter.getTitle()
                            .toLowerCase() + "%") : cb.conjunction(),
                    blogFilter.getCategory() != null && !blogFilter.getCategory().trim().equals("")
                            ? cb.equal(root.get("category"), blogFilter.getCategory()) : cb.conjunction(),
                    blogFilter.getTag() != 0 ? root.join("tags")
                            .in(Arrays.asList(Tag.builder().id(blogFilter.getTag()).build())) : cb.conjunction());
        }, pageable);
    }
}
