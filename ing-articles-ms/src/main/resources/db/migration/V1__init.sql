create table if not exists comment
(
    id                bigint auto_increment primary key,
    child_count       bigint       null,
    comment           varchar(255) null,
    created_at        datetime     not null,
    deleted           bit          not null,
    modified_at       datetime     null,
    parent_comment_id bigint       null,
    parent_id         bigint       not null,
    parent_type       varchar(255) not null,
    created_by        varchar(255) not null,
    modified_by       varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table if not exists article
(
    id                       bigint auto_increment primary key,
    created_at               datetime     null,
    created_by_display_name  varchar(255) null,
    created_by               varchar(255) null,
    description              varchar(255) null,
    modified_at              datetime     null,
    modified_by_display_name varchar(255) null,
    modified_by              varchar(255) null,
    title                    varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
create table if not exists stick_note
(
    id                       bigint auto_increment primary key,
    article_id               bigint       null,
    title                    varchar(255) null,
    x_coordinate             bigint       not null,
    y_coordinate             bigint       not null,
    created_at               datetime     null,
    created_by               varchar(255) null,
    created_by_display_name  varchar(255) null,
    modified_at              datetime     null,
    modified_by              varchar(255) null,
    modified_by_display_name varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table if not exists user
(
    username  varchar(255) not null primary key,
    email     varchar(255) not null,
    firstname varchar(255) not null,
    lastname  varchar(255) not null,
    phone     varchar(255) null,
    photo_id  bigint       null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table if not exists blog
(
    id           bigint auto_increment primary key,
    content      longtext     null,
    created_at   datetime     not null,
    deleted      bit          not null,
    modified_at  datetime     null,
    photo_id     bigint       null,
    publish_date datetime     null,
    status       varchar(255) not null,
    title        varchar(255) null,
    category     varchar(255) null,
    created_by   varchar(255) not null,
    modified_by  varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table tag
(
    id bigint auto_increment
        primary key,
    deleted bit not null,
    name    varchar(255) not null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table blog_tags
(
    blog_id bigint not null,
    tags_id   bigint not null,
    foreign key (blog_id) references blog (id),
    foreign key (tags_id) references tag (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
