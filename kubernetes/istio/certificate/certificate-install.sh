#!/bin/bash
#Create secret
kubectl create -n istio-system secret tls istio-ingressgateway-certs \
   --key ingress_academy.key \
   --cert ingress_academy.crt

