1. Install Istio
Source : https://konghq.com/blog/kong-istio-setting-service-mesh-kubernetes-kiali-observability/
Very helpful demo: https://www.youtube.com/watch?v=VzQetbvXCAw

curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.2.4 sh -
cd istio-1.2.4
export PATH=$PWD/bin:$PATH
for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done
kubectl apply -f http://bit.ly/istiomtls


2. Install certificates for https, UPDATE private key contents
navigate tp certificates folder, and run certificates-install.sh, use certificate-install-check.sh
to verify if they were created

3. Apply ing-lms-gateway and virtual service
