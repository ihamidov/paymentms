package com.ibrahim.az.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibrahim.az.dto.InvoiceDto;
import com.ibrahim.az.exception.InvoiceNotFoundException;
import com.ibrahim.az.exception.InvoicePositiveException;
import com.ibrahim.az.model.Invoice;
import com.ibrahim.az.service.InvoiceService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(InvoiceController.class)
public class InvoiceControllerTest {
    private static final String INVOICE_API = "/account/invoice/";
    private static final long ID =1;
    private static final int AMOUNTPERUNIT=100;
    private static final LocalDateTime DUEDATE=LocalDateTime.now();
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String TIMESTAMP = "$.timestamp";
    private static final String INVOICE_NOT_FOUND = "Invoice ID not found";
    private static final String INVOICE_MUST_BE_POSSITIVE = "Invoice ID must be positive";

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private InvoiceService invoiceService;

    @Autowired
    private ObjectMapper objectMapper;
    private Invoice invoice;
    private InvoiceDto invoiceDto;

    @Before
    public void setUp() {
        List clientList = new ArrayList<>();
        invoiceDto = InvoiceDto.builder().client(clientList)
                .amountPerUnit(AMOUNTPERUNIT).quanity(100)
                .dueDate(DUEDATE).discount(30)
                .description("string").build();
        invoice = Invoice.builder().id(1L)
                .clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.now())
                .discount(30).subtotal(10000).total(7000)
                .status("PAID").dueDate(DUEDATE)
                .description("string")
                .build();
    }

    @Test
    public  void getInvoiceIsOk() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(INVOICE_API+ID))
            .andExpect(status().isOk());
    }

    @Test
    public void getInvoiceNotFoundException() throws  Exception{
        when(invoiceService.getInvoiceById(ID)).thenThrow(new InvoiceNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.get(INVOICE_API+ID))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(404))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty());
    }

    @Test
    public void getInvoiceNotPossitiveException() throws Exception {
        when(invoiceService.getInvoiceById(-2)).thenThrow(new InvoicePositiveException());
        mockMvc.perform(MockMvcRequestBuilders.get(INVOICE_API + -2))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty());
    }

    @Test
    public void createInvoiceIsOk() throws Exception {
        when(invoiceService.postInvoice(invoiceDto)).thenReturn(invoice);
        mockMvc.perform(MockMvcRequestBuilders.post(INVOICE_API)
                .content(asJsonString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
    @Test
    public void updateInvoiveIsOk() throws Exception {
       // when(invoiceService.getInvoiceById(anyLong())).thenReturn(invoice);
        when(invoiceService.updateInvoice(1,invoiceDto)).thenReturn(invoice);

        mockMvc.perform(MockMvcRequestBuilders.put(INVOICE_API+1)
                .content(asJsonString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk()); }

    @Test
    public void givenInvoiceNotExistWhenUpdateInvoiceThenNotFound() throws Exception {
        when(invoiceService.updateInvoice(ID, invoiceDto)).thenThrow(new InvoiceNotFoundException());

        mockMvc.perform(MockMvcRequestBuilders.put(INVOICE_API + ID)
                .content(objectMapper.writeValueAsString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_NOT_FOUND));
    }

    @Test
    public void updateInvoiveNotFound() throws Exception {
        Invoice invoice= new Invoice();
        InvoiceDto invoiceDto= new InvoiceDto();
        //when(invoiceService.getInvoiceById(anyLong())).thenReturn(invoice);
        when(invoiceService.updateInvoice(-1,invoiceDto)).thenThrow(new InvoicePositiveException());
        mockMvc.perform(MockMvcRequestBuilders.put(INVOICE_API+-1)
                .content(asJsonString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE));
    }

    @Test
    public void deleteInvoiceIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(INVOICE_API+1))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteInvoiceNotFound() throws Exception {
        doThrow(new InvoiceNotFoundException()).when(invoiceService).deleteInvoiceById(anyLong());
     //   when(invoiceService.deleteInvoiceById(111).thenThrow(new InvoiceNotFoundException());// olmur?why
        mockMvc.perform(MockMvcRequestBuilders.delete(INVOICE_API+111)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(404))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(TIMESTAMP).isNotEmpty());
    }

    @Test
    public void getInvoiceByListIsOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(INVOICE_API))
                .andExpect(status().isOk());
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
