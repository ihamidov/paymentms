package com.ibrahim.az.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import com.ibrahim.az.dto.InvoiceDto;
import com.ibrahim.az.exception.InvoiceNotFoundException;
import com.ibrahim.az.model.Invoice;
import com.ibrahim.az.repository.InvoiceRepository;
import com.ibrahim.az.service.impl.InvoiceServiceImpl;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceImplTest {
    private static final long ID=1;
    private static final int AMOUNTPERUNIT=100;
    private static final LocalDateTime DUEDATE=LocalDateTime.now();
    private static final String INVOICE_NOT_FOUND = "Invoice ID not found";

    @Mock
    private InvoiceRepository invoiceRepository;
    @InjectMocks
    private InvoiceServiceImpl invoiceServiceImpl;
    private Invoice invoice;
    private InvoiceDto invoiceDto;

    @Before
    public void setUP(){
        List idDtos=new ArrayList<>();
        idDtos.add(1L);
        idDtos.add(2L);
         invoice=Invoice.builder().id(ID)
                 .clientId(1)
                 .amountPerUnit(AMOUNTPERUNIT)
                 .quantity(10)
                 .discount(2)
                 .subtotal(2)
                 .total(1000)
                 .status("s")
                 .dueDate(DUEDATE)
                 .description("a")
                 .build();
         invoiceDto=InvoiceDto.builder()
                .cliendId(2l)
                .client(idDtos)
                .description("5")
                .discount(2)
                .dueDate(DUEDATE)
                .quanity(2)
                .status("2")
                .amountPerUnit(AMOUNTPERUNIT).build();
    }
    @Test
    public void findByIdIsOk(){
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.of(invoice));
        Invoice invoice=invoiceServiceImpl.getInvoiceById(ID);
        assertThat(invoiceDto.getAmountPerUnit()).isEqualTo(AMOUNTPERUNIT);
    }

    @Test
    public void getInvoiceByIdNotFoundException() {
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> invoiceServiceImpl.getInvoiceById(ID))
                .isInstanceOf(InvoiceNotFoundException.class)
                .hasMessage(INVOICE_NOT_FOUND);
    }

    @Test
    public void updateInvoiceIsOk() {
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.of(invoice));
        when(invoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
        Invoice invoice = invoiceServiceImpl.updateInvoice(anyLong(), invoiceDto);
        assertThat(invoice.getAmountPerUnit()).isEqualTo(invoice.getAmountPerUnit());
    }

    @Test
    public void deleteInvoiceIsOk() {
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.of(invoice));
      //  Invoice invoice=invoiceServiceImpl.getInvoiceById(ID);
       invoiceServiceImpl.deleteInvoiceById(ID);
    }

    @Test
    public void postInvoiceIsOk() {
        when(invoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        Invoice invoice = invoiceServiceImpl.postInvoice(invoiceDto);
        assertThat(invoice.getAmountPerUnit()).isEqualTo(invoice.getAmountPerUnit());
    }
}
