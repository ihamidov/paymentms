package com.ibrahim.az.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

    private static final String PHONE_NUMBER_PATTERN = "\\+\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d";
    private static final String MAIL = "String s = \"^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$\"; mail";

    @Override
    public boolean isValid(String phonePhoneNumber, ConstraintValidatorContext context) {
        Pattern pattern = Pattern.compile(PHONE_NUMBER_PATTERN);
        Matcher matcher = pattern.matcher(phonePhoneNumber);
        return matcher.matches();
    }
}








