package com.ibrahim.az.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * ValidPhoneNumber.java
 * Interface class that has the following methods.
 *
 * @since 04-04-2020
 */
@Documented
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneNumberValidator.class)
public @interface ValidPhoneNumber {


    /**
     * @return return Nothing.
     */
    String message() default "Invalid phone number";

    /**
     * @return return default groups
     */
    Class<?>[] groups() default { };

    /**
     * @return return default regex
     */
    Class<? extends Payload>[] payload() default { };

}
