package com.ibrahim.az.repository;

import com.ibrahim.az.model.Invoice;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice,Long> {
    Optional<Invoice> findAccountByPhoneNumber(String phoneNumber);

}
