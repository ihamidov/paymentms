package com.ibrahim.az.exception;

public class InvoicePositiveException extends RuntimeException {
    private static final long serialVersionUID = -1L;

    private static final String MESSENGE = "Invoice ID must be positive";

    public InvoicePositiveException() {
        super(MESSENGE);
    }
//    public InvoicePossitiveException(long id) {
//        super(String.format("Invoice %id must be possitive", id));
//    }
}
