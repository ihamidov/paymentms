package com.ibrahim.az.exception;

public class InvoiceWithGivenPhoneNumberlAlreadyExistException extends RuntimeException {
    private static final long serialVersionUID = -3042686055658047285L;

    public InvoiceWithGivenPhoneNumberlAlreadyExistException(String phoneNumber) {
        super(String.format("Account with phone number: '%s' already exists", phoneNumber));
    }
}
