package com.ibrahim.az.exception;

import com.ibrahim.az.dto.DtoError;
import java.util.Calendar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RestController
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(InvoiceNotFoundException.class)
    public ResponseEntity handle(InvoiceNotFoundException ex) {
        DtoError errorDetail = new DtoError(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(),
                ex.getLocalizedMessage(),
                Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(InvoicePositiveException.class)
    public ResponseEntity invalideValueException(InvoicePositiveException ex) {
        DtoError errorDto = new DtoError(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                ex.getMessage(),
                Calendar.getInstance());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }
}
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    @ExceptionHandler(ConstraintViolationException.class)
//    public ResponseEntity handle(ConstraintViolationException ex) {
//        DtoError errorDetail = new DtoError(HttpStatus.BAD_REQUEST.value(),
//                this.getConstraintViolationExceptionMessege(ex),
//                this.getConstraintViolationExceptionMessege(ex),
//                Calendar.getInstance());
//        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
//    }
//
//    private String getConstraintViolationExceptionMessege(ConstraintViolationException e) {
//        return e.getConstraintViolations().stream()
//                .map(ConstraintViolation::getMessage)
//                .collect(Collectors.toList()).get(0);
//    }

