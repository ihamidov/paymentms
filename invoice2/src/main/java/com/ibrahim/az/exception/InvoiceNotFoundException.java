package com.ibrahim.az.exception;

public class InvoiceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private static final String MESSENGE = "Invoice ID not found";

    public InvoiceNotFoundException() {
        super(MESSENGE);
    }
//    public InvoiceNotFoundException(long id) {
//        super(String.format("Invoice %id not found", id));
//    }
}
