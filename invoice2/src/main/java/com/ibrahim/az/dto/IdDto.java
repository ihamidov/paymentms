package com.ibrahim.az.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IdDto {
    private Long id;
}
