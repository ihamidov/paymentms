package com.ibrahim.az.dto;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDto {

    private long cliendId;
    private List<IdDto> client;
    private int amountPerUnit;
    private int quanity;
    private LocalDateTime dueDate;
    private int discount;
    private String status;
    private String description;
    @Pattern(regexp = "\\+\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d")
    @NotNull(message = "cannot be empty phone number")
    private String phoneNumber;
}
