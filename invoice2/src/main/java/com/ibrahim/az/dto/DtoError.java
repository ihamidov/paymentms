package com.ibrahim.az.dto;

import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class DtoError {
    private int code;
    private String technicalMessage;
    private String userMessage;
    private Calendar timestamp;
}
