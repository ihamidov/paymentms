package com.ibrahim.az.service;

import com.ibrahim.az.dto.InvoiceDto;
import com.ibrahim.az.model.Invoice;
import org.springframework.data.domain.Page;

public interface InvoiceService {
//    Page<Invoice> getInvoiceList(int page, int pageSize);
    Iterable<Invoice> getInvoiceByList();
    Invoice getInvoiceById(long id);
    Invoice postInvoice(InvoiceDto invoiceDto);
    Invoice updateInvoice(long id, InvoiceDto invoiceDto);
    void deleteInvoiceById(long id);
}

