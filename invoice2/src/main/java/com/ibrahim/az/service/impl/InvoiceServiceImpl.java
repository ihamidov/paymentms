package com.ibrahim.az.service.impl;

import com.ibrahim.az.dto.InvoiceDto;
import com.ibrahim.az.exception.InvoiceNotFoundException;
import com.ibrahim.az.exception.InvoiceWithGivenPhoneNumberlAlreadyExistException;
import com.ibrahim.az.model.Invoice;
import com.ibrahim.az.repository.InvoiceRepository;
import com.ibrahim.az.service.InvoiceService;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    private Invoice setInvoice(InvoiceDto invoiceDto){
        LocalDateTime localDateTime = LocalDateTime.now();
        Invoice invoice=  new Invoice();
        invoice.setClientId(invoiceDto.getCliendId());
        invoice.setAmountPerUnit(invoiceDto.getAmountPerUnit());
        invoice.setDescription(invoiceDto.getDescription());
        invoice.setDiscount(invoiceDto.getDiscount());
        invoice.setDueDate(localDateTime);
        invoice.setQuantity(invoiceDto.getQuanity());
        invoice.setStatus(invoiceDto.getStatus());
        return invoice;
    }
    private void checkIfExist(Optional<Invoice> invoice) {
        if (!invoice.isPresent()) {
            throw new InvoiceNotFoundException();
        }
    }

//    @Override
//    public Page<Invoice> getInvoiceList(int page, int pageSize) {
//        PageRequest pageRequest = new PageRequest(page, pageSize, new Sort(Sort.Direction.DESC, "id"));
//        return invoiceRepository.findAll(pageRequest);
//    }

    @Override
    public Iterable<Invoice> getInvoiceByList() {
        return invoiceRepository.findAll();
    }

    @Override
    public Invoice getInvoiceById(long id) {
       final Optional<Invoice> invoice = invoiceRepository.findById(id);
        checkIfExist(invoice);
        return invoice.get();
    }

    @Override
    public Invoice postInvoice(InvoiceDto invoiceDto) {
        return invoiceRepository.save(setInvoice(invoiceDto));
    }

    @Override
    public Invoice updateInvoice(long id, InvoiceDto invoiceDto) {
        Optional<Invoice> invoice1 = invoiceRepository.findById(id);
        checkIfExist(invoice1);
        final  Invoice invoice=Invoice.builder()
                .id(id)
                .build();
        return invoiceRepository.save(invoice);
    }

    @Override
    public void deleteInvoiceById(long id) {
        final Optional<Invoice> byId = invoiceRepository.findById(id);
        checkIfExist(byId);
        invoiceRepository.deleteById(id);
    }

    private void checkAccountByPhoneNumber(String phoneNumber) {
        Optional<Invoice> account = invoiceRepository.findAccountByPhoneNumber(phoneNumber);
        account.ifPresent(a -> {
            throw new InvoiceWithGivenPhoneNumberlAlreadyExistException(phoneNumber);
        });
    }
}
