package com.ibrahim.az.controller;

import com.ibrahim.az.dto.InvoiceDto;
import com.ibrahim.az.exception.InvoicePositiveException;
import com.ibrahim.az.model.Invoice;
import com.ibrahim.az.service.InvoiceService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
public class InvoiceController {
    private InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    private void checkPosivitity(long id) {
        if (id <= 0) {
            throw new InvoicePositiveException();
        }
    }

    @GetMapping("invoice")
    public Iterable getInvoice() {
        return invoiceService.getInvoiceByList();
    }

    @GetMapping("invoice/{id}")
    public Invoice getInvoiceById(@PathVariable  long id) {
        checkPosivitity(id);
        return  invoiceService.getInvoiceById(id);
    }

    @PostMapping("invoice")
    public Invoice createMerchant(@RequestBody() InvoiceDto invoiceDto) {
        return invoiceService.postInvoice(invoiceDto);
    }

    @PutMapping("invoice/{id}")
    public Invoice updateInvoice(@PathVariable("id") long id, @RequestBody InvoiceDto invoiceDto) {
        return invoiceService.updateInvoice(id,invoiceDto);
    }

    @DeleteMapping("invoice/{id}")
    public void deleteInvoiceById(@PathVariable("id") long id) {
        invoiceService.deleteInvoiceById(id);
    }

}
