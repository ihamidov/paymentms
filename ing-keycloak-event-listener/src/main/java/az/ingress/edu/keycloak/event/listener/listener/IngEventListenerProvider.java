package az.ingress.edu.keycloak.event.listener.listener;

import az.ingress.edu.keycloak.event.listener.rabbitmq.IRabbitMq;
import az.ingress.edu.keycloak.event.listener.rabbitmq.RabbitMq;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;

@Slf4j
public class IngEventListenerProvider implements EventListenerProvider {

    private final IRabbitMq rabbitMq = new RabbitMq();

    @Override
    public void onEvent(Event event) {
        rabbitMq.sendEvent(event);
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean condition) {
        rabbitMq.sendAdminEvent(adminEvent);
    }

    @Override
    public void close() {
        //ignore
    }
}
