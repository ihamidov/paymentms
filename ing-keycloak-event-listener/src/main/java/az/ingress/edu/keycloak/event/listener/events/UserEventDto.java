package az.ingress.edu.keycloak.event.listener.events;

import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.keycloak.events.EventType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEventDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long time;

    private EventType type;

    private String realmId;

    private String clientId;

    private String userId;

    private String sessionId;

    private String ipAddress;

    private String error;

    private Map<String, String> details;

}
