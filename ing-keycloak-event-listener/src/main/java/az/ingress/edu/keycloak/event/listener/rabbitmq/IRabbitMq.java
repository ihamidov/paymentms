package az.ingress.edu.keycloak.event.listener.rabbitmq;

import org.keycloak.events.Event;
import org.keycloak.events.admin.AdminEvent;

public interface IRabbitMq {

    void sendEvent(Event event);

    void sendAdminEvent(AdminEvent adminEvent);

}
