package az.ingress.edu.keycloak.event.listener.listener;

import lombok.AllArgsConstructor;
import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

@AllArgsConstructor
public class IngEventListenerProviderFactory implements EventListenerProviderFactory {

    @Override
    public EventListenerProvider create(KeycloakSession keycloakSession) {
        return new IngEventListenerProvider();
    }

    @Override
    public void init(Config.Scope scope) {
        //ignore
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
        //ignore
    }

    @Override
    public void close() {
        //ignore
    }

    @Override
    public String getId() {
        return "ing-events-listener";
    }
}
