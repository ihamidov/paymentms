package az.ingress.edu.keycloak.event.listener.rabbitmq;

import az.ingress.edu.keycloak.event.listener.events.AdminEventDto;
import az.ingress.edu.keycloak.event.listener.events.UserEventDto;
import az.ingress.edu.keycloak.event.listener.mapping.IMappingToAdminEventDto;
import az.ingress.edu.keycloak.event.listener.mapping.IMappingToEventDto;
import az.ingress.edu.keycloak.event.listener.mapping.MappingToAdminEventDtoImpl;
import az.ingress.edu.keycloak.event.listener.mapping.MappingToEventDtoImpl;
import az.ingress.edu.keycloak.event.listener.serialization.EventsSerializer;
import az.ingress.edu.keycloak.event.listener.serialization.IEventsSerializer;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.events.Event;
import org.keycloak.events.admin.AdminEvent;

@Slf4j
public class RabbitMq implements IRabbitMq {

    private static final String RABBIT_ADMIN_EVENTS_QUEUE = System.getenv("RABBIT_ADMIN_EVENTS_QUEUE");
    private static final String RABBIT_EVENTS_QUEUE = System.getenv("RABBIT_EVENTS_QUEUE");

    private final IEventsSerializer eventsSerializer = new EventsSerializer();

    private final IMappingToAdminEventDto mappingToAdminEventDto = new MappingToAdminEventDtoImpl();

    private final IMappingToEventDto mappingToEventDto = new MappingToEventDtoImpl();

    @Override
    public void sendEvent(Event event) {
        UserEventDto userEventDto;
        userEventDto = mappingToEventDto.map(event);
        publishEvent(RABBIT_EVENTS_QUEUE, eventsSerializer.serializeObject(userEventDto));

    }

    @Override
    public void sendAdminEvent(AdminEvent adminEvent) {
        AdminEventDto adminEventDto;
        adminEventDto = mappingToAdminEventDto.map(adminEvent);
        publishEvent(RABBIT_ADMIN_EVENTS_QUEUE, eventsSerializer.serializeObject(adminEventDto));
    }

    private void publishEvent(String queueNameForEvents, byte[] bytes) {
        Connection connection = null;
        Channel channel = null;
        try {
            connection = connectionToRabbitMq().newConnection();
            channel = connection.createChannel();
            channel.queueDeclare(queueNameForEvents, true, false, false, null);
            channel.basicPublish("", queueNameForEvents, null, bytes);
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            try {
                if (connection != null && connection.isOpen()) {
                    connection.close();
                }
                if (channel != null && channel.isOpen()) {
                    channel.close();
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private ConnectionFactory connectionToRabbitMq() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBIT_HOST"));
        factory.setPort(Integer.parseInt(System.getenv("RABBIT_PORT")));
        factory.setUsername(System.getenv("RABBIT_USERNAME"));
        factory.setPassword(System.getenv("RABBIT_PASSWORD"));
        factory.setConnectionTimeout(300000);
        return factory;
    }
}
