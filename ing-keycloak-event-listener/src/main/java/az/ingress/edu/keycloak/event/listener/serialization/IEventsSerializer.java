package az.ingress.edu.keycloak.event.listener.serialization;

import java.io.Serializable;

public interface IEventsSerializer {

    byte[] serializeObject(Serializable serializableEvent);
}
