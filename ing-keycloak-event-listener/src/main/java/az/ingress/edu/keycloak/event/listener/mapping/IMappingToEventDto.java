package az.ingress.edu.keycloak.event.listener.mapping;

import az.ingress.edu.keycloak.event.listener.events.UserEventDto;
import org.keycloak.events.Event;

public interface IMappingToEventDto {

    UserEventDto map(Event event);
}
