package az.ingress.edu.keycloak.event.listener.serialization;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventsSerializer implements IEventsSerializer {

    @Override
    public byte[] serializeObject(Serializable serializableEvent) {
        byte[] myByte = null;
        try (
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bos)) {
            out.writeObject(serializableEvent);
            out.flush();
            myByte = bos.toByteArray();
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return myByte;
    }
}
