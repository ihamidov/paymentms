package az.ingress.edu.keycloak.event.listener.mapping;

import az.ingress.edu.keycloak.event.listener.events.AdminEventDto;
import org.keycloak.events.admin.AdminEvent;

public class MappingToAdminEventDtoImpl implements IMappingToAdminEventDto {

    @Override
    public AdminEventDto map(AdminEvent event) {
        return AdminEventDto.builder()
                .time(event.getTime())
                .operationType(event.getOperationType())
                .realmId(event.getAuthDetails().getRealmId())
                .clientId(event.getAuthDetails().getClientId())
                .userId(event.getAuthDetails().getUserId())
                .ipAddress(event.getAuthDetails().getIpAddress())
                .resourcePath(event.getResourcePath())
                .error(event.getError())
                .representation(event.getRepresentation())
                .build();
    }
}
