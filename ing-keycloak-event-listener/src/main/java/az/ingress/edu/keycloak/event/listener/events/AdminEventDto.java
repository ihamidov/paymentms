package az.ingress.edu.keycloak.event.listener.events;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.keycloak.events.admin.OperationType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminEventDto implements Serializable {

    private static final long serialVersionUID = 2L;

    private long time;

    private String realmId;

    private String clientId;

    private String userId;

    private String ipAddress;

    private String resourceType;

    private OperationType operationType;

    private String resourcePath;

    private String representation;

    private String error;
}
