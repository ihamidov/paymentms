package az.ingress.edu.keycloak.event.listener.mapping;

import az.ingress.edu.keycloak.event.listener.events.UserEventDto;
import org.keycloak.events.Event;

public class MappingToEventDtoImpl implements IMappingToEventDto {

    @Override
    public UserEventDto map(Event event) {
        return UserEventDto.builder()
                .clientId(event.getClientId())
                .details(event.getDetails())
                .error(event.getError())
                .ipAddress(event.getIpAddress())
                .realmId(event.getRealmId())
                .sessionId(event.getSessionId())
                .time(event.getTime())
                .type(event.getType())
                .userId(event.getUserId())
                .build();
    }
}
