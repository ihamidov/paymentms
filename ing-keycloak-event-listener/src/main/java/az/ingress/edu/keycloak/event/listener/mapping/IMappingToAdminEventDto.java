package az.ingress.edu.keycloak.event.listener.mapping;

import az.ingress.edu.keycloak.event.listener.events.AdminEventDto;
import org.keycloak.events.admin.AdminEvent;

public interface IMappingToAdminEventDto {

    AdminEventDto map(AdminEvent event);

}
