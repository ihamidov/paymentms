package az.ingress.edu.keycloak.event.listener;

import static org.assertj.core.api.Java6Assertions.assertThat;

import az.ingress.edu.keycloak.event.listener.events.UserEventDto;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.events.Event;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IngEventListenerProviderTest {

    private static final String USER_ID = "1";

    @Mock
    private ConnectionFactory connectionFactory;

    @Mock
    private Connection connection;

    @Mock
    private Channel channel;

    private UserEventDto userEventDto;

    private Event event;

    @Before
    public void setUp() {
        event = new Event();
        event.setUserId(USER_ID);

        userEventDto = UserEventDto.builder()
                .userId(event.getUserId())
                .build();
    }

    @Test
    public void givenEventMappingToLmsEventThenOk() {
        assertThat(userEventDto).isNotNull();
        assertThat(event).isNotNull();
    }

}
