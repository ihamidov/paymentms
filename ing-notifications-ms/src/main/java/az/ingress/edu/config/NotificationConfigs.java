package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("notification")
@EnableConfigurationProperties(NotificationConfigs.class)
@Setter
@Getter
public class NotificationConfigs {

    private String slackNotifyUrl;
}
