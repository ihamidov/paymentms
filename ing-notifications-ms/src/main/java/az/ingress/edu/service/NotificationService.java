package az.ingress.edu.service;

import az.ingress.edu.dto.EnrollmentInfoDto;
import org.springframework.boot.configurationprocessor.json.JSONException;

public interface NotificationService {

    void notifySlack(EnrollmentInfoDto enrollmentInfo) throws JSONException;
}
