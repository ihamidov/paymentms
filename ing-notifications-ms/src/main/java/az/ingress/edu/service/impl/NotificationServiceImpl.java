package az.ingress.edu.service.impl;

import az.ingress.edu.config.NotificationConfigs;
import az.ingress.edu.dto.EnrollmentInfoDto;
import az.ingress.edu.service.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private static final String SLACK_NOTIFY_MESSAGE = "User '%s %s' >> https://ingress.academy/student-detail.html?"
            + "id=%s wants to enroll to this course >> https://ingress.academy/course-detail.html?id=%d";

    private final NotificationConfigs notificationConfigs;
    private final RestTemplate restTemplate;

    @Override
    @RabbitListener(queues = {"${rabbit.slack-enroll-notifier-queue}"})
    public void notifySlack(EnrollmentInfoDto enrollmentInfo) throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("text", String.format(SLACK_NOTIFY_MESSAGE, enrollmentInfo.getFirstName(),
                 enrollmentInfo.getLastName(),enrollmentInfo.getUsername(),
                enrollmentInfo.getCourseSessionId()));
        headers.setContentType(MediaType.APPLICATION_JSON);
        restTemplate.postForObject(notificationConfigs
                        .getSlackNotifyUrl(), new HttpEntity<>(jsonObject.toString(), headers), String.class);
    }
}
