package az.ingress.edu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.model.InvoiceStatus;
import az.ingress.edu.service.InvoiceService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(InvoiceController.class)
public class InvoiceControllerTest {

    private static final String GET_INVOICE_LIST = "/invoice";
    private static final String CREATE_INVOICE = "/account/{accountId}/invoice";
    private static final String GET_INVOICE_LIST_BY_ACCOUNT_ID = "/account/{accountId}/invoice";
    private static final long ACCOUNT_ID = 1L;
    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "$.userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String ACCOUNT_NOT_FOUND_MESSAGE = "Account can't be found";
    private static final String CLIENT_NOT_FOUND_MESSAGE = "Client not found.";

    private CreateInvoiceDto createInvoiceDto;
    private Invoice invoice;
    private List<Invoice> invoiceList;

    @MockBean
    private InvoiceService invoiceService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        Client client = Client.builder()
                .id(1L)
                .build();
        createInvoiceDto = CreateInvoiceDto.builder().clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.now())
                .discount(30)
                .status(InvoiceStatus.PAID)
                .description("String")
                .build();
        invoice = Invoice.builder().id(1L)
                .client(client).accountId(ACCOUNT_ID)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.now())
                .discount(30)
                .status(InvoiceStatus.PAID)
                .description("String")
                .build();
        invoiceList = new ArrayList();
        invoiceList.add(invoice);
    }

    @Test
    public void givenInvoiceListWhenGetInvoiceListThenOK() throws Exception {
        when(invoiceService.getInvoiceList()).thenReturn(invoiceList);

        mvc.perform(get(GET_INVOICE_LIST)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(invoiceList)))
                .andExpect(status().isOk());
    }

    public void givenValidDataWhenCreateInvoiceThenOk() throws Exception {
        when(invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto)).thenReturn(invoice);

        mvc.perform(post(CREATE_INVOICE, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createInvoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(invoice.getId()));
    }

    @Test
    public void givenAccountNotExistWhenCreateInvoiceThenNotFound() throws Exception {
        when(invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto)).thenThrow(new AccountNotFoundException());

        mvc.perform(post(CREATE_INVOICE, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createInvoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE));
    }

    @Test
    public void givenClientNotExistWhenCreateInvoiceThenNotFound() throws Exception {
        when(invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto)).thenThrow(new ClientNotFoundException());

        mvc.perform(post(CREATE_INVOICE, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createInvoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(CLIENT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(CLIENT_NOT_FOUND_MESSAGE));
    }

    @Test
    public void givenInvoiceListWhenGetInvoiceListByAccountIdThenOk() throws Exception {
        when(invoiceService.getInvoiceListByAccountId(ACCOUNT_ID)).thenReturn(invoiceList);

        mvc.perform(get(GET_INVOICE_LIST_BY_ACCOUNT_ID, ACCOUNT_ID)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(invoiceList)));
    }

    @Test
    public void givenAccountNotExistsWhenGetInvoiceListByAccountIdThenNotFound() throws Exception {
        when(invoiceService.getInvoiceListByAccountId(ACCOUNT_ID)).thenThrow(new AccountNotFoundException());

        mvc.perform(get(GET_INVOICE_LIST_BY_ACCOUNT_ID, ACCOUNT_ID)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE));
    }
}
