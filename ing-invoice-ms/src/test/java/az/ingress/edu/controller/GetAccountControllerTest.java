package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.service.AccountService;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class GetAccountControllerTest {

    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_ACCOUNT_PATH = "/account/{accountId}";
    private static final String NAME = "Ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final String ACCOUNT_ID_MUST_BE_POSITIVE = "Account id must be positive";
    private static final long DUMMY_ID = 1L;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @Test
    public void givenExistingAccountIdExpectOk() throws Exception {
        Account account = Account.builder()
                .id(DUMMY_ID)
                .name(NAME)
                .lastName(LAST_NAME)
                .userUuid(USER_UUID)
                .email(EMAIL)
                .logo(LOGO)
                .build();
        when(accountService.getAccount(DUMMY_ID)).thenReturn(account);
        mockMvc.perform(get(GET_ACCOUNT_PATH, DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(account.getId()));
    }

    @Test
    public void givenMethodArgumentTypeMismatchExceptionMessage() throws Exception {
        final String stringId = "aaaa";
        when(accountService.getAccount(anyLong())).thenThrow(MethodArgumentTypeMismatchException.class);
        mockMvc.perform(get(GET_ACCOUNT_PATH, stringId)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(checkMisMatchedAccountIdMessage(stringId))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(checkMisMatchedAccountIdMessage(stringId))));

    }

    @Test
    public void givenNonExistingAccountIdExpectErrorMessage() throws Exception {
        long id = 1;
        when(accountService.getAccount(anyLong())).thenThrow(new AccountNotFoundException(id));
        mockMvc.perform(get(GET_ACCOUNT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(checkAccountWithIdNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(checkAccountWithIdNotFoundErrorMessage(id))));
    }

    @Test
    public void givenNonExistingAccountExpectErrorMessage() throws Exception {
        long id = 1;
        when(accountService.getAccount(anyLong())).thenThrow(new AccountNotFoundException());
        mockMvc.perform(get(GET_ACCOUNT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(checkAccountNotFoundErrorMessage())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(checkAccountNotFoundErrorMessage())));
    }

    @Test
    public void givenAccountWithInvalidIdExpectErrorMessage() throws Exception {
        long id = -1L;
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_ACCOUNT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(ACCOUNT_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_ID_MUST_BE_POSITIVE));
    }

    private String checkAccountWithIdNotFoundErrorMessage(Long id) {
        return String.format("Account with id: '%s' can't be found", id);
    }

    private String checkAccountNotFoundErrorMessage() {
        return String.format("Account can't be found");
    }

    private String checkMisMatchedAccountIdMessage(String id) {
        return String.format("Failed to convert value of type 'java.lang.String' "
                + "to required type 'java.lang.Long'; nested exception "
                + "is java.lang.NumberFormatException: For input string: "
                + "\"" + "%s" + "\"", id);
    }
}
