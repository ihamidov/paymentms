package az.ingress.edu.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.model.Account;
import az.ingress.edu.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class GetAccountListControllerTest {

    private static final String GET_ACCOUNT_LIST_PATH = "/account";
    private static final String NAME = "Ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final long DUMMY_ID = 1L;
    private List<Account> accountList;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        Account account = Account.builder()
                .id(DUMMY_ID)
                .name(NAME)
                .lastName(LAST_NAME)
                .userUuid(USER_UUID)
                .email(EMAIL)
                .logo(LOGO)
                .build();
        accountList = new ArrayList<>();
        accountList.add(account);
    }

    @Test
    public void givenAccountListExpectStatusOk() throws Exception {
        when(accountService.getAllAccounts()).thenReturn(accountList);
        mockMvc.perform(get(GET_ACCOUNT_LIST_PATH)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(accountList)));
    }

    @Test
    public void givenAccountListEmptyExpectStatusOk() throws Exception {
        accountList = new ArrayList<>();
        assertThat(accountList.isEmpty()).isTrue();
        when(accountService.getAllAccounts()).thenReturn(accountList);
        mockMvc.perform(get(GET_ACCOUNT_LIST_PATH)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(accountList)));
    }
}
