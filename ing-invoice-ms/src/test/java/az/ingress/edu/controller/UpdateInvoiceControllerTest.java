package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.InvoiceDto;
import az.ingress.edu.exception.InvalidInvoiceIdException;
import az.ingress.edu.exception.InvoiceNotFoundException;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.model.InvoiceStatus;
import az.ingress.edu.service.InvoiceService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(InvoiceController.class)
public class UpdateInvoiceControllerTest {

    private static final String INVOICE_API = "/invoice/";
    private static final long ID_POSITIVE = 1;
    private static final long ID_NEGATIVE = -1L;
    private static final int AMOUNTPERUNIT = 100;
    private static final LocalDateTime DUEDATE = LocalDateTime.now();
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String INVOICE_MUST_BE_POSSITIVE = "Invoice ID must be positive";
    private static final String INVOICE_NOT_FOUND = "Invoice not found";

    @MockBean
    private InvoiceService invoiceService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private InvoiceDto invoiceDto;
    private Invoice invoice;

    @Before
    public void setUp() {
        List<IdDto> clientList = new ArrayList<>();
        Client client = Client.builder()
                .id(1L)
                .build();
        invoiceDto = InvoiceDto.builder().client(clientList)
                .amountPerUnit(AMOUNTPERUNIT).quanity(100)
                .dueDate(DUEDATE).discount(30)
                .description("string").build();
        invoice = Invoice.builder().id(1L)
                .client(client)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.now())
                .discount(30).subtotal(10000).total(7000)
                .status(InvoiceStatus.PAID).dueDate(DUEDATE)
                .description("string")
                .build();
    }

    @Test
    public void givenInvoiceNotExistWhenUpdateInvoiceThenNotFound() throws Exception {
        when(invoiceService.updateInvoice(ID_POSITIVE, invoiceDto)).thenThrow(new InvoiceNotFoundException());

        mvc.perform(MockMvcRequestBuilders.put(INVOICE_API + ID_POSITIVE)
                .content(objectMapper.writeValueAsString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_NOT_FOUND));
    }

    @Test
    public void givenInvoiceUpdateWhenPutInvoiceThenOK() throws Exception {
        when(invoiceService.updateInvoice(ID_POSITIVE, invoiceDto)).thenReturn(invoice);

        mvc.perform(MockMvcRequestBuilders.put(INVOICE_API + ID_POSITIVE)
                .content(asJsonString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInvoiceUpdateWhenPutThenInvoiceInvalideValue() throws Exception {
        InvoiceDto invoiceDto = new InvoiceDto();
        when(invoiceService.updateInvoice(ID_NEGATIVE, invoiceDto)).thenThrow(new InvalidInvoiceIdException());
        mvc.perform(MockMvcRequestBuilders.put(INVOICE_API + ID_NEGATIVE)
                .content(asJsonString(invoiceDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_MUST_BE_POSSITIVE));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void givenInvoiceDeleteWhenDeleteInvoiceThenOK() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(INVOICE_API + 1))
                .andExpect(status().isOk());
    }

    @Test
    public void givenInvoiceDeleteWhenDeleteInvoiceThenNotFound() throws Exception {
        doThrow(new InvoiceNotFoundException()).when(invoiceService).deleteInvoiceById(anyLong());

        mvc.perform(MockMvcRequestBuilders.delete(INVOICE_API + 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(404))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INVOICE_NOT_FOUND))
                .andExpect(jsonPath(USER_MESSAGE).value(INVOICE_NOT_FOUND));
    }

}
