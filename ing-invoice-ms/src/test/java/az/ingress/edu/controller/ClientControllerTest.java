package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateClientDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateClientDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientAlreadyExistsException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.service.ClientService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    private static final String ACCOUNT_API = "/account";
    private static final String CLIENT_API = "/client";
    private static final String UPDATE_CLIENT_API = "/client/{id}";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String ACCOUNT_NOT_FOUND = "Account with id: '1' can't be found";
    private static final String NEGATIVE_ID_MESSAGE = "Id must be positive";
    private static final long DUMMY_ID = 1L;
    private static final long NEGATIVE_DUMMY_ID = -1L;
    private static final String EMAIL = "info@ingress.az";
    private static final String NAME = "Ichigo";
    private static final String LAST_NAME = "Kurasaki";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClientService clientService;

    private final CreateClientDto clientDto = CreateClientDto.builder()
            .name(NAME)
            .lastName(LAST_NAME)
            .email(EMAIL)
            .build();

    private final IdDto idDto = IdDto.builder()
            .id(DUMMY_ID)
            .build();

    private final UpdateClientDto updateClientDto = UpdateClientDto.builder()
            .name(NAME)
            .lastName(LAST_NAME)
            .build();

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenValidClientDataWhenCreateClientThenOk() throws Exception {
        when(clientService.createClient(1L, clientDto)).thenReturn(IdDto.builder().id(1L).build());

        mvc.perform(post(ACCOUNT_API + "/" + 1 + CLIENT_API)
                .content(asJsonString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenAccountNotExistWhenCreateClientThenNotFound() throws Exception {
        doThrow(new AccountNotFoundException(1L)).when(clientService).createClient(1L, clientDto);

        mvc.perform(post(ACCOUNT_API + "/" + 1 + CLIENT_API)
                .content(asJsonString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ACCOUNT_NOT_FOUND)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ACCOUNT_NOT_FOUND)));
    }

    @Test
    public void whenClientForGivenAccountAlreadyExistThenBadRequest() throws Exception {

        when(clientService.createClient(1, clientDto)).thenReturn(idDto);
        when(clientService.createClient(1, clientDto))
                .thenThrow(new ClientAlreadyExistsException(EMAIL));

        mvc.perform(post(ACCOUNT_API + "/" + 1 + CLIENT_API)
                .content(objectMapper.writeValueAsString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(checkClientWithEmailExistMessage(EMAIL))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(checkClientWithEmailExistMessage(EMAIL))));
    }

    @Test
    public void whenClientUpdateForGivenClientIdNotExistThenNotFound() throws Exception {
        when(clientService.updateClientById(DUMMY_ID, updateClientDto))
                .thenThrow(new ClientNotFoundException(DUMMY_ID));

        mvc.perform(put(UPDATE_CLIENT_API, DUMMY_ID)
                .content(asJsonString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(checkClientWithGivenIdNotFound(DUMMY_ID))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(checkClientWithGivenIdNotFound(DUMMY_ID))));
    }

    @Test
    public void whenClientUpdateForGivenClientIdIsExistThenIsOk() throws Exception {
        when(clientService.updateClientById(DUMMY_ID, updateClientDto))
                .thenReturn(IdDto.builder().id(DUMMY_ID).build());

        mvc.perform(put(UPDATE_CLIENT_API, DUMMY_ID)
                .content(asJsonString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void whenClientUpdateThenNegativeIdMessage() throws Exception {

        mvc.perform(put(UPDATE_CLIENT_API, NEGATIVE_DUMMY_ID)
                .content(asJsonString(clientDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(NEGATIVE_ID_MESSAGE)))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(NEGATIVE_ID_MESSAGE)));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    private String checkClientWithEmailExistMessage(String email) {
        return String.format("Client with email: '%s' already exists", email);
    }

    private String checkClientWithGivenIdNotFound(Long id) {
        return String.format("Client with id %d not found.", id);
    }
}
