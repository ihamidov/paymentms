package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.AccountWithGivenEmailAlreadyExistException;
import az.ingress.edu.exception.AccountWithGivenUserUuidAlreadyExistException;
import az.ingress.edu.model.Account;
import az.ingress.edu.repository.AccountRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {

    private static final String NAME = "ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final long DUMMY_ID = 1L;

    private CreateAccountDto createAccountDto;

    private Account account;

    private List<Account> accountList;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountService;

    @Before
    public void setUp() {
        createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .userUuid(USER_UUID)
                .logo(LOGO)
                .build();
        account = Account.builder()
                .id(DUMMY_ID)
                .name(createAccountDto.getName())
                .lastName(createAccountDto.getLastName())
                .email(createAccountDto.getEmail())
                .userUuid(createAccountDto.getUserUuid())
                .logo(LOGO)
                .build();
        accountList = new ArrayList<>();
        accountList.add(account);
    }

    @Test
    public void createAccount() {
        when(accountRepository.save(any())).thenReturn(account);
        Account returnedAccount = accountService.createAccount(createAccountDto);
        assertThat(returnedAccount).isEqualTo(account);
    }

    @Test
    public void whenAccountExistsGetByID() {
        when(accountRepository.findById(anyLong())).thenReturn(Optional.of(account));
        assertThat(accountService.getAccount(anyLong())).isEqualTo(account);
        verify(accountRepository).findById(anyLong());
    }

    @Test
    public void givenAccountNotFoundExpectException() {
        when(accountRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> accountService.getAccount(1L)).isInstanceOf(AccountNotFoundException.class);
        verify(accountRepository).findById(1L);
    }

    @Test
    public void givenAccountWithEmailExistExpectException() {
        when(accountRepository.findAccountByEmail(EMAIL)).thenReturn(Optional.of(account));
        assertThatThrownBy(() -> accountService.createAccount(createAccountDto))
                .isInstanceOf(AccountWithGivenEmailAlreadyExistException.class);
    }

    @Test
    public void givenAccountWithUserUuidAlreadyExistExpectException() {
        when(accountRepository.findAccountByUserUuid(USER_UUID)).thenReturn(Optional.of(account));
        assertThatThrownBy(() -> accountService.createAccount(createAccountDto))
                .isInstanceOf(AccountWithGivenUserUuidAlreadyExistException.class);
    }

    @Test
    public void givenValidRequestGetAccountListExpectOk() {
        when(accountService.getAllAccounts()).thenReturn(accountList);
        assertThat(accountService.getAllAccounts())
                .isInstanceOf(Iterable.class)
                .hasSize(1);
    }

    @Test
    public void givenValidRequestGetEmptyAccountListExpectOk() {
        accountList.remove(account);
        when(accountService.getAllAccounts()).thenReturn(accountList);
        assertThat(accountService.getAllAccounts())
                .isInstanceOf(Iterable.class)
                .hasSize(0);
    }
}
