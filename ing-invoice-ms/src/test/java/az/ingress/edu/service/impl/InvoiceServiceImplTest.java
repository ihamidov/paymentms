package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.InvoiceDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.model.InvoiceStatus;
import az.ingress.edu.repository.AccountRepository;
import az.ingress.edu.repository.ClientRepository;
import az.ingress.edu.repository.InvoiceRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceServiceImplTest {

    private static final long ID = 1;
    private static final int AMOUNTPERUNIT = 100;
    private static final LocalDateTime DUEDATE = LocalDateTime.now();

    private static final Long ACCOUNT_ID = 1L;
    private static final String ACCOUNT_NOT_FOUND_MESSAGE = "Account can't be found";
    private static final String CLIENT_NOT_FOUND_MESSAGE = "Client not found.";

    private CreateInvoiceDto createInvoiceDto;
    private Invoice invoice;
    private Account account;
    private Client client;
    private List<Invoice> invoiceList;

    @Mock
    private InvoiceRepository invoiceRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private InvoiceServiceImpl invoiceService;
    private Invoice invoice2;
    private InvoiceDto invoiceDto;

    @Before
    public void setUp() {
        List<IdDto> clientList = new ArrayList<>();
        invoice2 = Invoice.builder().id(ID).client(client)
                .amountPerUnit(AMOUNTPERUNIT).quantity(100)
                .discount(30).subtotal(10000).total(7000)
                .status(InvoiceStatus.PAID).dueDate(DUEDATE)
                .description("string")
                .build();
        invoiceDto = InvoiceDto.builder().client(clientList)
                .amountPerUnit(AMOUNTPERUNIT).quanity(100)
                .dueDate(DUEDATE).discount(30)
                .description("string")
                .build();
        createInvoiceDto = CreateInvoiceDto.builder().clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.of(2020, 3, 3, 3, 3, 3))
                .discount(30).status(InvoiceStatus.PAID).description("String")
                .build();
        client = Client.builder().id(1L).build();
        invoice = Invoice.builder().client(client)
                .accountId(ACCOUNT_ID).amountPerUnit(100).quantity(100)
                .dueDate(LocalDateTime.of(2020, 3, 3, 3, 3, 3))
                .discount(30).status(InvoiceStatus.PAID)
                .description("String")
                .build();
        invoiceList = new ArrayList();
        invoiceList.add(invoice);
        account = Account.builder().id(ACCOUNT_ID).build();
    }

    @Test
    public void givenValidRequestGetInvoiceListExpectOk() {
        when(invoiceRepository.findAll()).thenReturn(invoiceList);

        assertThat(invoiceService.getInvoiceList())
                .isInstanceOf(List.class)
                .contains(invoice)
                .hasSize(1);
    }

    @Test
    public void givenUpdateInvoiceWhenPutInvoiceThenOk() {
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.of(invoice2));
        when(invoiceRepository.save(any(Invoice.class))).thenReturn(invoice2);
        Invoice invoice = invoiceService.updateInvoice(anyLong(), invoiceDto);
        assertThat(invoice.getAmountPerUnit()).isEqualTo(invoice.getAmountPerUnit());
    }

    @Test
    public void givenDeleteInvoiceWhenDeleteInvoiceThenOk() {
        when(invoiceRepository.findById(anyLong())).thenReturn(Optional.of(invoice));
        invoiceService.deleteInvoiceById(ID);
    }

    public void givenValidDataWhenCreateInvoiceThenOk() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(clientRepository.findById(createInvoiceDto.getClientId())).thenReturn(Optional.of(client));
        when(invoiceRepository.save(invoice)).thenReturn(invoice);
        assertThat(invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto)).isEqualTo(invoice);
    }

    @Test
    public void givenAccountNotExistWhenCreateInvoiceThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage(ACCOUNT_NOT_FOUND_MESSAGE);
    }

    @Test
    public void givenClientNotExistWhenCreateInvoiceThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(clientRepository.findById(createInvoiceDto.getClientId()))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> invoiceService.createInvoice(ACCOUNT_ID, createInvoiceDto))
                .isInstanceOf(ClientNotFoundException.class)
                .hasMessage(CLIENT_NOT_FOUND_MESSAGE);

    }

    @Test
    public void givenValidDataWhenGetInvoiceListByAccountIdThenInvoiceList() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(invoiceRepository.findByAccountId(ACCOUNT_ID)).thenReturn(invoiceList);

        assertThat(invoiceService.getInvoiceListByAccountId(ACCOUNT_ID))
                .isInstanceOf(List.class)
                .contains(invoice)
                .hasSize(1);
    }

    @Test
    public void givenAccountNotExistsWhenGetInvoiceListByAccountIdThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenThrow(new AccountNotFoundException());

        assertThatThrownBy(() -> invoiceService.getInvoiceListByAccountId(ACCOUNT_ID))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage(ACCOUNT_NOT_FOUND_MESSAGE);
    }
}
