package az.ingress.edu.model;

public enum InvoiceStatus {

    ACTIVE, CANCELLED, NOTIFIED, PAID, REJECTED
}
