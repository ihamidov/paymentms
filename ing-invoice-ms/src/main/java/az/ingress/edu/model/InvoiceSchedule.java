package az.ingress.edu.model;

import az.ingress.edu.dto.InvoiceScheduleStatus;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account account;

    @OneToMany
    private Set<Invoice> invoices;

    private LocalDateTime dateToSend;

    private String periodType;

    private Integer numberOfTimesToRepeat;

    private InvoiceScheduleStatus status;

}
