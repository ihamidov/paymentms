package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.InvoiceDto;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.service.InvoiceService;
import java.io.IOException;
import java.util.List;
import javax.validation.constraints.Positive;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class InvoiceController {

    public static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @PutMapping("invoice/{id}")
    public Invoice updateInvoice(@PathVariable("id") long id, @RequestBody InvoiceDto invoiceDto) {
        return invoiceService.updateInvoice(id, invoiceDto);
    }

    @DeleteMapping("invoice/{id}")
    public void deleteInvoiceById(@Positive(message = ID_MUST_BE_POSITIVE) @PathVariable("id") long id) {
        invoiceService.deleteInvoiceById(id);
    }


    @GetMapping("invoice")
    public Iterable<Invoice> getInvoiceList() {
        return invoiceService.getInvoiceList();
    }

    @PostMapping("account/{accountId}/invoice")
    public IdDto createInvoice(@PathVariable long accountId,
                               @Validated @RequestBody CreateInvoiceDto createInvoiceDto) {
        return IdDto.builder()
                .id(invoiceService.createInvoice(accountId, createInvoiceDto).getId())
                .build();
    }

    @GetMapping("account/{accountId}/invoice")
    public List<Invoice> getInvoiceListByAccountId(@PathVariable long accountId) {
        return invoiceService.getInvoiceListByAccountId(accountId);
    }

    @GetMapping(value = "/invoice/{id}/content/html", produces = "text/html")
    public ResponseEntity<String> getInvoiceAsHtml(@Positive(message = ID_MUST_BE_POSITIVE)
                                                   @PathVariable long id) throws IOException {
        return ResponseEntity.ok(invoiceService.getInvoiceAsHtml(id));

    }

    @GetMapping(value = "/invoice/{id}/content/pdf", produces = "application/pdf")
    public ResponseEntity<byte[]> getInvoiceAsPdf(@Positive(message = ID_MUST_BE_POSITIVE)
                                                  @PathVariable long id) throws IOException {
        return ResponseEntity.ok(invoiceService.getInvoiceAsPdf(id));
    }
}
