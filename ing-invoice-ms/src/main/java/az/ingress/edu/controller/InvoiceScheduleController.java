package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateInvoiceScheduleDto;
import az.ingress.edu.model.InvoiceSchedule;
import az.ingress.edu.service.InvoiceScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class InvoiceScheduleController {

    private final InvoiceScheduleService invoiceScheduleService;

    @PostMapping("account/{accountId}/invoice-schedule")
    public InvoiceSchedule createInvoice(@PathVariable long accountId,
            @Validated @RequestBody CreateInvoiceScheduleDto dto) {
        return invoiceScheduleService.create(accountId, dto);
    }

}
