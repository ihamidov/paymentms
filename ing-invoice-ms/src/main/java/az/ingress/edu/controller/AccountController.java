package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.dto.SearchDto;
import az.ingress.edu.model.Account;
import az.ingress.edu.service.AccountService;
import javax.validation.constraints.Positive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/account")
public class AccountController {

    private static final String ACCOUNT_ID_MUST_BE_POSITIVE = "Account id must be positive";

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("{accountId}")
    public Account getAccountById(
            @Positive(message = ACCOUNT_ID_MUST_BE_POSITIVE) @PathVariable("accountId") Long accountId) {
        return accountService.getAccount(accountId);
    }

    @GetMapping
    public Iterable<Account> getAccountList() {
        return accountService.getAllAccounts();
    }

    @PostMapping
    public Account createAccount(@Validated @RequestBody CreateAccountDto accountDto) {
        return accountService.createAccount(accountDto);
    }

    @PostMapping("/search")
    public Page<Account> searchAll(@RequestBody SearchDto searchDto, Pageable pageable) {
        return accountService.search(searchDto, pageable);
    }
}
