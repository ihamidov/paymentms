package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateClientDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateClientDto;
import az.ingress.edu.model.Client;
import az.ingress.edu.service.ClientService;
import javax.validation.Valid;
import javax.validation.constraints.Positive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
public class ClientController {

    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/account/{accountId}/client")
    public IdDto createClient(@PathVariable Long accountId, @RequestBody @Valid CreateClientDto dto) {
        return clientService.createClient(accountId, dto);
    }

    @GetMapping("/client/{id}")
    public Client getClientWithId(@Positive(message = ID_MUST_BE_POSITIVE) @PathVariable(name = "id") long id) {
        return clientService.getClientById(id);
    }

    @GetMapping("/client")
    public Page<Client> getClientList(Pageable pageable) {
        return clientService.getClientList(pageable);
    }

    @PutMapping("/client/{id}")
    public IdDto updateClient(@Positive(message = ID_MUST_BE_POSITIVE) @PathVariable(name = "id") long id,
            @RequestBody @Valid UpdateClientDto updateClientDto) {
        return clientService.updateClientById(id, updateClientDto);
    }
}
