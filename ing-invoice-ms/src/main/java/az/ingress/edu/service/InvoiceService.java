package az.ingress.edu.service;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.dto.InvoiceDto;
import az.ingress.edu.model.Invoice;
import java.io.IOException;
import java.util.List;

public interface InvoiceService {

    Iterable<Invoice> getInvoiceList();

    Invoice updateInvoice(long id, InvoiceDto invoiceDto);

    Invoice createInvoice(long accountId, CreateInvoiceDto createInvoiceDto);

    List<Invoice> getInvoiceListByAccountId(long accountId);

    Invoice getInvoice(long invoiceId);

    String getInvoiceAsHtml(long invoiceId) throws IOException;

    byte[] getInvoiceAsPdf(long invoiceId) throws IOException;

    void deleteInvoiceById(long id);

}
