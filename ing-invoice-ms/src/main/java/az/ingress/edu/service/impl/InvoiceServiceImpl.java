package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.dto.InvoiceDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.exception.InvoiceNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.repository.AccountRepository;
import az.ingress.edu.repository.ClientRepository;
import az.ingress.edu.repository.InvoiceRepository;
import az.ingress.edu.service.AccountService;
import az.ingress.edu.service.ClientService;
import az.ingress.edu.service.InvoiceService;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;
    private final ClientService clientService;
    private final AccountService accountService;

    @Override
    public Invoice updateInvoice(long id, InvoiceDto invoiceDto) {
        Optional<Invoice> invoice1 = invoiceRepository.findById(id);
        checkIfExist(invoice1);
        final Invoice invoice = Invoice.builder()
                .id(id)
                .amountPerUnit(invoiceDto.getAmountPerUnit())
                .quantity(invoiceDto.getQuanity())
                .dueDate(invoiceDto.getDueDate())
                .discount(invoiceDto.getDiscount())
                .description(invoiceDto.getDescription())
                .build();
        return invoiceRepository.save(invoice);
    }

    @Override
    public void deleteInvoiceById(long id) {
        final Optional<Invoice> byId = invoiceRepository.findById(id);
        checkIfExist(byId);
        invoiceRepository.deleteById(id);
    }

    @Override
    public Iterable<Invoice> getInvoiceList() {
        return invoiceRepository.findAll();
    }

    @Override
    public Invoice createInvoice(long accountId, CreateInvoiceDto createInvoiceDto) {
        checkIfAccountExists(accountId);
        checkIfClientExists(createInvoiceDto.getClientId());
        Invoice invoice = toEntity(createInvoiceDto);
        invoice.setAccountId(accountId);
        return invoiceRepository.save(invoice);
    }

    @Override
    public List<Invoice> getInvoiceListByAccountId(long accountId) {
        checkIfAccountExists(accountId);
        return invoiceRepository.findByAccountId(accountId);
    }

    @Override
    public Invoice getInvoice(long invoiceId) {
        return invoiceRepository.findById(invoiceId).orElseThrow(InvoiceNotFoundException::new);
    }

    @Override
    public String getInvoiceAsHtml(long invoiceId) throws IOException {
        Invoice invoice = getInvoice(invoiceId);
        Client client = clientService.getClientById(invoice.getClient().getId());
        Account account = accountService.getAccount(invoice.getAccountId());
        JtwigTemplate jtwigTemplate = JtwigTemplate
                .fileTemplate(ResourceUtils.getFile("classpath:assets/invoice.html"));
        JtwigModel jtwigModel = setTemplateValues(invoice, client, account);
        return jtwigTemplate.render(jtwigModel);
    }

    @Override
    public byte[] getInvoiceAsPdf(long invoiceId) throws IOException {
        return generatePdfFromHtml(getInvoiceAsHtml(invoiceId));
    }

    private JtwigModel setTemplateValues(Invoice invoice, Client client, Account account) {
        return new JtwigModel()
                .with("from", account.getName() + " " + account.getLastName())
                .with("fromEmail", account.getEmail())
                .with("invoiceId", invoice.getId())
                .with("date", LocalDate.now())
                .with("description", invoice.getDescription())
                .with("due", invoice.getDueDate())
                .with("price", invoice.getAmountPerUnit())
                .with("to", client.getName() + " " + client.getLastName())
                .with("toEmail", client.getEmail())
                .with("discount", invoice.getDiscount())
                .with("finalPrice", calculateFinalPrice(invoice));
    }

    private static byte[] generatePdfFromHtml(String content) {
        byte[] pdfContent = null;
        try (ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream()) {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(content);
            renderer.layout();
            renderer.createPDF(pdfOutputStream, false, 1);
            renderer.finishPDF();
            log.info("Pdf generated from html successfully");
            pdfContent = pdfOutputStream.toByteArray();
        } catch (DocumentException | IOException ex) {
            log.error("Invoice generation failed");
        }
        return pdfContent;
    }

    private void checkIfAccountExists(long accountId) {
        Optional<Account> byId = accountRepository.findById(accountId);
        byId.orElseThrow(() -> new AccountNotFoundException());
    }

    private void checkIfClientExists(long clientId) {
        Optional<Client> byId = clientRepository.findById(clientId);
        byId.orElseThrow(() -> new ClientNotFoundException());
    }

    private Invoice toEntity(CreateInvoiceDto dto) {
        return Invoice.builder()
                .client(clientRepository.findById(dto.getClientId()).get())
                .amountPerUnit(dto.getAmountPerUnit())
                .discount(dto.getDiscount())
                .description(dto.getDescription())
                .quantity(dto.getQuantity())
                .status(dto.getStatus())
                .dateToSend(dto.getDateToSend())
                .subtotal(dto.getSubtotal())
                .total(dto.getSubtotal())
                .dueDate(dto.getDueDate())
                .build();
    }

    private void checkIfExist(Optional<Invoice> invoice) {
        invoice.orElseThrow(InvoiceNotFoundException::new);
    }

    private double calculateFinalPrice(Invoice invoice) {
        double subTotal = invoice.getAmountPerUnit() * invoice.getQuantity();
        return subTotal - subTotal * invoice.getDiscount() / 100;
    }
}
