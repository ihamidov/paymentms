package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreateInvoiceDto;
import az.ingress.edu.dto.CreateInvoiceScheduleDto;
import az.ingress.edu.dto.InvoiceScheduleStatus;
import az.ingress.edu.exception.UnknownPeriodTypeException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Invoice;
import az.ingress.edu.model.InvoiceSchedule;
import az.ingress.edu.repository.InvoiceScheduleRepository;
import az.ingress.edu.service.InvoiceScheduleService;
import az.ingress.edu.service.InvoiceService;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class InvoiceScheduleServiceImpl implements InvoiceScheduleService {

    private InvoiceScheduleRepository invoiceScheduleRepository;

    private InvoiceService invoiceService;

    private AccountServiceImpl accountService;

    @Override
    @Transactional
    public InvoiceSchedule create(long accountId, CreateInvoiceScheduleDto dto) {
        final Account account = accountService.getAccount(accountId);
        dateToSendVerifyAndSet(dto);
        statusVerifyAndSet(dto);
        final InvoiceSchedule invoiceSchedule = map(account, dto);
        final InvoiceSchedule invoiceScheduleSaved = invoiceScheduleRepository.save(invoiceSchedule);
        final Set<Invoice> invoices = createInvoice(accountId, dto);
        invoiceSchedule.setInvoices(invoices);
        invoiceScheduleRepository.save(invoiceSchedule);
        return invoiceScheduleSaved;
    }

    private Set<Invoice> createInvoice(long accountId, CreateInvoiceScheduleDto dto) {
        Set<Invoice> invoices = new HashSet<>();
        for (int i = 0; i < dto.getNumberOfTimesToRepeat(); i++) {
            final LocalDateTime dateToSend = getDate(dto.getDateToSend(), dto.getPeriodType(), i);
            final Invoice invoice = createInvoice(accountId, dto, dateToSend);
            invoices.add(invoice);
        }
        return invoices;
    }

    private Invoice createInvoice(long accountId, CreateInvoiceScheduleDto dto,
            LocalDateTime dateToSend) {
        final CreateInvoiceDto invoice = dto.getInvoice();
        invoice.setDateToSend(dateToSend);
        return invoiceService.createInvoice(accountId, invoice);
    }

    private LocalDateTime getDate(LocalDateTime startDate, String periodType, int periodCount) {
        switch (periodType) {
            case "MONTH":
                return startDate.plusMonths(periodCount);
            case "WEEK":
                return startDate.plusWeeks(periodCount);
            case "DAY":
                return startDate.plusDays(periodCount);
            default:
                throw new UnknownPeriodTypeException(String.format("The period type %s not known", periodType));
        }
    }

    private void statusVerifyAndSet(CreateInvoiceScheduleDto dto) {
        if (dto.getStatus() == null) {
            dto.setStatus(InvoiceScheduleStatus.ACTIVE);
        }
    }

    private InvoiceSchedule map(Account account, CreateInvoiceScheduleDto dto) {
        return InvoiceSchedule.builder()
                .account(account)
                .dateToSend(dto.getDateToSend())
                .numberOfTimesToRepeat(dto.getNumberOfTimesToRepeat())
                .periodType(dto.getPeriodType())
                .status(dto.getStatus())
                .build();
    }

    private void dateToSendVerifyAndSet(CreateInvoiceScheduleDto dto) {
        if (dto.getDateToSend() == null) {
            dto.setDateToSend(LocalDateTime.now());
        }
    }
}
