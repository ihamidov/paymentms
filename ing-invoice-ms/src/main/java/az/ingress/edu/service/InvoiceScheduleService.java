package az.ingress.edu.service;

import az.ingress.edu.dto.CreateInvoiceScheduleDto;
import az.ingress.edu.model.InvoiceSchedule;

public interface InvoiceScheduleService {

    InvoiceSchedule create(long accountId, CreateInvoiceScheduleDto dto);
}
