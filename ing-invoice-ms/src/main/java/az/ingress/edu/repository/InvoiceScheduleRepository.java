package az.ingress.edu.repository;

import az.ingress.edu.model.InvoiceSchedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceScheduleRepository extends CrudRepository<InvoiceSchedule, Long> {

}
