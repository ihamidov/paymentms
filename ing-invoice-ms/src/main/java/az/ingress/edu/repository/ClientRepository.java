package az.ingress.edu.repository;

import az.ingress.edu.model.Client;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

    Optional<Client> findByEmailAndAccountId(String email, long id);

    Page<Client> findAll(Pageable pageable);

}
