package az.ingress.edu.repository;

import az.ingress.edu.model.Account;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountRepository extends PagingAndSortingRepository<Account, Long>,
        JpaSpecificationExecutor<Account> {

    Optional<Account> findAccountByEmail(String email);

    Optional<Account> findAccountByUserUuid(String userUuid);

    Optional<Account> findAccountByPhoneNumber(String phoneNumber);
}
