package az.ingress.edu.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateClientDto {

    @NotNull(message = "Client name can't be empty")
    private String name;

    @NotNull(message = "Client last name can't be empty")
    private String lastName;

    @NotNull(message = "Client email can't be empty")
    private String email;
}
