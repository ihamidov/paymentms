package az.ingress.edu.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateInvoiceScheduleDto {

    private CreateInvoiceDto invoice;

    private LocalDateTime dateToSend;

    private String periodType;

    private Integer numberOfTimesToRepeat;

    private InvoiceScheduleStatus status;

}
