package az.ingress.edu.dto;

import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDto {

    private long cliendId;
    private List<IdDto> client;
    private int amountPerUnit;
    private int quanity;
    private LocalDateTime dueDate;
    private int discount;
    private String status;
    private String description;
}
