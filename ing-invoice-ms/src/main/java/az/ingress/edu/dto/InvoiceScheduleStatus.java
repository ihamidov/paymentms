package az.ingress.edu.dto;

public enum InvoiceScheduleStatus {

    ACTIVE, CANCELLED
}
