package az.ingress.edu.dto;

import az.ingress.edu.repository.spec.SearchCriteria;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchDto {

    List<SearchCriteria> criteria;
}
