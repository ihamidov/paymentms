package az.ingress.edu.dto;

import az.ingress.edu.model.InvoiceStatus;
import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateInvoiceDto {

    @NotNull(message = "Client id can't be empty")
    private long clientId;

    @NotNull(message = "Amount can't be empty")
    private int amountPerUnit;

    @NotNull(message = "Quantity can't be empty")
    private int quantity;

    @NotNull(message = "Discount can't be empty")
    private int discount;

    @NotNull(message = "Subtotal can't be empty")
    private int subtotal;

    @NotNull(message = "Total can't be empty")
    private int total;

    @NotNull(message = "Status can't be empty")
    private InvoiceStatus status;

    @NotNull(message = "Date can't be empty")
    private LocalDateTime dueDate;

    @NotNull(message = "Description can't be empty")
    private String description;

    private LocalDateTime dateToSend;
}
