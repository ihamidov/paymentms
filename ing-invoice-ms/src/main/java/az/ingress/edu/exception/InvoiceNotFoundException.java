package az.ingress.edu.exception;

public class InvoiceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private static final String MESSENGE = "Invoice not found";

    public InvoiceNotFoundException() {
        super(MESSENGE);
    }
}
