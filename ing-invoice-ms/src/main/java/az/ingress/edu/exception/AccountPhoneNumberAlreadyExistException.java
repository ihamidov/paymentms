package az.ingress.edu.exception;

public class AccountPhoneNumberAlreadyExistException extends RuntimeException {
    private static final long serialVersionUID = -3042686055658047285L;

    public AccountPhoneNumberAlreadyExistException(String phoneNumber) {
        super(String.format("Account with phone number: '%s' already exists", phoneNumber));
    }
}

