package az.ingress.edu.exception;

public class InvalidInvoiceIdException extends RuntimeException {

    private static final long serialVersionUID = -1L;

    private static final String MESSENGE = "Invoice ID must be positive";

    public InvalidInvoiceIdException() {
        super(MESSENGE);
    }
}
