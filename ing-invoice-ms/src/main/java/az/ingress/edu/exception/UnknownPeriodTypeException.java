package az.ingress.edu.exception;

public class UnknownPeriodTypeException extends RuntimeException {

    private static final long serialVersionUID = 3836305953954547096L;

    public UnknownPeriodTypeException(String message) {
        super(message);
    }
}
