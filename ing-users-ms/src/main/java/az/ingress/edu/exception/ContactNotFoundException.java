package az.ingress.edu.exception;

public class ContactNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public ContactNotFoundException(long id) {
        super(String.format("Contact with id %d not found.", id));
    }
}
