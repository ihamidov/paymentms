package az.ingress.edu.exception;

public class InvalidCaptchaException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public InvalidCaptchaException() {
        super(String.format("Invalid captcha"));
    }
}
