package az.ingress.edu.exception;

public class UnauthorizedOperationException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public UnauthorizedOperationException() {
        super("You are not allowed to perform this operation!");
    }
}
