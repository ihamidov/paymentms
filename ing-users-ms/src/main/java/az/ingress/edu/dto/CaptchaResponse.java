package az.ingress.edu.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Calendar;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaptchaResponse {

    private Boolean success;

    private Calendar timestamp;

    private String hostname;

    @JsonProperty("error-codes")
    private List<String> errorCodes;
}
