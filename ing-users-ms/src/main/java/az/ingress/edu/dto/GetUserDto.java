package az.ingress.edu.dto;

import az.ingress.edu.model.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetUserDto {

    private long id;

    private String username;

    private String firstname;

    private String lastname;

    private ContactInfoDto contactDetails;

    private UserType type;

    private String photo;

    private String description;
}
