package az.ingress.edu.dto;

import az.ingress.edu.model.UserType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserFilterDto {

    UserType type;

    String order = "id";

    String direction = "ASC";
}
