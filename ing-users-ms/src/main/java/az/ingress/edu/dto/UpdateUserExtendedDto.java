package az.ingress.edu.dto;

import az.ingress.edu.model.UserType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UpdateUserExtendedDto extends UpdateUserDto {

    @NotNull(message = "User type can't be empty")
    private UserType type;

    @Builder(builderMethodName = "updateUserExtendedDtoBuilder")
    UpdateUserExtendedDto(String firstname, String lastname, String photo, ContactInfoDto contactInfo, String desc,
                          UserType type) {
        super(firstname, lastname, photo, contactInfo, desc);
        this.type = type;
    }
}
