package az.ingress.edu.dto;

import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorDto {

    private int code;
    private String technicalMessage;
    private String userMessage;
    private Calendar timestamp;
}
