package az.ingress.edu.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UpdateUserDto {

    @NotEmpty(message = "Firstname can't be empty")
    private String firstname;

    @NotEmpty(message = "Lastname can't be empty")
    private String lastname;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;

    @Valid
    private ContactInfoDto contactInfoDto;

    private String description;
}
