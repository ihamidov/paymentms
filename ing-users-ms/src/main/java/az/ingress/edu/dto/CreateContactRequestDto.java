package az.ingress.edu.dto;

import az.ingress.edu.model.ContactRequest;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateContactRequestDto {

    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

    @NotEmpty(message = "Email can't be empty")
    @Email(message = "Invalid email")
    private String email;

    @NotEmpty(message = "Phone can't be empty")
    private String phone;

    @NotEmpty(message = "Message can't be empty")
    @Size(min = 10, max = 1000, message = "Min 10 max 1000 characters allowed")
    private String message;

    private String captchaResponse;

    public ContactRequest toContactRequest() {
        return ContactRequest.builder()
                .email(email)
                .firstName(firstName)
                .lastName(lastName)
                .phone(phone)
                .message(message)
                .build();
    }
}
