package az.ingress.edu.controller;

import az.ingress.edu.dto.ContactFilterDto;
import az.ingress.edu.dto.CreateContactRequestDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.service.ContactService;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("/contact")
@AllArgsConstructor
public class ContactController {

    private final ContactService contactService;
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";


    @PostMapping
    IdDto createContactRequest(@Validated @RequestBody CreateContactRequestDto contactRequest) {
        return new IdDto(contactService.createContactRequest(contactRequest).getId());
    }

    @GetMapping("/{id}")
    public ContactRequest getContactRequest(@Positive(message = "Request id must be positive") @PathVariable long id) {
        return contactService.getContactRequest(id);
    }

    @GetMapping("/list")
    public Page<ContactRequest> getContactRequestList(@RequestParam(defaultValue = "0")
                                                      @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE)
                                                              int page,
                                                      @RequestParam(defaultValue = "10")
                                                      @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                                      ContactFilterDto contactFilter) {
        return contactService.getContactRequestList(page, size, contactFilter);
    }
}
