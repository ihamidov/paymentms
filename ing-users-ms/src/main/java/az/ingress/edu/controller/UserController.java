package az.ingress.edu.controller;

import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.dto.UpdateUserExtendedDto;
import az.ingress.edu.dto.UserComboDto;
import az.ingress.edu.dto.UserFilterDto;
import az.ingress.edu.model.User;
import az.ingress.edu.service.UserService;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private static final String USER_ID_MUST_BE_POSITIVE = "User id must be positive";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";

    private final UserService userService;
    private final RabbitMqConfig rabbitMqConfig;
    private final RabbitTemplate rabbitTemplate;

    @GetMapping("/profile/{username}")
    public GetUserDto getUserProfile(@PathVariable String username) {
        return userService.getUser(username);
    }

    @GetMapping("/{username}")
    public GetUserDto getUser(@PathVariable String username) {
        userService.saveCurrentUserIfNotPersisted();
        User currentUser = userService.getCurrentUser();
        rabbitTemplate.convertAndSend(rabbitMqConfig.getCourseMsExchange(),
                rabbitMqConfig.getCourseMsUserCreateRoutingKey(), currentUser);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getArticleMsExchange(),
                rabbitMqConfig.getArticleMsUserCreateRoutingKey(), currentUser);
        return userService.getUser(username);
    }

    @PutMapping("profile/{id}")
    public IdDto updateUser(@Validated @RequestBody UpdateUserDto updateUserDto,
                            @Positive(message = USER_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        userService.saveCurrentUserIfNotPersisted();
        User updatedUser = userService.updateUser(updateUserDto, id);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getCourseMsExchange(),
                rabbitMqConfig.getCourseMsUserUpdateRoutingKey(), updatedUser);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getArticleMsExchange(),
                rabbitMqConfig.getArticleMsUserUpdateRoutingKey(), updatedUser);
        return new IdDto(updatedUser.getId());
    }

    @PutMapping("/{id}")
    public IdDto updateUserForManagement(@Validated @RequestBody UpdateUserExtendedDto updateUserDto,
                                         @Positive(message = USER_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        userService.saveCurrentUserIfNotPersisted();
        User updatedUser = userService.updateUserForManagement(updateUserDto, id);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getCourseMsExchange(),
                rabbitMqConfig.getCourseMsUserUpdateRoutingKey(), updatedUser);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getArticleMsExchange(),
                rabbitMqConfig.getArticleMsUserUpdateRoutingKey(), updatedUser);
        return new IdDto(updatedUser.getId());
    }

    @GetMapping("/list")
    public Page<UserComboDto> getUserListForManagement(@RequestParam(defaultValue = "0")
                                                         @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE)
                                                                     int page,
                                                         @RequestParam(defaultValue = "10")
                                                         @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                                         UserFilterDto userFilter) {
        return userService.getAllUsers(page, size, userFilter);
    }
}
