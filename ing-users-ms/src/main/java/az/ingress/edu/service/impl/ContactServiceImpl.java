package az.ingress.edu.service.impl;

import az.ingress.edu.config.MailServiceConfig;
import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.dto.ContactFilterDto;
import az.ingress.edu.dto.CreateContactRequestDto;
import az.ingress.edu.dto.EmailDto;
import az.ingress.edu.exception.ContactNotFoundException;
import az.ingress.edu.exception.InvalidCaptchaException;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.repository.ContactRepository;
import az.ingress.edu.service.CaptchaService;
import az.ingress.edu.service.ContactService;
import az.ingress.edu.service.MailService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ContactServiceImpl implements ContactService {

    private static final String CONTACT_REQUEST_SUBJECT = "New Contact Request has been submitted";
    private static final String CONTACT_MESSAGE_TEMPLATE = "New Contact request:%n"
            + "First Name: %s%n"
            + "Last Name: %s%n"
            + "Email: %s%n"
            + "Phone: %s%n"
            + "Message: %s%n";

    private final ContactRepository contactRepository;
    private final CaptchaService captchaService;
    private final MailService mailService;
    private final MailServiceConfig mailServiceConfig;
    private final RabbitMqConfig rabbitMqConfig;
    private final RabbitTemplate rabbitTemplate;


    @Override
    public ContactRequest createContactRequest(CreateContactRequestDto requestDto) {

        if (!captchaService.isValidCaptcha(requestDto.getCaptchaResponse())) {
            throw new InvalidCaptchaException();
        }

        ContactRequest request = requestDto.toContactRequest();
        EmailDto emailDto = prepareEmail(request);
        mailService.sendEmail(emailDto);
        rabbitTemplate.convertAndSend(rabbitMqConfig.getUserMsExchange(),
                rabbitMqConfig.getUserMsMailServiceRoutingKey(), emailDto);
        return contactRepository.save(request);
    }

    @Override
    public ContactRequest getContactRequest(long id) {
        return contactRepository.findById(id)
                .orElseThrow(() -> new ContactNotFoundException(id));
    }

    @Override
    public Page<ContactRequest> getContactRequestList(int page, int size, ContactFilterDto filterDto) {
        return contactRepository.findAll(PageRequest
                .of(page, size, Sort.by(Sort.Direction.fromString(filterDto.getDirection()), filterDto.getOrder())));
    }

    private String getContactMessageForEmailService(ContactRequest contactRequest) {
        return String.format(CONTACT_MESSAGE_TEMPLATE, contactRequest.getFirstName(),
                contactRequest.getLastName(), contactRequest.getEmail(), contactRequest.getPhone(),
                contactRequest.getMessage());
    }

    private EmailDto prepareEmail(ContactRequest contactRequest) {
        return EmailDto.builder()
                .from(mailServiceConfig.getHostMail())
                .to(mailServiceConfig.getInfoMail())
                .subject(CONTACT_REQUEST_SUBJECT)
                .message(getContactMessageForEmailService(contactRequest)).build();
    }
}
