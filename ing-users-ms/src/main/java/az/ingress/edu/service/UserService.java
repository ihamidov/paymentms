package az.ingress.edu.service;

import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.dto.UpdateUserExtendedDto;
import az.ingress.edu.dto.UserComboDto;
import az.ingress.edu.dto.UserFilterDto;
import az.ingress.edu.model.User;
import org.springframework.data.domain.Page;

public interface UserService {

    User updateUser(UpdateUserDto updateUserDto, long id);

    User updateUserForManagement(UpdateUserExtendedDto userDto, long id);

    GetUserDto getUser(String username);

    void saveCurrentUserIfNotPersisted();

    Page<UserComboDto> getAllUsers(int page, int size, UserFilterDto filter);

    User getCurrentUser();
}
