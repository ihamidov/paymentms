package az.ingress.edu.service.impl;

import az.ingress.edu.dto.ContactInfoDto;
import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.dto.UpdateUserExtendedDto;
import az.ingress.edu.dto.UserComboDto;
import az.ingress.edu.dto.UserFilterDto;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.User;
import az.ingress.edu.model.UserType;
import az.ingress.edu.repository.UserRepository;
import az.ingress.edu.service.UserService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AccessToken accessToken;

    @Override
    public User updateUser(UpdateUserDto updateUserDto, long id) {
        User userToUpdate = retrieveUser(id);
        checkUserAccess(userToUpdate);
        setUpdatedProperties(updateUserDto, userToUpdate);
        return userRepository.save(userToUpdate);
    }

    @Override
    public User updateUserForManagement(UpdateUserExtendedDto userDto, long id) {
        User userToUpdate = retrieveUser(id);
        setUpdatedProperties(userDto, userToUpdate);
        userToUpdate.setType(userDto.getType());
        return userRepository.save(userToUpdate);
    }

    @Override
    public GetUserDto getUser(String username) {
        return userRepository.findByUsername(username).orElseThrow(() ->
                new UserNotFoundException(username)).toGetUserDto();
    }

    @Override
    public void saveCurrentUserIfNotPersisted() {
        User currentUser = getCurrentUser();
        userRepository.findByUsername(currentUser.getUsername()).orElseGet(() ->
                userRepository.save(currentUser));
    }

    @Override
    public Page<UserComboDto> getAllUsers(int page, int size, UserFilterDto filter) {
        Pageable pageable = PageRequest
                .of(page, size, Sort.Direction.fromString(filter.getDirection()), filter.getOrder());
        Page<User> usersPage = userRepository.findAll(filter.getType(),pageable);
        List<UserComboDto> userComboDtoList = convertUserToUserComboDto(usersPage.getContent());
        return new PageImpl<>(userComboDtoList,pageable,usersPage.getTotalElements());
    }

    @Override
    public User getCurrentUser() {
        return User.builder()
                .username(accessToken.getPreferredUsername())
                .contactDetails(ContactInfo.builder()
                        .email(accessToken.getEmail())
                        .phone(accessToken.getPhoneNumber())
                        .build())
                .type(UserType.USER)
                .firstname(accessToken.getGivenName())
                .lastname(accessToken.getFamilyName())
                .build();
    }

    private void setUpdatedProperties(UpdateUserDto updateUserDto, User currentUser) {
        currentUser.setPhoto(updateUserDto.getPhoto());
        currentUser.setDescription(updateUserDto.getDescription());
        currentUser.setFirstname(updateUserDto.getFirstname());
        currentUser.setLastname(updateUserDto.getLastname());
        ContactInfoDto contactInfoDto = updateUserDto.getContactInfoDto();
        ContactInfo contactInfo = currentUser.getContactDetails();
        contactInfo.setEmail(contactInfoDto.getEmail());
        contactInfo.setAddress(contactInfoDto.getAddress());
        contactInfo.setPhone(contactInfoDto.getPhone());
        contactInfo.setFb(contactInfoDto.getFb());
        contactInfo.setTwitter(contactInfoDto.getTwitter());
        contactInfo.setLinkedin(contactInfoDto.getLinkedin());
        contactInfo.setWebsite(contactInfoDto.getWebsite());
    }

    private void checkUserAccess(User user) {
        if (!user.getUsername().equals(accessToken.getPreferredUsername())) {
            throw new UnauthorizedOperationException();
        }
    }

    private User retrieveUser(long id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    private List<UserComboDto> convertUserToUserComboDto(List<User> users) {
        return users.stream().map(user -> UserComboDto.builder()
                .username(user.getUsername())
                .userType(user.getType())
                .firstname(user.getFirstname())
                .lastname(user.getLastname())
                .photo(user.getPhoto())
                .build()).collect(Collectors.toList());
    }
}
