package az.ingress.edu.service;

import az.ingress.edu.dto.EmailDto;

public interface MailService {
    void sendEmail(EmailDto emailDto);
}
