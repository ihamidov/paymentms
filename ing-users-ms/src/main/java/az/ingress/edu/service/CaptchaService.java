package az.ingress.edu.service;

public interface CaptchaService {

    boolean isValidCaptcha(String response);
}
