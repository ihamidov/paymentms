package az.ingress.edu.service.impl;

import az.ingress.edu.config.CaptchaConfiguration;
import az.ingress.edu.dto.CaptchaResponse;
import az.ingress.edu.service.CaptchaService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class CaptchaServiceImpl implements CaptchaService {

    private static final String GOOGLE_RECAPTCHA_ENDPOINT = "https://www.google.com/recaptcha/api/siteverify";

    private final RestTemplate restTemplate;
    private final CaptchaConfiguration captchaConfiguration;

    @Override
    public boolean isValidCaptcha(String response) {
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.add("secret", captchaConfiguration.getSecret());
        valueMap.add("response", response);
        CaptchaResponse captchaResponse = restTemplate
                .postForObject(GOOGLE_RECAPTCHA_ENDPOINT, valueMap, CaptchaResponse.class);
        if (captchaResponse == null) {
            return false;
        }
        return captchaResponse.getSuccess();
    }
}
