package az.ingress.edu.service.impl;

import az.ingress.edu.config.MailServiceConfig;
import az.ingress.edu.dto.EmailDto;
import az.ingress.edu.service.MailService;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;
    private final MailServiceConfig mailServiceConfig;

    @Override
    @RabbitListener(queues = {"${rabbit.user-ms-mail-service-queue}"})
    public void sendEmail(EmailDto emailDto) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailServiceConfig.getHostMail());
        message.setTo(emailDto.getTo());
        message.setSubject(emailDto.getSubject());
        message.setText(emailDto.getMessage());
        javaMailSender.send(message);
    }
}
