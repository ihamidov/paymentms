package az.ingress.edu.service;

import az.ingress.edu.dto.ContactFilterDto;
import az.ingress.edu.dto.CreateContactRequestDto;
import az.ingress.edu.model.ContactRequest;
import org.springframework.data.domain.Page;

public interface ContactService {

    ContactRequest createContactRequest(CreateContactRequestDto request);

    ContactRequest getContactRequest(long id);

    Page<ContactRequest> getContactRequestList(int page, int size, ContactFilterDto filter);
}
