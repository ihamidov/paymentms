package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("google.recaptcha")
@EnableConfigurationProperties(CaptchaConfiguration.class)
@Setter
@Getter
public class CaptchaConfiguration {

    private String secret;

}

