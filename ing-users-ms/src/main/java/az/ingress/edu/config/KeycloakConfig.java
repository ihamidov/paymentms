package az.ingress.edu.config;

import az.ingress.edu.exception.IngressAccessDeniedHandler;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Profile("!test")
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class KeycloakConfig extends KeycloakWebSecurityConfigurerAdapter {

    private static final String USERS = "/user/**";
    private static final String CONTACT = "/contact/**";
    private static final String USER_PROFILE = "/user/profile/**";
    private static final String USER_LIST = "/user/list/**";
    private static final String ROLE_USER = "USER";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_MANAGER = "COURSE_MANAGER";

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {

        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, CONTACT)
                .permitAll()
                .antMatchers(CONTACT)
                .hasAnyRole(ROLE_ADMIN, ROLE_MANAGER)
                .antMatchers(HttpMethod.GET, USER_PROFILE)
                .permitAll()
                .antMatchers(HttpMethod.PUT, USER_PROFILE)
                .hasRole(ROLE_USER)
                .antMatchers(HttpMethod.PUT, USERS)
                .hasRole(ROLE_ADMIN)
                .antMatchers(USER_LIST).hasAnyRole(ROLE_ADMIN, ROLE_MANAGER)
                .antMatchers(USERS).hasAnyRole(ROLE_MANAGER, ROLE_ADMIN, ROLE_USER)
                .anyRequest().permitAll()
                .and()
                .csrf().disable();
    }

    @Bean
    public IngressAccessDeniedHandler accessDeniedHandler() {
        return new IngressAccessDeniedHandler();
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AccessToken getAccessToken() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return ((KeycloakAuthenticationToken) request.getUserPrincipal())
                .getAccount().getKeycloakSecurityContext().getToken();
    }
}
