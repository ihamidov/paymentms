package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("rabbit")
@EnableConfigurationProperties(RabbitMqConfig.class)
@Setter
@Getter
public class RabbitMqConfig {

    private String courseMsUserCreateQueue;
    private String courseMsUserUpdateQueue;
    private String courseMsExchange;
    private String courseMsUserCreateRoutingKey;
    private String articleMsUserCreateRoutingKey;
    private String courseMsUserUpdateRoutingKey;
    private String articleMsExchange;
    private String articleMsUserCreateQueue;
    private String articleMsUserUpdateQueue;
    private String articleMsUserUpdateRoutingKey;
    private String userMsExchange;
    private String userMsMailServiceQueue;
    private String userMsMailServiceRoutingKey;

    @Bean
    public TopicExchange articleMsExchange() {
        return new TopicExchange(articleMsExchange);
    }

    @Bean
    public Queue articleMsUserCreateQueue() {
        return new Queue(articleMsUserCreateQueue);
    }

    @Bean
    public Binding articleMsUserCreateBinding() {
        return BindingBuilder.bind(articleMsUserCreateQueue())
                .to(articleMsExchange()).with(articleMsUserCreateRoutingKey);
    }

    @Bean
    public Queue articleMsUserUpdateQueue() {
        return new Queue(articleMsUserUpdateQueue);
    }

    @Bean
    public Binding articleMsUserUpdateBinding() {
        return BindingBuilder.bind(articleMsUserUpdateQueue())
                .to(articleMsExchange()).with(articleMsUserUpdateRoutingKey);
    }

    @Bean
    public TopicExchange courseMsExchange() {
        return new TopicExchange(courseMsExchange);
    }

    @Bean
    public Queue courseMsUserCreateQueue() {
        return new Queue(courseMsUserCreateQueue);
    }

    @Bean
    public Binding courseMsUserCreateBinding() {
        return BindingBuilder.bind(courseMsUserCreateQueue())
                .to(courseMsExchange()).with(courseMsUserCreateRoutingKey);
    }

    @Bean
    public Queue courseMsUserUpdateQueue() {
        return new Queue(courseMsUserUpdateQueue);
    }

    @Bean
    public Binding courseMsUserUpdateBinding() {
        return BindingBuilder.bind(courseMsUserUpdateQueue())
                .to(courseMsExchange()).with(courseMsUserUpdateRoutingKey);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public TopicExchange userMsExchange() {
        return new TopicExchange(userMsExchange);
    }

    @Bean
    public Queue userMsMailServiceQueue() {
        return new Queue(userMsMailServiceQueue);
    }

    @Bean
    public Binding userMsMailServiceRoutingKey() {
        return BindingBuilder.bind(userMsMailServiceQueue())
                .to(userMsExchange()).with(userMsMailServiceRoutingKey);
    }
}
