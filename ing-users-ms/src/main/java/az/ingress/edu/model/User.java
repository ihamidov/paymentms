package az.ingress.edu.model;

import az.ingress.edu.dto.ContactInfoDto;
import az.ingress.edu.dto.GetUserDto;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    @NotEmpty(message = "Firstname can't be empty")
    private String firstname;

    @NotEmpty(message = "Lastname can't be empty")
    private String lastname;

    @OneToOne(cascade = CascadeType.ALL)
    private ContactInfo contactDetails;

    @Size(max = 2000,message = "Max 2000 characters allowed")
    private String photo;

    @Enumerated(EnumType.STRING)
    private UserType type;

    @Lob
    private String description;

    public GetUserDto toGetUserDto() {
        return GetUserDto.builder()
                .id(id)
                .username(username)
                .firstname(firstname)
                .lastname(lastname)
                .photo(photo)
                .description(description)
                .type(type)
                .contactDetails(ContactInfoDto
                        .builder()
                        .address(contactDetails.getAddress())
                        .email(contactDetails.getEmail())
                        .phone(contactDetails.getPhone())
                        .fb(contactDetails.getFb())
                        .twitter(contactDetails.getTwitter())
                        .linkedin(contactDetails.getLinkedin())
                        .website(contactDetails.getWebsite())
                        .build())
                .build();
    }
}
