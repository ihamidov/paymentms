package az.ingress.edu.model;

public enum UserType {
    USER, TEACHER, STUDENT
}
