package az.ingress.edu.repository;

import az.ingress.edu.model.User;
import az.ingress.edu.model.UserType;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>,
        JpaSpecificationExecutor<User> {

    @Override
    List<User> findAll();

    default Page<User> findAll(UserType type, Pageable pageable) {
        return findAll((root, criteriaQuery, cb) ->
                cb.and(type == null
                        ? cb.conjunction() : root.get("type").in(type)), pageable);
    }

    Optional<User> findByUsername(String username);
}
