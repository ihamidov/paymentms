package az.ingress.edu.repository;

import az.ingress.edu.model.ContactRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends PagingAndSortingRepository<ContactRequest, Long> {
}
