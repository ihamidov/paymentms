CREATE TABLE contact_info
(
    id       bigint(20) NOT NULL AUTO_INCREMENT,
    address  varchar(255) DEFAULT NULL,
    email    varchar(255) DEFAULT NULL,
    fb       varchar(255) DEFAULT NULL,
    linkedin varchar(255) DEFAULT NULL,
    phone    varchar(255) DEFAULT NULL,
    twitter  varchar(255) DEFAULT NULL,
    website  varchar(255) DEFAULT NULL,
    PRIMARY KEY (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE user
(
    id                 bigint(20) NOT NULL AUTO_INCREMENT,
    firstname          varchar(255) DEFAULT NULL,
    lastname           varchar(255) DEFAULT NULL,
    photo_id           bigint(20)   DEFAULT NULL,
    description        longtext DEFAULT NULL,
    username           varchar(255) UNIQUE,
    contact_details_id bigint(20)   DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (contact_details_id) REFERENCES contact_info (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
