create table contact_request
(
    id         bigint auto_increment
        primary key,
    email      varchar(255)  null,
    first_name varchar(255)  null,
    last_name  varchar(255)  null,
    message    varchar(1000) null,
    phone      varchar(255)  null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

