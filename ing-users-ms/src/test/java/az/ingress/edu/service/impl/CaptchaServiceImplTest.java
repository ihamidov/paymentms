package az.ingress.edu.service.impl;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.config.CaptchaConfiguration;
import az.ingress.edu.dto.CaptchaResponse;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class CaptchaServiceImplTest {

    private static final String RESPONSE = "RESPONSE-TEXT";
    private static final String GOOGLE_CAPTCHA_ENDPOINT = "https://www.google.com/recaptcha/api/siteverify";
    private static final String SECRET = "This is so secret";

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private CaptchaConfiguration captchaConfiguration;

    @InjectMocks
    private CaptchaServiceImpl captchaService;

    @Test
    public void givenResponseValidateCaptchaSuccess() {
        CaptchaResponse captchaResponse = CaptchaResponse.builder()
                .hostname("hostname").timestamp(Calendar.getInstance()).success(true).build();
        when(captchaConfiguration.getSecret()).thenReturn(SECRET);
        when(restTemplate.postForObject(anyString(), any(MultiValueMap.class), any()))
                .thenReturn(captchaResponse);
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        valueMap.add("secret", SECRET);
        valueMap.add("response", RESPONSE);

        boolean result = captchaService.isValidCaptcha(RESPONSE);
        assertThat(result).isEqualTo(true);
        verify(restTemplate).postForObject(GOOGLE_CAPTCHA_ENDPOINT, valueMap, CaptchaResponse.class);

    }

    @Test
    public void givenResponseValidateCaptchaReturnFail() {

        when(captchaConfiguration.getSecret()).thenReturn(SECRET);

        boolean result = captchaService.isValidCaptcha(RESPONSE);
        assertThat(result).isEqualTo(false);
    }
}
