package az.ingress.edu.service.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.ContactInfoDto;
import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.dto.UpdateUserExtendedDto;
import az.ingress.edu.dto.UserComboDto;
import az.ingress.edu.dto.UserFilterDto;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.User;
import az.ingress.edu.model.UserType;
import az.ingress.edu.repository.UserRepository;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private static final String USERNAME = "ichigo";
    private static final String FIRSTNAME = "Ichigo";
    private static final String LASTNAME = "Kurasaki";
    private static final String EMAIL = "info@ichigo.com";
    private static final String UNAUTHORIZED_USER = "otar";
    private static final String PHOTO = "www.dummy.com/myprofile.png";

    private UpdateUserDto updateUserDto;
    private UpdateUserExtendedDto updateUserExtendedDto;
    private User user;

    @Mock
    private UserRepository userRepository;

    @Mock
    private AccessToken accessToken;

    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setUp() {
        user = User.builder()
                .id(1L)
                .firstname(FIRSTNAME)
                .lastname(LASTNAME)
                .username(USERNAME)
                .type(UserType.USER)
                .contactDetails(ContactInfo.builder()
                        .email(EMAIL).build())
                .build();
        ContactInfoDto contactInfo = ContactInfoDto.builder()
                .email(EMAIL).build();
        updateUserDto = UpdateUserDto.builder().firstname(FIRSTNAME).lastname(LASTNAME).photo(PHOTO)
                .contactInfoDto(contactInfo)
                .build();
        updateUserExtendedDto = UpdateUserExtendedDto
                .updateUserExtendedDtoBuilder().firstname(FIRSTNAME).lastname(LASTNAME).contactInfo(contactInfo)
                .type(UserType.TEACHER)
                .photo(PHOTO)
                .build();
    }

    @Test
    public void givenNonExistingUserIdGetUserExpectException() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.getUser(USERNAME)).isInstanceOf(UserNotFoundException.class);
        verify(userRepository).findByUsername(USERNAME);
    }

    @Test
    public void givenExistingUserIdGetUser() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));

        GetUserDto userDto = userService.getUser(USERNAME);

        assertThat(userDto.getId()).isEqualTo(user.getId());
        assertThat(userDto.getFirstname()).isEqualTo(user.getFirstname());
        assertThat(userDto.getLastname()).isEqualTo(user.getLastname());
        assertThat(userDto.getContactDetails().getEmail()).isEqualTo(user.getContactDetails().getEmail());
        verify(userRepository).findByUsername(USERNAME);
    }

    @Test
    public void givenNonExistingUserUpdateUserExpectException() {
        long id = 1L;
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.updateUser(updateUserDto, id))
                .isInstanceOf(UserNotFoundException.class);
        verify(userRepository).findById(id);
    }

    @Test
    public void givenNonExistingUserUpdateUserForManagementExpectException() {
        long id = 1L;
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.updateUserForManagement(updateUserExtendedDto, id))
                .isInstanceOf(UserNotFoundException.class);
        verify(userRepository).findById(id);
    }

    @Test
    public void givenUserSameWithLoggedUserUpdateUser() {
        long id = 1L;
        ContactInfo contactInfo = ContactInfo.builder()
                .email(EMAIL).build();
        User updatedUser = new User(1L, USERNAME, FIRSTNAME, LASTNAME, contactInfo,PHOTO, UserType.USER,null);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(accessToken.getPreferredUsername()).thenReturn(user.getUsername());
        when(userRepository.save(updatedUser)).thenReturn(updatedUser);

        User actualUser = userService.updateUser(updateUserDto, id);

        assertThat(actualUser.getId()).isEqualTo(id);
        verify(userRepository).findById(id);
        verify(userRepository).save(updatedUser);
    }

    @Test
    public void givenUserUpdateUserForManagement() {
        long id = 1L;
        ContactInfo contactInfo = ContactInfo.builder()
                .email(EMAIL).build();
        User updatedUser = new User(1L, USERNAME, FIRSTNAME, LASTNAME, contactInfo, PHOTO, UserType.TEACHER,null);
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(userRepository.save(updatedUser)).thenReturn(updatedUser);

        User actualUser = userService.updateUserForManagement(updateUserExtendedDto, id);

        assertThat(actualUser.getId()).isEqualTo(id);
        assertThat(actualUser.getType()).isEqualTo(updateUserExtendedDto.getType());
        verify(userRepository).findById(id);
        verify(userRepository).save(updatedUser);
    }

    @Test
    public void givenUserNotSameWithLoggedUserUpdateUser() {
        long id = 1L;
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
        when(accessToken.getPreferredUsername()).thenReturn(UNAUTHORIZED_USER);

        assertThatThrownBy(() -> userService.updateUser(updateUserDto, id))
                .isInstanceOf(UnauthorizedOperationException.class);
        verify(userRepository).findById(id);
    }

    @Test
    public void givenLoggedUserPersistedSaveCurrentUserIfNotPersisted() {

        when(accessToken.getPreferredUsername()).thenReturn(user.getUsername());
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user));
        userService.saveCurrentUserIfNotPersisted();

        verify(userRepository).findByUsername(user.getUsername());
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void givenLoggedUserNotPersistedSaveCurrentUserIfNotPersisted() {
        final User userToSave = User.builder()
                .username(USERNAME)
                .firstname(FIRSTNAME)
                .lastname(LASTNAME)
                .type(UserType.USER)
                .contactDetails(ContactInfo.builder()
                        .email(EMAIL).build())
                .build();
        when(accessToken.getPreferredUsername()).thenReturn(USERNAME);
        when(accessToken.getGivenName()).thenReturn(FIRSTNAME);
        when(accessToken.getFamilyName()).thenReturn(LASTNAME);
        when(accessToken.getEmail()).thenReturn(EMAIL);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

        userService.saveCurrentUserIfNotPersisted();

        verify(userRepository).findByUsername(USERNAME);
        verify(userRepository).save(userToSave);
    }

    @Test
    public void givenUserFilterGetUserList() {
        int page = 0;
        int size = 1;
        UserFilterDto filter = UserFilterDto.builder().direction("ASC").order("id").type(UserType.USER).build();
        PageRequest pageRequest = PageRequest.of(page,size,Sort.by("id").ascending());
        Page<User> userPage = new PageImpl<>(Collections.singletonList(user),pageRequest,1);
        when(userRepository.findAll(any(UserType.class),any(PageRequest.class)))
                .thenReturn(userPage);
        Page<UserComboDto> actualPage = userService.getAllUsers(page,size,filter);

        assertThat(actualPage.getTotalPages()).isEqualTo(userPage.getTotalPages());
        assertThat(actualPage.getTotalElements()).isEqualTo(userPage.getTotalElements());
        assertThat(actualPage.getContent().size()).isEqualTo(userPage.getContent().size());
        verify(userRepository).findAll(UserType.USER,pageRequest);
    }

}
