package az.ingress.edu.service.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.ContactFilterDto;
import az.ingress.edu.dto.CreateContactRequestDto;
import az.ingress.edu.exception.ContactNotFoundException;
import az.ingress.edu.exception.InvalidCaptchaException;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.repository.ContactRepository;
import az.ingress.edu.service.CaptchaService;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceImplTest {

    private static final String CAPTCHA_RESPONSE = "DUMMY CAPTCHA RESPONSE";
    private static final String INFO_MAIL = "info@ingress.az";
    private static final String EMAIL = "otarkhan.mv@gmail.com";
    private static final String FIRST_NAME = "otaru";
    private static final String LAST_NAME = "mv";
    private static final String PHONE = "5555555";
    private static final String DUMMY_MESSAGE = "Simple dummy message!";

    @Mock
    private ContactRepository contactRepository;
    @Mock
    private CaptchaService captchaService;
    @InjectMocks
    private ContactServiceImpl contactService;

    private CreateContactRequestDto contactRequestDto;
    private ContactRequest request;

    @Before
    public void setUp() {
        request = ContactRequest.builder()
                .id(1L)
                .email(INFO_MAIL)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .phone(PHONE)
                .message(DUMMY_MESSAGE).build();
        contactRequestDto = CreateContactRequestDto
                .builder()
                .email(EMAIL)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .phone(PHONE)
                .captchaResponse(CAPTCHA_RESPONSE)
                .message(DUMMY_MESSAGE).build();
    }

    @Test
    public void givenContactRequestAndInvalidResponseCreateContactExpectException() {
        when(captchaService.isValidCaptcha(anyString())).thenReturn(false);

        assertThatThrownBy(() -> contactService.createContactRequest(contactRequestDto))
                .isInstanceOf(InvalidCaptchaException.class);

        verify(captchaService).isValidCaptcha(CAPTCHA_RESPONSE);
    }

    @Test
    public void givenNonContactRequestIdGetContactExpectException() {
        long id = 1;
        when(contactRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> contactService.getContactRequest(id))
                .isInstanceOf(ContactNotFoundException.class);

        verify(contactRepository).findById(id);
    }

    @Test
    public void givenExistingContactRequestGetContact() {
        long id = 1;

        when(contactRepository.findById(anyLong())).thenReturn(Optional.of(request));

        ContactRequest result = contactService.getContactRequest(id);

        assertThat(result.getId()).isEqualTo(request.getId());
        verify(contactRepository).findById(id);
    }

    @Test
    public void givenContactFilterGetContactList() {
        int page = 0;
        int size = 1;
        ContactFilterDto filter = ContactFilterDto.builder().direction("ASC").order("id").build();
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("id").ascending());
        Page<ContactRequest> contactPage = new PageImpl<>(Collections.singletonList(request), pageRequest, 1);
        when(contactRepository.findAll(any(PageRequest.class)))
                .thenReturn(contactPage);
        Page<ContactRequest> actualPage = contactService.getContactRequestList(page, size, filter);

        assertThat(actualPage.getTotalPages()).isEqualTo(contactPage.getTotalPages());
        assertThat(actualPage.getTotalElements()).isEqualTo(contactPage.getTotalElements());
        assertThat(actualPage.getContent().size()).isEqualTo(contactPage.getContent().size());
        verify(contactRepository).findAll(pageRequest);
    }
}
