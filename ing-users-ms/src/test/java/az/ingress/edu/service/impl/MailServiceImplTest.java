package az.ingress.edu.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.config.MailServiceConfig;
import az.ingress.edu.dto.EmailDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RunWith(MockitoJUnitRunner.class)
public class MailServiceImplTest {

    private static final String HOST_MAIL = "info@ingress.az";

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private MailServiceConfig mailServiceConfig;

    @InjectMocks
    private MailServiceImpl mailService;

    @Test
    public void givenEmailDtoSendEmail() {
        EmailDto emailDto = EmailDto
                .builder()
                .from(HOST_MAIL).to("otarkhan.mv@gmail.com").subject("Subject").message("Message").build();
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(emailDto.getFrom());
        simpleMailMessage.setTo(emailDto.getTo());
        simpleMailMessage.setSubject(emailDto.getSubject());
        simpleMailMessage.setText(emailDto.getMessage());
        when(mailServiceConfig.getHostMail()).thenReturn(HOST_MAIL);

        mailService.sendEmail(emailDto);

        verify(javaMailSender).send(simpleMailMessage);
    }
}
