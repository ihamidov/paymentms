package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.User;
import az.ingress.edu.service.UserService;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@ActiveProfiles("test")
public class GetUserControllerTest {

    private static final String ID = "$.id";
    private static final String USERNAME = "ichigo";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_USER_PATH = "/user/{username}";
    private static final String ARTICLE_MS = "article-ms";
    private static final String COURSE_MS = "course-ms";
    private static final String COURSE_USER_CREATE_KEY = "course-user-create-routing-key";
    private static final String ARTICLE_USER_CREATE_KEY = "article-user-create-routing-key";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private RabbitMqConfig rabbitMqConfig;

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Before
    public void setUp() {
        when(rabbitMqConfig.getArticleMsExchange()).thenReturn(ARTICLE_MS);
        when(rabbitMqConfig.getCourseMsExchange()).thenReturn(COURSE_MS);

        when(rabbitMqConfig.getArticleMsUserCreateRoutingKey()).thenReturn(ARTICLE_USER_CREATE_KEY);
        when(rabbitMqConfig.getCourseMsUserCreateRoutingKey()).thenReturn(COURSE_USER_CREATE_KEY);
    }

    @Test
    public void givenNonExistingUserIdExpectErrorMessage() throws Exception {

        when(userService.getUser(anyString())).thenThrow(new UserNotFoundException(USERNAME));

        mockMvc.perform(get(GET_USER_PATH, USERNAME)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(userNotFoundErrorMessage(USERNAME))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(userNotFoundErrorMessage(USERNAME))));

        verify(userService).saveCurrentUserIfNotPersisted();
        verify(userService).getUser(USERNAME);
    }

    @Test
    public void givenExistingUserIdExpectOk() throws Exception {
        User user = User.builder()
                .id(1L)
                .username("ichigo")
                .firstname("Ichigo")
                .lastname("Kurasaki")
                .photo("www.google.com/dummy.png")
                .contactDetails(ContactInfo.builder()
                        .email("info@ichigo.com").build())
                .build();
        when(userService.getCurrentUser()).thenReturn(user);
        when(userService.getUser(anyString())).thenReturn(user.toGetUserDto());

        mockMvc.perform(get(GET_USER_PATH, USERNAME)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(user.getId()));

        verify(userService).saveCurrentUserIfNotPersisted();
        verify(userService).getUser(USERNAME);
        verify(rabbitTemplate).convertAndSend(COURSE_MS, COURSE_USER_CREATE_KEY,user);
        verify(rabbitTemplate).convertAndSend(ARTICLE_MS, ARTICLE_USER_CREATE_KEY,user);
    }

    private String userNotFoundErrorMessage(String username) {
        return String.format("User with username: %s not found.", username);
    }
}
