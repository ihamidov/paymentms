package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ContactNotFoundException;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.service.ContactService;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
@ActiveProfiles("test")
public class GetContactControllerTest {

    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_CONTACT_PATH = "/contact/{id}";
    private static final String EMAIL = "otarkhan.mv@gmail.com";
    private static final String FIRST_NAME = "otaru";
    private static final String LAST_NAME = "mv";
    private static final String PHONE = "5555555";
    private static final String DUMMY_MESSAGE = "Simple dummy message!";
    private static final String ID_MUST_BE_POSITIVE = "Request id must be positive";


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ContactService contactService;

    private ContactRequest contactRequest;

    @Before
    public void setUp() {
        contactRequest = contactRequest.builder()
                .id(1L)
                .email(EMAIL)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .phone(PHONE)
                .message(DUMMY_MESSAGE).build();
    }

    @Test
    public void givenInvalidIdExpectErrorMessage() throws Exception {
        long id = -1L;
        mockMvc.perform(get(GET_CONTACT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingIdExpectException() throws Exception {
        long id = 1;
        when(contactService.getContactRequest(anyLong())).thenThrow(new ContactNotFoundException(id));
        mockMvc.perform(get(GET_CONTACT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(contactNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(contactNotFoundErrorMessage(id))));
    }

    @Test
    public void givenExistingIdExpectOk() throws Exception {
        long id = 1;
        when(contactService.getContactRequest(anyLong())).thenReturn(contactRequest);
        mockMvc.perform(get(GET_CONTACT_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(id));
    }

    private String contactNotFoundErrorMessage(long id) {
        return String.format("Contact with id %d not found.", id);
    }
}
