package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.ContactFilterDto;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.service.ContactService;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
@ActiveProfiles("test")
public class GetContactRequestListControllerTest {

    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_CONTACT_LIST_PATH = "/contact/list";
    private static final String EMAIL = "otarkhan.mv@gmail.com";
    private static final String FIRST_NAME = "otaru";
    private static final String LAST_NAME = "mv";
    private static final String PHONE = "5555555";
    private static final String DUMMY_MESSAGE = "Simple dummy message!";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String PAGE = "page";
    private static final String SIZE = "size";


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ContactService contactService;

    private ContactRequest contactRequest;

    @Before
    public void setUp() {
        contactRequest = contactRequest.builder()
                .id(1L)
                .email(EMAIL)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .phone(PHONE)
                .message(DUMMY_MESSAGE).build();
    }

    @Test
    public void givenNegativePageNumberExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_CONTACT_LIST_PATH)
                .param(PAGE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)));
    }

    @Test
    public void givenNegativePageSizeNumberExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_CONTACT_LIST_PATH)
                .param(SIZE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenValidPageAndSizeExpectOk() throws Exception {
        ContactFilterDto filter = ContactFilterDto.builder().order("id").direction("ASC").build();
        List<ContactRequest> requests = Collections.singletonList(contactRequest);

        Page<ContactRequest> requestPage = new PageImpl<>(requests, PageRequest.of(0, 10, Sort.by(ID).ascending()), 1);

        when(contactService.getContactRequestList(anyInt(), anyInt(), any(ContactFilterDto.class)))
                .thenReturn(requestPage);

        mockMvc.perform(get(GET_CONTACT_LIST_PATH)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages")
                        .value(requestPage.getTotalPages()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfElements")
                        .value(requestPage.getTotalElements()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());

        verify(contactService).getContactRequestList(0, 10, filter);
    }
}
