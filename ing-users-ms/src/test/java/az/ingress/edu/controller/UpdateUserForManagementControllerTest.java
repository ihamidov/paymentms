package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.dto.ContactInfoDto;
import az.ingress.edu.dto.UpdateUserExtendedDto;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.User;
import az.ingress.edu.model.UserType;
import az.ingress.edu.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@ActiveProfiles("test")
public class UpdateUserForManagementControllerTest {

    private static final String ID = "$.id";
    private static final String USER_ID_MUST_BE_POSITIVE = "User id must be positive";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String UPDATE_USER_PATH = "/user/{id}";
    private static final String FIRSTNAME = "Ichigo";
    private static final String LASTNAME = "Kurasaki";
    private static final String EMAIL = "info@ichigo.com";
    private static final String FIRSTNAME_CAN_T_BE_EMPTY = "Firstname can't be empty";
    private static final String LASTNAME_CAN_T_BE_EMPTY = "Lastname can't be empty";
    private static final String EMAIL_IS_NOT_VALID = "Email is not valid";
    private static final String USER_TYPE_CANT_BE_EMPTY = "User type can't be empty";
    private static final String ARTICLE_MS = "article-ms";
    private static final String COURSE_MS = "course-ms";
    private static final String COURSE_USER_UPDATE_KEY = "course-user-update-routing-key";
    private static final String ARTICLE_USER_UPDATE_KEY = "article-user-update-routing-key";
    private static final String PHOTO = "www.dummy.com/profile.png";

    private UpdateUserExtendedDto updateUserDto;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @MockBean
    private RabbitMqConfig rabbitMqConfig;

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Before
    public void setUp() {
        when(rabbitMqConfig.getArticleMsExchange()).thenReturn(ARTICLE_MS);
        when(rabbitMqConfig.getCourseMsExchange()).thenReturn(COURSE_MS);

        when(rabbitMqConfig.getArticleMsUserUpdateRoutingKey()).thenReturn(ARTICLE_USER_UPDATE_KEY);
        when(rabbitMqConfig.getCourseMsUserUpdateRoutingKey()).thenReturn(COURSE_USER_UPDATE_KEY);
        updateUserDto = UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                .firstname(FIRSTNAME)
                .lastname(LASTNAME)
                .photo(PHOTO).contactInfo(ContactInfoDto.builder()
                        .email(EMAIL).build())
                .type(UserType.TEACHER)
                .build();
    }

    @Test
    public void givenNegativeUserIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateUserDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(USER_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(USER_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenUserWithEmptyFirstNameExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto = UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .firstname("")
                        .lastname(LASTNAME)
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email(EMAIL).build())
                        .type(UserType.USER)
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(FIRSTNAME_CAN_T_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(FIRSTNAME_CAN_T_BE_EMPTY)));
    }

    @Test
    public void givenUserWithNullFirstNameExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto =
                UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .lastname(LASTNAME)
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email(EMAIL).build())
                        .type(UserType.USER)
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(FIRSTNAME_CAN_T_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(FIRSTNAME_CAN_T_BE_EMPTY)));
    }

    @Test
    public void givenUserWithEmptyLastNameExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto =
                UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .firstname(FIRSTNAME)
                        .lastname("")
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email(EMAIL).build())
                        .type(UserType.USER)
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LASTNAME_CAN_T_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LASTNAME_CAN_T_BE_EMPTY)));
    }

    @Test
    public void givenUserWithNullLastNameExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto =
                UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .firstname(FIRSTNAME)
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email(EMAIL).build())
                        .type(UserType.USER)
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LASTNAME_CAN_T_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LASTNAME_CAN_T_BE_EMPTY)));
    }

    @Test
    public void givenUserWithInvalidEmailExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto =
                UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .firstname(FIRSTNAME)
                        .lastname(LASTNAME)
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email("gmail.com").build())
                        .type(UserType.USER)
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(EMAIL_IS_NOT_VALID)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(EMAIL_IS_NOT_VALID)));
    }

    @Test
    public void givenUserWithNullUserTypeExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateUserExtendedDto updateDto =
                UpdateUserExtendedDto.updateUserExtendedDtoBuilder()
                        .firstname(FIRSTNAME)
                        .lastname(LASTNAME)
                        .photo(PHOTO)
                        .contactInfo(ContactInfoDto.builder()
                                .email(EMAIL).build())
                        .build();
        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(USER_TYPE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(USER_TYPE_CANT_BE_EMPTY)));
    }

    @Test
    public void givenNonExistingUserIdExpectErrorMessage() throws Exception {
        long id = 1L;
        when(userService.updateUserForManagement(any(UpdateUserExtendedDto.class), anyLong()))
                .thenThrow(new UserNotFoundException(id));

        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateUserDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(userNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(userNotFoundErrorMessage(id))));

        verify(userService).saveCurrentUserIfNotPersisted();
        verify(userService).updateUserForManagement(updateUserDto, id);
    }

    @Test
    public void givenExistingValidUserExpect() throws Exception {
        long id = 1L;
        User updatedUser = User.builder().id(id).username(FIRSTNAME).lastname(LASTNAME)
                .photo(PHOTO).build();
        when(userService.updateUserForManagement(any(UpdateUserExtendedDto.class), anyLong()))
                .thenReturn(updatedUser);

        mockMvc.perform(put(UPDATE_USER_PATH, id)
                .content(objectMapper.writeValueAsString(updateUserDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(id));

        verify(userService).saveCurrentUserIfNotPersisted();
        verify(rabbitTemplate).convertAndSend(ARTICLE_MS, ARTICLE_USER_UPDATE_KEY,updatedUser);
        verify(rabbitTemplate).convertAndSend(COURSE_MS, COURSE_USER_UPDATE_KEY,updatedUser);
        verify(userService).updateUserForManagement(updateUserDto, id);
    }

    private String userNotFoundErrorMessage(long id) {
        return String.format("User with id %d not found.", id);
    }
}
