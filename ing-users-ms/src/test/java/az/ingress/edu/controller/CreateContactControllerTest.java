package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateContactRequestDto;
import az.ingress.edu.exception.InvalidCaptchaException;
import az.ingress.edu.model.ContactRequest;
import az.ingress.edu.service.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
@ActiveProfiles("test")
public class CreateContactControllerTest {

    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String CREATE_CONTACT_PATH = "/contact";
    private static final String EMAIL = "otarkhan.mv@gmail.com";
    private static final String FIRST_NAME = "otaru";
    private static final String LAST_NAME = "mv";
    private static final String PHONE = "5555555";
    private static final String DUMMY_MESSAGE = "Simple dummy message!";
    private static final String FIST_NAME_CANT_BE_EMPTY = "First name can't be empty";
    private static final String LAST_NAME_CANT_BE_EMPTY = "Last name can't be empty";
    private static final String EMAIL_CANT_BE_EMPTY = "Email can't be empty";
    private static final String PHONE_CANT_BE_EMPTY = "Phone can't be empty";
    private static final String MESSAGE_CANT_BE_EMPTY = "Message can't be empty";
    private static final String INVALID_MESSAGE_SIZE = "Min 10 max 1000 characters allowed";
    private static final String INVALID_EMAIL = "Invalid email";
    private static final String INVALID_CAPTCHA = "Invalid captcha";


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ContactService contactService;

    private CreateContactRequestDto contactRequestDto;

    @Before
    public void setUp() {
        contactRequestDto = CreateContactRequestDto.builder()
                .email(EMAIL)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .phone(PHONE)
                .message(DUMMY_MESSAGE).build();
    }

    @Test
    public void givenContactRequestWithEmptyFirstNameExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().lastName(LAST_NAME)
                .email(EMAIL).phone(PHONE).message(DUMMY_MESSAGE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(FIST_NAME_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(FIST_NAME_CANT_BE_EMPTY)));


    }

    @Test
    public void givenContactRequestWithEmptyLastNameExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME)
                .email(EMAIL).phone(PHONE).message(DUMMY_MESSAGE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LAST_NAME_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LAST_NAME_CANT_BE_EMPTY)));


    }

    @Test
    public void givenContactRequestWithEmptyEmailExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME)
                .lastName(LAST_NAME).phone(PHONE).message(DUMMY_MESSAGE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(EMAIL_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(EMAIL_CANT_BE_EMPTY)));


    }

    @Test
    public void givenContactRequestWithEmptyPhoneExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME).email(EMAIL)
                .lastName(LAST_NAME).message(DUMMY_MESSAGE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PHONE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PHONE_CANT_BE_EMPTY)));

    }

    @Test
    public void givenContactRequestWithEmptyMessageExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME).email(EMAIL)
                .lastName(LAST_NAME).phone(PHONE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(MESSAGE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(MESSAGE_CANT_BE_EMPTY)));

    }

    @Test
    public void givenContactRequestWithInvalidMessageExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME).message("s").email(EMAIL)
                .lastName(LAST_NAME).phone(PHONE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(INVALID_MESSAGE_SIZE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(INVALID_MESSAGE_SIZE)));

    }

    @Test
    public void givenContactRequestWithInvalidEmailExpectErrorMessage() throws Exception {
        CreateContactRequestDto requestDto = CreateContactRequestDto
                .builder().firstName(FIRST_NAME).message(MESSAGE_CANT_BE_EMPTY).email("mail")
                .lastName(LAST_NAME).phone(PHONE).build();
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(requestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(INVALID_EMAIL)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(INVALID_EMAIL)));

    }

    @Test
    public void givenContactRequestWithInvalidCaptchaExpectErrorMessage() throws Exception {
        when(contactService.createContactRequest(any(CreateContactRequestDto.class)))
                .thenThrow(new InvalidCaptchaException());
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(contactRequestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(INVALID_CAPTCHA)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(INVALID_CAPTCHA)));

    }

    @Test
    public void givenContactRequestCreateContactRequest() throws Exception {
        ContactRequest contactRequest = contactRequestDto.toContactRequest();
        contactRequest.setId(1L);
        when(contactService.createContactRequest(any(CreateContactRequestDto.class)))
                .thenReturn(contactRequest);
        mockMvc.perform(post(CREATE_CONTACT_PATH)
                .content(objectMapper.writeValueAsString(contactRequestDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID, CoreMatchers.is(1)));

    }
}
