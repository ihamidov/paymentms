package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.dto.UserComboDto;
import az.ingress.edu.dto.UserFilterDto;
import az.ingress.edu.model.UserType;
import az.ingress.edu.service.UserService;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@ActiveProfiles("test")
@SuppressWarnings("PMD")
public class GetUserListControllerTest {

    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_USER_LIST_PATH = "/user/list";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String ID = "id";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RabbitMqConfig rabbitMqConfig;

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @MockBean
    private UserService userService;

    @Test
    public void givenNegativePageNumberExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_USER_LIST_PATH)
                .param(PAGE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)));
    }

    @Test
    public void givenNegativePageSizeNumberExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_USER_LIST_PATH)
                .param(SIZE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenValidPageAndSizeExpectOk() throws Exception {
        UserFilterDto filter = UserFilterDto.builder().order(ID).direction("ASC").build();
        List<UserComboDto> userList = Collections
                .singletonList(UserComboDto.builder().firstname("Haruto").lastname("Migashi")
                        .username("haruto").photo("dummy.com/dummy.png").userType(UserType.USER)
                        .build());

        Page<UserComboDto> userPage = new PageImpl<>(userList, PageRequest.of(0, 10, Sort.by(ID).ascending()), 1);

        when(userService.getAllUsers(anyInt(), anyInt(), any(UserFilterDto.class))).thenReturn(userPage);

        mockMvc.perform(get(GET_USER_LIST_PATH)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(userPage.getTotalPages()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfElements").value(userPage.getTotalElements()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());

        verify(userService).getAllUsers(0, 10, filter);
    }
}
