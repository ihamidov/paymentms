package az.ingress.edu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.model.Client;
import az.ingress.edu.service.ClientService;
import java.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class GetClientListTest {

    private static final String GET_CLIENT_LIST_PATH = "/client";

    private static final long CLIENT_ID = 1L;
    private static final Long ACCOUNT_ID = 1234L;
    private static final String NAME = "Ayaz";
    private static final String LAST_NAME = "Nacafli";
    private static final String EMAIL = "ayaz.nacafli@mail.ru";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    private Client client;

    @Before
    public void setUp() {
        client = Client.builder().id(CLIENT_ID).accountId(ACCOUNT_ID).email(EMAIL)
                .name(NAME).lastName(LAST_NAME).build();
    }

    @Test
    public void givenValidPaageableExpectOk() throws Exception {
        int page = 0;
        int size = 10;
        Pageable pageable = PageRequest.of(page, size);
        Page<Client> clientPage = new PageImpl<>(Collections.singletonList(client), pageable, 1);
        when(clientService.getClientList(pageable)).thenReturn(clientPage);

        mockMvc.perform(get(GET_CLIENT_LIST_PATH)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }


}
