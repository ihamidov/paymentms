package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.exception.AccountWithGivenEmailAlreadyExistException;
import az.ingress.edu.exception.AccountWithGivenUserUuidAlreadyExistException;
import az.ingress.edu.model.Account;
import az.ingress.edu.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class CreateAccountControllerTest {

    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String CREATE_ACCOUNT_PATH = "/account";
    private static final String NAME = "Ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String PHONENUMBER = "+994055555555";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final String NAME_CANT_BE_EMPTY = "name, cannot be empty or blank";
    private static final String LAST_NAME_CANT_BE_EMPTY = "lastName, cannot be empty or blank";
    private static final String EMAIL_CANT_BE_EMPTY = "email, cannot be empty";
    private static final String EMAIL_IS_NOT_VALID = "email, is not valid";
    private static final String LOGO_SIZE_IS_NOT_VALID = "logo, max 2000 characters allowed";
    private static final String USER_UUID_SIZE_IS_NOT_VALID = "userUuid, max 36 characters allowed";
    private static final long DUMMY_ID = 1L;

    private final CreateAccountDto createAccountDto = CreateAccountDto.builder()
            .name(NAME)
            .lastName(LAST_NAME)
            .email(EMAIL)
            .userUuid(USER_UUID)
            .logo(LOGO)
            .phoneNumber(PHONENUMBER)
            .build();

    private final Account account = Account.builder()
            .id(DUMMY_ID)
            .name(createAccountDto.getName())
            .lastName(createAccountDto.getLastName())
            .email(createAccountDto.getEmail())
            .logo(createAccountDto.getLogo())
            .userUuid(createAccountDto.getUserUuid())
            .phoneNumber(PHONENUMBER)
            .build();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenValidAccountDetailsThenOk() throws Exception {
        when(accountService.createAccount(createAccountDto)).thenReturn(account);

        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(account.getId()));
    }

    @Test
    public void whenAccountWithEmailAlreadyExistThenBadRequest() throws Exception {

        when(accountService.createAccount(createAccountDto)).thenReturn(account);
        when(accountService.createAccount(createAccountDto))
                .thenThrow(new AccountWithGivenEmailAlreadyExistException(EMAIL));

        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(checkAccountWithEmailExistMessage(EMAIL))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(checkAccountWithEmailExistMessage(EMAIL))));
    }

    @Test
    public void whenAccoutWithUserUuidAlreadyExistThenBadRequest() throws Exception {

        when(accountService.createAccount(createAccountDto)).thenReturn(account);
        when(accountService.createAccount(createAccountDto))
                .thenThrow(new AccountWithGivenUserUuidAlreadyExistException(USER_UUID));

        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(checkAccountWithUserUuidExistMessage(USER_UUID))))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(checkAccountWithUserUuidExistMessage(USER_UUID))));
    }

    @Test
    public void givenAccountRequestWithEmptyNameExpectErrorMessage() throws Exception {
        CreateAccountDto createAccountDto = CreateAccountDto
                .builder()
                .lastName(LAST_NAME)
                .email(EMAIL)
                .logo(LOGO)
                .userUuid(USER_UUID)
                .phoneNumber(PHONENUMBER)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(NAME_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(NAME_CANT_BE_EMPTY)));
    }

    @Test
    public void givenAccountRequestWithEmptyLastNameExpectErrorMessage() throws Exception {
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .userUuid(USER_UUID)
                .email(EMAIL)
                .logo(LOGO)
                .phoneNumber(PHONENUMBER)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LAST_NAME_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LAST_NAME_CANT_BE_EMPTY)));
    }

    @Test
    public void givenAccountWithEmptyEmailExceptErrorMessage() throws Exception {
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .logo(LOGO)
                .userUuid(USER_UUID)
                .phoneNumber(PHONENUMBER)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(EMAIL_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(EMAIL_CANT_BE_EMPTY)));
    }

    @Test
    public void givenAccountWithInvalidEmailExpectErrorMessage() throws Exception {
        final String invalidEmail = "infoingress.az";
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .email(invalidEmail)
                .logo(LOGO)
                .userUuid(USER_UUID)
                .phoneNumber(PHONENUMBER)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(EMAIL_IS_NOT_VALID)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(EMAIL_IS_NOT_VALID)));

    }

    @Test
    public void givenAccountWithInvalidLogoSizeExpectErrorMessage() throws Exception {
        final String invalidLogo = setStringSize(2002);
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .userUuid(USER_UUID)
                .email(EMAIL)
                .logo(invalidLogo)
                .phoneNumber(PHONENUMBER)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LOGO_SIZE_IS_NOT_VALID)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LOGO_SIZE_IS_NOT_VALID)));
    }

    @Test
    public void givenAccountWithValidLogoSizeThenStatusOk() throws Exception {
        when(accountService.createAccount(createAccountDto)).thenReturn(account);
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(account.getId()));
    }

    @Test
    public void givenAccountWithInvalidUserUuidSizeThenBadRequest() throws Exception {
        final String userUuid = setStringSize(37);
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .logo(LOGO)
                .phoneNumber(PHONENUMBER)
                .userUuid(userUuid)
                .build();
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(USER_UUID_SIZE_IS_NOT_VALID)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(USER_UUID_SIZE_IS_NOT_VALID)));
    }

    @Test
    public void givenAccountWithValidUserUuidSizeThenStatusOk() throws Exception {
        final String userUuid = setStringSize(36);
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .email(EMAIL)
                .logo(LOGO)
                .phoneNumber(PHONENUMBER)
                .userUuid(userUuid)
                .build();
        when(accountService.createAccount(createAccountDto)).thenReturn(account);
        mockMvc.perform(post(CREATE_ACCOUNT_PATH)
                .content(objectMapper.writeValueAsString(createAccountDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(account.getId()));
    }

    private String checkAccountWithEmailExistMessage(String email) {
        return String.format("Account with email: '%s' already exists", email);
    }

    private String checkAccountWithUserUuidExistMessage(String userUuid) {
        return String.format("Account with uuid: '%s' already exists", userUuid);
    }

    private String setStringSize(int stringLength) {
        final String string = "*";
        StringBuilder stringBuilder = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }
}

