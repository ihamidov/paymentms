package az.ingress.edu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.Currency;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.dto.PaymentType;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.service.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    private static final String GET_PAYMENT_LIST = "/payment";
    private static final String CREATE_PAYMENT = "/account/{accountId}/payment";
    private static final String GET_PAYMENT_LIST_BY_ACCOUNT_ID = "/account/{accountId}/payment";
    private static final long ACCOUNT_ID = 1L;
    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "$.userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String ACCOUNT_NOT_FOUND_MESSAGE = "Account can't be found";
    private static final String CLIENT_NOT_FOUND_MESSAGE = "Client not found.";
    private static final PaymentStatus PAYMENT_STATUS = PaymentStatus.PAID;
    private static final PaymentType PAYMENT_TYPE = PaymentType.EXPENSE;

    private CreatePaymentDto createPaymentDto;
    private Payment payment;
    private List<Payment> paymentList;

    @MockBean
    private PaymentService paymentService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        Client client = Client.builder()
                .id(1L)
                .build();
        createPaymentDto = CreatePaymentDto.builder().clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.now())
                .discount(30)
                .status(PAYMENT_STATUS)
                .description("String")
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().plusMonths(2))
                .paymentType(PAYMENT_TYPE)
                .build();
        payment = Payment.builder().id(1L)
                .client(client).accountId(ACCOUNT_ID)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.now())
                .discount(30)
                .status(PAYMENT_STATUS)
                .description("String")
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().plusMonths(2))
                .build();
        paymentList = new ArrayList();
        paymentList.add(payment);
    }

    @Test
    public void givenPaymentListWhenGetPaymentListThenOK() throws Exception {
        when(paymentService.getPaymentList()).thenReturn(paymentList);

        mvc.perform(get(GET_PAYMENT_LIST)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(content().json(objectMapper.writeValueAsString(paymentList)))
                .andExpect(status().isOk());
    }

    public void givenValidDataWhenCreatePaymentThenOk() throws Exception {
        when(paymentService.createPayment(ACCOUNT_ID, createPaymentDto)).thenReturn(payment);

        mvc.perform(post(CREATE_PAYMENT, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createPaymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(payment.getId()));
    }

    @Test
    public void givenAccountNotExistWhenCreatePaymentThenNotFound() throws Exception {
        when(paymentService.createPayment(ACCOUNT_ID, createPaymentDto)).thenThrow(new AccountNotFoundException());

        mvc.perform(post(CREATE_PAYMENT, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createPaymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE));
    }

    @Test
    public void givenClientNotExistWhenCreatePaymentThenNotFound() throws Exception {
        when(paymentService.createPayment(ACCOUNT_ID, createPaymentDto)).thenThrow(new ClientNotFoundException());

        mvc.perform(post(CREATE_PAYMENT, ACCOUNT_ID)
                .content(objectMapper.writeValueAsString(createPaymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(CLIENT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(CLIENT_NOT_FOUND_MESSAGE));
    }

    @Test
    public void givenPaymentListWhenGetPaymentListByAccountIdThenOk() throws Exception {
        when(paymentService.getPaymentListByAccountId(ACCOUNT_ID)).thenReturn(paymentList);

        mvc.perform(get(GET_PAYMENT_LIST_BY_ACCOUNT_ID, ACCOUNT_ID)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(paymentList)));
    }

    @Test
    public void givenAccountNotExistsWhenGetPaymentListByAccountIdThenNotFound() throws Exception {
        when(paymentService.getPaymentListByAccountId(ACCOUNT_ID)).thenThrow(new AccountNotFoundException());

        mvc.perform(get(GET_PAYMENT_LIST_BY_ACCOUNT_ID, ACCOUNT_ID)
                .content(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(ACCOUNT_NOT_FOUND_MESSAGE));
    }
}
