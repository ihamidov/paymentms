package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PaymentDto;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.exception.InvalidPaymentIdException;
import az.ingress.edu.exception.PaymentNotFoundException;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.service.PaymentService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
public class UpdatePaymentControllerTest {

    private static final String PAYMENT_API = "/payment/";
    private static final long ID_POSITIVE = 1;
    private static final long ID_NEGATIVE = -1L;
    private static final int AMOUNT_PER_UNIT = 100;
    private static final LocalDate DUE_DATE = LocalDate.now();
    private static final String CODE = "$.code";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String PAYMENT_ID_MUST_BE_POSITIVE = "Payment ID must be positive";
    private static final String PAYMENT_NOT_FOUND = "Payment not found";
    private static final PaymentStatus PAYMENT_STATUS = PaymentStatus.PAID;

    @MockBean
    private PaymentService paymentService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private PaymentDto paymentDto;
    private Payment payment;

    @Before
    public void setUp() {
        List<IdDto> clientList = new ArrayList<>();
        Client client = Client.builder()
                .id(1L)
                .build();
        paymentDto = PaymentDto.builder().client(clientList)
                .amountPerUnit(AMOUNT_PER_UNIT).quantity(100)
                .dueDate(DUE_DATE).discount(30)
                .description("string").build();
        payment = Payment.builder().id(1L)
                .client(client)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.now())
                .discount(30).subtotal(10000).total(7000)
                .status(PAYMENT_STATUS).dueDate(DUE_DATE)
                .description("string")
                .build();
    }

    @Test
    public void givenPaymentNotExistWhenUpdatePaymentThenNotFound() throws Exception {
        when(paymentService.updatePayment(ID_POSITIVE, paymentDto)).thenThrow(new PaymentNotFoundException());

        mvc.perform(MockMvcRequestBuilders.put(PAYMENT_API + ID_POSITIVE)
                .content(objectMapper.writeValueAsString(paymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(USER_MESSAGE).value(PAYMENT_NOT_FOUND))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PAYMENT_NOT_FOUND));
    }

    @Test
    public void givenPaymentUpdateWhenPutPaymentThenOK() throws Exception {
        when(paymentService.updatePayment(ID_POSITIVE, paymentDto)).thenReturn(payment);

        mvc.perform(MockMvcRequestBuilders.put(PAYMENT_API + ID_POSITIVE)
                .content(asJsonString(paymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenPaymentUpdateWhenPutThenPaymentInvalidValue() throws Exception {
        PaymentDto paymentDto = new PaymentDto();
        when(paymentService.updatePayment(ID_NEGATIVE, paymentDto)).thenThrow(new InvalidPaymentIdException());
        mvc.perform(MockMvcRequestBuilders.put(PAYMENT_API + ID_NEGATIVE)
                .content(asJsonString(paymentDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PAYMENT_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(PAYMENT_ID_MUST_BE_POSITIVE));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void givenPaymentDeleteWhenDeletePaymentThenOK() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete(PAYMENT_API + 1))
                .andExpect(status().isOk());
    }

    @Test
    public void givenPaymentDeleteWhenDeletePaymentThenNotFound() throws Exception {
        doThrow(new PaymentNotFoundException()).when(paymentService).deletePaymentById(anyLong());

        mvc.perform(MockMvcRequestBuilders.delete(PAYMENT_API + 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(404))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PAYMENT_NOT_FOUND))
                .andExpect(jsonPath(USER_MESSAGE).value(PAYMENT_NOT_FOUND));
    }

}
