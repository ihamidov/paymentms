package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.CreatePaymentScheduleDto;
import az.ingress.edu.dto.Currency;
import az.ingress.edu.dto.PaymentScheduleStatus;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.dto.PaymentType;
import az.ingress.edu.dto.PeriodType;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.model.PaymentSchedule;
import az.ingress.edu.service.PaymentScheduleService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentScheduleController.class)
public class PaymentScheduleControllerTest {

    private static final String PAYMENT_SCHEDULE_API = "/account/{accountId}/payment-schedule";
    private static final long ID = 1L;
    private static final PaymentStatus PAYMENT_STATUS = PaymentStatus.PAID;
    private static final String DESCRIPTION = "String";
    private static final Integer NUMBER_OF_TIME_TO_REPEAT = 5;
    private static final PeriodType PERIOD_TYPE = PeriodType.MONTH;
    private static final PaymentScheduleStatus PAYMENT_SCHEDULE_STATUS = PaymentScheduleStatus.ACTIVE;
    private static final String NAME = "Ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String ACCOUNT_NOT_FOUND = "Account can't be found";
    private static final String CLIENT_NOT_FOUND = "Client not found.";
    private static final PaymentType PAYMENT_TYPE = PaymentType.EXPENSE;

    @MockBean
    private PaymentScheduleService paymentScheduleService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private CreatePaymentScheduleDto createPaymentScheduleDto;

    private PaymentSchedule paymentSchedule;

    private final List<Payment> payments = new ArrayList<>();

    private final Account account = Account.builder()
            .name(NAME)
            .lastName(LAST_NAME)
            .email(EMAIL)
            .userUuid(USER_UUID)
            .logo(LOGO)
            .build();

    private final Client client = Client.builder()
            .id(1L)
            .build();

    private final CreatePaymentDto createPaymentDto = CreatePaymentDto.builder().clientId(1L)
            .amountPerUnit(100).quantity(100)
            .dueDate(LocalDate.of(2020, 3, 3))
            .discount(30)
            .status(PAYMENT_STATUS).description(DESCRIPTION)
            .currency(Currency.AZN)
            .dateToSend(LocalDate.now().plusMonths(2))
            .paymentType(PAYMENT_TYPE)
            .build();

    @Before
    public void setUp() {

        createPaymentScheduleDto = CreatePaymentScheduleDto.builder()
                .payment(createPaymentDto)
                .numberOfTimesToRepeat(NUMBER_OF_TIME_TO_REPEAT)
                .periodType(PERIOD_TYPE)
                .status(PAYMENT_SCHEDULE_STATUS)
                .build();

        Payment payment = Payment.builder().id(1L)
                .client(client).accountId(ID)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.now())
                .discount(30)
                .status(PAYMENT_STATUS)
                .description(DESCRIPTION)
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().plusMonths(2))
                .paymentType(PAYMENT_TYPE)
                .build();

        payments.add(payment);

        paymentSchedule = PaymentSchedule.builder()
                .id(ID)
                .account(account)
                .dateToSend(LocalDate.now().plusMonths(5))
                .numberOfTimesToRepeat(NUMBER_OF_TIME_TO_REPEAT)
                .periodType(PERIOD_TYPE)
                .status(PAYMENT_SCHEDULE_STATUS)
                .payments(payments)
                .build();

    }

    @Test
    public void givenValidPaymentDataWhenCreatePaymentScheduleThenOk() throws Exception {
        when(paymentScheduleService.create(ID, createPaymentScheduleDto)).thenReturn(paymentSchedule);

        mvc.perform(post(PAYMENT_SCHEDULE_API, ID)
                .content(objectMapper.writeValueAsString(createPaymentScheduleDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void accountNotExistWhenCreatePaymentScheduleThenNotFound() throws Exception {
        doThrow(new AccountNotFoundException()).when(paymentScheduleService).create(ID, createPaymentScheduleDto);

        mvc.perform(post(PAYMENT_SCHEDULE_API, ID)
                .content(asJsonString(createPaymentScheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ACCOUNT_NOT_FOUND)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ACCOUNT_NOT_FOUND)));
    }

    @Test
    public void clientNotExistWhenCreatePaymentScheduleThenNotFound() throws Exception {
        doThrow(new ClientNotFoundException()).when(paymentScheduleService).create(ID, createPaymentScheduleDto);

        mvc.perform(post(PAYMENT_SCHEDULE_API, ID)
                .content(asJsonString(createPaymentScheduleDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(CLIENT_NOT_FOUND)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(CLIENT_NOT_FOUND)));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

}
