package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Client;
import az.ingress.edu.service.ClientService;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class GetClientWithIdTest {

    private static final String GET_CLIENT_PATH = "/client/{id}";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final long CLIENT_ID = 1L;
    private static final long CLIENT_NEGATIV_ID = -1L;
    private static final Long ACCOUNT_ID = 1234L;
    private static final String NAME = "Ayaz";
    private static final String LAST_NAME = "Nacafli";
    private static final String EMAIL = "ayaz.nacafli@mail.ru";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    private Client client;

    @Before
    public void setUp() {
        client = Client.builder()
                .id(CLIENT_ID)
                .accountId(ACCOUNT_ID)
                .email(EMAIL)
                .name(NAME)
                .lastName(LAST_NAME)
                .build();
    }

    @Test
    public void givenExistingClientIdExpectOk() throws Exception {
        when(clientService.getClientById(CLIENT_ID)).thenReturn(client);

        mockMvc.perform(get(GET_CLIENT_PATH, CLIENT_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(client.getId()));

        verify(clientService).getClientById(CLIENT_ID);
    }

    @Test
    public void givenInvalidIdExpectErrorMessage() throws Exception {
        mockMvc.perform(get(GET_CLIENT_PATH, CLIENT_NEGATIV_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ID_MUST_BE_POSITIVE)));

    }

    @Test
    public void givenValidIdExpectClientNotFoundException() throws Exception {
        when(clientService.getClientById(CLIENT_ID))
                .thenThrow(new ClientNotFoundException(CLIENT_ID));

        mockMvc.perform(get(GET_CLIENT_PATH, CLIENT_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(clientNotFoundErrorMessage(CLIENT_ID))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(clientNotFoundErrorMessage(CLIENT_ID))));
    }

    private String clientNotFoundErrorMessage(long id) {
        return String.format("Client with id %d not found.", id);
    }

}
