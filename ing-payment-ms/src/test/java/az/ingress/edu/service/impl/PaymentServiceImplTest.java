package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.Currency;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PaymentDto;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.exception.InvalidDateToSendException;
import az.ingress.edu.exception.PaymentNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.repository.AccountRepository;
import az.ingress.edu.repository.ClientRepository;
import az.ingress.edu.repository.PaymentRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceImplTest {

    private static final long ID = 1;
    private static final int AMOUNT_PER_UNIT = 100;
    private static final LocalDate DUE_DATE = LocalDate.now();

    private static final Long ACCOUNT_ID = 1L;
    private static final String ACCOUNT_NOT_FOUND_MESSAGE = "Account can't be found";
    private static final String CLIENT_NOT_FOUND_MESSAGE = "Client not found.";
    private static final String INVALID_DATE_TO_SEND_MESSAGE = "DateToSend must be greater or equals to today's date";
    private static final PaymentStatus PAYMENT_STATUS = PaymentStatus.PAID;
    private static final String DESCRIPTION = "String";

    private CreatePaymentDto createPaymentDto;
    private Payment payment;
    private Account account;
    private Client client;
    private List<Payment> paymentList;

    @Mock
    private PaymentRepository paymentRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private PaymentServiceImpl paymentService;
    private Payment payment2;
    private PaymentDto paymentDto;

    @Before
    public void setUp() {
        List<IdDto> clientList = new ArrayList<>();
        payment2 = Payment.builder().id(ID).client(client)
                .amountPerUnit(AMOUNT_PER_UNIT).quantity(100)
                .discount(30).subtotal(10000).total(7000)
                .status(PAYMENT_STATUS).dueDate(DUE_DATE)
                .description(DESCRIPTION).currency(Currency.AZN).dateToSend(LocalDate.now().plusMonths(2))
                .status(PaymentStatus.PAID).dueDate(DUE_DATE)
                .build();
        paymentDto = PaymentDto.builder().client(clientList)
                .amountPerUnit(AMOUNT_PER_UNIT).quantity(100)
                .dueDate(DUE_DATE).discount(30)
                .description(DESCRIPTION).currency(Currency.AZN).dateToSend(LocalDate.now().plusMonths(2))
                .build();
        createPaymentDto = CreatePaymentDto.builder().clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.of(2020, 3, 3))
                .discount(30).status(PAYMENT_STATUS).description(DESCRIPTION)
                .currency(Currency.AZN).dateToSend(LocalDate.now().plusMonths(2)).build();

        client = Client.builder().id(1L).build();
        payment = Payment.builder().client(client)
                .accountId(ACCOUNT_ID).amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.of(2020, 3, 3))
                .discount(30).status(PAYMENT_STATUS)
                .description(DESCRIPTION).currency(Currency.AZN).dateToSend(LocalDate.now().plusMonths(2)).build();

        paymentList = new ArrayList();
        paymentList.add(payment);
        account = Account.builder().id(ACCOUNT_ID).build();
    }

    @Test
    public void givenValidRequestGetPaymentListExpectOk() {
        when(paymentRepository.findAll()).thenReturn(paymentList);

        assertThat(paymentService.getPaymentList())
                .isInstanceOf(List.class)
                .contains(payment)
                .hasSize(1);
    }

    @Test
    public void givenUpdatePaymentWhenPutPaymentThenOk() {
        when(paymentRepository.findById(anyLong())).thenReturn(Optional.of(payment2));
        when(paymentRepository.save(any(Payment.class))).thenReturn(payment2);
        Payment payment = paymentService.updatePayment(anyLong(), paymentDto);
        assertThat(payment.getAmountPerUnit()).isEqualTo(payment.getAmountPerUnit());
    }

    @Test
    public void givenDeletePaymentWhenDeletePaymentThenOk() {
        when(paymentRepository.findById(anyLong())).thenReturn(Optional.of(payment));
        paymentService.deletePaymentById(ID);
    }

    @Test
    public void givenValidDataWhenCreatePaymentThenOk() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(clientRepository.findById(createPaymentDto.getClientId())).thenReturn(Optional.of(client));
        when(paymentRepository.save(payment)).thenReturn(payment);
        assertThat(paymentService.createPayment(ACCOUNT_ID, createPaymentDto)).isEqualTo(payment);
    }

    @Test
    public void givenAccountNotExistWhenCreatePaymentThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> paymentService.createPayment(ACCOUNT_ID, createPaymentDto))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage(ACCOUNT_NOT_FOUND_MESSAGE);
    }

    @Test
    public void givenClientNotExistWhenCreatePaymentThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(clientRepository.findById(createPaymentDto.getClientId()))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> paymentService.createPayment(ACCOUNT_ID, createPaymentDto))
                .isInstanceOf(ClientNotFoundException.class)
                .hasMessage(CLIENT_NOT_FOUND_MESSAGE);

    }

    @Test
    public void givenValidDataWhenGetPaymentListByAccountIdThenOk() {
        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(paymentRepository.findByAccountId(ACCOUNT_ID)).thenReturn(paymentList);

        assertThat(paymentService.getPaymentListByAccountId(ACCOUNT_ID))
                .isInstanceOf(List.class)
                .contains(payment)
                .hasSize(1);
    }

    @Test
    public void givenAccountNotExistsWhenGetPaymentListByAccountIdThenNotFound() {
        when(accountRepository.findById(ACCOUNT_ID)).thenThrow(new AccountNotFoundException());

        assertThatThrownBy(() -> paymentService.getPaymentListByAccountId(ACCOUNT_ID))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage(ACCOUNT_NOT_FOUND_MESSAGE);
    }

    @Test
    public void givenInvalidDateToSendWhenCreatePaymentThenBadRequest() {
        CreatePaymentDto createPaymentDto = CreatePaymentDto.builder().clientId(1L)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.of(2020, 3, 3))
                .discount(30).status(PAYMENT_STATUS).description(DESCRIPTION)
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().minusMonths(2))
                .build();

        when(accountRepository.findById(ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(clientRepository.findById(createPaymentDto.getClientId())).thenReturn(Optional.of(client));

        assertThatThrownBy(() -> paymentService.createPayment(ACCOUNT_ID, createPaymentDto))
                .isInstanceOf(InvalidDateToSendException.class)
                .hasMessage(INVALID_DATE_TO_SEND_MESSAGE);
    }

    @Test
    public void givenInvalidDateToSendWhenUpdatePaymentThenBadRequest() {
        List<IdDto> clientList = new ArrayList<>();
        PaymentDto paymentDto = PaymentDto.builder().client(clientList)
                .amountPerUnit(AMOUNT_PER_UNIT).quantity(100)
                .dueDate(DUE_DATE).discount(30)
                .description(DESCRIPTION)
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().minusMonths(2))
                .build();

        when(paymentRepository.findById(anyLong())).thenReturn(Optional.of(payment2));

        assertThatThrownBy(() -> paymentService.updatePayment(ACCOUNT_ID, paymentDto))
                .isInstanceOf(InvalidDateToSendException.class)
                .hasMessage(INVALID_DATE_TO_SEND_MESSAGE);
    }

    @Test
    public void givenIdWhenFindPaymentByIdThenOk() {
        when(paymentRepository.findById(ID)).thenReturn(Optional.of(payment2));

        assertThat(paymentService.getPayment(ID)).isEqualTo(payment2);
    }

    @Test
    public void givenIdWhenFindPaymentByIdThenNotFound() {
        when(paymentRepository.findById(ID)).thenThrow(new PaymentNotFoundException());

        assertThatThrownBy(() -> paymentService.getPayment(ID)).isInstanceOf(PaymentNotFoundException.class);
    }
}
