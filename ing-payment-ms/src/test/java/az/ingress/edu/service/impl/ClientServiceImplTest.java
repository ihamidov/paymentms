package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.dto.CreateClientDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateClientDto;
import az.ingress.edu.exception.ClientAlreadyExistsException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.repository.ClientRepository;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest {

    private static final long ID = 1;
    private static final Long ACCOUNT_ID = 1234L;
    private static final String NAME = "Ayaz";
    private static final String LAST_NAME = "Nacafli";
    private static final String EMAIL = "ayaz.nacafli@mail.ru";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final long DUMMY_ID = 1L;

    private CreateClientDto createClientDto;

    private UpdateClientDto updateClientDto;

    private Client clientBeforeSave;

    private Client clientAfterSave;

    private IdDto idDto;

    private Account account;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private AccountServiceImpl accountService;

    @InjectMocks
    private ClientServiceImpl clientService;

    private Client client;

    @Before
    public void setUp() {
        createClientDto = CreateClientDto.builder().name(NAME).lastName(LAST_NAME).email(EMAIL).build();
        updateClientDto = UpdateClientDto.builder()
                .name(NAME)
                .lastName(LAST_NAME)
                .build();
        clientBeforeSave = Client.builder().name(createClientDto.getName())
                .lastName(createClientDto.getLastName()).email(createClientDto.getEmail())
                .accountId(DUMMY_ID).build();
        idDto = IdDto.builder()
                .id(DUMMY_ID).build();
        CreateAccountDto createAccountDto = CreateAccountDto.builder()
                .name(NAME).lastName(LAST_NAME)
                .email(EMAIL).userUuid(USER_UUID).build();
        account = Account.builder().id(DUMMY_ID)
                .name(createAccountDto.getName())
                .lastName(createAccountDto.getLastName())
                .email(createAccountDto.getEmail())
                .build();
        clientAfterSave = Client.builder()
                .id(DUMMY_ID).name(createClientDto.getName())
                .lastName(createClientDto.getLastName())
                .email(createClientDto.getEmail())
                .accountId(DUMMY_ID)
                .build();
        client = Client.builder().id(ID).accountId(ACCOUNT_ID).email(EMAIL).name(NAME).lastName(LAST_NAME).build();
    }

    @Test
    public void givenClientCreateClient() {
        when(accountService.getAccount(anyLong())).thenReturn(account);
        when(clientRepository.save(clientBeforeSave)).thenReturn(clientAfterSave);
        IdDto returnId = clientService.createClient(1, createClientDto);

        assertThat(returnId).isEqualTo(idDto);
    }

    @Test
    public void whenClientWithEmailAlreadyExistThenBadRequest() throws Exception {
        when(accountService.getAccount(anyLong())).thenReturn(account);
        when(clientRepository.findByEmailAndAccountId(EMAIL, 1)).thenReturn(Optional.of(clientAfterSave));

        assertThatThrownBy(() -> clientService.createClient(1, createClientDto))
                .isInstanceOf(ClientAlreadyExistsException.class);

    }

    @Test
    public void getClientWithValidIdExpectResponse() {
        when(clientRepository.findById(ID)).thenReturn(Optional.of(client));

        Client result = clientService.getClientById(ID);
        assertThat(result.getId()).isEqualTo(ID);
        assertThat(result.getName()).isEqualTo(client.getName());
        assertThat(result.getLastName()).isEqualTo(client.getLastName());
        assertThat(result.getEmail()).isEqualTo(client.getEmail());
        verify(clientRepository).findById(ID);
    }

    @Test
    public void givenNonExistingClientIdGetClientExpectException() {
        when(clientRepository.findById(ID)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> clientService.getClientById(ID))
                .isInstanceOf(ClientNotFoundException.class);
        verify(clientRepository).findById(ID);
    }

    @Test
    public void givenValidDataWHenGetClientListThenOk() {
        int page = 0;
        int size = 10;
        Pageable pageable = PageRequest.of(page, size);
        Page<Client> clientPage = new PageImpl<>(Collections.singletonList(client), pageable, 1);
        when(clientRepository.findAll(pageable)).thenReturn(clientPage);

        Page<Client> result = clientService.getClientList(pageable);
        assertThat(result.getTotalPages()).isEqualTo(clientPage.getTotalPages());
        assertThat(result.getTotalElements()).isEqualTo(clientPage.getTotalElements());
        assertThat(result.getContent().size()).isEqualTo(clientPage.getContent().size());
        verify(clientRepository).findAll(pageable);
    }

    @Test
    public void givenValidClientWhenUpdateClientThenOk() {
        when(clientRepository.findById(ID)).thenReturn(Optional.of(client));
        when(clientRepository.save(client)).thenReturn(client);

        IdDto result = clientService.updateClientById(ID, updateClientDto);

        assertThat(result.getId()).isEqualTo(ID);
    }

    @Test
    public void givenInvalidClientWhenUpdateClientThenNotFound() {
        when(clientRepository.findById(ID)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> clientService.updateClientById(ID, updateClientDto))
                .isInstanceOf(ClientNotFoundException.class);
    }

}
