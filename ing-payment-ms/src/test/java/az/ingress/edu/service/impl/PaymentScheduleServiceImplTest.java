package az.ingress.edu.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.CreatePaymentScheduleDto;
import az.ingress.edu.dto.Currency;
import az.ingress.edu.dto.PaymentScheduleStatus;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.dto.PeriodType;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.model.PaymentSchedule;
import az.ingress.edu.repository.PaymentScheduleRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaymentScheduleServiceImplTest {

    private static final long ID = 1L;
    private static final String NAME = "ichigo";
    private static final String LAST_NAME = "Kurasaki";
    private static final String EMAIL = "info@ingress.az";
    private static final String LOGO = "https://www.google.com/logo.png";
    private static final String USER_UUID = "3ede261a-5f89-11ea-bc55-0242ac130003";
    private static final long DUMMY_ID = 1L;
    private static final PaymentStatus PAYMENT_STATUS = PaymentStatus.PAID;
    private static final String DESCRIPTION = "String";
    private static final Integer NUMBER_OF_TIME_TO_REPEAT = 2;
    private static final PeriodType PERIOD_TYPE = PeriodType.MONTH;
    private static final PaymentScheduleStatus PAYMENT_SCHEDULE_STATUS = PaymentScheduleStatus.ACTIVE;
    private static final String ACCOUNT_NOT_FOUND = "Account can't be found";

    @Mock
    private AccountServiceImpl accountService;

    @Mock
    private PaymentScheduleRepository paymentScheduleRepository;

    @Mock
    private PaymentServiceImpl paymentService;

    @InjectMocks
    private PaymentScheduleServiceImpl paymentScheduleService;

    private final Account account = Account.builder()
            .id(DUMMY_ID)
            .name(NAME)
            .lastName(LAST_NAME)
            .email(EMAIL)
            .userUuid(USER_UUID)
            .logo(LOGO)
            .build();

    private PaymentSchedule paymentSchedule;

    private Payment payment;

    private CreatePaymentScheduleDto createPaymentScheduleDto;

    private final CreatePaymentDto createPaymentDto = CreatePaymentDto.builder().clientId(1L)
            .amountPerUnit(100).quantity(100)
            .dueDate(LocalDate.of(2020, 3, 3))
            .discount(30).status(PAYMENT_STATUS).description(DESCRIPTION)
            .currency(Currency.AZN).dateToSend(LocalDate.now().plusMonths(2)).build();

    private final Client client = Client.builder()
            .id(1L)
            .build();

    private final List<Payment> payments = new ArrayList<>();

    @Before
    public void setUp() {
        createPaymentScheduleDto = CreatePaymentScheduleDto.builder()
                .payment(createPaymentDto)
                .numberOfTimesToRepeat(NUMBER_OF_TIME_TO_REPEAT)
                .periodType(PERIOD_TYPE)
                .status(PAYMENT_SCHEDULE_STATUS)
                .build();

        payment = Payment.builder().id(1L)
                .client(client).accountId(ID)
                .amountPerUnit(100).quantity(100)
                .dueDate(LocalDate.now())
                .discount(30)
                .status(PAYMENT_STATUS)
                .description(DESCRIPTION)
                .currency(Currency.AZN)
                .dateToSend(LocalDate.now().plusMonths(2))
                .build();

        payments.add(payment);
        payments.add(payment);

        paymentSchedule = PaymentSchedule.builder()
                .payments(payments)
                .account(account)
                .numberOfTimesToRepeat(NUMBER_OF_TIME_TO_REPEAT)
                .periodType(PERIOD_TYPE)
                .status(PAYMENT_SCHEDULE_STATUS)
                .build();

    }

    @Test
    public void givenValidPaymentScheduleDataWhenCreatePaymentThenOk() {
        when(accountService.getAccount(DUMMY_ID)).thenReturn(account);
        when(paymentScheduleRepository.save(any())).thenReturn(paymentSchedule);
        when(paymentService.createPayment(DUMMY_ID, createPaymentDto)).thenReturn(payment);

        assertThat(paymentScheduleService.create(DUMMY_ID, createPaymentScheduleDto)).isEqualTo(paymentSchedule);
    }

    @Test
    public void accountNotExistWhenCreatePaymentScheduleThenNotFound() {
        when(accountService.getAccount(ID)).thenThrow(new AccountNotFoundException());

        assertThatThrownBy(() -> paymentScheduleService.create(ID, createPaymentScheduleDto))
                .isInstanceOf(AccountNotFoundException.class)
                .hasMessage(ACCOUNT_NOT_FOUND);
    }

    @Test
    public void givenNumberOfTimesToRepeatWhenCreatePaymentThenOk() {
        when(accountService.getAccount(anyLong())).thenReturn(account);
        when(paymentService.createPayment(DUMMY_ID, createPaymentDto)).thenReturn(payment);
        when(paymentScheduleRepository.save(any())).thenReturn(paymentSchedule);
        PaymentSchedule paymentSchedule2 = paymentScheduleService.create(DUMMY_ID, createPaymentScheduleDto);

        assertThat(paymentSchedule2.getPayments().size()).isEqualTo(NUMBER_OF_TIME_TO_REPEAT);
        verify(paymentScheduleRepository).save(paymentSchedule);
    }

}
