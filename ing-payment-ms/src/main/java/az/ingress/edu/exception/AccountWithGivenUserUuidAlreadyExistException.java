package az.ingress.edu.exception;

public class AccountWithGivenUserUuidAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public AccountWithGivenUserUuidAlreadyExistException(String userUuid) {
        super(String.format("Account with uuid: '%s' already exists", userUuid));
    }
}
