package az.ingress.edu.exception;

public class AccountNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public AccountNotFoundException() {
        super(String.format("Account can't be found"));
    }

    public AccountNotFoundException(Long id) {
        super(String.format("Account with id: '%s' can't be found", id));
    }
}
