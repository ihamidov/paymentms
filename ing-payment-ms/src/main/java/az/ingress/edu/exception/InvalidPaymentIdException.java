package az.ingress.edu.exception;

public class InvalidPaymentIdException extends RuntimeException {

    private static final long serialVersionUID = -1L;

    private static final String MESSAGE = "Payment ID must be positive";

    public InvalidPaymentIdException() {
        super(MESSAGE);
    }
}
