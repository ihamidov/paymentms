package az.ingress.edu.exception;

import az.ingress.edu.dto.ErrorDto;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Objects;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;
import javax.validation.ConstraintDefinitionException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RestController
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ClientAlreadyExistsException.class)
    public final ResponseEntity<ErrorDto> handleClientAlreadyExistsException(ClientAlreadyExistsException ex) {
        final ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), ex.getMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ErrorDto> handleAccountNotFound(AccountNotFoundException ex) {

        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(),
                ex.getMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorDto> handleConverterErrors(MethodArgumentTypeMismatchException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                ex.getLocalizedMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountWithGivenEmailAlreadyExistException.class)
    public final ResponseEntity<ErrorDto> handleAccountWithEmailAlreadyExists(
            AccountWithGivenEmailAlreadyExistException ex) {
        final ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), ex.getMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountWithGivenUserUuidAlreadyExistException.class)
    public final ResponseEntity<ErrorDto> handleAccountWithUserUuidAlreadyExists(
            AccountWithGivenUserUuidAlreadyExistException ex) {
        final ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), ex.getMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        Throwable mostSpecificCause = ex.getMostSpecificCause();
        String message = mostSpecificCause.getMessage();
        ErrorDto errorDetail = ErrorDto.builder()
                .userMessage(message)
                .code(status.value())
                .timestamp(Calendar.getInstance())
                .technicalMessage(message)
                .build();
        return new ResponseEntity(errorDetail, headers, status);
    }

    @ExceptionHandler(PropertyBindingException.class)
    public ResponseEntity<ErrorDto> handlePropertyBinding(PropertyBindingException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                ex.getLocalizedMessage(),
                Calendar.getInstance());
        log.warn(ex.getMessage());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        ObjectError error = ex.getBindingResult().getAllErrors().stream().findAny().orElse(null);
        FieldError fieldError = (FieldError) error;
        ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                Objects.requireNonNull(fieldError).getField() + ", " + fieldError.getDefaultMessage(),
                fieldError.getField() + ", " + fieldError.getDefaultMessage(),
                Calendar.getInstance());
        log.warn(fieldError.getDefaultMessage());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleConstraintViolationException(ConstraintViolationException ex)
            throws IOException {
        ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                getConstraintViolationExceptionMessage(ex),
                getConstraintViolationExceptionMessage(ex),
                Calendar.getInstance());
        log.warn(getConstraintViolationExceptionMessage(ex));
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(PaymentNotFoundException.class)
    public ResponseEntity handle(PaymentNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(),
                ex.getLocalizedMessage(),
                Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidPaymentIdException.class)
    public ResponseEntity invalidValueException(InvalidPaymentIdException ex) {
        ErrorDto errorDto = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(),
                ex.getMessage(),
                Calendar.getInstance());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ClientNotFoundException.class)
    public ResponseEntity constraintViolationException(ClientNotFoundException ex) {
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .technicalMessage(ex.getMessage())
                .userMessage(ex.getMessage())
                .timestamp(Calendar.getInstance()).build();
        return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidDateToSendException.class)
    public ResponseEntity invalidDateToSendException(InvalidDateToSendException ex) {
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .technicalMessage(ex.getMessage())
                .userMessage(ex.getMessage())
                .timestamp(Calendar.getInstance()).build();
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

    private String getConstraintViolationExceptionMessage(ConstraintViolationException ex) {
        return ex.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList()).get(0);
    }

    @ExceptionHandler(PatternSyntaxException.class)
    public ResponseEntity constraintViolationExcep(PatternSyntaxException ex) {
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .technicalMessage(ex.getMessage())
                .userMessage(ex.getMessage())
                .timestamp(Calendar.getInstance()).build();
        return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintDefinitionException.class)
    public ResponseEntity constraintViolationExcep(ConstraintDefinitionException ex) {
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .technicalMessage(ex.getMessage())
                .userMessage(ex.getMessage())
                .timestamp(Calendar.getInstance()).build();
        return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
    }

}

