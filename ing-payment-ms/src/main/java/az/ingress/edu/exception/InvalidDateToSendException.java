package az.ingress.edu.exception;

public class InvalidDateToSendException extends RuntimeException {

    private static final long serialVersionUID = 58432132465811L;

    private static final String MESSAGE = "DateToSend must be greater or equals to today's date";

    public InvalidDateToSendException() {
        super(MESSAGE);
    }
}
