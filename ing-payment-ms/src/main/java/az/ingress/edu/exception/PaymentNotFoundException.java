package az.ingress.edu.exception;

public class PaymentNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private static final String MESSAGE = "Payment not found";

    public PaymentNotFoundException() {
        super(MESSAGE);
    }
}
