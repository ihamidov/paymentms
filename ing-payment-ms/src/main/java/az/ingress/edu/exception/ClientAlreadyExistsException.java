package az.ingress.edu.exception;

public class ClientAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ClientAlreadyExistsException(String email) {
        super(String.format("Client with email: '%s' already exists", email));
    }
}
