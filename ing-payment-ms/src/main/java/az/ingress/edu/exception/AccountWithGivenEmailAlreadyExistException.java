package az.ingress.edu.exception;

public class AccountWithGivenEmailAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public AccountWithGivenEmailAlreadyExistException(String email) {
        super(String.format("Account with email: '%s' already exists", email));
    }
}
