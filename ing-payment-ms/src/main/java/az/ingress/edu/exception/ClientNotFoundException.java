package az.ingress.edu.exception;

public class ClientNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public ClientNotFoundException() {
        super("Client not found.");
    }

    public ClientNotFoundException(long id) {
        super(String.format("Client with id %d not found.", id));
    }

}
