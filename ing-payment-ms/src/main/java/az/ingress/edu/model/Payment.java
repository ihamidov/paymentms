package az.ingress.edu.model;

import az.ingress.edu.dto.Currency;
import az.ingress.edu.dto.PaymentStatus;
import az.ingress.edu.dto.PaymentType;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;
    private Long schedulerId;
    private LocalDate dateToSend;
    private long accountId;
    private int amountPerUnit;
    private int quantity;
    private int discount;
    private int subtotal;
    private int total;
    private LocalDate dueDate;
    private String description;
    private PaymentStatus status;
    private Currency currency;
    private PaymentType paymentType;

}
