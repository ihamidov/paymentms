package az.ingress.edu.model;

import az.ingress.edu.dto.PaymentScheduleStatus;
import az.ingress.edu.dto.PeriodType;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentSchedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account account;

    @OneToMany
    private List<Payment> payments;

    private LocalDate dateToSend;

    private PeriodType periodType;

    private Integer numberOfTimesToRepeat;

    private PaymentScheduleStatus status;

}
