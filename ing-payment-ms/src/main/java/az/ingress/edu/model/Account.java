package az.ingress.edu.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 36,message = " max 36 characters allowed")
    private String userUuid;

    @NotBlank(message = " cannot be empty or blank")
    private String name;

    @NotBlank(message = " cannot be empty or blank")
    private String lastName;

    @NotNull(message = " cannot be empty")
    @Email(message = " is not valid")
    private String email;

    @Size(max = 2000, message = " max 2000 characters allowed")
    private String logo;

    private String phoneNumber;

}
