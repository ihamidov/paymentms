package az.ingress.edu.dto;

public enum PeriodType {
    MONTH,WEEK,YEAR,DAY
}
