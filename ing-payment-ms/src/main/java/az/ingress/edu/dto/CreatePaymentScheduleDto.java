package az.ingress.edu.dto;

import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentScheduleDto {

    @Valid
    private CreatePaymentDto payment;

    private PeriodType periodType;

    private Integer numberOfTimesToRepeat;

    private PaymentScheduleStatus status;

}
