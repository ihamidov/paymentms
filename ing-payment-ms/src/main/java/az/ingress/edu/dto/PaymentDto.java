package az.ingress.edu.dto;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDto {

    private long cliendId;
    private List<IdDto> client;
    private int amountPerUnit;
    private int quantity;
    private LocalDate dueDate;
    private int discount;
    private PaymentStatus status;
    private String description;
    private LocalDate dateToSend;
    private Currency currency;
    private PaymentType paymentType;
}
