package az.ingress.edu.dto;

public enum PaymentScheduleStatus {

    ACTIVE, CANCELLED
}
