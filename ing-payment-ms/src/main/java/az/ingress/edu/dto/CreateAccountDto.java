package az.ingress.edu.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
public class CreateAccountDto {

    @Size(max = 36,message = "max 36 characters allowed")
    private String userUuid;

    @NotBlank(message = "cannot be empty or blank")
    private String name;

    @NotBlank(message = "cannot be empty or blank")
    private String lastName;

    @Size(max = 2000, message = "max 2000 characters allowed")
    private String logo;

    @NotNull(message = "cannot be empty")
    @Email(message = "is not valid")
    private String email;

    @Pattern(regexp = "\\+\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d")
    @NotNull(message = "cannot be empty")
    private String phoneNumber;


}
