package az.ingress.edu.dto;

public enum PaymentStatus {

    ACTIVE, CANCELLED, NOTIFIED, PAID, REJECTED
}
