package az.ingress.edu.dto;

public enum Currency {

    AZN,
    USD,
    EUR,
    RUB,
    GBP;

}
