package az.ingress.edu.dto;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePaymentDto {

    @NotNull(message = "Client id can't be empty")
    private long clientId;

    @NotNull(message = "Amount can't be empty")
    private int amountPerUnit;

    @NotNull(message = "Quantity can't be empty")
    private int quantity;

    @NotNull(message = "Discount can't be empty")
    private int discount;

    @NotNull(message = "Subtotal can't be empty")
    private int subtotal;

    @NotNull(message = "Total can't be empty")
    private int total;

    @NotNull(message = "Status can't be empty")
    private PaymentStatus status;

    @NotNull(message = "Date can't be empty")
    private LocalDate dueDate;

    @NotNull(message = "Description can't be empty")
    private String description;

    @NotNull(message = "Date to send can't be empty")
    private LocalDate dateToSend;

    @NotNull(message = "Currency can't be empty")
    private Currency currency;

    @NotNull(message = "Payment type can't be empty")
    private PaymentType paymentType;

}
