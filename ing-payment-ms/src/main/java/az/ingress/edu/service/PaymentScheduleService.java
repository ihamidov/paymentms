package az.ingress.edu.service;

import az.ingress.edu.dto.CreatePaymentScheduleDto;
import az.ingress.edu.model.PaymentSchedule;

public interface PaymentScheduleService {

    PaymentSchedule create(long accountId, CreatePaymentScheduleDto dto);
}
