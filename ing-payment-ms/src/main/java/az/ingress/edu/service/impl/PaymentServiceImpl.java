package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.PaymentDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.exception.InvalidDateToSendException;
import az.ingress.edu.exception.PaymentNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.model.Payment;
import az.ingress.edu.repository.AccountRepository;
import az.ingress.edu.repository.ClientRepository;
import az.ingress.edu.repository.PaymentRepository;
import az.ingress.edu.service.AccountService;
import az.ingress.edu.service.ClientService;
import az.ingress.edu.service.PaymentService;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

@Service
@RequiredArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;
    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;
    private final ClientService clientService;
    private final AccountService accountService;

    @Override
    public Payment updatePayment(long id, PaymentDto paymentDto) {
        Optional<Payment> payment1 = paymentRepository.findById(id);
        checkIfExist(payment1);
        checkIfDateToSendIsValid(paymentDto);
        final Payment payment = Payment.builder()
                .id(id)
                .amountPerUnit(paymentDto.getAmountPerUnit())
                .quantity(paymentDto.getQuantity())
                .dueDate(paymentDto.getDueDate())
                .discount(paymentDto.getDiscount())
                .description(paymentDto.getDescription())
                .dateToSend(paymentDto.getDateToSend())
                .build();
        return paymentRepository.save(payment);
    }

    @Override
    public void deletePaymentById(long id) {
        final Optional<Payment> byId = paymentRepository.findById(id);
        checkIfExist(byId);
        paymentRepository.deleteById(id);
    }

    @Override
    public Iterable<Payment> getPaymentList() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment createPayment(long accountId, CreatePaymentDto createPaymentDto) {
        checkIfAccountExists(accountId);
        checkIfClientExists(createPaymentDto.getClientId());
        checkIfDateToSendIsValid(createPaymentDto);
        Payment payment = toEntity(createPaymentDto);
        payment.setAccountId(accountId);
        return paymentRepository.save(payment);
    }

    @Override
    public List<Payment> getPaymentListByAccountId(long accountId) {
        checkIfAccountExists(accountId);
        return paymentRepository.findByAccountId(accountId);
    }

    @Override
    public Payment getPayment(long paymentId) {
        return paymentRepository.findById(paymentId).orElseThrow(PaymentNotFoundException::new);
    }

    @Override
    public String getPaymentAsHtml(long paymentId) throws IOException {
        Payment payment = getPayment(paymentId);
        Client client = clientService.getClientById(payment.getClient().getId());
        Account account = accountService.getAccount(payment.getAccountId());
        JtwigTemplate jtwigTemplate = JtwigTemplate
                .fileTemplate(ResourceUtils.getFile("classpath:assets/payment.html"));
        JtwigModel jtwigModel = setTemplateValues(payment, client, account);
        return jtwigTemplate.render(jtwigModel);
    }

    @Override
    public byte[] getPaymentAsPdf(long paymentId) throws IOException {
        return generatePdfFromHtml(getPaymentAsHtml(paymentId));
    }

    private JtwigModel setTemplateValues(Payment payment, Client client, Account account) {
        return new JtwigModel()
                .with("from", account.getName() + " " + account.getLastName())
                .with("fromEmail", account.getEmail())
                .with("invoiceId", payment.getId())
                .with("date", LocalDate.now())
                .with("description", payment.getDescription())
                .with("due", payment.getDueDate())
                .with("price", payment.getAmountPerUnit())
                .with("to", client.getName() + " " + client.getLastName())
                .with("toEmail", client.getEmail())
                .with("discount", payment.getDiscount())
                .with("currency", payment.getCurrency())
                .with("paymentType",payment.getPaymentType())
                .with("finalPrice", calculateFinalPrice(payment));
    }

    private static byte[] generatePdfFromHtml(String content) {
        byte[] pdfContent = null;
        try (ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream()) {
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(content);
            renderer.layout();
            renderer.createPDF(pdfOutputStream, false, 1);
            renderer.finishPDF();
            log.info("Pdf generated from html successfully");
            pdfContent = pdfOutputStream.toByteArray();
        } catch (DocumentException | IOException ex) {
            log.error("Payment generation failed");
        }
        return pdfContent;
    }

    private void checkIfAccountExists(long accountId) {
        Optional<Account> byId = accountRepository.findById(accountId);
        byId.orElseThrow(() -> new AccountNotFoundException());
    }

    private void checkIfClientExists(long clientId) {
        Optional<Client> byId = clientRepository.findById(clientId);
        byId.orElseThrow(() -> new ClientNotFoundException());
    }

    private Payment toEntity(CreatePaymentDto dto) {
        return Payment.builder()
                .client(clientRepository.findById(dto.getClientId()).get())
                .amountPerUnit(dto.getAmountPerUnit())
                .discount(dto.getDiscount())
                .description(dto.getDescription())
                .quantity(dto.getQuantity())
                .status(dto.getStatus())
                .dateToSend(dto.getDateToSend())
                .subtotal(dto.getSubtotal())
                .total(dto.getSubtotal())
                .dueDate(dto.getDueDate())
                .dateToSend(dto.getDateToSend())
                .currency(dto.getCurrency())
                .paymentType(dto.getPaymentType())
                .build();
    }

    private void checkIfExist(Optional<Payment> payment) {
        payment.orElseThrow(PaymentNotFoundException::new);
    }

    private double calculateFinalPrice(Payment payment) {
        double subTotal = payment.getAmountPerUnit() * payment.getQuantity();
        return subTotal - subTotal * payment.getDiscount() / 100;
    }

    private void checkIfDateToSendIsValid(CreatePaymentDto createPaymentDto) {
        if (createPaymentDto.getDateToSend().isBefore(LocalDate.now())) {
            throw new InvalidDateToSendException();
        }
    }

    private void checkIfDateToSendIsValid(PaymentDto paymentDto) {
        if (paymentDto.getDateToSend().isBefore(LocalDate.now())) {
            throw new InvalidDateToSendException();
        }
    }

}
