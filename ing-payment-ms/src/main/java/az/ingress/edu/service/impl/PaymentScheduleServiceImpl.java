package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.CreatePaymentScheduleDto;
import az.ingress.edu.dto.PaymentScheduleStatus;
import az.ingress.edu.dto.PeriodType;
import az.ingress.edu.exception.UnknownPeriodTypeException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Payment;
import az.ingress.edu.model.PaymentSchedule;
import az.ingress.edu.repository.PaymentScheduleRepository;
import az.ingress.edu.service.PaymentScheduleService;
import az.ingress.edu.service.PaymentService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class PaymentScheduleServiceImpl implements PaymentScheduleService {

    private PaymentScheduleRepository paymentScheduleRepository;

    private PaymentService paymentService;

    private AccountServiceImpl accountService;

    @Override
    @Transactional
    public PaymentSchedule create(long accountId, CreatePaymentScheduleDto dto) {
        final Account account = accountService.getAccount(accountId);
        dateToSendVerifyAndSet(dto);
        statusVerifyAndSet(dto);
        final PaymentSchedule paymentSchedule = map(account, dto);
        final List<Payment> payments = createInvoice(accountId, dto);
        paymentSchedule.setPayments(payments);
        return paymentScheduleRepository.save(paymentSchedule);
    }

    private List<Payment> createInvoice(long accountId, CreatePaymentScheduleDto dto) {
        List<Payment> payments = new ArrayList<>();
        for (int i = 0; i < dto.getNumberOfTimesToRepeat(); i++) {
            final LocalDate dateToSend = getDate(dto.getPayment().getDateToSend(), dto.getPeriodType(), 1);
            final Payment payment = createInvoice(accountId, dto, dateToSend);
            payments.add(payment);
        }
        return payments;
    }

    private Payment createInvoice(long accountId, CreatePaymentScheduleDto dto,
                                  LocalDate dateToSend) {
        final CreatePaymentDto invoice = dto.getPayment();
        invoice.setDateToSend(dateToSend);
        return paymentService.createPayment(accountId, invoice);
    }

    private LocalDate getDate(LocalDate startDate, PeriodType periodType, int periodCount) {
        switch (periodType) {
            case MONTH:
                return startDate.plusMonths(periodCount);
            case WEEK:
                return startDate.plusWeeks(periodCount);
            case YEAR:
                return startDate.plusYears(periodCount);
            case DAY:
                return startDate.plusDays(periodCount);
            default:
                throw new UnknownPeriodTypeException(String.format("The period type %s not known", periodType));
        }
    }

    private void statusVerifyAndSet(CreatePaymentScheduleDto dto) {
        if (dto.getStatus() == null) {
            dto.setStatus(PaymentScheduleStatus.ACTIVE);
        }
    }

    private PaymentSchedule map(Account account, CreatePaymentScheduleDto dto) {
        return PaymentSchedule.builder()
                .account(account)
                .numberOfTimesToRepeat(dto.getNumberOfTimesToRepeat())
                .periodType(dto.getPeriodType())
                .status(dto.getStatus())
                .build();
    }

    private void dateToSendVerifyAndSet(CreatePaymentScheduleDto dto) {
        if (dto.getPayment().getDateToSend() == null) {
            dto.getPayment().setDateToSend(LocalDate.now());
        }
    }
}
