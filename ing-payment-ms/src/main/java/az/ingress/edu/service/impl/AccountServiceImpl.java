package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.dto.SearchDto;
import az.ingress.edu.exception.AccountNotFoundException;
import az.ingress.edu.exception.AccountPhoneNumberAlreadyExistException;
import az.ingress.edu.exception.AccountWithGivenEmailAlreadyExistException;
import az.ingress.edu.exception.AccountWithGivenUserUuidAlreadyExistException;
import az.ingress.edu.model.Account;
import az.ingress.edu.repository.AccountRepository;
import az.ingress.edu.repository.spec.SearchSpecification;
import az.ingress.edu.service.AccountService;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccount(Long id) {
        final Optional<Account> accountDto = accountRepository.findById(id);
        checkIfExist(accountDto);
        return accountDto.get();
    }

    @Override
    public Account createAccount(CreateAccountDto accountDto) {
        checkAccountByEmail(accountDto);
        checkAccountByPhoneNumber(accountDto.getPhoneNumber());
        if (accountDto.getUserUuid() != null) {
            checkAccountByUserUuid(accountDto);
        }
        Account account = Account.builder()
                .name(accountDto.getName())
                .lastName(accountDto.getLastName())
                .email(accountDto.getEmail())
                .logo(accountDto.getLogo())
                .userUuid(accountDto.getUserUuid())
                .phoneNumber(accountDto.getPhoneNumber())
                .build();
        return accountRepository.save(account);
    }

    @Override
    public Iterable<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public Page<Account> search(SearchDto searchDto, Pageable pageable) {
        SearchSpecification searchSpecification = new SearchSpecification(searchDto.getCriteria());
        return accountRepository.findAll(searchSpecification, pageable);
    }

    private void checkIfExist(Optional<Account> existUser) {
        existUser.orElseThrow(() -> new AccountNotFoundException());
    }

    private void checkAccountByEmail(CreateAccountDto dto) {
        Optional<Account> existUser = accountRepository.findAccountByEmail(dto.getEmail());
        existUser.ifPresent(a -> {
            throw new AccountWithGivenEmailAlreadyExistException(dto.getEmail());
        });
    }

    private void checkAccountByUserUuid(CreateAccountDto dto) {
        Optional<Account> existUser = accountRepository.findAccountByUserUuid(dto.getUserUuid());
        existUser.ifPresent(a -> {
            throw new AccountWithGivenUserUuidAlreadyExistException(dto.getUserUuid());
        });
    }

    private void checkAccountByPhoneNumber(String phoneNumber) {
        Optional<Account> account = accountRepository.findAccountByPhoneNumber(phoneNumber);
        account.ifPresent(a -> {
            throw new AccountPhoneNumberAlreadyExistException(phoneNumber);
        });
    }

}

