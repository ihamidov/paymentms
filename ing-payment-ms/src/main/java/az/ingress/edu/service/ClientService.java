package az.ingress.edu.service;

import az.ingress.edu.dto.CreateClientDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateClientDto;
import az.ingress.edu.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClientService {

    IdDto createClient(long accountId, CreateClientDto dto);

    Client getClientById(long id);

    Page<Client> getClientList(Pageable pageable);

    IdDto updateClientById(long id, UpdateClientDto updateClientDto);
}
