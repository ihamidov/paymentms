package az.ingress.edu.service.impl;

import az.ingress.edu.dto.CreateClientDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateClientDto;
import az.ingress.edu.exception.ClientAlreadyExistsException;
import az.ingress.edu.exception.ClientNotFoundException;
import az.ingress.edu.model.Account;
import az.ingress.edu.model.Client;
import az.ingress.edu.repository.ClientRepository;
import az.ingress.edu.service.AccountService;
import az.ingress.edu.service.ClientService;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final AccountService accountService;

    public ClientServiceImpl(ClientRepository clientRepository, AccountService accountService) {
        this.clientRepository = clientRepository;
        this.accountService = accountService;
    }

    @Override
    public IdDto createClient(long accountId, CreateClientDto dto) {
        Account accountById = accountService.getAccount(accountId);
        checkClientByEmail(dto, accountId);
        Client client = Client.builder()
                .accountId(accountById.getId())
                .name(dto.getName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .build();
        return IdDto.builder()
                .id(clientRepository.save(client)
                        .getId())
                .build();
    }

    @Override
    public Client getClientById(long id) {
        Client client = clientRepository.findById(id).orElseThrow(() ->
                new ClientNotFoundException(id));
        return client;
    }

    @Override
    public Page<Client> getClientList(Pageable pageable) {
        return clientRepository.findAll(pageable);
    }

    @Override
    public IdDto updateClientById(long id, UpdateClientDto updateClientDto) {
        Client client = clientRepository.findById(id).orElseThrow(() -> new ClientNotFoundException(id));
        client.setName(updateClientDto.getName());
        client.setLastName(updateClientDto.getLastName());
        clientRepository.save(client);
        return new IdDto(id);
    }

    private void checkClientByEmail(CreateClientDto dto, long accountId) {
        Optional<Client> existUser = clientRepository.findByEmailAndAccountId(dto.getEmail(), accountId);
        existUser.ifPresent(a -> {
            throw new ClientAlreadyExistsException(dto.getEmail());
        });
    }
}
