package az.ingress.edu.service;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.PaymentDto;
import az.ingress.edu.model.Payment;
import java.io.IOException;
import java.util.List;

public interface PaymentService {

    Iterable<Payment> getPaymentList();

    Payment updatePayment(long id, PaymentDto paymentDto);

    Payment createPayment(long accountId, CreatePaymentDto createPaymentDto);

    List<Payment> getPaymentListByAccountId(long accountId);

    Payment getPayment(long paymentId);

    String getPaymentAsHtml(long paymentId) throws IOException;

    byte[] getPaymentAsPdf(long paymentId) throws IOException;

    void deletePaymentById(long id);

}
