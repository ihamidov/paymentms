package az.ingress.edu.service;

import az.ingress.edu.dto.CreateAccountDto;
import az.ingress.edu.dto.SearchDto;
import az.ingress.edu.model.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AccountService {

    Account getAccount(Long id);

    Account createAccount(CreateAccountDto createAccountDto);

    Iterable<Account> getAllAccounts();

    Page<Account> search(SearchDto searchDto, Pageable pageable);

}
