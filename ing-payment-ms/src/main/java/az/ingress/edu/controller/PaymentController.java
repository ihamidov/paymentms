package az.ingress.edu.controller;

import az.ingress.edu.dto.CreatePaymentDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PaymentDto;
import az.ingress.edu.model.Payment;
import az.ingress.edu.service.PaymentService;
import java.io.IOException;
import java.util.List;
import javax.validation.constraints.Positive;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class PaymentController {

    public static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PutMapping("payment/{id}")
    public Payment updatePayment(@PathVariable("id") long id, @RequestBody PaymentDto paymentDto) {
        return paymentService.updatePayment(id, paymentDto);
    }

    @DeleteMapping("payment/{id}")
    public void deletePaymentById(@Positive(message = ID_MUST_BE_POSITIVE) @PathVariable("id") long id) {
        paymentService.deletePaymentById(id);
    }

    @GetMapping("payment")
    public Iterable<Payment> getPaymentList() {
        return paymentService.getPaymentList();
    }

    @PostMapping("account/{accountId}/payment")
    public IdDto createPayment(@PathVariable long accountId,
                               @Validated @RequestBody CreatePaymentDto createPaymentDto) {
        return IdDto.builder()
                .id(paymentService.createPayment(accountId, createPaymentDto).getId())
                .build();
    }

    @GetMapping("account/{accountId}/payment")
    public List<Payment> getPaymentListByAccountId(@PathVariable long accountId) {
        return paymentService.getPaymentListByAccountId(accountId);
    }

    @GetMapping(value = "/payment/{id}/content/html", produces = "text/html")
    public ResponseEntity<String> getPaymentAsHtml(@Positive(message = ID_MUST_BE_POSITIVE)
                                                   @PathVariable long id) throws IOException {
        return ResponseEntity.ok(paymentService.getPaymentAsHtml(id));

    }

    @GetMapping(value = "/payment/{id}/content/pdf", produces = "application/pdf")
    public ResponseEntity<byte[]> getPaymentAsPdf(@Positive(message = ID_MUST_BE_POSITIVE)
                                                  @PathVariable long id) throws IOException {
        return ResponseEntity.ok(paymentService.getPaymentAsPdf(id));
    }
}
