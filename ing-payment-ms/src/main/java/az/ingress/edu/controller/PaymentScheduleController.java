package az.ingress.edu.controller;

import az.ingress.edu.dto.CreatePaymentScheduleDto;
import az.ingress.edu.model.PaymentSchedule;
import az.ingress.edu.service.PaymentScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PaymentScheduleController {

    private final PaymentScheduleService paymentScheduleService;

    @PostMapping("account/{accountId}/payment-schedule")
    public PaymentSchedule createPayment(@PathVariable long accountId,
                                         @Validated @RequestBody CreatePaymentScheduleDto dto) {
        return paymentScheduleService.create(accountId, dto);
    }

}
