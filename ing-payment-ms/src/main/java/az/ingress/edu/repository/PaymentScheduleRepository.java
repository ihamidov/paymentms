package az.ingress.edu.repository;

import az.ingress.edu.model.PaymentSchedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentScheduleRepository extends CrudRepository<PaymentSchedule, Long> {

}
