package az.ingress.edu.repository;

import az.ingress.edu.model.Payment;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findByAccountId(Long accountId);

}
