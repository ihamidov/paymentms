create table question_answer
(
    id             bigint auto_increment
        primary key,
    answer         varchar(255) null,
    correct_answer int          null,
    question_id    bigint       null,
    foreign key (question_id) references question (id)
) engine=InnoDB;