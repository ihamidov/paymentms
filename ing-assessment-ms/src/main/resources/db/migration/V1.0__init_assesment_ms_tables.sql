create TABLE quiz
(
    id bigint AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) not null
);

create table question_level
(
  id          bigint auto_increment
    primary key,
  description varchar(255) null
);

create table question
(
  id                bigint auto_increment
    primary key,
  question          varchar(255) null,
  question_level_id bigint       null,
  question_type     int          null
);

create index FK_question_level_id
  on question (question_level_id);

create table question_answers
(
  question_id bigint not null,
  answers_id  bigint not null,
  constraint UK_answer
  unique (answers_id)
);

create index FK_question_id
  on question_answers (question_id);



create table quiz_questions
(
  quiz_id      bigint not null,
  questions_id bigint not null,
  primary key (quiz_id, questions_id),
  constraint UK_questions_id
  unique (questions_id)
);




