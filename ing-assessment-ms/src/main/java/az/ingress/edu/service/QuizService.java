package az.ingress.edu.service;

import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.QuizDto;
import az.ingress.edu.model.Quiz;
import org.springframework.data.domain.Page;

public interface QuizService {

    Quiz findById(long id);

    Quiz add(QuizDto quizDto);

    Quiz update(QuizDto quizDto, long id);

    Quiz createQuizQuestion(long quizId, long questionId);

    void deleteById(long id);

    Page<Quiz> getQuizList(PageDto pageDto, String name);
}
