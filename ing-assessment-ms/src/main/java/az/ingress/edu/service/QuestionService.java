package az.ingress.edu.service;

import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.model.Question;
import java.util.List;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public interface QuestionService {

    Question findById(long id);

    Question findByIdNoAnswer(long id);

    Question add(QuestionDto questionDto);

    Question update(QuestionDto questionDto, long id);

    List<Question> toEntityList(Set<QuestionDto> dtoList);

    void deleteById(long id);
}
