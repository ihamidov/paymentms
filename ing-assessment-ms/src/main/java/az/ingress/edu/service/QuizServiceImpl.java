package az.ingress.edu.service;

import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.QuizDto;
import az.ingress.edu.exception.QuizNotFoundException;
import az.ingress.edu.model.Question;
import az.ingress.edu.model.Quiz;
import az.ingress.edu.repository.QuizRepository;
import az.ingress.edu.util.CheckCollection;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    QuizRepository quizRepository;

    @Autowired
    QuestionService questionService;

    @Override
    public Quiz findById(long id) {
        return quizRepository.findById(id).orElseThrow(() -> new QuizNotFoundException(id));
    }

    @Override
    public Quiz add(QuizDto quizDto) {
        Quiz quiz = toEntity(quizDto);
        return quizRepository.save(quiz);
    }

    @Override
    public Quiz update(QuizDto quizDto, long id) {
        Quiz quiz = toEntity(quizDto);
        quiz.setId(id);
        return quizRepository.save(quiz);
    }

    @Override
    public Quiz createQuizQuestion(long quizId, long questionId) {
        Quiz quiz = findById(quizId);
        Question question = questionService.findById(questionId);
        if (CheckCollection.isEmpty(quiz.getQuestions())) {
            quiz.setQuestions(new ArrayList<>());
        }
        quiz.getQuestions().add(question);
        return quizRepository.save(quiz);
    }

    @Override
    public void deleteById(long id) {
        quizRepository.deleteById(id);
    }

    @Override
    public Page<Quiz> getQuizList(PageDto pageDto, String name) {
        return quizRepository.findAll(PageRequest.of(pageDto.getPageNumber(),pageDto.getPageSize(), Sort.by(
                Sort.Direction.fromString(pageDto.getSortDirection()), pageDto.getSortColumn())),name);
    }

    private Quiz toEntity(QuizDto quizDto) {
        List<Question> questions = questionService.toEntityList(quizDto.getQuestions());
        return Quiz.builder()
                .name(quizDto.getName())
                .questions(questions)
                .build();
    }
}
