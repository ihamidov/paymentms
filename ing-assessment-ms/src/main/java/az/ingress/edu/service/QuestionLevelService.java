package az.ingress.edu.service;

import az.ingress.edu.model.QuestionLevel;

public interface QuestionLevelService {

    QuestionLevel findById(long id);
}
