package az.ingress.edu.service;

import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.enums.QuestionTypeEnum;
import az.ingress.edu.exception.QuestionNotFoundException;
import az.ingress.edu.exception.QuestionTypeException;
import az.ingress.edu.model.Question;
import az.ingress.edu.model.QuestionAnswer;
import az.ingress.edu.model.QuestionLevel;
import az.ingress.edu.repository.QuestionRepository;
import az.ingress.edu.util.CheckCollection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    private final QuestionAnswerService questionAnswerService;

    private final QuestionLevelService questionLavelService;

    @Override
    public Question findById(long id) {
        return questionRepository.findById(id).orElseThrow(() -> new QuestionNotFoundException(id));
    }

    @Override
    public Question findByIdNoAnswer(long id) {
        Question question = findById(id);
        if (question != null && !CheckCollection.isEmpty(question.getAnswers())) {
            question.getAnswers().forEach(questionAnswer -> {
                questionAnswer.setCorrectAnswer(null);
            });
        }
        return question;
    }

    @Override
    public Question add(QuestionDto questionDto) {
        Question question = toEntity(questionDto);
        return questionRepository.save(question);
    }

    @Override
    public Question update(QuestionDto questionDto, long id) {
        Question quiz = toEntity(questionDto);
        quiz.setId(id);
        return questionRepository.save(quiz);
    }

    @Override
    public void deleteById(long id) {
        questionRepository.deleteById(id);
    }

    private void chechQuestionLavel(long level) {
        questionLavelService.findById(level);
    }

    private void chechQuestionType(QuestionTypeEnum questionTypeEnum) {
        if (questionTypeEnum == null) {
            throw new QuestionTypeException();
        }
    }

    @Override
    public List<Question> toEntityList(Set<QuestionDto> dtoList) {
        if (!CheckCollection.isEmpty(dtoList)) {
            return dtoList.stream().map(t -> toEntity(t)).collect(Collectors.toList());
        }
        return new ArrayList<Question>();
    }

    private Question toEntity(QuestionDto questionDto) {
        chechQuestionLavel(questionDto.getQuestionLevel().getId());
        chechQuestionType(questionDto.getQuestionType());
        List<QuestionAnswer> questionAnswers = questionAnswerService.toEntityList(questionDto.getAnswers());
        Question question = Question.builder()
                .question(questionDto.getQuestion())
                .answers(questionAnswers)
                .questionLevel(
                        QuestionLevel.builder()
                                .id(questionDto.getQuestionLevel().getId())
                                .build())
                .questionType(questionDto.getQuestionType())
                .build();
        return question;
    }
}
