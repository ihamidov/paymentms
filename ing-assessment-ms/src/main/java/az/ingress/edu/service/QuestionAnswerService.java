package az.ingress.edu.service;

import az.ingress.edu.dto.QuestionAnswerDto;
import az.ingress.edu.model.QuestionAnswer;
import java.util.List;
import java.util.Set;

public interface QuestionAnswerService {

    List<QuestionAnswer> toEntityList(Set<QuestionAnswerDto> dtoList);
}
