package az.ingress.edu.service;

import az.ingress.edu.dto.QuestionAnswerDto;
import az.ingress.edu.model.QuestionAnswer;
import az.ingress.edu.util.CheckCollection;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class QuestionAnswerServiceImpl implements QuestionAnswerService {

    @Override
    public List<QuestionAnswer> toEntityList(Set<QuestionAnswerDto> dtoList) {
        if (!CheckCollection.isEmpty(dtoList)) {
            return dtoList.stream().map(t -> toEntity(t)).collect(Collectors.toList());
        }
        return new ArrayList<QuestionAnswer>();
    }

    private QuestionAnswer toEntity(QuestionAnswerDto questionAnswerDto) {
        Boolean answer = false;
        if (questionAnswerDto.getCorrectAnswer() != null) {
            answer = questionAnswerDto.getCorrectAnswer();
        }
        return QuestionAnswer.builder()
                .answer(questionAnswerDto.getAnswer())
                .correctAnswer(answer)
                .build();
    }
}
