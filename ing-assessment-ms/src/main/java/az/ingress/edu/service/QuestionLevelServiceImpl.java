package az.ingress.edu.service;

import az.ingress.edu.exception.QuestionLevelNotFoundException;
import az.ingress.edu.model.QuestionLevel;
import az.ingress.edu.repository.QuestionLevelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class QuestionLevelServiceImpl implements QuestionLevelService {

    private final QuestionLevelRepository questionLavelRepository;

    @Override
    public QuestionLevel findById(long id) {
        return questionLavelRepository.findById(id).orElseThrow(() -> new QuestionLevelNotFoundException(id));
    }
}
