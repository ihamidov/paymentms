package az.ingress.edu.exception;

import az.ingress.edu.util.Common;

public class QuizNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public QuizNotFoundException(long id) {
        super(String.format(Common.QUIZ_BY_ID_NOT_FOUND, id));
    }

}
