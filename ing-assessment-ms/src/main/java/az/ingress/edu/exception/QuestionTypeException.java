package az.ingress.edu.exception;

import az.ingress.edu.util.Common;

public class QuestionTypeException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public QuestionTypeException() {
        super(Common.QUESTION_TYPE_EXCEPTION);
    }

}
