package az.ingress.edu.exception;

import az.ingress.edu.util.Common;

public class QuestionLevelNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public QuestionLevelNotFoundException(long id) {
        super(String.format(Common.QUESTION_LEVEL_BY_ID_NOT_FOUND, id));
    }

}
