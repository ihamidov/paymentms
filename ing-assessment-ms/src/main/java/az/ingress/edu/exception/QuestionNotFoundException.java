package az.ingress.edu.exception;

import az.ingress.edu.util.Common;

public class QuestionNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public QuestionNotFoundException(long id) {
        super(String.format(Common.QUESTION_BY_ID_NOT_FOUND, id));
    }

}
