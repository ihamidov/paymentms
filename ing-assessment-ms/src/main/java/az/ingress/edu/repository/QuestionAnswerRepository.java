package az.ingress.edu.repository;

import az.ingress.edu.model.QuestionAnswer;
import org.springframework.data.repository.CrudRepository;

public interface QuestionAnswerRepository extends CrudRepository<QuestionAnswer, Long> {

}
