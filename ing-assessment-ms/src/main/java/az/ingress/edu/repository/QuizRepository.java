package az.ingress.edu.repository;

import az.ingress.edu.model.Quiz;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.util.StringUtils;

public interface QuizRepository extends CrudRepository<Quiz, Long>, PagingAndSortingRepository<Quiz, Long>,
        JpaSpecificationExecutor<Quiz> {

    default Page<Quiz> findAll(Pageable pageable, String name) {
        return findAll((root, query, cb) ->
                        cb.and(!StringUtils.isEmpty(name) ? cb.like(cb.lower(root.get("name")), "%"
                                + name.toLowerCase() + "%") : cb.conjunction()), pageable);
    }

}
