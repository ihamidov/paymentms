package az.ingress.edu.repository;

import az.ingress.edu.model.Question;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {

}
