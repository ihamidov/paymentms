package az.ingress.edu.repository;

import az.ingress.edu.model.QuestionLevel;
import org.springframework.data.repository.CrudRepository;

public interface QuestionLevelRepository extends CrudRepository<QuestionLevel, Long> {

}
