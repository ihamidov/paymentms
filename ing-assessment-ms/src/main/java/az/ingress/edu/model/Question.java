package az.ingress.edu.model;

import az.ingress.edu.enums.QuestionTypeEnum;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Question can't be empty")
    @Size(min = 10, message = "Question should have at least 10 characthers")
    private String question;

    @ManyToOne
    private QuestionLevel questionLevel;

    @Enumerated(EnumType.ORDINAL)
    private QuestionTypeEnum questionType;

    @OneToMany(cascade = CascadeType.ALL)
    private List<QuestionAnswer> answers;
}
