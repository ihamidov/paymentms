package az.ingress.edu.controller;

import static az.ingress.edu.util.Common.DEFAULT_PAGE_NUMBER;
import static az.ingress.edu.util.Common.DEFAULT_PAGE_SIZE;
import static az.ingress.edu.util.Common.DESC;
import static az.ingress.edu.util.Common.ID;
import static az.ingress.edu.util.Common.MIN_KEY_LENGTH;
import static az.ingress.edu.util.Common.PAGE_INDEX_MUST_BE_POSITIVE;
import static az.ingress.edu.util.Common.PAGE_SIZE_MUST_BE_POSITIVE;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.QuizDto;
import az.ingress.edu.model.Quiz;
import az.ingress.edu.service.QuizService;
import az.ingress.edu.util.Common;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/quiz")
@Api(Common.QUIZ_API)
@AllArgsConstructor
public class QuizRestController {

    private final QuizService quizService;

    @GetMapping("/{id}")
    @ApiOperation(Common.GET_QUIZ_BY_ID)
    public Quiz findQuestionById(@PathVariable @Positive(message = Common.QUIZ_ID_MUST_BE_POSITIVE) long id) {
        return quizService.findById(id);
    }

    @PostMapping("/")
    @ApiOperation(Common.ADD_QUIZ)
    public IdDto createQuiz(@Validated @RequestBody QuizDto quizDto) {
        return new IdDto(quizService.add(quizDto).getId());
    }

    @PutMapping("/{id}")
    @ApiOperation(Common.UPDATE_QUIZ_BY_ID)
    public IdDto updateQuiz(@Validated @RequestBody QuizDto quizDto,
                            @PathVariable @Positive(message = Common.QUIZ_ID_MUST_BE_POSITIVE) long id) {
        quizService.findById(id);
        return new IdDto(quizService.update(quizDto, id).getId());
    }

    @DeleteMapping("/{id}")
    @ApiOperation(Common.DELETE_QUIZ_BY_ID)
    public void deleteQuiz(
            @PathVariable @Positive(message = Common.QUIZ_ID_MUST_BE_POSITIVE) long id) {
        quizService.findById(id);
        quizService.deleteById(id);
    }

    @PostMapping("/{quizId}/question/{questionId}")
    @ApiOperation("Add a new quiz question")
    public IdDto createQuizQuestion(
            @PathVariable @Positive(message = Common.QUIZ_ID_MUST_BE_POSITIVE) long quizId,
            @PathVariable @Positive(message = Common.QUESTION_ID_MUST_BE_POSITIVE) long questionId) {
        return new IdDto(quizService.createQuizQuestion(quizId, questionId).getId());
    }


    @GetMapping
    @ApiOperation("Get quiz list")
    public Page<Quiz> getQuizList(@RequestParam(defaultValue = DEFAULT_PAGE_NUMBER)
                                  @PositiveOrZero(message = PAGE_INDEX_MUST_BE_POSITIVE) int page,
                                  @RequestParam(defaultValue = DEFAULT_PAGE_SIZE)
                                  @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                  @RequestParam(defaultValue = ID) String sortCol,
                                  @RequestParam(defaultValue = DESC) String sortDir,
                                  @Length(min = 3, message = MIN_KEY_LENGTH)
                                  @RequestParam(required = false) String name) {
        return quizService.getQuizList(PageDto.builder()
                .pageNumber(page)
                .pageSize(size)
                .sortColumn(sortCol)
                .sortDirection(sortDir)
                .build(), name);
    }
}
