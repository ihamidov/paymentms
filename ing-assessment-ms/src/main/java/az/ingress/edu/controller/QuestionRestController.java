package az.ingress.edu.controller;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.model.Question;
import az.ingress.edu.service.QuestionService;
import az.ingress.edu.util.Common;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/question")
@Api(Common.QUESTION_API)
@AllArgsConstructor
public class QuestionRestController {

    private final QuestionService questionService;

    @GetMapping("/{id}")
    @ApiOperation(Common.GET_QUESTION_BY_ID)
    public Question findQuestionById(@PathVariable @Positive(message = Common.QUESTION_ID_MUST_BE_POSITIVE) long id) {
        return questionService.findById(id);
    }

    @GetMapping("/{id}/testMode")
    @ApiOperation(Common.GET_QUESTION_BY_ID)
    public Question findQuestionByIdNoAnswer(
            @PathVariable @Positive(message = Common.QUESTION_ID_MUST_BE_POSITIVE) long id) {
        return questionService.findByIdNoAnswer(id);
    }

    @PostMapping("/")
    @ApiOperation(Common.ADD_QUESTION)
    public IdDto createQuestion(@Validated @RequestBody QuestionDto questionDto) {
        return new IdDto(questionService.add(questionDto).getId());
    }

    @PutMapping("/{id}")
    @ApiOperation(Common.UPDATE_QUESTION_BY_ID)
    public IdDto updateQuestion(@Validated @RequestBody QuestionDto questionDto,
            @PathVariable @Positive(message = Common.QUESTION_ID_MUST_BE_POSITIVE) long id) {
        questionService.findById(id);
        return new IdDto(questionService.update(questionDto, id).getId());
    }

    @DeleteMapping("/{id}")
    @ApiOperation(Common.DELETE_QUESTION_BY_ID)
    public void deleteQuestion(
            @PathVariable @Positive(message = Common.QUESTION_ID_MUST_BE_POSITIVE) long id) {
        questionService.findById(id);
        questionService.deleteById(id);
    }
}
