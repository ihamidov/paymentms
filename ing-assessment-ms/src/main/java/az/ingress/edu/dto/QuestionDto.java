package az.ingress.edu.dto;

import az.ingress.edu.enums.QuestionTypeEnum;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDto {

    @NotEmpty(message = "Question can't be empty")
    @Size(min = 10, message = "Question should have at least 10 characthers")
    private String question;

    @NotNull(message = "Question Level can't be null")
    private IdDto questionLevel;

    private QuestionTypeEnum questionType;

    private Set<QuestionAnswerDto> answers;
}
