package az.ingress.edu.dto;

import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuizDto {

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 2, message = "Name should have at least 2 characthers")
    private String name;

    private Set<QuestionDto> questions;
}
