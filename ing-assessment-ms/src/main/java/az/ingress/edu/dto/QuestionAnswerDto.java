package az.ingress.edu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionAnswerDto {

    @NotEmpty(message = "Question answer can't be empty")
    @Size(min = 2, message = "Question answer should have at least 2 characthers")
    private String answer;

    @JsonInclude(Include.NON_NULL)
    private Boolean correctAnswer;
}
