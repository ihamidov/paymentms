package az.ingress.edu.util;

public class Common {
    public static final String QUIZ_API = "Quiz API";
    public static final String GET_QUIZ_BY_ID = "Get quiz by id";
    public static final String ADD_QUIZ = "Add quiz";
    public static final String UPDATE_QUIZ_BY_ID = "Update quiz by id";
    public static final String DELETE_QUIZ_BY_ID = "Delete quiz by id";
    public static final String QUIZ_ID_MUST_BE_POSITIVE = "Quiz id must be positive";
    public static final String QUIZ_BY_ID_NOT_FOUND = "Quiz with id %d not found";

    public static final String QUESTION_API = "Question API";
    public static final String GET_QUESTION_BY_ID = "Get question by id";
    public static final String ADD_QUESTION = "Add question";
    public static final String UPDATE_QUESTION_BY_ID = "Update question by id";
    public static final String DELETE_QUESTION_BY_ID = "Delete question by id";
    public static final String QUESTION_ID_MUST_BE_POSITIVE = "Question id must be positive";
    public static final String QUESTION_BY_ID_NOT_FOUND = "Question with id %d not found";
    public static final String QUESTION_TYPE_EXCEPTION = "Question type should be single or multiply";
    public static final String QUESTION_LEVEL_BY_ID_NOT_FOUND = "Question Level with id %d not found";

    public static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    public static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    public static final String ID = "id";
    public static final String DEFAULT_PAGE_NUMBER = "0";
    public static final String DEFAULT_PAGE_SIZE = "10";
    public static final String DESC = "DESC";
    public static final String MIN_KEY_LENGTH = "Minimum 3 character required";
}
