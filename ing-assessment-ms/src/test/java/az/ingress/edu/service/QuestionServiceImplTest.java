package az.ingress.edu.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.enums.QuestionTypeEnum;
import az.ingress.edu.model.Question;
import az.ingress.edu.model.QuestionAnswer;
import az.ingress.edu.model.QuestionLevel;
import az.ingress.edu.repository.QuestionRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class QuestionServiceImplTest {

    private static final String QUESTION_NAME = "How old are you?";

    @Mock
    private QuestionRepository questionRepository;

    @Mock
    private QuestionLevelService questionLavelService;

    @Mock
    private QuestionAnswerService questionAnswerService;

    @InjectMocks
    private QuestionServiceImpl questionService;

    private Question question;

    private QuestionLevel questionLevel;

    private QuestionDto questionDto;

    @Before
    public void setUp() {
        questionLevel = QuestionLevel.builder()
                .id(1L)
                .build();
        question = Question.builder()
                .id(1L)
                .question(QUESTION_NAME)
                .questionLevel(questionLevel)
                .questionType(QuestionTypeEnum.SINGLE)
                .build();
        questionDto = QuestionDto.builder()
                .question(QUESTION_NAME)
                .questionLevel(new IdDto(1L))
                .questionType(QuestionTypeEnum.SINGLE)
                .build();
    }

    @Test
    public void findById() {
        when(questionRepository.findById(anyLong())).thenReturn(Optional.of(question));

        assertThat(questionService.findById(anyLong()).getQuestion()).isEqualTo(QUESTION_NAME);
        verify(questionRepository).findById(anyLong());
    }

    @Test
    public void findByIdNoAnswer() {
        //question.setCorrectAnswer(null);
        when(questionRepository.findById(anyLong())).thenReturn(Optional.of(question));

        assertThat(questionService.findByIdNoAnswer(anyLong()).getQuestion()).isEqualTo(QUESTION_NAME);
        verify(questionRepository).findById(anyLong());
    }

    @Test
    public void add() {
        final Question quizForSave = Question.builder()
                .question(QUESTION_NAME)
                .questionLevel(questionLevel)
                .questionType(QuestionTypeEnum.SINGLE)
                .answers(new ArrayList<>())
                .build();
        List<QuestionAnswer> expectedAnswers = new ArrayList<>();
        when(questionLavelService.findById(anyLong())).thenReturn(questionLevel);
        when(questionRepository.save(any(Question.class))).thenReturn(question);
        when(questionAnswerService.toEntityList(questionDto.getAnswers())).thenReturn(expectedAnswers);

        Question expectedQuiz = questionService.add(questionDto);
        assertThat(expectedQuiz.getQuestion()).isEqualTo(QUESTION_NAME);

        verify(questionRepository).save(quizForSave);
    }

    @Test
    public void update() {
        final Question quizForSave = Question.builder()
                .id(1)
                .question(QUESTION_NAME)
                .questionLevel(questionLevel)
                .questionType(QuestionTypeEnum.SINGLE)
                .answers(new ArrayList<>())
                .build();
        List<QuestionAnswer> expectedAnswers = new ArrayList<>();
        when(questionLavelService.findById(anyLong())).thenReturn(questionLevel);
        when(questionRepository.save(any(Question.class))).thenReturn(question);
        when(questionAnswerService.toEntityList(questionDto.getAnswers())).thenReturn(expectedAnswers);

        Question expectedQuiz = questionService.update(questionDto, 1L);
        assertThat(expectedQuiz.getQuestion()).isEqualTo(QUESTION_NAME);

        verify(questionRepository).save(quizForSave);
    }

    @Test
    public void deleteById() {
        questionService.deleteById(question.getId());

        verify(questionRepository).deleteById(question.getId());
    }
}
