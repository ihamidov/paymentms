package az.ingress.edu.service;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.QuizDto;
import az.ingress.edu.model.Question;
import az.ingress.edu.model.Quiz;
import az.ingress.edu.repository.QuizRepository;
import az.ingress.edu.util.Common;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class QuizServiceImplTest {

    private static final String QUIZ_NAME = "Ingress";

    private static final String QUESTION_NAME = "How old are you?";

    @Mock
    private QuizRepository quizRepository;

    @Mock
    QuestionService questionService;

    @InjectMocks
    private QuizServiceImpl quizService;

    private Question question;

    private QuizDto quizDto;

    private Quiz quiz;

    @Before
    public void setUp() {
        quizDto = QuizDto.builder()
                .name(QUIZ_NAME)
                .build();
        quiz = Quiz.builder()
                .id(1L)
                .name(QUIZ_NAME)
                .questions(new ArrayList<>())
                .build();
        question = Question.builder()
                .id(1L)
                .question(QUESTION_NAME)
                .build();
    }

    @Test
    public void findById() {
        when(quizRepository.findById(anyLong())).thenReturn(Optional.of(quiz));

        assertThat(quizService.findById(anyLong()).getName()).isEqualTo(QUIZ_NAME);
        verify(quizRepository).findById(anyLong());
    }

    @Test
    public void add() {
        final Quiz quizForSave = Quiz.builder()
                .name(QUIZ_NAME)
                .questions(new ArrayList<>())
                .build();
        when(quizRepository.save(any(Quiz.class))).thenReturn(quiz);

        Quiz expectedQuiz = quizService.add(quizDto);
        assertThat(expectedQuiz.getName()).isEqualTo(QUIZ_NAME);

        verify(quizRepository).save(quizForSave);
    }

    @Test
    public void update() {
        final Quiz quizForSave = Quiz.builder()
                .id(1L)
                .name(QUIZ_NAME)
                .questions(new ArrayList<>())
                .build();
        when(quizRepository.save(any(Quiz.class))).thenReturn(quiz);

        quiz = quizService.update(quizDto, quiz.getId());
        assertThat(quiz.getName()).isEqualTo(quizDto.getName());

        verify(quizRepository).save(quizForSave);
    }

    @Test
    public void createQuizQuestion() {
        when(quizRepository.findById(anyLong())).thenReturn(Optional.of(quiz));
        when(questionService.findById(anyLong())).thenReturn(question);
        when(quizRepository.save(any(Quiz.class))).thenReturn(quiz);

        assertThat(quizService.createQuizQuestion(1, 1).getName()).isEqualTo(QUIZ_NAME);

        verify(quizRepository).save(quiz);
    }

    @Test
    public void deleteById() {
        quizService.deleteById(quiz.getId());

        verify(quizRepository).deleteById(quiz.getId());
    }

    @Test
    public void getQuizList() {
        PageDto pageDto = PageDto.builder()
                .pageNumber(0)
                .pageSize(10)
                .sortColumn(Common.ID)
                .sortDirection(Common.DESC)
                .build();
        List<Quiz> quizList = Collections.singletonList(quiz);
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.fromString(Common.DESC), Common.ID));
        Page quizPage = new PageImpl(quizList, pageable, 1);
        when(quizRepository.findAll(any(Pageable.class), anyString())).thenReturn(quizPage);

        Page<Quiz> resultPage = quizService.getQuizList(pageDto, QUIZ_NAME);
        assertThat(resultPage.getContent().get(0).getName()).isEqualTo(QUIZ_NAME);
        verify(quizRepository).findAll(pageable, QUIZ_NAME);
    }
}
