package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.TestUtils;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.exception.QuestionLevelNotFoundException;
import az.ingress.edu.exception.QuestionNotFoundException;
import az.ingress.edu.model.Question;
import az.ingress.edu.service.QuestionServiceImpl;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(QuestionRestController.class)
@ActiveProfiles("test")
public class QuestionRestControllerTest {

    private static final String QUESTION_NAME = "How old are you?";

    private static final String QUESTION_NOT_FOUND_EXCEPTION = "Question with id 1 not found";
    private static final String LEVEL_NOT_FOUND_EXCEPTION = "Question Level with id 4 not found";
    private static final String QUESTION_LENGTH_EXCEPTION = "Question should have at least 10 characthers";

    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHINICAL_MESSAGE = "technicalMessage";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QuestionServiceImpl questionService;

    private Question question;

    private QuestionDto questionDto;

    @Before
    public void setUp() {
        questionDto = QuestionDto.builder()
                .question(QUESTION_NAME)
                .questionLevel(new IdDto(1L))
                .build();
        question = Question.builder()
                .id(1L)
                .question(QUESTION_NAME)
                .build();
    }

    @Test
    public void findQuestionById() throws Exception {
        when(questionService.findById(anyLong())).thenReturn(question);

        mvc.perform(get("/question/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findQuestionByIdNoAnswer() throws Exception {
        when(questionService.findById(anyLong())).thenReturn(question);

        mvc.perform(get("/question/{id}/testMode", 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findQuestionByIdReturnsNotFound() throws Exception {
        when(questionService.findById(anyLong())).thenThrow(new QuestionNotFoundException(1));

        mvc.perform(get("/question/1")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(QUESTION_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(QUESTION_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void deleteQuestion() throws Exception {
        mvc.perform(delete("/question/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void createQuestion() throws Exception {
        when(questionService.add(questionDto)).thenReturn(question);

        mvc.perform(post("/question/")
                .content(new TestUtils().asJsonString(questionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(question.getId()));
    }

    @Test
    public void createQuestionQuestionLeveNotFound() throws Exception {
        questionDto.setQuestionLevel(new IdDto(4L));
        when(questionService.add(questionDto)).thenThrow(new QuestionLevelNotFoundException(4L));

        mvc.perform(post("/question/")
                .content(new TestUtils().asJsonString(questionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LEVEL_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(LEVEL_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void createQuestionQuestionValidate() throws Exception {
        when(questionService.add(questionDto)).thenReturn(question);
        questionDto.setQuestion("Whats up?");

        mvc.perform(post("/question/")
                .content(new TestUtils().asJsonString(questionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(QUESTION_LENGTH_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(QUESTION_LENGTH_EXCEPTION)));
    }

    @Test
    public void updateQuestion() throws Exception {
        when(questionService.findById(anyLong())).thenReturn(question);
        when(questionService.update(questionDto, 1)).thenReturn(question);

        mvc.perform(put("/question/{id}", 1)
                .content(new TestUtils().asJsonString(questionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }
}
