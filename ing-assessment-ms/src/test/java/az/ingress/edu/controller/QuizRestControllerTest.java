package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.TestUtils;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.QuestionDto;
import az.ingress.edu.dto.QuizDto;
import az.ingress.edu.exception.QuestionTypeException;
import az.ingress.edu.exception.QuizNotFoundException;
import az.ingress.edu.model.Question;
import az.ingress.edu.model.Quiz;
import az.ingress.edu.service.QuizService;
import az.ingress.edu.util.Common;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(QuizRestController.class)
@ActiveProfiles("test")
public class QuizRestControllerTest {

    private static final String QUIZ_NAME = "Ingress";
    private static final String QUESTION_NAME = "How old are you?";

    private static final String QUIZ_NOT_FOUND_EXCEPTION = "Quiz with id 1 not found";
    public static final String QUESTION_TYPE_EXCEPTION = "Question type should be single or multiply";

    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHINICAL_MESSAGE = "technicalMessage";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private QuizService quizService;

    private Quiz quiz;

    private QuizDto quizDto;

    private QuestionDto questionDto;

    @Before
    public void setUp() {
        questionDto = QuestionDto.builder()
                .question(QUESTION_NAME)
                .questionLevel(new IdDto(1L))
                .build();
        Question question = Question.builder()
                .id(1L)
                .question(QUESTION_NAME)
                .build();
        quizDto = QuizDto.builder()
                .name(QUIZ_NAME)
                .questions(Stream.of(questionDto).collect(Collectors.toSet()))
                .build();
        quiz = Quiz.builder()
                .id(1L)
                .name(QUIZ_NAME)
                .questions(Arrays.asList(question))
                .build();
    }

    @Test
    public void findQuizById() throws Exception {
        when(quizService.findById(anyLong())).thenReturn(quiz);

        mvc.perform(get("/quiz/{id}", 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findQuizByIdReturnsNotFound() throws Exception {
        doThrow(new QuizNotFoundException(1)).when(quizService).findById(anyLong());

        mvc.perform(get("/quiz/1")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(QUIZ_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(QUIZ_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void deleteQuiz() throws Exception {
        mvc.perform(delete("/quiz/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void createQuiz() throws Exception {
        when(quizService.add(quizDto)).thenReturn(quiz);

        mvc.perform(post("/quiz/")
                .content(new TestUtils().asJsonString(quizDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(quiz.getId()));
    }

    @Test
    public void createQuizQuestion() throws Exception {
        questionDto = QuestionDto.builder()
                .questionLevel(new IdDto(1L))
                .build();
        quizDto.setQuestions(Stream.of(questionDto).collect(Collectors.toSet()));

        when(quizService.add(quizDto)).thenThrow(new QuestionTypeException());

        mvc.perform(post("/quiz/")
                .content(new TestUtils().asJsonString(quizDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(QUESTION_TYPE_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(QUESTION_TYPE_EXCEPTION)));
    }

    @Test
    public void updateQuiz() throws Exception {
        when(quizService.findById(anyLong())).thenReturn(quiz);
        when(quizService.update(quizDto, 1)).thenReturn(quiz);

        mvc.perform(put("/quiz/{id}", 1)
                .content(new TestUtils().asJsonString(quizDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    public void createQuizQuestionTest() throws Exception {
        when(quizService.createQuizQuestion(anyLong(), anyLong())).thenReturn(quiz);

        mvc.perform(post("/quiz/1/question/1")
                .content(new TestUtils().asJsonString(quizDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(quiz.getId()));
    }

    @Test
    public void givenInvalidPageNumberGetQuizListExpectErrorMessage() throws Exception {
        mvc.perform(get("/quiz")
                .param("page", "-1")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(Common.PAGE_INDEX_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(Common.PAGE_INDEX_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenInvalidPageSizeGetQuizListExpectErrorMessage() throws Exception {
        mvc.perform(get("/quiz")
                .param("size", "-1")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(Common.PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(Common.PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenValidQueryParamsGetQuizListExpectOk() throws Exception {
        PageDto pageDto = PageDto.builder()
                .pageNumber(0)
                .pageSize(10)
                .sortColumn(Common.ID)
                .sortDirection(Common.DESC)
                .build();
        Page page = new PageImpl(Collections.singletonList(quiz), PageRequest.of(0, 10, Sort
                .by(Sort.Direction.fromString(Common.DESC), Common.ID)), 1L);
        when(quizService.getQuizList(pageDto, QUIZ_NAME)).thenReturn(page);
        mvc.perform(get("/quiz")
                .param("page", "0")
                .param("size", "10")
                .param("name", QUIZ_NAME)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].name", is(QUIZ_NAME)));
    }
}
