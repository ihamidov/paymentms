create table tag
(
    id bigint auto_increment
        primary key,
    deleted bit not null,
    name    varchar(255) not null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table category
(
    id bigint auto_increment primary key,
    deleted bit not null,
    name    varchar(255) not null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table user
(
    email     varchar(255) not null,
    firstname varchar(255) not null,
    lastname  varchar(255) not null,
    phone     varchar(255) null,
    username  varchar(255) not null,
    photo_id  bigint       null,
    primary key (username)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table contact_info
(
    id      bigint auto_increment
        primary key,
    address varchar(255) null,
    email   varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table contact_info_contact_numbers
(
    contact_info_id bigint       not null,
    contact_numbers varchar(255) null,
    foreign key (contact_info_id) references contact_info (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table training_center
(
    id              bigint auto_increment
        primary key,
    deleted         bit          not null,
    description     longtext     null,
    name            varchar(255) null,
    photo_id        bigint       null,
    contact_info_id bigint       null,
    foreign key (contact_info_id) references contact_info (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table course
(
    id                 bigint auto_increment
        primary key,
    description        longtext     null,
    name               varchar(255) null,
    requirements       longtext null,
    short_description  varchar(255) null,
    status             varchar(255) null,
    created_at         datetime(6)  not null,
    deleted            bit          not null,
    modified_at        datetime(6)  null,
    photo_id           bigint       null,
    category           bigint       null,
    created_by         varchar(255)       not null,
    modified_by        varchar(255)       null,
    training_center_id bigint       null,
    foreign key (modified_by) references user (username),
    foreign key (created_by) references user (username),
    foreign key (category) references category (id),
    foreign key (training_center_id) references training_center (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table course_tags
(
    course_id bigint not null,
    tags_id   bigint not null,
        foreign key (course_id) references course (id),
        foreign key (tags_id) references tag (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table course_session
(
    id                   bigint auto_increment
        primary key,
    course_id            bigint       not null,
    course_schedule      varchar(255) not null,
    deleted              bit          not null,
    end_date             datetime(6)  not null,
    enrolled_student_cnt int          not null,
    name                 varchar(255) null,
    number_of_lessons    int          not null,
    photo_id             bigint       null,
    required_student_cnt int          not null,
    short_description    varchar(255) null,
    start_date           datetime(6)  not null,
    status               varchar(255) not null,
    trainer              bigint       null,
    category             bigint       null,
    foreign key (category) references category (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table course_session_trainers
(
    course_session_id bigint not null,
    trainers_username varchar(255) not null,
    foreign key(course_session_id) references course_session(id),
    foreign key (trainers_username) references user(username)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table course_session_tags
(
    course_session_id bigint not null,
    tags_id           bigint not null,
    foreign key (tags_id) references tag (id),
    foreign key (course_session_id) references course_session (id)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table enrollment
(
    id                bigint auto_increment
        primary key,
    course_session_id bigint       null,
    created_at        datetime     null,
    deleted           bit          not null,
    enrollment_status varchar(255) null,
    username          varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table review
(
    id         bigint auto_increment
        primary key,
    comment    longtext     null,
    created_at datetime(6)  null,
    item_id    bigint       not null,
    item_type  varchar(255) not null,
    rating     int          not null,
    created_by varchar(255) not null,
    foreign key (created_by) references user (username)
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table syllabus
(
    id          bigint auto_increment
        primary key,
    child_count bigint       not null,
    course_id   bigint       not null,
    description varchar(255) null,
    parent_id   bigint       null,
    title       varchar(255) null,
    duration    time         null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
