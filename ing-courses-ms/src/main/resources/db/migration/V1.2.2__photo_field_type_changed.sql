ALTER TABLE course CHANGE photo_id  photo varchar(2000) null;
ALTER TABLE course_session CHANGE photo_id photo varchar(2000) null;
ALTER TABLE training_center CHANGE photo_id photo varchar(2000) null;
ALTER TABLE user CHANGE photo_id photo varchar(2000) null;

