package az.ingress.edu.service;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateSyllabusDto;
import az.ingress.edu.model.Syllabus;
import java.util.List;

public interface SyllabusService {

    Syllabus createSyllabus(Syllabus syllabus);

    List<Syllabus> getSyllabi(long courseId);

    Syllabus getSyllabus(long id);

    void deleteSyllabus(long id);
    
    IdDto updateSyllabus(UpdateSyllabusDto syllabusDto,long id);

    List<Syllabus> getChildSyllabi(long parentId);
}
