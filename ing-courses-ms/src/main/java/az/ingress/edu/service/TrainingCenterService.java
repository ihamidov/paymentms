package az.ingress.edu.service;

import az.ingress.edu.dto.TrainingCenterDto;
import az.ingress.edu.model.TrainingCenter;

public interface TrainingCenterService {

    TrainingCenter findById(Long id);

    Iterable<TrainingCenter> findAll();

    TrainingCenter add(TrainingCenterDto trainingCenterDto);

    TrainingCenter update(TrainingCenterDto trainingCenterDto, Long id);

    void deleteById(Long id);
}
