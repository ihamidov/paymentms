package az.ingress.edu.service;

import az.ingress.edu.dto.CourseSessionDto;
import az.ingress.edu.dto.CourseSessionFilterDto;
import az.ingress.edu.dto.UsernameDto;
import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.exception.MinimumSessionDurationException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.CourseSessionRepository;
import az.ingress.edu.repository.UserRepository;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CourseSessionServiceImpl implements CourseSessionService {

    private CourseService courseService;
    private UserRepository userRepository;
    private CourseSessionRepository courseSessionRepository;

    @Override
    public CourseSession createCourseSession(CourseSessionDto sessionDto, long courseId) {
        Course course = courseService.findById(courseId);
        CourseSession courseSession = CourseSessionDto.toCourseSession(sessionDto);
        courseSession.setTrainers(getIfTrainersExist(sessionDto.getTrainers()));
        checkMinimumCourseSessionDuration(courseSession);
        getCourseProperties(courseSession, course);
        return courseSessionRepository.save(courseSession);
    }

    @Override
    public Page<CourseSession> getCourseSessionList(long courseId, String direction, String order, int page, int size) {
        checkIfCourseExists(courseId);
        return courseSessionRepository
                .findAllByCourseIdAndDeletedFalse(courseId, PageRequest.of(page, size,
                        Sort.by(Sort.Direction.fromString(direction), order)));
    }

    @Override
    public CourseSession getCourseSession(long id) {
        return courseSessionRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new CourseSessionNotFoundException(id));
    }

    @Override
    public void deleteCourseSession(long id) {
        CourseSession session = getCourseSession(id);
        session.setDeleted(true);
        courseSessionRepository.save(session);
    }

    @Override
    public CourseSession updateCourseSession(CourseSessionDto sessionDto, long id) {
        CourseSession sessionToUpdate = getCourseSession(id);
        CourseSession courseSession = CourseSessionDto.toCourseSession(sessionDto);
        checkMinimumCourseSessionDuration(courseSession);
        courseSession.setTrainers(getIfTrainersExist(sessionDto.getTrainers()));
        setUpdatedFields(courseSession, sessionToUpdate);
        return courseSessionRepository.save(sessionToUpdate);
    }

    @Override
    public Page<CourseSession> findAllSession(int page, int size, CourseSessionFilterDto filter) {
        Pageable pageable = PageRequest.of(page, size,
                Sort.by(Sort.Direction.fromString(filter.getDirection()), filter.getOrder()));
        return courseSessionRepository.findAll(filter.getTags(), filter.getCategories(), pageable);
    }

    private void getCourseProperties(CourseSession courseSession, Course course) {
        courseSession.setCourseId(course.getId());
        courseSession.setName(course.getCourseDetails().getName());
        courseSession.setShortDescription(course.getCourseDetails().getShortDescription());
        courseSession.setPhoto(course.getPhoto());
        courseSession.setTags(new ArrayList<>(course.getTags()));
        courseSession.setCategory(course.getCategory());
    }

    private Course checkIfCourseExists(long courseId) {
        return courseService.findById(courseId);
    }

    private List<User> getIfTrainersExist(List<UsernameDto> idDtoList) {
        List<User> users = new ArrayList<>();
        idDtoList.forEach(usernameDto -> users.add(userRepository.findByUsername(usernameDto.getUsername())
                .orElseThrow(() -> new UserNotFoundException(usernameDto.getUsername()))));
        return users;
    }

    private void checkMinimumCourseSessionDuration(CourseSession courseSession) {
        Duration duration = Duration.between(courseSession.getStartDate(), courseSession.getEndDate());
        if (duration.isNegative() || duration.toDays() < 1) {
            throw new MinimumSessionDurationException();
        }
    }

    private void setUpdatedFields(CourseSession courseSession, CourseSession sessionToUpdate) {
        sessionToUpdate.setCourseSchedule(courseSession.getCourseSchedule());
        sessionToUpdate.setEndDate(courseSession.getEndDate());
        sessionToUpdate.setStartDate(courseSession.getStartDate());
        sessionToUpdate.setNumberOfLessons(courseSession.getNumberOfLessons());
        sessionToUpdate.setRequiredStudentCnt(courseSession.getRequiredStudentCnt());
        sessionToUpdate.setStatus(courseSession.getStatus());
        sessionToUpdate.setTeacherDescription(courseSession.getTeacherDescription());
        sessionToUpdate.setTrainers(new ArrayList<>(courseSession.getTrainers()));
        sessionToUpdate.setPrice(courseSession.getPrice());
    }
}
