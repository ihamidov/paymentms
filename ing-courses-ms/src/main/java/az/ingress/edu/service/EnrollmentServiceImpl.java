package az.ingress.edu.service;

import az.ingress.edu.config.RabbitMqConfig;
import az.ingress.edu.dto.CreateEnrollmentDto;
import az.ingress.edu.dto.EnrollmentDto;
import az.ingress.edu.dto.EnrollmentInfoDto;
import az.ingress.edu.dto.EnrollmentStatusDto;
import az.ingress.edu.dto.UpdateEnrollmentDto;
import az.ingress.edu.exception.AlreadyEnrolledException;
import az.ingress.edu.exception.AttemptToChangeCancelledEnrollment;
import az.ingress.edu.exception.CourseSessionIsFullException;
import az.ingress.edu.exception.EnrollmentNotFound;
import az.ingress.edu.exception.NoEnrollmentToCancelException;
import az.ingress.edu.exception.NumberIsRequiredException;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.Enrollment;
import az.ingress.edu.model.EnrollmentStatus;
import az.ingress.edu.model.EnrollmentStatusView;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.CourseSessionRepository;
import az.ingress.edu.repository.EnrollmentRepository;
import az.ingress.edu.repository.UserRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@AllArgsConstructor
public class EnrollmentServiceImpl implements EnrollmentService {

    private final CourseSessionRepository sessionRepository;
    private final CourseSessionService courseSessionService;
    private final EnrollmentRepository enrollmentRepository;
    private final UserService userService;
    private final UserRepository userRepository;
    private final RabbitTemplate rabbitTemplate;
    private final RabbitMqConfig rabbitMqConfig;

    @Override
    @Transactional
    public Enrollment add(long sessionId, CreateEnrollmentDto createEnrollmentDto) {
        CourseSession session = courseSessionService.getCourseSession(sessionId);
        checkIfRoomLeftToEnroll(session);
        User currentUser = userService.getCurrentUser();
        checkIfPhoneNumberSet(createEnrollmentDto, currentUser);
        checkIfUserAlreadyEnrolled(currentUser.getUsername(), sessionId);
        EnrollmentInfoDto enrollmentInfoDto = EnrollmentInfoDto.builder()
                .firstName(currentUser.getFirstname())
                .lastName(currentUser.getLastname())
                .username(currentUser.getUsername())
                .courseSessionId(sessionId)
                .courseName(session.getName()).build();
        rabbitTemplate.convertAndSend(rabbitMqConfig.getNotificationMsExchange(),
                rabbitMqConfig.getSlackEnrollNotifierRoutingKey(), enrollmentInfoDto);
        return enrollmentRepository.save(prepareEnrollment(currentUser, session));
    }

    @Override
    public void deleteById(Long id) {
        Enrollment enrollment = findById(id);
        enrollment.setDeleted(true);
        enrollmentRepository.save(enrollment);
    }

    @Override
    public Enrollment findById(Long id) {
        return enrollmentRepository.findById(id).orElseThrow(() -> new EnrollmentNotFound(id));
    }

    @Override
    public Enrollment update(UpdateEnrollmentDto enrollmentDto, Long id) {
        Enrollment enrollment = findById(id);
        if (enrollment.getEnrollmentStatus().equals(EnrollmentStatus.CANCELLED)) {
            throw new AttemptToChangeCancelledEnrollment();
        }
        CourseSession session = enrollment.getSession();
        updateSessionEnrolledStudentCount(enrollmentDto.getEnrollmentStatus(),
                enrollment.getEnrollmentStatus(), session);
        enrollment.setEnrollmentStatus(enrollmentDto.getEnrollmentStatus());
        return enrollmentRepository.save(enrollment);
    }

    @Override
    public Page<EnrollmentDto> findAllEnrolment(int page, int size, String order, String direction) {
        Page<Enrollment> enrollments = enrollmentRepository.findAllNonCancelled(
                PageRequest.of(page, size, Sort.Direction.fromString(direction), order));
        return convertToEnrollmentDtoPage(enrollments);
    }

    @Override
    public Page<EnrollmentDto> findAllByCourseSessionId(long sessionId, int page, int size, String order, String dir) {
        Page<Enrollment> enrollments = enrollmentRepository.findAllNonCancelledByCourseSessionId(sessionId,
                PageRequest.of(page, size, Sort.Direction.fromString(dir), order));
        return convertToEnrollmentDtoPage(enrollments);
    }

    @Override
    public EnrollmentStatusDto getEnrolmentStatus(long sessionId) {
        courseSessionService.getCourseSession(sessionId);
        return enrollmentRepository
                .findByCourseSessionIdAndUsername(userService.getCurrentUser().getUsername(), sessionId)
                .map(e -> EnrollmentStatusDto.builder()
                        .enrollmentStatus(EnrollmentStatusView.valueOf(e.getEnrollmentStatus().toString())).build())
                .orElseGet(() ->
                        EnrollmentStatusDto.builder().enrollmentStatus(EnrollmentStatusView.NOT_ENROLLED).build());
    }

    @Override
    public void cancelEnrollment(long sessionId) {
        CourseSession session = courseSessionService.getCourseSession(sessionId);
        Enrollment enrollment = enrollmentRepository
                .findByCourseSessionIdAndUsername(userService.getCurrentUser().getUsername(), sessionId)
                .orElseThrow(() -> new NoEnrollmentToCancelException());
        updateSessionEnrolledStudentCount(EnrollmentStatus.CANCELLED, enrollment.getEnrollmentStatus(), session);
        enrollment.setEnrollmentStatus(EnrollmentStatus.CANCELLED);
        enrollmentRepository.save(enrollment);
    }

    private Enrollment prepareEnrollment(User user, CourseSession session) {
        return Enrollment.builder()
                .user(user)
                .session(session)
                .enrollmentStatus(EnrollmentStatus.PENDING)
                .build();
    }

    private void checkIfUserAlreadyEnrolled(String username, long sessionId) {
        if (enrollmentRepository.findByCourseSessionIdAndUsername(username, sessionId).isPresent()) {
            throw new AlreadyEnrolledException();
        }
    }

    private void checkIfRoomLeftToEnroll(CourseSession session) {
        if (session.getEnrolledStudentCnt() == session.getRequiredStudentCnt()) {
            throw new CourseSessionIsFullException();
        }
    }

    private void updateSessionEnrolledStudentCount(EnrollmentStatus updated, EnrollmentStatus current,
                                                   CourseSession session) {
        int enrolledStudentCnt = session.getEnrolledStudentCnt();
        if (current.equals(EnrollmentStatus.APPROVED)
                && (updated.equals(EnrollmentStatus.PENDING) || updated.equals(EnrollmentStatus.CANCELLED))) {
            session.setEnrolledStudentCnt(enrolledStudentCnt - 1);
        } else if (current.equals(EnrollmentStatus.PENDING) && updated.equals(EnrollmentStatus.APPROVED)) {
            checkIfRoomLeftToEnroll(session);
            session.setEnrolledStudentCnt(enrolledStudentCnt + 1);
        }
        sessionRepository.save(session);
    }

    private Page<EnrollmentDto> convertToEnrollmentDtoPage(Page<Enrollment> page) {
        List<Enrollment> enrollmentList = page.getContent();
        List<EnrollmentDto> enrollmentDtoList = enrollmentList.stream().map(EnrollmentDto::from)
                .collect(Collectors.toList());
        return new PageImpl<>(enrollmentDtoList, page.getPageable(), page.getTotalElements());
    }

    private void checkIfPhoneNumberSet(CreateEnrollmentDto createEnrollmentDto, User currentUser) {
        if (StringUtils.isEmpty(currentUser.getPhone())) {
            if (!StringUtils.isEmpty(createEnrollmentDto.getPhone())) {
                currentUser.setPhone(createEnrollmentDto.getPhone());
                userRepository.save(currentUser);
            } else {
                throw new NumberIsRequiredException();
            }
        }
    }
}
