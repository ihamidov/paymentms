package az.ingress.edu.service;

import az.ingress.edu.dto.TrainingCenterDto;
import az.ingress.edu.exception.TrainingCenterNotFound;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.TrainingCenter;
import az.ingress.edu.repository.ContactInfoRepository;
import az.ingress.edu.repository.TrainingCenterRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TrainingCenterServiceImpl implements TrainingCenterService {

    TrainingCenterRepository tCRepo;
    ContactInfoRepository contactInfoRepository;

    @Override
    public TrainingCenter findById(Long id) {
        return tCRepo.findByIdAndDeletedFalse(id).orElseThrow(() -> new TrainingCenterNotFound());
    }

    @Override
    public Iterable<TrainingCenter> findAll() {
        return tCRepo.findAllByDeletedFalse();
    }

    @Override
    public TrainingCenter add(TrainingCenterDto trainingCenterDto) {
        TrainingCenter trainingCenter = dtoToEntity(trainingCenterDto);
        contactInfoRepository.save(trainingCenter.getContactInfo());
        return tCRepo.save(trainingCenter);
    }

    @Override
    public TrainingCenter update(TrainingCenterDto trainingCenterDto, Long id) {
        TrainingCenter tc = findById(id);
        tc.setContactInfo(trainingCenterDto.getContactInfo());
        tc.setDescription(trainingCenterDto.getDescription());
        tc.setName(trainingCenterDto.getName());
        tc.setPhoto(trainingCenterDto.getPhoto());
        tCRepo.save(tc);
        return tc;
    }

    @Override
    public void deleteById(Long id) {
        TrainingCenter trainingCenter = findById(id);
        trainingCenter.setDeleted(true);
        tCRepo.save(trainingCenter);
    }

    private TrainingCenter dtoToEntity(TrainingCenterDto trainingCenterDto) {
        ContactInfo contactInfo = ContactInfo.builder()
                .contactNumbers(trainingCenterDto.getContactInfo().getContactNumbers())
                .address(trainingCenterDto.getContactInfo().getAddress())
                .email(trainingCenterDto.getContactInfo().getEmail())
                .build();

        return TrainingCenter.builder()
                .photo(trainingCenterDto.getPhoto())
                .name(trainingCenterDto.getName())
                .description(trainingCenterDto.getDescription())
                .contactInfo(contactInfo)
                .build();
    }
}
