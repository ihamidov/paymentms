package az.ingress.edu.service;

import az.ingress.edu.dto.CategoryDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.model.Category;
import org.springframework.data.domain.Page;

public interface CategoryService {

    Category findById(long id);

    Category add(CategoryDto categoryDto);

    Category update(CategoryDto categoryDto, long id);

    void deleteById(long id);

    Page<Category> list(PageDto pageDto);
}

