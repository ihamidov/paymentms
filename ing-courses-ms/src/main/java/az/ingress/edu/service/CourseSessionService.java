package az.ingress.edu.service;

import az.ingress.edu.dto.CourseSessionDto;
import az.ingress.edu.dto.CourseSessionFilterDto;
import az.ingress.edu.model.CourseSession;
import org.springframework.data.domain.Page;


public interface CourseSessionService {

    CourseSession createCourseSession(CourseSessionDto sessionDto, long courseId);

    Page<CourseSession> getCourseSessionList(long courseId, String direction, String order, int page, int size);

    CourseSession getCourseSession(long id);

    void deleteCourseSession(long id);

    CourseSession updateCourseSession(CourseSessionDto sessionDto, long id);

    Page<CourseSession> findAllSession(int page, int size, CourseSessionFilterDto courseSessionFilter);
}
