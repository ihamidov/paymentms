package az.ingress.edu.service;

import static az.ingress.edu.util.CollectionsUtil.isEmpty;

import az.ingress.edu.dto.CourseDto;
import az.ingress.edu.dto.ListCourseDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.Tag;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.CourseRepository;
import az.ingress.edu.repository.CourseSessionRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseSessionRepository sessionRepository;

    private final CourseRepository courseRepository;

    private final CategoryService categoryService;

    private final TagService tagService;

    private final UserService userService;

    private final TrainingCenterService trainingCenterService;

    @Override
    public Course findById(long id) {
        return courseRepository.findByIdAndDeletedFalse(id)
                .orElseThrow(() -> new CourseNotFoundException(id));
    }

    @Override
    public Course add(CourseDto courseDto) {
        Course course = toEntity(courseDto);
        course.setCategory(categoryService.findById(courseDto.getCategoryId()));
        checkIfTagsExist(course);
        course.setTrainingCenter(trainingCenterService.findById(courseDto.getTrainingCenterId()));
        return courseRepository.save(course);
    }

    @Override
    @Transactional
    public Course update(CourseDto courseDto, long id) {
        Course course = retrieveCourse(id);
        setPropertiestToCourseForUpdate(courseDto, course);
        checkIfTagsExist(course);
        course.setTrainingCenter(trainingCenterService.findById(courseDto.getTrainingCenterId()));
        Course updatedCourse = courseRepository.save(course);
        updateDependentCourseSessions(updatedCourse);
        return updatedCourse;
    }

    @Override
    public void deleteById(long id) {
        Course course = findById(id);
        course.setDeleted(true);
        courseRepository.save(course);
    }

    @Override
    public ListDto list(PageDto pageDto) {
        return getListDtoForCourse(courseRepository.findAllCourses(toPageRequest(pageDto)));
    }

    public Course toEntity(CourseDto courseDto) {
        List<Tag> tagList = tagService.toEntityList(courseDto.getTags());
        User user = userService.getCurrentUser();
        return Course.builder()
                .courseDetails(CourseDetails.builder()
                        .name(courseDto.getName())
                        .description(courseDto.getDescription())
                        .shortDescription(courseDto.getShortDescription())
                        .requirements(courseDto.getRequirements())
                        .status(courseDto.getStatus())
                        .build())
                .photo(courseDto.getPhoto())
                .tags(tagList)
                .createdBy(user)
                .modifiedBy(user).build();
    }

    private void checkIfTagsExist(Course course) {
        if (!isEmpty(course.getTags())) {
            course.getTags().forEach(tag -> tagService.findById(tag.getId()));
        }
    }

    private PageRequest toPageRequest(PageDto pageDto) {
        return PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(),
                Sort.Direction.fromString(pageDto.getSortDirection()), pageDto.getSortColumn());
    }

    private Course retrieveCourse(long id) {
        return courseRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new CourseNotFoundException(id));
    }

    private void setPropertiestToCourseForUpdate(CourseDto courseDto, Course course) {
        course.setCategory(categoryService.findById(courseDto.getCategoryId()));
        course.getCourseDetails().setName(courseDto.getName());
        course.setPhoto(courseDto.getPhoto());
        course.getCourseDetails().setShortDescription(courseDto.getShortDescription());
        course.getCourseDetails().setDescription(courseDto.getDescription());
        course.getCourseDetails().setRequirements(courseDto.getRequirements());
        course.getCourseDetails().setStatus(courseDto.getStatus());
        course.setTags(tagService.toEntityList(courseDto.getTags()));
        course.setModifiedBy(userService.getCurrentUser());
    }

    private ListDto getListDtoForCourse(Page<Course> coursePage) {
        return new ListDto(coursePage.getContent().stream().map(course -> ListCourseDto
                .builder()
                .id(course.getId())
                .rating(Optional.ofNullable(course.getRating()).orElse(0.0))
                .category(course.getCategory())
                .name(course.getCourseDetails().getName())
                .shortDescription(course.getCourseDetails().getShortDescription())
                .photo(course.getPhoto())
                .tags(course.getTags())
                .build()
        ).collect(Collectors.toList()), coursePage.getTotalElements(), coursePage.getTotalPages());
    }

    private void updateDependentCourseSessions(Course course) {
        List<CourseSession> sessions = sessionRepository.findAllByCourseId(course.getId());
        for (CourseSession session : sessions) {
            session.setName(course.getCourseDetails().getName());
            session.setShortDescription(course.getCourseDetails().getShortDescription());
            session.setCategory(course.getCategory());
            session.setTags(new ArrayList<>(course.getTags()));
            session.setPhoto(course.getPhoto());
            sessionRepository.save(session);
        }
    }
}
