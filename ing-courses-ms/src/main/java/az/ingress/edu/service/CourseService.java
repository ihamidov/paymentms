package az.ingress.edu.service;

import az.ingress.edu.dto.CourseDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.model.Course;

public interface CourseService {

    Course findById(long id);

    Course add(CourseDto courseDto);

    Course update(CourseDto courseDto, long id);

    void deleteById(long id);

    ListDto list(PageDto pageDto);

}
