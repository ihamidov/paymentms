package az.ingress.edu.service;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateSyllabusDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.exception.SyllabusWithChildException;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.repository.CourseRepository;
import az.ingress.edu.repository.SyllabusRepository;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SyllabusServiceImpl implements SyllabusService {

    private static final String ID = "id";
    private final SyllabusRepository syllabusRepository;
    private final CourseRepository courseRepository;

    public SyllabusServiceImpl(SyllabusRepository syllabusRepository,
                               CourseRepository courseRepository) {
        this.syllabusRepository = syllabusRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public Syllabus createSyllabus(Syllabus syllabus) {
        checkIfCourseExists(syllabus.getCourseId());
        if (isChildSyllabus(syllabus)) {
            Syllabus parentSyllabus = retrieveParentSyllabus(syllabus);
            updateChildCountAndSave(parentSyllabus);
        }
        return syllabusRepository.save(syllabus);
    }

    @Override
    public List<Syllabus> getSyllabi(long courseId) {
        checkIfCourseExists(courseId);
        List<Syllabus> syllabusList = syllabusRepository
                .findAllByCourseId(courseId, Sort.by(ID).ascending());
        return convertSyllabusMapToList(
                establishParentChildRelationship(convertSyllabusListToSyllabusMap(syllabusList)));
    }

    @Override
    public Syllabus getSyllabus(long id) {
        return syllabusRepository.findById(id).orElseThrow(() -> new SyllabusNotFoundException(id));
    }

    @Override
    @Transactional
    public void deleteSyllabus(long id) {

        Syllabus syllabus = getSyllabus(id);

        if (hasChilds(syllabus)) {
            throw new SyllabusWithChildException();
        }

        if (isChildSyllabus(syllabus)) {
            decreaseChildCountAndSave(syllabus.getParentId());
        }

        syllabusRepository.delete(syllabus);
    }

    @Override
    public IdDto updateSyllabus(UpdateSyllabusDto syllabusDto, long id) {
        Syllabus syllabus = getSyllabus(id);
        syllabus.setTitle(syllabusDto.getTitle());
        syllabus.setDescription(syllabusDto.getDescription());
        syllabus.setDuration(syllabusDto.getDuration());
        return new IdDto(syllabusRepository.save(syllabus).getId());
    }

    @Override
    public List<Syllabus> getChildSyllabi(long parentId) {
        syllabusRepository.findById(parentId).orElseThrow(() -> new SyllabusNotFoundException(parentId));
        return syllabusRepository.findAllByParentIdOrderById(parentId);
    }

    private void decreaseChildCountAndSave(long parentId) {
        Syllabus parentSyllabus = getSyllabus(parentId);
        parentSyllabus.setChildCount(parentSyllabus.getChildCount() - 1);
        syllabusRepository.save(parentSyllabus);
    }

    private boolean hasChilds(Syllabus syllabus) {
        return syllabus.getChildCount() > 0;
    }

    private void checkIfCourseExists(long courseId) {
        courseRepository.findById(courseId).orElseThrow(() -> new CourseNotFoundException(courseId));
    }

    private boolean isChildSyllabus(Syllabus syllabus) {
        return syllabus.getParentId() != null;
    }

    private Syllabus retrieveParentSyllabus(Syllabus syllabus) {
        return syllabusRepository.findById(syllabus.getParentId())
                .orElseThrow(() -> new SyllabusNotFoundException(syllabus.getParentId()));
    }

    private void updateChildCountAndSave(Syllabus syllabus) {
        syllabus.setChildCount(syllabus.getChildCount() + 1);
        syllabusRepository.save(syllabus);
    }

    private Map<Long, Syllabus> convertSyllabusListToSyllabusMap(List<Syllabus> syllabusList) {
        return syllabusList.stream().collect(LinkedHashMap::new, (map, syllabus) -> {
            syllabus.setChildren(new ArrayList<>());
            map.put(syllabus.getId(), syllabus);
        }, Map::putAll);
    }

    private Map<Long, Syllabus> establishParentChildRelationship(Map<Long, Syllabus> syllabusMap) {
        syllabusMap.forEach((syllabusId, syllabus) -> {
            Long parentId = syllabus.getParentId();
            if (isChildSyllabus(syllabus)) {
                Syllabus parentSyllabus = syllabusMap.get(parentId);
                parentSyllabus.getChildren().add(syllabus);
            }
        });
        return syllabusMap;
    }

    private List<Syllabus> convertSyllabusMapToList(Map<Long, Syllabus> syllabusMap) {
        return syllabusMap.values().stream().filter(comment -> !isChildSyllabus(comment))
                .collect(Collectors.toList());
    }
}
