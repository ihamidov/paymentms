package az.ingress.edu.service;

import az.ingress.edu.dto.GetUserDto;
import az.ingress.edu.dto.UpdateUserDto;
import az.ingress.edu.exception.UserAlreadyExistsException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.UserRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessToken;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AccessToken accessToken;

    @Override
    public User saveUser(User user) {

        checkIfUserAlreadyExists(user);
        return userRepository.save(user);
    }

    @Override
    public User getCurrentUser() {
        String username = accessToken.getPreferredUsername();
        return userRepository.findByUsername(username).orElseGet(() ->
                userRepository.save(User.builder()
                        .username(username)
                        .email(accessToken.getEmail())
                        .firstname(accessToken.getGivenName())
                        .lastname(accessToken.getFamilyName())
                        .phone(accessToken.getPhoneNumber())
                        .build()));
    }

    @Override
    @RabbitListener(queues = {"${rabbit.course-ms-user-create-queue}"})
    @Transactional
    public void userCreated(GetUserDto user) {
        Optional<User> optionalUser = userRepository
                .findByUsername(user.getUsername());
        if (!optionalUser.isPresent()) {
            userRepository.save(User.builder()
                    .username(user.getUsername())
                    .lastname(user.getLastname())
                    .firstname(user.getFirstname())
                    .phone(user.getPhoto())
                    .email(user.getContactDetails().getEmail())
                    .build());
        }
    }

    @Override
    @RabbitListener(queues = {"${rabbit.course-ms-user-update-queue}"})
    @Transactional
    public void userUpdated(UpdateUserDto user) {
        User userToUpdate = userRepository.findByUsername(user.getUsername())
                .orElseThrow(() -> new UserNotFoundException(user.getUsername()));
        setUpdatedProperties(user, userToUpdate);
        userRepository.save(userToUpdate);
    }

    private void checkIfUserAlreadyExists(User user) {
        Optional<User> existUser = userRepository.findByUsername(user.getUsername());
        if (existUser.isPresent()) {
            throw new UserAlreadyExistsException(user.getUsername());
        }
    }

    private void setUpdatedProperties(UpdateUserDto updateUserDto, User user) {
        user.setFirstname(updateUserDto.getFirstname());
        user.setLastname(updateUserDto.getLastname());
        user.setEmail(updateUserDto.getContactDetails().getEmail());
        user.setPhoto(updateUserDto.getPhoto());
        user.setPhone(updateUserDto.getContactDetails().getPhone());
    }
}
