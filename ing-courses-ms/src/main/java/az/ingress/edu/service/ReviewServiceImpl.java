package az.ingress.edu.service;

import az.ingress.edu.config.SwaggerConfiguration;
import az.ingress.edu.dto.CreateReviewDto;
import az.ingress.edu.dto.EnrollmentStatusDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ReviewFilterDto;
import az.ingress.edu.dto.ReviewStatusDto;
import az.ingress.edu.dto.UpdateReviewDto;
import az.ingress.edu.exception.ReviewNotFoundException;
import az.ingress.edu.exception.ReviewWithoutEnrollmentException;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.model.EnrollmentStatusView;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.model.ReviewStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.ReviewRepository;
import lombok.AllArgsConstructor;
import org.keycloak.representations.AccessToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ReviewServiceImpl implements ReviewService {

    private final CourseService courseService;
    private final CourseSessionService courseSessionService;
    private final TrainingCenterService trainingCenterService;
    private final UserService userService;
    private final ReviewRepository reviewRepository;
    private final EnrollmentService enrollmentService;
    private final AccessToken accessToken;
    private SwaggerConfiguration swaggerConfiguration;

    @Override
    public IdDto createReview(CreateReviewDto createReviewDto) {
        String itemName = checkIfItemExists(createReviewDto.getItemType(), createReviewDto.getItemId());
        checkIfEnrolledToSession(createReviewDto.getItemType(), createReviewDto.getItemId());
        Review review = createReviewDto.toReview(createReviewDto);
        review.setItemName(itemName);
        review.setCreatedBy(userService.getCurrentUser());
        return new IdDto(reviewRepository.save(review).getId());
    }

    @Override
    public Review getReview(long id) {
        return reviewRepository.findById(id).orElseThrow(() -> new ReviewNotFoundException(id));
    }

    @Override
    public void deleteReview(long id) {
        Review reviewToDelete = getReview(id);
        checkIfHasAuthority(reviewToDelete);
        reviewRepository.delete(reviewToDelete);
    }

    @Override
    public IdDto updateReview(UpdateReviewDto updateReviewDto, long id) {
        Review review = getReview(id);
        checkIfHasAuthority(review);
        review.setRating(updateReviewDto.getRating());
        review.setComment(updateReviewDto.getComment());
        return new IdDto(reviewRepository.save(review).getId());
    }

    @Override
    public Page<Review> getReviewList(ItemType itemType, long id, int page, int size) {
        checkIfItemExists(itemType, id);
        return reviewRepository.findAllReviews(id, itemType,
                PageRequest.of(page, size, Sort.by("id").ascending()));
    }

    @Override
    public IdDto changeStatus(long id, ReviewStatusDto statusDto) {
        Review review = getReview(id);
        review.setStatus(statusDto.getReviewStatus());
        reviewRepository.save(review);
        return new IdDto(id);
    }

    @Override
    public Page<Review> getReviewListForManagement(int page, int size, ReviewFilterDto filter) {
        return reviewRepository.findAll(filter.getItemType(), filter.getReviewStatus(), PageRequest
                .of(page, size, Sort.Direction.fromString(filter.getDirection()), filter.getOrder()));
    }

    private String checkIfItemExists(ItemType itemType, long itemId) {
        if (itemType.equals(ItemType.COURSE)) {
            return courseService.findById(itemId).getCourseDetails().getName();
        } else if (itemType.equals(ItemType.COURSE_SESSION)) {
            return courseSessionService.getCourseSession(itemId).getName();
        } else {
            return trainingCenterService.findById(itemId).getName();
        }
    }

    private void checkIfEnrolledToSession(ItemType itemType, long itemId) {
        if (itemType.equals(ItemType.COURSE_SESSION)) {
            EnrollmentStatusDto statusDto = enrollmentService.getEnrolmentStatus(itemId);
            if (!statusDto.getEnrollmentStatus().equals(EnrollmentStatusView.APPROVED)) {
                throw new ReviewWithoutEnrollmentException();
            }
        }
    }

    private void checkIfHasAuthority(Review review) {
        User currentUser = userService.getCurrentUser();
        String clientId = swaggerConfiguration.getClientId();
        if (!accessToken
                .getResourceAccess(clientId).getRoles().contains("ADMIN")
                && !accessToken
                .getResourceAccess(clientId).getRoles().contains("COURSE_MANAGER")
                && !(review.getCreatedBy().getUsername().equals(currentUser.getUsername())
                && review.getStatus().equals(ReviewStatus.APPROVED))) {
            throw new UnauthorizedOperationException();
        }
    }
}
