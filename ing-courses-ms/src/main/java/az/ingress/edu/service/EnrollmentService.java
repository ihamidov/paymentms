package az.ingress.edu.service;

import az.ingress.edu.dto.CreateEnrollmentDto;
import az.ingress.edu.dto.EnrollmentDto;
import az.ingress.edu.dto.EnrollmentStatusDto;
import az.ingress.edu.dto.UpdateEnrollmentDto;
import az.ingress.edu.model.Enrollment;
import org.springframework.data.domain.Page;

public interface EnrollmentService {

    Enrollment add(long courseSessionId, CreateEnrollmentDto enrollmentDto);

    void deleteById(Long id);

    Enrollment findById(Long id);

    Enrollment update(UpdateEnrollmentDto enrollmentDto, Long id);

    Page<EnrollmentDto> findAllEnrolment(int page, int size, String order, String direction);

    Page<EnrollmentDto> findAllByCourseSessionId(long sessionId, int page, int size, String order, String direction);

    EnrollmentStatusDto getEnrolmentStatus(long sessionId);

    void cancelEnrollment(long sessionId);
}
