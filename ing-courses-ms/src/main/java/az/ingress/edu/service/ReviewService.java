package az.ingress.edu.service;

import az.ingress.edu.dto.CreateReviewDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ReviewFilterDto;
import az.ingress.edu.dto.ReviewStatusDto;
import az.ingress.edu.dto.UpdateReviewDto;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import org.springframework.data.domain.Page;

public interface ReviewService {

    IdDto createReview(CreateReviewDto createReviewDto);

    Review getReview(long id);

    void deleteReview(long id);

    IdDto updateReview(UpdateReviewDto updateReviewDto, long id);

    Page<Review> getReviewList(ItemType reviewType, long id, int page, int size);

    IdDto changeStatus(long id, ReviewStatusDto statusDto);

    Page<Review> getReviewListForManagement(int page, int size, ReviewFilterDto filter);
}
