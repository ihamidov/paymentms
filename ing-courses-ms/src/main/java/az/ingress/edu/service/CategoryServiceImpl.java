package az.ingress.edu.service;

import az.ingress.edu.dto.CategoryDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CategoryAlreadyExistsException;
import az.ingress.edu.exception.CategoryInUseException;
import az.ingress.edu.exception.CategoryNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.repository.CategoryRepository;
import az.ingress.edu.repository.CourseRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CourseRepository courseRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CourseRepository courseRepository) {
        this.categoryRepository = categoryRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public Category findById(long id) {
        return categoryRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new CategoryNotFoundException(id));
    }

    @Override
    public Category add(CategoryDto categoryDto) {
        Category category = toEntity(categoryDto);
        checkIfCategoryAlreadyExists(category);
        return categoryRepository.save(category);
    }

    @Override
    public Category update(CategoryDto categoryDto, long id) {
        Category category = findById(id);
        category.setName(categoryDto.getName());
        return categoryRepository.save(category);
    }

    @Override
    public void deleteById(long id) {
        Category category = findById(id);
        checkIfCategoryIsUsing(category);
        category.setDeleted(true);
        categoryRepository.save(category);
    }

    private void checkIfCategoryIsUsing(Category category) {
        List<Course> courses = courseRepository.findCourseByCategory(category);
        if (!courses.isEmpty()) {
            throw new CategoryInUseException(category.getName());
        }
    }

    @Override
    public Page<Category> list(PageDto pageDto) {
        return categoryRepository.findAllByDeletedFalse(toPageRequest(pageDto));
    }

    private PageRequest toPageRequest(PageDto pageDto) {
        return PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(),
                Sort.Direction.fromString(pageDto.getSortDirection()), pageDto.getSortColumn());
    }

    private void checkIfCategoryAlreadyExists(Category category) {
        Optional<Category> existCategory = categoryRepository.findCategoryByNameAndDeletedFalse(category.getName());
        if (existCategory.isPresent()) {
            throw new CategoryAlreadyExistsException(category.getName());
        }
    }

    private Category toEntity(CategoryDto categoryDto) {
        return Category.builder()
                .name(categoryDto.getName())
                .build();
    }
}
