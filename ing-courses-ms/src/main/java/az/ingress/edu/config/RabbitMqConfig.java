package az.ingress.edu.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("rabbit")
@EnableConfigurationProperties(RabbitMqConfig.class)
@Setter
@Getter
public class RabbitMqConfig {

    private String courseMsUserCreateQueue;
    private String courseMsUserUpdateQueue;
    private String courseMsExchange;
    private String courseMsUserCreateRoutingKey;
    private String courseMsUserUpdateRoutingKey;
    private String slackEnrollNotifierQueue;
    private String notificationMsExchange;
    private String slackEnrollNotifierRoutingKey;

    @Bean
    public TopicExchange courseMsExchange() {
        return new TopicExchange(courseMsExchange);
    }

    @Bean
    public Queue courseMsUserCreateQueue() {
        return new Queue(courseMsUserCreateQueue);
    }

    @Bean
    public Binding courseMsUserCreateBinding() {
        return BindingBuilder.bind(courseMsUserCreateQueue())
                .to(courseMsExchange()).with(courseMsUserCreateRoutingKey);
    }

    @Bean
    public Queue courseMsUserUpdateQueue() {
        return new Queue(courseMsUserUpdateQueue);
    }

    @Bean
    public Binding courseMsUserUpdateBinding() {
        return BindingBuilder.bind(courseMsUserUpdateQueue())
                .to(courseMsExchange()).with(courseMsUserUpdateRoutingKey);
    }

    @Bean
    public TopicExchange notificationMsExchange() {
        return new TopicExchange(notificationMsExchange);
    }

    @Bean
    public Queue notificationMsSlackEnrollNotifierQueue() {
        return new Queue(slackEnrollNotifierQueue);
    }

    @Bean
    public Binding notificationMsSlackEnrollNotifierBinding() {
        return BindingBuilder.bind(notificationMsSlackEnrollNotifierQueue())
                .to(notificationMsExchange()).with(slackEnrollNotifierRoutingKey);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
