package az.ingress.edu.config;

import az.ingress.edu.exception.IngressAccessDeniedHandler;
import javax.servlet.http.HttpServletRequest;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Profile("!test")
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class KeycloakConfig extends KeycloakWebSecurityConfigurerAdapter {

    private static final String CATEGORY = "/category/**";
    private static final String ENROLLMENT = "/enrollment/**";
    private static final String ENROLLMENT_CANCEL = "/enrollment/cancel/**";
    private static final String ENROLLMENT_STATUS = "/enrollment/status/**";
    private static final String ROLE_USER = "USER";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_MANAGER = "COURSE_MANAGER";
    private static final String REVIEW = "/review/**";
    private static final String COURSE = "/course/**";
    private static final String SYLLABUS = "/syllabus/**";
    private static final String TAG = "/tag/**";
    private static final String TRAINING_CENTER = "/training-center/**";
    private static final String REVIEW_STATUS = "/review/*/status";
    private static final String REVIEW_LIST = "/review/list";

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {

        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET,REVIEW_LIST).hasAnyRole(ROLE_ADMIN,ROLE_MANAGER)
                .antMatchers(HttpMethod.GET, CATEGORY, REVIEW, COURSE, SYLLABUS, TAG, TRAINING_CENTER)
                .permitAll()
                .antMatchers(HttpMethod.GET,ENROLLMENT_STATUS).hasAnyRole(ROLE_ADMIN,ROLE_USER,ROLE_MANAGER)
                .antMatchers(HttpMethod.GET,ENROLLMENT).hasAnyRole(ROLE_MANAGER,ROLE_ADMIN)
                .antMatchers(HttpMethod.POST, ENROLLMENT,REVIEW).hasAnyRole(ROLE_USER,ROLE_ADMIN,ROLE_MANAGER)
                .antMatchers(HttpMethod.POST,CATEGORY, COURSE, SYLLABUS, TAG).hasAnyRole(ROLE_MANAGER,ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT,REVIEW_STATUS).hasAnyRole(ROLE_MANAGER,ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT,REVIEW, ENROLLMENT_CANCEL).hasAnyRole(ROLE_USER,ROLE_ADMIN,ROLE_MANAGER)
                .antMatchers(HttpMethod.DELETE,REVIEW).hasAnyRole(ROLE_MANAGER,ROLE_USER,ROLE_ADMIN)
                .antMatchers(HttpMethod.PUT, CATEGORY, COURSE, SYLLABUS, TAG,ENROLLMENT)
                .hasAnyRole(ROLE_ADMIN,ROLE_MANAGER)
                .antMatchers(HttpMethod.DELETE, CATEGORY, COURSE, SYLLABUS, TAG,ENROLLMENT)
                .hasAnyRole(ROLE_ADMIN,ROLE_MANAGER)
                .antMatchers("/users/**").hasRole(ROLE_ADMIN)
                .antMatchers(TRAINING_CENTER).hasRole(ROLE_ADMIN)
                .antMatchers(REVIEW).hasAnyRole(ROLE_ADMIN,ROLE_USER,ROLE_MANAGER)
                .anyRequest().permitAll()
                .and().csrf().disable();
    }

    @Bean
    public IngressAccessDeniedHandler accessDeniedHandler() {
        return new IngressAccessDeniedHandler();
    }

    @Bean
    @Scope(scopeName = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AccessToken getAccessToken() {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return ((KeycloakAuthenticationToken) request.getUserPrincipal())
                .getAccount().getKeycloakSecurityContext().getToken();
    }
}
