package az.ingress.edu.model;

public enum ItemType {
    COURSE, TRAINING_CENTER, COURSE_SESSION
}
