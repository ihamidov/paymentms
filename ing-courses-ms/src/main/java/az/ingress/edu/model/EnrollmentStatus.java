package az.ingress.edu.model;

public enum EnrollmentStatus {
    PENDING, APPROVED, CANCELLED;
}
