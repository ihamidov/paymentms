package az.ingress.edu.model;

public enum CourseSessionStatus {
    DRAFT, UPCOMING, IN_PROGRESS, COMPLETED, CANCELLED
}
