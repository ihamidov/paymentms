package az.ingress.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Where;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SuppressWarnings("PMD")
@Where(clause = "deleted = false")
public class CourseSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @NotNull(message = "Course id can't be empty")
    @Positive(message = "Course id must be positive")
    private Long courseId;

    @NotNull(message = "Status can not be empty")
    @Enumerated(EnumType.STRING)
    CourseSessionStatus status;

    @NotNull(message = "Trainers can not be empty")
    @ManyToMany
    private List<User> trainers;

    @NotNull(message = "Number of lessons can not be empty")
    @Positive(message = "Number of lessons must be positive")
    private Integer numberOfLessons;

    @NotNull(message = "Start date can not be empty")
    private Instant startDate;

    @NotNull(message = "End date can not be empty")
    private Instant endDate;

    @NotNull(message = "Enrolled student count can not be empty")
    private int enrolledStudentCnt;

    @NotNull(message = "Required student count can't be empty")
    @Positive(message = "Required student count must be positive")
    private Integer requiredStudentCnt;

    @NotNull(message = "Course Schedule can not be empty")
    private String courseSchedule;

    private String shortDescription;

    @Lob
    private String teacherDescription;

    private String photo;

    private Long price;

    @ManyToOne
    @JoinColumn(name = "category")
    private Category category;

    @Formula("(SELECT COALESCE(AVG(r.rating),0.0) from review r where r.item_id = course_id and r.item_type ='COURSE'"
            + " and r.status = 'APPROVED')")
    private Double rating;

    @ManyToMany
    private List<Tag> tags;

    @JsonIgnore
    private boolean deleted;
}
