package az.ingress.edu.model;

import javax.persistence.Embeddable;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseDetails {

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 2, message = "Name should have at least 2 characthers")
    private String name;

    @NotEmpty(message = "Description can't be empty")
    @Size(min = 10, message = "Description should have at least 10 characthers")
    @Lob
    private String description;

    @NotEmpty(message = "Short description can't be empty")
    @Size(min = 10, max = 255 ,message = "Short description must contain min 10 max 255 character")
    private String shortDescription;

    @NotEmpty(message = "Requirements can't be empty")
    @Size(min = 2, message = "Requirements should have at least 2 characthers")
    @Lob
    private String requirements;

    @NotEmpty(message = "Status can't be empty")
    @Size(min = 2, message = "Status should have at least 2 characthers")
    private String status;
}
