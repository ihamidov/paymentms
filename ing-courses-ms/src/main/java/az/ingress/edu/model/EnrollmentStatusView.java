package az.ingress.edu.model;

public enum EnrollmentStatusView {
    PENDING, APPROVED, CANCELLED,NOT_ENROLLED;
}
