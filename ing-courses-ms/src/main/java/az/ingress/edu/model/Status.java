package az.ingress.edu.model;

public enum Status {
    PENDING, IN_PROGRESS,COMPLETED, APPROVED, REJECTED
}
