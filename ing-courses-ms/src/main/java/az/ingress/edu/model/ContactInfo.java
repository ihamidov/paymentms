package az.ingress.edu.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class ContactInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @ElementCollection(targetClass = String.class)
    private List<String> contactNumbers;

    @NotEmpty(message = "Address can't be empty")
    @Size(min = 3, message = "Address should have at least 3 characthers")
    private String address;

    @NotEmpty(message = "Email can't be empty")
    @Size(min = 3, message = "Email should have at least 3 characthers")
    private String email;
}
