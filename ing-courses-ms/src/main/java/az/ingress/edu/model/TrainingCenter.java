package az.ingress.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class TrainingCenter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 3, message = "Name should have at least 3 characthers")
    private String name;

    @NotEmpty(message = "Description can't be empty")
    @Size(min = 3, message = "Description should have at least 3 characthers")
    @Lob
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    private ContactInfo contactInfo;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;

    @JsonIgnore
    private boolean deleted;
}
