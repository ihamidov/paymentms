package az.ingress.edu.model;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table
public class Syllabus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Course id can not be empty")
    @Positive(message = "Course id must be positive")
    private Long courseId;

    @Positive(message = "Parent id must be positive")
    private Long parentId;

    @NotEmpty(message = "Title can not be empty")
    @Size(max = 255,message = "Max 255 characters allowed for title")
    private String title;

    @NotEmpty(message = "Description can not be empty")
    @Size(max = 255,message = "Max 255 characters allowed for description")
    private String description;

    private long childCount;

    @Column(columnDefinition = "TIME")
    @ApiModelProperty(example = "02:00:00")
    private LocalTime duration;

    @Transient
    private List<Syllabus> children;
}
