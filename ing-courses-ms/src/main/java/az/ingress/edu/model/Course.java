package az.ingress.edu.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private CourseDetails courseDetails;

    @ManyToOne
    @JoinColumn(name = "category")
    private Category category;

    @Where(clause = "deleted = false")
    @ManyToOne
    private TrainingCenter trainingCenter;

    @ManyToMany
    private List<Tag> tags;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Calendar createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar modifiedAt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    private User createdBy;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;

    @Formula("(SELECT COALESCE(AVG(r.rating),0.0) from review r where r.item_id = id and r.item_type = 'COURSE' "
            + "and r.status = 'APPROVED')")
    private Double rating;

    @JsonIgnore
    private boolean deleted;
}
