package az.ingress.edu.model;

import java.time.Instant;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@EntityListeners({AuditingEntityListener.class})
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Item id can't be empty")
    @Positive(message = "Item id must be positive")
    private Long itemId;

    private String itemName;

    @NotNull(message = "rating can't be empty")
    @Range(min = 0,max = 5,message = "Rating must be between 0 and 5")
    private Integer rating;

    @NotNull(message = "Item type can't be empty")
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    @NotEmpty(message = "Comment can't be empty")
    @Lob
    @Column(length = 1000)
    private String comment;

    @Column(updatable = false)
    @CreatedDate
    private Instant createdAt;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "created_by")
    private User createdBy;

    @Enumerated(EnumType.STRING)
    private ReviewStatus status;
}
