package az.ingress.edu.model;


public enum ReviewStatus {
    PENDING, APPROVED
}
