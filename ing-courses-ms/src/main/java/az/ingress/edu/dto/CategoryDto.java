package az.ingress.edu.dto;

import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto {

    public static final String NAME_CAN_NOT_BE_NULL = "Category name can not be null";

    @NotEmpty(message = NAME_CAN_NOT_BE_NULL)
    private String name;

}
