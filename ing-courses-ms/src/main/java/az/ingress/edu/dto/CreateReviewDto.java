package az.ingress.edu.dto;

import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.model.ReviewStatus;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CreateReviewDto {

    @NotNull(message = "Item id can't be empty")
    @Positive(message = "Item id must be positive")
    private Long itemId;

    @NotNull(message = "rating can't be empty")
    @Range(min = 0, max = 5, message = "Rating must be between 0 and 5")
    private Integer rating;

    @NotNull(message = "Item type can't be empty")
    private ItemType itemType;

    @NotEmpty(message = "Comment can't be empty")
    @Size(max = 1000,message = "Max 1000 characters allowed")
    private String comment;

    public static Review toReview(CreateReviewDto createReviewDto) {
        return Review.builder()
                .itemId(createReviewDto.getItemId())
                .itemType(createReviewDto.getItemType())
                .rating(createReviewDto.getRating())
                .status(ReviewStatus.PENDING)
                .comment(createReviewDto.getComment())
                .build();
    }
}
