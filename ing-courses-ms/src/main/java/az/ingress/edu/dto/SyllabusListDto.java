package az.ingress.edu.dto;

import az.ingress.edu.model.Syllabus;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SyllabusListDto {

    private long courseId;

    private List<Syllabus> syllabus;
}
