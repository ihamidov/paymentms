package az.ingress.edu.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalTime;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateSyllabusDto {

    @NotEmpty(message = "Title can not be empty")
    private String title;

    @NotEmpty(message = "Description can not be empty")
    private String description;

    @ApiModelProperty(example = "02:00:00")
    @NotNull(message = "Duration can't be empty")
    private LocalTime duration;
}
