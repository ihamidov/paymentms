package az.ingress.edu.dto;

import az.ingress.edu.model.ReviewStatus;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewStatusDto {

    @NotNull(message = "Review status can't be empty")
    private ReviewStatus reviewStatus;
}
