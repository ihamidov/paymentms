package az.ingress.edu.dto;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalTime;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateSyllabusDto {

    @NotNull(message = "Course id can not be empty")
    @Positive(message = "Course id must be positive")
    private Long courseId;

    @Positive(message = "Parent id must be positive")
    private Long parentId;

    @NotEmpty(message = "Title can not be empty")
    private String title;

    @NotEmpty(message = "Description can not be empty")
    private String description;

    @NotNull(message = "Duration can't be empty")
    @ApiModelProperty(example = "02:00:00")
    private LocalTime duration;
}
