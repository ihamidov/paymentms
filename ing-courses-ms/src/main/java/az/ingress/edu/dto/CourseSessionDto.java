package az.ingress.edu.dto;

import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.CourseSessionStatus;
import java.time.Instant;
import java.util.List;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseSessionDto {

    @NotNull(message = "Status can not be empty")
    @Enumerated(EnumType.STRING)
    CourseSessionStatus status;

    @NotNull(message = "Number of lessons can not be empty")
    @Positive(message = "Number of lessons must be positive")
    private Integer numberOfLessons;

    @NotNull(message = "Start date can not be empty")
    private Instant startDate;

    @NotNull(message = "End date can not be empty")
    private Instant endDate;

    @NotNull(message = "Required student count can't be empty")
    @Positive(message = "Required student count must be positive")
    private Integer requiredStudentCnt;

    @NotEmpty(message = "Course schedule can't be empty")
    private String courseSchedule;

    @NotEmpty(message = "At least one trainer must be included")
    private List<UsernameDto> trainers;

    @NotNull(message = "Price can't be empty")
    @PositiveOrZero(message = "Invalid price")
    private Long price;

    private String teacherDescription;

    public static CourseSession toCourseSession(CourseSessionDto courseSessionDto) {
        return CourseSession.builder()
            .courseSchedule(courseSessionDto.getCourseSchedule())
            .startDate(courseSessionDto.getStartDate())
            .enrolledStudentCnt(0)
            .endDate(courseSessionDto.getEndDate())
            .requiredStudentCnt(courseSessionDto.getRequiredStudentCnt())
            .numberOfLessons(courseSessionDto.getNumberOfLessons())
            .teacherDescription(courseSessionDto.getTeacherDescription())
            .status(courseSessionDto.getStatus())
            .price(courseSessionDto.getPrice())
            .build();
    }
}
