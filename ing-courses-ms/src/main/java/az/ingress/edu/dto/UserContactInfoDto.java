package az.ingress.edu.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserContactInfoDto {

    private String address;

    @NotEmpty(message = "Email can't be empty")
    @Email(message = "Email is not valid")
    private String email;

    private String phone;

    private String fb;

    private String twitter;

    private String linkedin;

    private String website;
}
