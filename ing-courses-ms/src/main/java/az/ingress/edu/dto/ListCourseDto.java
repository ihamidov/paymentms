package az.ingress.edu.dto;

import az.ingress.edu.model.Category;
import az.ingress.edu.model.Tag;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class ListCourseDto {

    private long id;

    private String photo;

    private String name;

    private String shortDescription;

    private Category category;

    private double rating;

    private List<Tag> tags;
}
