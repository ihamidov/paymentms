package az.ingress.edu.dto;

import az.ingress.edu.model.ContactInfo;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainingCenterDto {

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 3, message = "Name should have at least 3 characthers")
    private String name;

    @NotEmpty(message = "Description can't be empty")
    @Size(min = 10, message = "Description should have at least 3 characthers")
    private String description;

    private ContactInfo contactInfo;

    @Size(max = 2000, message = "Max 2000 characters allowed")
    private String photo;
}
