package az.ingress.edu.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UpdateReviewDto {

    @NotNull(message = "rating can't be empty")
    @Range(min = 0, max = 5, message = "Rating must be between 0 and 5")
    private Integer rating;

    @NotEmpty(message = "Comment can't be empty")
    @Size(max = 1000,message = "Max 1000 characters allowed")
    private String comment;
}
