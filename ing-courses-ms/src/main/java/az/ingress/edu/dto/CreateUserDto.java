package az.ingress.edu.dto;

import az.ingress.edu.model.User;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserDto {

    @NotEmpty(message = "Username can't be empty")
    private String username;

    @NotEmpty(message = "Firstname can't be empty")
    private String firstname;

    @NotEmpty(message = "Lastname can't be empty")
    private String lastname;

    @NotEmpty(message = "Email can't be empty")
    private String email;

    private String phone;

    public static User toUser(CreateUserDto createUserDto) {
        return User
                .builder()
                .username(createUserDto.getUsername())
                .firstname(createUserDto.getFirstname())
                .lastname(createUserDto.getLastname())
                .email(createUserDto.getEmail())
                .phone(createUserDto.getPhone())
                .build();
    }
}
