package az.ingress.edu.dto;

import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.Enrollment;
import az.ingress.edu.model.EnrollmentStatus;
import az.ingress.edu.model.User;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnrollmentDto {

    private Long id;

    private User user;

    private ListCourseSessionDto session;

    private EnrollmentStatus enrollmentStatus;

    private Instant createdAt;

    public static EnrollmentDto from(Enrollment enrollment) {
        CourseSession session = enrollment.getSession();
        return EnrollmentDto.builder()
                .id(enrollment.getId())
                .user(enrollment.getUser())
                .enrollmentStatus(enrollment.getEnrollmentStatus())
                .createdAt(enrollment.getCreatedAt())
                .session(ListCourseSessionDto.builder()
                .id(session.getId())
                .courseId(session.getCourseId())
                .name(session.getName())
                .startDate(session.getStartDate())
                .endDate(session.getEndDate())
                .enrolledStudentCnt(session.getEnrolledStudentCnt())
                .requiredStudentCnt(session.getRequiredStudentCnt())
                .build()).build();
    }
}
