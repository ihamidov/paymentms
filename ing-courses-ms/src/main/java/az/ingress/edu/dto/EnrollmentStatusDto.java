package az.ingress.edu.dto;

import az.ingress.edu.model.EnrollmentStatusView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnrollmentStatusDto {

    private EnrollmentStatusView enrollmentStatus;
}
