package az.ingress.edu.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnrollmentInfoDto {

    private String firstName;

    private String lastName;

    private String username;

    private long courseSessionId;

    private String courseName;
}
