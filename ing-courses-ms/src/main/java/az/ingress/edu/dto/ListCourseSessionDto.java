package az.ingress.edu.dto;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListCourseSessionDto {

    private Long id;

    private String name;

    private Long courseId;

    private Instant startDate;

    private Instant endDate;

    private int enrolledStudentCnt;

    private Integer requiredStudentCnt;
}
