package az.ingress.edu.dto;

import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.ReviewStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewFilterDto {

    ItemType itemType;

    ReviewStatus reviewStatus;

    String order = "id";

    String direction = "ASC";
}
