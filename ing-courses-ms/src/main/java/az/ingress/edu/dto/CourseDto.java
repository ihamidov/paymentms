package az.ingress.edu.dto;

import java.util.List;
import javax.persistence.Lob;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDto {

    @NotNull(message = "Category can't be empty")
    @Positive(message = "Category id must be positive")
    private Long categoryId;

    @NotEmpty(message = "Name can't be empty")
    @Size(min = 2, message = "Name should have at least 2 characters")
    private String name;

    @NotEmpty(message = "Description can't be empty")
    @Size(min = 10, message = "Description should have at least 10 characters")
    @Lob
    private String description;

    @NotEmpty(message = "Requirements can't be empty")
    @Size(min = 2, message = "Requirements should have at least 2 characters")
    private String requirements;

    @NotEmpty(message = "Status can't be empty")
    @Size(min = 2, message = "Status should have at least 2 characters")
    private String status;

    @NotNull(message = "Photo can't be empty")
    @Size(max = 2000,message = "Max 2000 characters allowed")
    private String photo;

    @NotEmpty(message = "Short description can't be empty")
    @Size(min = 10, max = 255, message = "Short description must contain min 10 max 255 character")
    private String shortDescription;

    @NotNull(message = "Training center id can't be empty")
    private Long trainingCenterId;

    private List<IdDto> tags;
}
