package az.ingress.edu.dto;

import az.ingress.edu.model.EnrollmentStatus;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEnrollmentDto {

    @NotNull(message = "Enrollment status can't be empty")
    private EnrollmentStatus enrollmentStatus;

}
