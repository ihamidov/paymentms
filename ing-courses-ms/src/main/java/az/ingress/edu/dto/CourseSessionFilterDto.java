package az.ingress.edu.dto;

import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseSessionFilterDto {

    List<Long> tags;

    List<Long> categories;

    String order = "startDate";

    String direction = "ASC";

    public List<Long> getTags() {
        return tags == null ? Collections.emptyList() : tags;
    }

    public List<Long> getCategories() {
        return categories == null ? Collections.emptyList() : categories;
    }
}
