package az.ingress.edu.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageDto {

    private int pageNumber;

    private int pageSize;

    private String sortColumn;

    private String sortDirection;
}
