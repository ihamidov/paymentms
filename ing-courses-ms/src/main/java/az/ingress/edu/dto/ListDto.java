package az.ingress.edu.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ListDto {

    private List content;

    private long totalElements;

    private long totalPages;
}
