package az.ingress.edu.repository;

import az.ingress.edu.model.TrainingCenter;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingCenterRepository extends PagingAndSortingRepository<TrainingCenter, Long> {

    Optional<TrainingCenter> findByIdAndDeletedFalse(long id);

    Iterable<TrainingCenter> findAllByDeletedFalse();

}
