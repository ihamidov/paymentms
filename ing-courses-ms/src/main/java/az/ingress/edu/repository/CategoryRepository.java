package az.ingress.edu.repository;

import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> ,
        JpaSpecificationExecutor<Course> {

    Optional<Category> findByIdAndDeletedFalse(long id);

    Page<Category> findAllByDeletedFalse(Pageable pageable);

    Optional<Category> findCategoryByNameAndDeletedFalse(String name);
}
