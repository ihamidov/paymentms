package az.ingress.edu.repository;

import az.ingress.edu.model.Syllabus;
import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SyllabusRepository extends CrudRepository<Syllabus, Long> {

    List<Syllabus> findAllByCourseId(long courseId, Sort sort);

    List<Syllabus> findAllByParentIdOrderById(long parentId);
}
