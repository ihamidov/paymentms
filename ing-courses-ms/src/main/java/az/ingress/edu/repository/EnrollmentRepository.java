package az.ingress.edu.repository;

import az.ingress.edu.model.Enrollment;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EnrollmentRepository extends CrudRepository<Enrollment, Long>,
        PagingAndSortingRepository<Enrollment, Long> {

    @Query("from Enrollment e  where e.user.username = ?1 and e.session.id = ?2 and e.enrollmentStatus <> 'CANCELLED'")
    Optional<Enrollment> findByCourseSessionIdAndUsername(String username,long courseSessionId);

    @Query("from Enrollment e where e.enrollmentStatus <> 'CANCELLED'")
    Page<Enrollment> findAllNonCancelled(Pageable pageable);

    @Query("from Enrollment e where e.session.id = ?1 and e.enrollmentStatus <> 'CANCELLED'")
    Page<Enrollment> findAllNonCancelledByCourseSessionId(long courseSessionId ,Pageable pageable);
}
