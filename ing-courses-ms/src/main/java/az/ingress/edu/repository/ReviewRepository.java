package az.ingress.edu.repository;

import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.model.ReviewStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends PagingAndSortingRepository<Review, Long>,
        JpaSpecificationExecutor<Review> {

    @Query("from Review r where r.itemId = ?1 and r.itemType = ?2 and r.status = 'APPROVED'")
    Page<Review> findAllReviews(long itemId, ItemType itemType, Pageable pageable);

    default Page<Review> findAll(ItemType itemType, ReviewStatus status, Pageable pageable) {
        return findAll((root, criteriaQuery, cb) ->
                cb.and(itemType == null
                                ? cb.conjunction() : root.get("itemType").in(itemType),
                        status == null
                                ? cb.conjunction() : root.get("status").in(status)), pageable);
    }
}
