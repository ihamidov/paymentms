package az.ingress.edu.repository;

import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.TrainingCenter;
import java.util.List;
import java.util.Optional;
import javax.persistence.criteria.Join;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends PagingAndSortingRepository<Course, Long> , JpaSpecificationExecutor<Course> {

    String DELETED = "deleted";

    List<Course> findCourseByCategory(Category category);

    Optional<Course> findByIdAndDeletedFalse(long id);

    default Page<Course> findAllCourses(Pageable pageable) {
        return findAll((root, query, cb) -> {
            Join<Course, TrainingCenter> trainingCenterJoin = root.join("trainingCenter");
            return cb.and(cb.equal(root.get(DELETED),false),cb.equal(trainingCenterJoin.get(DELETED),false));
        },pageable);
    }
}
