package az.ingress.edu.repository;

import az.ingress.edu.model.CourseSession;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseSessionRepository extends PagingAndSortingRepository<CourseSession, Long>,
        JpaSpecificationExecutor<CourseSession> {

    Page<CourseSession> findAllByCourseIdAndDeletedFalse(long courseId, Pageable pageable);

    Optional<CourseSession> findByIdAndDeletedFalse(long id);

    default Page<CourseSession> findAll(List<Long> tags, List<Long> categories, Pageable pageable) {
        return findAll((root, criteriaQuery, cb) ->
                cb.and(tags.isEmpty()
                                ? cb.conjunction() : root.join("tags").in(tags),
                        categories.isEmpty()
                                ? cb.conjunction() : root.get("category").in(categories)), pageable);
    }

    List<CourseSession> findAllByCourseId(long courseId);
}
