package az.ingress.edu.repository;

import az.ingress.edu.model.ContactInfo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactInfoRepository extends PagingAndSortingRepository<ContactInfo,Long> {
}
