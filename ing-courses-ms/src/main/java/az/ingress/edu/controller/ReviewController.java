package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateReviewDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ReviewFilterDto;
import az.ingress.edu.dto.ReviewStatusDto;
import az.ingress.edu.dto.UpdateReviewDto;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.service.ReviewService;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/review")
@AllArgsConstructor
@Validated
public class ReviewController {

    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String REVIEW_ID_MUST_BE_POSITIVE = "Review id must be positive";
    private final ReviewService reviewService;

    @PostMapping
    public IdDto createReview(@Validated @RequestBody CreateReviewDto createReviewDto) {
        return reviewService.createReview(createReviewDto);
    }

    @GetMapping("/{id}")
    public Review getReview(@Positive(message = REVIEW_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return reviewService.getReview(id);
    }

    @PutMapping("/{id}/status")
    public IdDto changeReviewStatus(@Positive(message = REVIEW_ID_MUST_BE_POSITIVE) @PathVariable long id,
                                    @RequestBody ReviewStatusDto reviewStatus) {
        return reviewService.changeStatus(id, reviewStatus);
    }

    @DeleteMapping("/{id}")
    public void deleteReview(@Positive(message = REVIEW_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        reviewService.deleteReview(id);
    }

    @PutMapping("/{id}")
    public IdDto updateReview(@Positive(message = REVIEW_ID_MUST_BE_POSITIVE) @PathVariable long id,
                              @Validated @RequestBody UpdateReviewDto updateReviewDto) {
        return reviewService.updateReview(updateReviewDto, id);
    }

    @GetMapping("/course/{id}/list")
    public Page<Review> getReviewListForCourse(
            @PathVariable @Positive(message = "Course id must be positive") long id,
            @RequestParam(defaultValue = "0") @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
            @RequestParam(defaultValue = "10") @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size) {
        return reviewService.getReviewList(ItemType.COURSE, id, page, size);
    }

    @GetMapping("/training-center/{id}/list")
    public Page<Review> getReviewListForTrainingCenter(
            @PathVariable @Positive(message = "Training center id must be positive") long id,
            @RequestParam(defaultValue = "0") @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
            @RequestParam(defaultValue = "10") @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size) {
        return reviewService.getReviewList(ItemType.TRAINING_CENTER, id, page, size);
    }

    @GetMapping("/course-session/{id}/list")
    public Page<Review> getReviewListForCourseSession(
            @PathVariable @Positive(message = "Course session id must be positive") long id,
            @RequestParam(defaultValue = "0") @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
            @RequestParam(defaultValue = "10") @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size) {
        return reviewService.getReviewList(ItemType.COURSE_SESSION, id, page, size);
    }

    @GetMapping("/list")
    public Page<Review> getReviewListForManagement(@RequestParam(defaultValue = "0")
                                                   @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
                                                   @RequestParam(defaultValue = "10")
                                                   @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                                   ReviewFilterDto reviewFilter) {
        return reviewService.getReviewListForManagement(page, size, reviewFilter);
    }
}
