package az.ingress.edu.controller;

import az.ingress.edu.dto.TrainingCenterDto;
import az.ingress.edu.model.TrainingCenter;
import az.ingress.edu.service.TrainingCenterService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/training-center")
public class TrainingCenterController {
    TrainingCenterService trainingCenterService;

    public TrainingCenterController(TrainingCenterService trainingCenterService) {
        this.trainingCenterService = trainingCenterService;
    }

    @PostMapping("/")
    public TrainingCenter createTrainingCenter(@Validated @RequestBody TrainingCenterDto trainingCenterDto) {
        return trainingCenterService.add(trainingCenterDto);
    }

    @PutMapping("/{id}")
    public TrainingCenter updateTC(@Validated @RequestBody TrainingCenterDto tcDTO, @PathVariable("id") Long id) {
        return trainingCenterService.update(tcDTO, id);
    }

    @GetMapping("/{id}")
    public TrainingCenter getTrainingCenter(@PathVariable("id") Long id) {
        return trainingCenterService.findById(id);
    }

    @GetMapping("/list")
    public Iterable<TrainingCenter> getAllList() {
        return trainingCenterService.findAll();
    }

    @DeleteMapping("/{id}")
    public void deleteTrainingCenter(@PathVariable("id") Long id) {
        trainingCenterService.deleteById(id);
    }
}
