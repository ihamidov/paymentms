package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateSyllabusDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.SyllabusListDto;
import az.ingress.edu.dto.UpdateSyllabusDto;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.service.SyllabusService;
import java.util.List;
import javax.validation.constraints.Positive;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/syllabus")
@Validated
public class SyllabusController {

    private static final String COURSE_ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String SYLLABUS_ID_MUST_BE_POSITIVE = "Syllabus id must be positive";
    private final SyllabusService syllabusService;

    public SyllabusController(SyllabusService syllabusService) {
        this.syllabusService = syllabusService;
    }

    @PostMapping
    public IdDto createSyllabus(@Validated @RequestBody CreateSyllabusDto createSyllabusDto) {
        Syllabus syllabus = Syllabus.builder()
                .courseId(createSyllabusDto.getCourseId())
                .parentId(createSyllabusDto.getParentId())
                .title(createSyllabusDto.getTitle())
                .description(createSyllabusDto.getDescription())
                .duration(createSyllabusDto.getDuration())
                .build();

        return new IdDto(syllabusService.createSyllabus(syllabus).getId());
    }

    @GetMapping("/course/{id}")
    public SyllabusListDto getSyllabusList(
            @Positive(message = COURSE_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return new SyllabusListDto(id, syllabusService.getSyllabi(id));
    }

    @GetMapping("/{id}")
    public Syllabus getSyllabus(@Positive(message = SYLLABUS_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return syllabusService.getSyllabus(id);
    }

    @DeleteMapping("/{id}")
    public void deleteSyllabus(@Positive(message = SYLLABUS_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        syllabusService.deleteSyllabus(id);
    }

    @PutMapping("/{id}")
    public IdDto updateSyllabus(@Validated @RequestBody UpdateSyllabusDto updateSyllabusDto,
                                @Positive(message = SYLLABUS_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return syllabusService.updateSyllabus(updateSyllabusDto, id);
    }

    @GetMapping("/children/{parentId}")
    public List<Syllabus> getChildSyllabi(@Positive(message = SYLLABUS_ID_MUST_BE_POSITIVE)
                                          @PathVariable long parentId) {
        return syllabusService.getChildSyllabi(parentId);
    }
}
