package az.ingress.edu.controller;

import az.ingress.edu.dto.CreateEnrollmentDto;
import az.ingress.edu.dto.EnrollmentDto;
import az.ingress.edu.dto.EnrollmentStatusDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateEnrollmentDto;
import az.ingress.edu.service.EnrollmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("Enrollment Api")
@RequestMapping("/enrollment")
public class EnrollmentController {

    private static final String ID = "id";
    private static final String ASC = "asc";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String SESSION_ID_MUST_BE_POSITIVE = "Session id must be positive";
    private final EnrollmentService enrollmentService;

    public EnrollmentController(EnrollmentService enrollmentService) {
        this.enrollmentService = enrollmentService;
    }

    @PostMapping("/{sessionId}")
    @ApiOperation("Create Enrollment")
    public IdDto createEnrollment(@PathVariable @Positive(message = SESSION_ID_MUST_BE_POSITIVE) long sessionId,
                                  @RequestBody CreateEnrollmentDto enrollmentDto) {
        return new IdDto(enrollmentService.add(sessionId,enrollmentDto).getId());
    }

    @GetMapping("/{id}")
    @ApiOperation("Retrieve Enrollment")
    public EnrollmentDto findById(@PathVariable("id") Long id) {
        return EnrollmentDto.from(enrollmentService.findById(id));
    }

    @PutMapping("/{id}")
    @ApiOperation("Update Enrollment")
    public IdDto updateEnrolment(@RequestBody UpdateEnrollmentDto enrollmentDto, @PathVariable("id") Long id) {
        return new IdDto(enrollmentService.update(enrollmentDto, id).getId());
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete Enrollment")
    public void deleteEnrollment(@PathVariable("id") Long id) {
        enrollmentService.deleteById(id);
    }

    @GetMapping("/list")
    @ApiOperation("Get Enrollment list")
    public Page<EnrollmentDto> findAllEnrollment(@RequestParam(defaultValue = "0")
                                              @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
                                              @RequestParam(defaultValue = "10")
                                              @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                              @RequestParam(defaultValue = ID) String order,
                                              @RequestParam(defaultValue = ASC) String direction) {
        return enrollmentService.findAllEnrolment(page, size, order, direction);
    }


    @GetMapping("/{sessionId}/list")
    @ApiOperation("Get Enrollment list")
    public Page<EnrollmentDto> findAllBySessionId(@PathVariable @Positive(message = SESSION_ID_MUST_BE_POSITIVE)
                                                                 long sessionId,
                                                         @RequestParam(defaultValue = "0")
                                                         @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE)
                                                                 int page,
                                                         @RequestParam(defaultValue = "10")
                                                         @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
                                                         @RequestParam(defaultValue = ID) String order,
                                                         @RequestParam(defaultValue = ASC) String direction) {
        return enrollmentService.findAllByCourseSessionId(sessionId, page, size, order, direction);
    }

    @GetMapping("/status/{sessionId}")
    public EnrollmentStatusDto getEnrollmentStatus(@Positive(message = SESSION_ID_MUST_BE_POSITIVE)
                                                   @PathVariable long sessionId) {
        return enrollmentService.getEnrolmentStatus(sessionId);
    }

    @PutMapping("/cancel/{sessionId}")
    public void cancelEnrollment(@Positive(message = SESSION_ID_MUST_BE_POSITIVE)
                                 @PathVariable long sessionId) {
        enrollmentService.cancelEnrollment(sessionId);
    }
}
