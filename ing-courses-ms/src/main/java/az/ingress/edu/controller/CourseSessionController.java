package az.ingress.edu.controller;

import az.ingress.edu.dto.CourseSessionDto;
import az.ingress.edu.dto.CourseSessionFilterDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.service.CourseSessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/course")
@Api("Course and Training Session Api")
@Validated
public class CourseSessionController {

    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String DIRECT_DEFAULT_VALUE = "asc";
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "10";
    private static final String ID = "id";
    private static final String ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String SESSION_ID_MUST_BE_POSITIVE = "Course session id must be positive";
    private final CourseSessionService sessionService;

    @PostMapping("/{id}/session")
    @ApiOperation("Add a new course session")
    public IdDto createCourseSession(@Validated @RequestBody CourseSessionDto courseSessionDto,
                                     @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        return new IdDto(sessionService.createCourseSession(courseSessionDto,id).getId());
    }

    @PutMapping(path = "/session/{id}")
    public IdDto updateCourseSession(@Validated @RequestBody CourseSessionDto courseSessionDto,
                                     @Positive(message = SESSION_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return new IdDto(sessionService.updateCourseSession(courseSessionDto,id).getId());
    }

    @GetMapping(path = "/{id}/session/list")
    @ApiOperation("Find session by course id")
    public Page<CourseSession> getCourseSessionList(
            @Positive(message = ID_MUST_BE_POSITIVE) @PathVariable long id,
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER)
            @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
            @RequestParam(defaultValue = ID) String order,
            @RequestParam(defaultValue = DIRECT_DEFAULT_VALUE) String direction) {
        return sessionService.getCourseSessionList(id, direction, order, page, size);
    }

    @GetMapping(path = "/session/{id}")
    public CourseSession getCourseSession(@Positive(message = SESSION_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return sessionService.getCourseSession(id);
    }

    @DeleteMapping(path = "/session/{id}")
    public void deleteCourseSession(@Positive(message = SESSION_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        sessionService.deleteCourseSession(id);
    }

    @GetMapping(path = "/session/list")
    public Page<CourseSession> getAllCourseSession(
            @RequestParam(defaultValue = "0") @PositiveOrZero(message = PAGE_MUST_BE_ZERO_OR_POSITIVE) int page,
            @RequestParam(defaultValue = "10") @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE) int size,
            CourseSessionFilterDto sessionFilter) {
        return sessionService.findAllSession(page,size,sessionFilter);
    }
}
