package az.ingress.edu.controller;

import az.ingress.edu.dto.CategoryDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.model.Category;
import az.ingress.edu.service.CategoryServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/category")
@Api("Category API")
@AllArgsConstructor
public class CategoryRestController {

    private static final String CATEGORY_ID_MUST_BE_POSITIVE = "Category id must be positive";
    private static final String GET_CATEGORY_BY_ID = "Get categoryId by id";
    private static final String DELETE_CATEGORY_BY_ID = "Delete categoryId by id";
    private static final String ADD_CATEGORY = "Add new categoryId";
    private static final String UPDATE_CATEGORY = "Update categoryId";
    private static final String FIND_ALL_CATEGORY_PAGE = "Find all category by page";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String DIRECT_DEFAULT_VALUE = "ASC";
    private static final String ID = "id";
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";

    private final CategoryServiceImpl categoryService;

    @GetMapping("/{id}")
    @ApiOperation(GET_CATEGORY_BY_ID)
    public Category findCategoryById(@PathVariable @Positive(message = CATEGORY_ID_MUST_BE_POSITIVE) long id) {
        return categoryService.findById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(DELETE_CATEGORY_BY_ID)
    public void deleteCategory(@Validated @PathVariable @Positive(message = CATEGORY_ID_MUST_BE_POSITIVE) long id) {
        categoryService.deleteById(id);
    }

    @PostMapping("/")
    @ApiOperation(ADD_CATEGORY)
    public IdDto addCategory(@Validated @RequestBody CategoryDto categoryDto) {
        return new IdDto(categoryService.add(categoryDto).getId());
    }

    @PutMapping("/{id}")
    @ApiOperation(UPDATE_CATEGORY)
    public IdDto updateCategory(@Validated @RequestBody CategoryDto categoryDto,
                                @PathVariable @Positive(message = CATEGORY_ID_MUST_BE_POSITIVE) long id) {
        return new IdDto(categoryService.update(categoryDto, id).getId());
    }

    @GetMapping(path = "/list")
    @ApiOperation(FIND_ALL_CATEGORY_PAGE)
    public Page<Category> list(
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) @PositiveOrZero(message = PAGE_INDEX_MUST_BE_POSITIVE)
                    int pageNumber,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE)
                    int pageSize,
            @RequestParam(defaultValue = ID) String sortColumn,
            @RequestParam(defaultValue = DIRECT_DEFAULT_VALUE) String sortDirection) {
        return categoryService.list(PageDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortColumn(sortColumn)
                .sortDirection(sortDirection)
                .build());
    }

}
