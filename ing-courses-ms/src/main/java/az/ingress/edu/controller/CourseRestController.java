package az.ingress.edu.controller;

import az.ingress.edu.dto.CourseDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.model.Course;
import az.ingress.edu.service.CourseServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("Course API")
@Validated
@RestController
@AllArgsConstructor
@RequestMapping("/course")
public class CourseRestController {

    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String ID_MUST_BE_POSITIVE = "Id must be positive";
    private static final String DIRECT_DEFAULT_VALUE = "ASC";
    private static final String ID = "id";
    private static final String DEFAULT_PAGE_NUMBER = "0";
    private static final String DEFAULT_PAGE_SIZE = "20";

    private final CourseServiceImpl courseService;

    @GetMapping("/{id}")
    @ApiOperation("Get course by id")
    public Course findCourseById(
            @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        return courseService.findById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete course by id")
    public void deleteCourse(
            @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        courseService.findById(id);
        courseService.deleteById(id);
    }

    @PostMapping("/")
    @ApiOperation("Add new course")
    public IdDto createCourse(@Validated @RequestBody CourseDto courseDto) {
        return new IdDto(courseService.add(courseDto).getId());
    }

    @PutMapping("/{id}")
    @ApiOperation("Update course")
    public IdDto updateCourse(@Validated @RequestBody CourseDto courseDto,
                              @PathVariable @Positive(message = ID_MUST_BE_POSITIVE) long id) {
        courseService.findById(id);
        return new IdDto(courseService.update(courseDto, id).getId());
    }

    @GetMapping(path = "/list")
    @ApiOperation("Find all courses by page")
    public ListDto list(
            @RequestParam(defaultValue = DEFAULT_PAGE_NUMBER) @PositiveOrZero(message = PAGE_INDEX_MUST_BE_POSITIVE)
                    int pageNumber,
            @RequestParam(defaultValue = DEFAULT_PAGE_SIZE) @Positive(message = PAGE_SIZE_MUST_BE_POSITIVE)
                    int pageSize,
            @RequestParam(defaultValue = ID) String sortColumn,
            @RequestParam(defaultValue = DIRECT_DEFAULT_VALUE) String sortDirection) {
        return courseService.list(PageDto.builder()
                .pageNumber(pageNumber)
                .pageSize(pageSize)
                .sortColumn(sortColumn)
                .sortDirection(sortDirection)
                .build());
    }

}
