package az.ingress.edu.exception;

public class MinimumSessionDurationException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public MinimumSessionDurationException() {
        super("Minimum allowed session duration is 1 day");
    }
}
