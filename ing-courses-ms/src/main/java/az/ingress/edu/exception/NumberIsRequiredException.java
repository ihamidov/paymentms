package az.ingress.edu.exception;

public class NumberIsRequiredException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public NumberIsRequiredException() {
        super("Please add phone number to send enrollment");
    }
}
