package az.ingress.edu.exception;

public class UserAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserAlreadyExistsException(String username) {
        super(String.format("User with username: '%s' already exists", username));
    }
}
