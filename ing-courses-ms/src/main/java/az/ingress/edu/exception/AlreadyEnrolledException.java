package az.ingress.edu.exception;

public class AlreadyEnrolledException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public AlreadyEnrolledException() {
        super("You have already sent enrollment request");
    }
}
