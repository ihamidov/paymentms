package az.ingress.edu.exception;

public class SyllabusNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public SyllabusNotFoundException(long id) {
        super(String.format("Syllabus with id %d not found.", id));
    }
}
