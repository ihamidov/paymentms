package az.ingress.edu.exception;

public class SyllabusWithChildException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public SyllabusWithChildException() {
        super("Syllabus with child can't be deleted");
    }
}
