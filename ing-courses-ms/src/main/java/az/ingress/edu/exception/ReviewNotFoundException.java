package az.ingress.edu.exception;

public class ReviewNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public ReviewNotFoundException(long id) {
        super(String.format("Review with id: '%d' not found",id));
    }
}
