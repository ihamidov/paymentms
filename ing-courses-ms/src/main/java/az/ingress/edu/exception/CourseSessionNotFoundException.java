package az.ingress.edu.exception;

public class CourseSessionNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public CourseSessionNotFoundException(long id) {
        super(String.format("Course session with id '%d' not found.", id));
    }
}
