package az.ingress.edu.exception;

public class UserNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public UserNotFoundException(long id) {
        super(String.format("User with id %d not found.", id));
    }

    public UserNotFoundException(String username) {
        super(String.format("User with username %s not found.", username));
    }

}
