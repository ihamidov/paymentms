package az.ingress.edu.exception;

public class NoEnrollmentToCancelException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public NoEnrollmentToCancelException() {
        super("No enrollment to cancel");
    }
}
