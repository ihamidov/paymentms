package az.ingress.edu.exception;

public class CategoryInUseException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CategoryInUseException(String name) {
        super(String.format("Category with name: '%s'is in use ", name));
    }
}
