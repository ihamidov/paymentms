package az.ingress.edu.exception;

public class CourseNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public CourseNotFoundException(long id) {
        super(String.format("Course with id %d not found.", id));
    }
}
