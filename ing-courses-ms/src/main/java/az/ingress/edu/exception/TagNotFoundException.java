package az.ingress.edu.exception;

public class TagNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public TagNotFoundException(long id) {
        super(String.format("Tag with id %d not found.", id));
    }
}
