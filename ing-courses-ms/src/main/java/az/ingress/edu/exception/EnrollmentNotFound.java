package az.ingress.edu.exception;

public class EnrollmentNotFound extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public EnrollmentNotFound(long id) {
        super(String.format("Enrollment request %d not found.", id));
    }
}
