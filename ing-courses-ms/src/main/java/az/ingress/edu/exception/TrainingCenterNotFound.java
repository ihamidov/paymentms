package az.ingress.edu.exception;

public class TrainingCenterNotFound extends RuntimeException {
    private static final long serialVersionUID = -3042686055658047285L;

    public TrainingCenterNotFound() {
        super("Training Center not found");
    }
}
