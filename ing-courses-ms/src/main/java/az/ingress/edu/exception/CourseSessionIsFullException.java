package az.ingress.edu.exception;

public class CourseSessionIsFullException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public CourseSessionIsFullException() {
        super("No room left to enroll");
    }
}
