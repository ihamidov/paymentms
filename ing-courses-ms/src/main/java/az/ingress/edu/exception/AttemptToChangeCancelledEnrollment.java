package az.ingress.edu.exception;

public class AttemptToChangeCancelledEnrollment extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public AttemptToChangeCancelledEnrollment() {
        super("Cancelled enrollment request can't be updated");
    }
}
