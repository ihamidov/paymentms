package az.ingress.edu.exception;

public class CategoryAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CategoryAlreadyExistsException(String name) {
        super(String.format("Category with name: '%s' already exists", name));
    }
}
