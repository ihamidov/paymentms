package az.ingress.edu.exception;

public class ReviewWithoutEnrollmentException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    public ReviewWithoutEnrollmentException() {
        super("You have to enroll this course in order to review");
    }
}
