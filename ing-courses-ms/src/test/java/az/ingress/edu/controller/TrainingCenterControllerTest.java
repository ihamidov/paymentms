package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.TrainingCenterDto;
import az.ingress.edu.exception.TrainingCenterNotFound;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.TrainingCenter;
import az.ingress.edu.service.TrainingCenterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(TrainingCenterController.class)
@ActiveProfiles("test")
public class TrainingCenterControllerTest {

    private static final String CODE = "$.code";
    private static final String TIMESTAMP = "$.timestamp";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String DESCRIPTION = "Kubernetes Training";
    private static final String NAME = "Kubernetes Training Program";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TrainingCenterService trainingCenterService;
    private TrainingCenter trainingCenter;
    private TrainingCenterDto trainingCenterDto;
    private static final String TRANING_CENTER_URL = "/training-center/{id}";

    @Before
    public void setUp() {
        ContactInfo contactInfo = ContactInfo.builder()
                .contactNumbers(Arrays.asList("0517891050", "0503777933"))
                .email("farid.ali.bey@gmail.com")
                .id((long) 1)
                .build();
        trainingCenterDto = TrainingCenterDto.builder()
                .contactInfo(contactInfo)
                .name(NAME)
                .photo(PHOTO)
                .description(DESCRIPTION)
                .build();
        trainingCenter = TrainingCenter.builder()
                .name(NAME)
                .description(DESCRIPTION)
                .photo(PHOTO)
                .contactInfo(contactInfo)
                .build();
    }

    @Test
    public void createTrainingCenter() throws Exception {
        when(trainingCenterService.add(trainingCenterDto)).thenReturn(trainingCenter);
        mvc.perform(post("/training-center/")
                .content(objectMapper.writeValueAsString(trainingCenterDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(trainingCenter.getId()));
    }

    @Test
    public void updateTrainingCenter() throws Exception {
        trainingCenter.setId(1L);
        when(trainingCenterService.findById(ArgumentMatchers.any())).thenReturn(trainingCenter);
        when(trainingCenterService.update(trainingCenterDto, 1L)).thenReturn(trainingCenter);
        mvc.perform(put(TRANING_CENTER_URL, 1L)
                .content(objectMapper.writeValueAsString(trainingCenterDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(NAME));
    }

    @Test
    public void getTrainingCenter() throws Exception {
        when(trainingCenterService.findById(ArgumentMatchers.any())).thenReturn(trainingCenter);
        mvc.perform(get(TRANING_CENTER_URL, 1L)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

    }

    @Test
    public void getAllTrainingCenterList() throws Exception {
        when(trainingCenterService.findAll()).thenReturn(Arrays.asList(trainingCenter));
        mvc.perform(get("/training-center/list", 1L)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteTrainingCenter() throws Exception {
        mvc.perform(delete(TRANING_CENTER_URL, 1L))
                .andExpect(status().isOk());
    }

    @Test
    public void whenTrainingCenterNotFound() throws Exception {
        when(trainingCenterService.findById(ArgumentMatchers.any())).thenThrow(new TrainingCenterNotFound());
        mvc.perform(get(TRANING_CENTER_URL, 1L)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE, is(NOT_FOUND.value())))
                .andExpect(jsonPath(TIMESTAMP, is(notNullValue())))
                .andExpect(jsonPath(USER_MESSAGE, is("Training Center not found")))
                .andExpect(jsonPath(TECHNICAL_MESSAGE, is("Training Center not found")));
    }
}
