package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateSyllabusDto;
import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.service.SyllabusService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalTime;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(SyllabusController.class)
@ActiveProfiles("test")
public class UpdateSyllabusControllerTest {

    private static final String ID = "$.id";
    private static final String SYLLABUS_ID_MUST_BE_POSITIVE = "Syllabus id must be positive";
    private static final String TITLE_CANT_BE_EMPTY = "Title can not be empty";
    private static final String DESCRIPTION_CANT_BE_EMPTY = "Description can not be empty";
    private static final String DURATION_CANT_BE_EMPTY = "Duration can't be empty";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String UPDATE_SYLLABUS_PATH = "/syllabus/{id}";
    private static final String TITLE = "Casting operators";
    private static final String DESCRIPTION = "How casting works in Java";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SyllabusService syllabusService;

    private UpdateSyllabusDto updateSyllabusDto;

    @Before
    public void setUp() {
        updateSyllabusDto = UpdateSyllabusDto.builder()
                .title(TITLE)
                .duration(LocalTime.now())
                .description(DESCRIPTION)
                .build();
    }

    @Test
    public void givenNegativeSyllabusIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(put(UPDATE_SYLLABUS_PATH, id)
                .content(objectMapper.writeValueAsString(updateSyllabusDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenSyllabusWithNullDurationExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateSyllabusDto syllabusDto = new UpdateSyllabusDto(TITLE, DESCRIPTION, null);

        mockMvc.perform(MockMvcRequestBuilders
                .put(UPDATE_SYLLABUS_PATH,id)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE).value(DURATION_CANT_BE_EMPTY))
                .andExpect(jsonPath(ERROR_USER_MESSAGE).value(DURATION_CANT_BE_EMPTY));
    }

    @Test
    public void givenSyllabusWithEmptyTitleExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateSyllabusDto syllabusDto = UpdateSyllabusDto.builder()
                .title("")
                .duration(LocalTime.now())
                .description(DESCRIPTION)
                .build();

        mockMvc.perform(put(UPDATE_SYLLABUS_PATH, id)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TITLE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(TITLE_CANT_BE_EMPTY)));
    }

    @Test
    public void givenSyllabusWithEmptyDescriptionExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateSyllabusDto syllabusDto = UpdateSyllabusDto.builder()
                .title(TITLE)
                .description("")
                .duration(LocalTime.now())
                .build();

        mockMvc.perform(put(UPDATE_SYLLABUS_PATH, id)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(DESCRIPTION_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(DESCRIPTION_CANT_BE_EMPTY)));
    }

    @Test
    public void givenNonExistingSyllabusExpectErrorMessage() throws Exception {
        long id = 1L;
        when(syllabusService.updateSyllabus(any(UpdateSyllabusDto.class), anyLong()))
                .thenThrow(new SyllabusNotFoundException(id));

        mockMvc.perform(put(UPDATE_SYLLABUS_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(objectMapper.writeValueAsString(updateSyllabusDto))
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))));

        verify(syllabusService).updateSyllabus(updateSyllabusDto, id);
    }

    @Test
    public void givenExistingSyllabusIdExpectOk() throws Exception {
        long id = 1L;
        when(syllabusService.updateSyllabus(any(UpdateSyllabusDto.class), anyLong()))
                .thenReturn(new IdDto(id));

        mockMvc.perform(put(UPDATE_SYLLABUS_PATH, id)
                .content(objectMapper.writeValueAsString(updateSyllabusDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(id));

        verify(syllabusService).updateSyllabus(updateSyllabusDto, id);
    }

    private String syllabusNotFoundErrorMessage(long id) {
        return String.format("Syllabus with id %d not found.", id);
    }
}
