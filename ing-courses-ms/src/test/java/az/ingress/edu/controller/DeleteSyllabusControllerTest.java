package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.exception.SyllabusWithChildException;
import az.ingress.edu.service.SyllabusService;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(SyllabusController.class)
@ActiveProfiles("test")
public class DeleteSyllabusControllerTest {

    private static final String SYLLABUS_ID_MUST_BE_POSITIVE = "Syllabus id must be positive";
    private static final String SYLLABUS_WITH_CHILD = "Syllabus with child can't be deleted";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DELETE_SYLLABUS_SESSION_PATH = "/syllabus/{id}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SyllabusService syllabusService;

    @Test
    public void givenNegativeSyllabusIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(delete(DELETE_SYLLABUS_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingSyllabusIdExpectErrorMessage() throws Exception {
        long id = 1L;
        doThrow(new SyllabusNotFoundException(id)).when(syllabusService).deleteSyllabus(anyLong());

        mockMvc.perform(delete(DELETE_SYLLABUS_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))));

        verify(syllabusService).deleteSyllabus(id);
    }

    @Test
    public void givenSyllabusWithChildExpectErrorMessage() throws Exception {
        long id = 1L;
        doThrow(new SyllabusWithChildException()).when(syllabusService).deleteSyllabus(anyLong());

        mockMvc.perform(delete(DELETE_SYLLABUS_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SYLLABUS_WITH_CHILD)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SYLLABUS_WITH_CHILD)));

        verify(syllabusService).deleteSyllabus(id);
    }

    @Test
    public void givenExistingSyllabusIdExpectOk() throws Exception {
        long id = 1L;

        mockMvc.perform(delete(DELETE_SYLLABUS_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

        verify(syllabusService).deleteSyllabus(id);
    }

    private String syllabusNotFoundErrorMessage(long id) {
        return String.format("Syllabus with id %d not found.", id);
    }
}
