package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateReviewDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.exception.TrainingCenterNotFound;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.service.ReviewService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
@ActiveProfiles("test")
public class CreateReviewControllerTest {

    private static final String COMMENT_CANT_BE_EMPTY = "Comment can't be empty";
    private static final String RATING_RANGE_MESSAGE = "Rating must be between 0 and 5";
    private static final String ITEM_TYPE_CANT_BE_EMPTY = "Item type can't be empty";
    private static final String ITEM_ID_MUST_BE_POSITIVE = "Item id must be positive";
    private static final String ITEM_ID_CANT_BE_EMPTY = "Item id must be positive";
    private static final String MAX_CHAR_ALLOWED_EXCEEDED = "Max 1000 characters allowed";
    private static final String T_CENTER_NOT_FOUND = "Training Center not found";
    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String COMMENT = "What a wonderful course!";
    private static final String CREATE_REVIEW_PATH = "/review";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    private CreateReviewDto createReviewDto;

    @Before
    public void setUp() {
        createReviewDto = CreateReviewDto.builder()
                .itemId(1L)
                .itemType(ItemType.COURSE)
                .rating(5)
                .comment(COMMENT)
                .build();
    }

    @Test
    public void givenReviewWithNegativeItemIdExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(-1L, 0, ItemType.COURSE, COMMENT);

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ITEM_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ITEM_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenReviewWithTooLongCommentExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,0,ItemType.COURSE, getTooLongComment());

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(MAX_CHAR_ALLOWED_EXCEEDED)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(MAX_CHAR_ALLOWED_EXCEEDED)));
    }

    @Test
    public void givenReviewWithNullItemIdExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(-1L, 0, ItemType.COURSE, COMMENT);

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ITEM_ID_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ITEM_ID_CANT_BE_EMPTY)));
    }

    @Test
    public void givenReviewWithInvalidRatingExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L, 100, ItemType.COURSE, COMMENT);

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(RATING_RANGE_MESSAGE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(RATING_RANGE_MESSAGE)));
    }

    @Test
    public void givenReviewWithEmptyTypeExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L, 5, null, COMMENT);

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(ITEM_TYPE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(ITEM_TYPE_CANT_BE_EMPTY)));
    }

    @Test
    public void givenReviewWithEmptyCommentExpectErrorMessage() throws Exception {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L, 5, ItemType.COURSE, "");

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(COMMENT_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(COMMENT_CANT_BE_EMPTY)));
    }

    @Test
    public void givenReviewWithNonExistingItemIdForCourseExpectErrorMessage() throws Exception {
        when(reviewService.createReview(any(CreateReviewDto.class)))
                .thenThrow(new CourseNotFoundException(createReviewDto.getItemId()));

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(courseNotFoundErrorMessage(createReviewDto.getItemId()))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(courseNotFoundErrorMessage(createReviewDto.getItemId()))));

        verify(reviewService).createReview(createReviewDto);
    }

    @Test
    public void givenReviewWithNonExistingItemIdForTrainingCenterExpectErrorMessage() throws Exception {
        CreateReviewDto trainingCenterReview = CreateReviewDto.builder()
                .itemId(1L)
                .itemType(ItemType.TRAINING_CENTER)
                .rating(5)
                .comment(COMMENT)
                .build();

        when(reviewService.createReview(any(CreateReviewDto.class)))
                .thenThrow(new TrainingCenterNotFound());

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(trainingCenterReview))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(T_CENTER_NOT_FOUND)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(T_CENTER_NOT_FOUND)));

        verify(reviewService).createReview(trainingCenterReview);
    }

    @Test
    public void givenReviewWithNonExistingItemIdForCourseSessionCenterExpectErrorMessage() throws Exception {
        CreateReviewDto sessionReview = CreateReviewDto.builder()
                .itemId(1L)
                .itemType(ItemType.COURSE_SESSION)
                .rating(5)
                .comment(COMMENT)
                .build();

        when(reviewService.createReview(any(CreateReviewDto.class)))
                .thenThrow(new CourseSessionNotFoundException(sessionReview.getItemId()));

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(sessionReview))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(courseSessionNotFoundErrorMessage(sessionReview.getItemId()))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(courseSessionNotFoundErrorMessage(sessionReview.getItemId()))));

        verify(reviewService).createReview(sessionReview);
    }

    @Test
    public void givenValidReviewExpectOk() throws Exception {
        when(reviewService.createReview(any(CreateReviewDto.class)))
                .thenReturn(new IdDto(1L));

        mockMvc.perform(post(CREATE_REVIEW_PATH)
                .content(objectMapper.writeValueAsString(createReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID, is(1)));

        verify(reviewService).createReview(createReviewDto);
    }

    private String courseNotFoundErrorMessage(long id) {
        return String.format("Course with id %d not found.", id);
    }

    private String courseSessionNotFoundErrorMessage(long id) {
        return String.format("Course session with id '%d' not found.", id);
    }

    public static String getTooLongComment() {
        StringBuilder builder = new StringBuilder(COMMENT);
        for (int i = 0; i < 1000; i++) {
            builder.append(COMMENT);
        }
        return builder.toString();
    }
}
