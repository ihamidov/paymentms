package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CreateSyllabusDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.service.SyllabusService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(SyllabusController.class)
@ActiveProfiles("test")
public class CreateSyllabusControllerTest {

    private static final String SYLLABUS = "/syllabus";
    private static final String ID = "$.id";
    private static final String CODE = "$.code";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String DUMMY = "dummy";
    private static final String COURSE_ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String PARENT_ID_MUST_BE_POSITIVE = "Parent id must be positive";
    private static final String TITLE_CANT_BE_EMPTY = "Title can not be empty";
    private static final String DESCRIPTION_CANT_BE_EMPTY = "Description can not be empty";
    private static final String DURATION_CANT_BE_EMPTY = "Duration can't be empty";
    private static final String COURSE_CANT_BE_EMPTY = "Course id can not be empty";

    @MockBean
    SyllabusService syllabusService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenSyllabusWithNegativeCourseIdExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(-1L, 1L, DUMMY, DUMMY, LocalTime.now());

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COURSE_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(COURSE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenSyllabusWithNullDurationExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, 1L, DUMMY, DUMMY, null);

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(DURATION_CANT_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(DURATION_CANT_BE_EMPTY));
    }

    @Test
    public void givenSyllabusWithNullCourseIdExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(null, 1L, DUMMY, DUMMY,LocalTime.now());

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COURSE_CANT_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(COURSE_CANT_BE_EMPTY));
    }

    @Test
    public void givenSyllabusWithInvalidParentIdExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, -1L, DUMMY, DUMMY,LocalTime.now());

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(PARENT_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(PARENT_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenSyllabusWithEmptyTitleExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, 1L, "", DUMMY,LocalTime.now());

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(TITLE_CANT_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(TITLE_CANT_BE_EMPTY));
    }

    @Test
    public void givenSyllabusWithEmptyDescriptionExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, 1L, DUMMY, "",LocalTime.now());

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(DESCRIPTION_CANT_BE_EMPTY))
                .andExpect(jsonPath(USER_MESSAGE).value(DESCRIPTION_CANT_BE_EMPTY));
    }

    @Test
    public void givenSyllabusWithNonExistingCourseExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(courseId, 1L, DUMMY, DUMMY,LocalTime.now());
        when(syllabusService.createSyllabus(any(Syllabus.class)))
                .thenThrow(new CourseNotFoundException(courseId));

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(courseNotFoundMessage(courseId)))
                .andExpect(jsonPath(USER_MESSAGE).value(courseNotFoundMessage(courseId)));
    }

    @Test
    public void givenSyllabusWithNonExistingParentExpectErrorMessage() throws Exception {
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, 1L, DUMMY, DUMMY,LocalTime.now());
        when(syllabusService.createSyllabus(any(Syllabus.class)))
                .thenThrow(new SyllabusNotFoundException(syllabusDto.getParentId()));

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE)
                        .value(syllabusNotFoundMessage(syllabusDto.getParentId())))
                .andExpect(jsonPath(USER_MESSAGE)
                        .value(syllabusNotFoundMessage(syllabusDto.getParentId())));
    }

    @Test
    public void givenValidSyllabusCreateExpectOk() throws Exception {
        LocalTime duration = LocalTime.now();
        CreateSyllabusDto syllabusDto = new CreateSyllabusDto(1L, 1L, DUMMY, DUMMY,duration);
        Syllabus createdSyllabus = new Syllabus(1L, 3L, 1L, DUMMY, DUMMY, 0,duration, null);
        when(syllabusService.createSyllabus(any(Syllabus.class)))
                .thenReturn(createdSyllabus);

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYLLABUS)
                .content(objectMapper.writeValueAsString(syllabusDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(createdSyllabus.getId()));

    }

    private String syllabusNotFoundMessage(long id) {
        return String.format("Syllabus with id %d not found.", id);
    }

    private String courseNotFoundMessage(long id) {
        return String.format("Course with id %d not found.", id);
    }
}
