package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateReviewDto;
import az.ingress.edu.exception.ReviewNotFoundException;
import az.ingress.edu.service.ReviewService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
@ActiveProfiles("test")
public class UpdateReviewControllerTest {

    private static final String COMMENT_CANT_BE_EMPTY = "Comment can't be empty";
    private static final String RATING_RANGE_MESSAGE = "Rating must be between 0 and 5";
    private static final String REVIEW_ID_MUST_BE_POSITIVE = "Review id must be positive";
    private static final String MAX_CHAR_ALLOWED_EXCEEDED = "Max 1000 characters allowed";
    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String COMMENT = "What a wonderful course!";
    private static final String UPDATE_REVIEW_PATH = "/review/{id}";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @Test
    public void givenReviewWithNegativeIdExpectErrorMessage() throws Exception {
        long id = -1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(1, COMMENT);

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(REVIEW_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(REVIEW_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenReviewWitTooLongCommentExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(1,getTooLongComment());

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(MAX_CHAR_ALLOWED_EXCEEDED)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(MAX_CHAR_ALLOWED_EXCEEDED)));
    }

    @Test
    public void givenReviewWithInvalidRatingExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(100, COMMENT);

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(RATING_RANGE_MESSAGE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(RATING_RANGE_MESSAGE)));
    }

    @Test
    public void givenReviewWithEmptyCommentExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(0, "");

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(COMMENT_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(COMMENT_CANT_BE_EMPTY)));
    }

    @Test
    public void givenReviewWithNonExistingExpectErrorMessage() throws Exception {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(1, COMMENT);
        when(reviewService.updateReview(any(UpdateReviewDto.class), anyLong()))
                .thenThrow(new ReviewNotFoundException(id));

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(reviewNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(reviewNotFoundErrorMessage(id))));

        verify(reviewService).updateReview(updateReviewDto, id);
    }

    @Test
    public void givenValidReviewExpectOk() throws Exception {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(1, COMMENT);
        when(reviewService.updateReview(any(UpdateReviewDto.class), anyLong())).thenReturn(new IdDto(1L));

        mockMvc.perform(put(UPDATE_REVIEW_PATH, id)
                .content(objectMapper.writeValueAsString(updateReviewDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID, is(1)));

        verify(reviewService).updateReview(updateReviewDto, id);
    }

    private String reviewNotFoundErrorMessage(long id) {
        return String.format("Review with id: '%d' not found", id);
    }

    public static String getTooLongComment() {
        StringBuilder builder = new StringBuilder(COMMENT);
        for (int i = 0; i < 1000; i++) {
            builder.append(COMMENT);
        }
        return builder.toString();
    }
}
