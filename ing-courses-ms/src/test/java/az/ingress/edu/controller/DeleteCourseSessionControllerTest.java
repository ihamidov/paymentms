package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.service.CourseSessionService;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseSessionController.class)
@ActiveProfiles("test")
public class DeleteCourseSessionControllerTest {

    private static final String SESSION_ID_MUST_BE_POSITIVE = "Course session id must be positive";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DELETE_COURSE_SESSION_PATH = "/course/session/{id}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseSessionService courseSessionService;

    @Test
    public void givenNegativeSessionIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(delete(DELETE_COURSE_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingSessionIdExpectErrorMessage() throws Exception {
        long id = 1L;
        doThrow(new CourseSessionNotFoundException(id)).when(courseSessionService).deleteCourseSession(anyLong());

        mockMvc.perform(delete(DELETE_COURSE_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(courseSessionNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(courseSessionNotFoundErrorMessage(id))));

        verify(courseSessionService).deleteCourseSession(id);
    }

    @Test
    public void givenExistingSessionIdExpectOk() throws Exception {
        long id = 1L;

        mockMvc.perform(delete(DELETE_COURSE_SESSION_PATH,id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());

        verify(courseSessionService).deleteCourseSession(id);
    }

    private String courseSessionNotFoundErrorMessage(long id) {
        return String.format("Course session with id '%d' not found.", id);
    }
}
