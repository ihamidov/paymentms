package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CategoryDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CategoryAlreadyExistsException;
import az.ingress.edu.exception.CategoryInUseException;
import az.ingress.edu.exception.CategoryNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.service.CategoryServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryRestController.class)
@ActiveProfiles("test")
public class CategoryRestControllerTest {

    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHINICAL_MESSAGE = "technicalMessage";

    private static final String ID = "id";
    private static final String NAME = "Microservice";
    private static final String NAME_CAN_NOT_BE_EMPTY = "Category with id 1 not found.";
    private static final String CATEGORY_CANT_BE_NULL = "Category name can not be null";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String CATEGORY_PRESENT_EXCEPTION = "Category with id 1 not found.";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final String PAGE_SORT_NUMBER_STRING = "1";
    private static final int PAGE_SORT_SIZE = 5;
    private static final String PAGE_SORT_SIZE_STRING = "5";
    private static final int TOTAL_ELEMENTS = 88;
    private static final String PAGE_URL = "/category/list";
    private static final String BASE_URL = "/category/";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String PAGE_SIZE = "pageSize";
    private static final String PAGE_SORT = "sort";
    private static final String PAGE_DIRECTION = "direction";


    @MockBean
    private CategoryServiceImpl categoryService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private Category category;

    private CategoryDto categoryDto;

    private PageDto pageDto;

    private List<Category> categoryList;

    @Before
    public void setUp() {
        categoryDto = CategoryDto.builder()
                .name(NAME)
                .build();

        category = Category.builder()
                .id(1L)
                .name(NAME)
                .build();

        pageDto = PageDto.builder()
                .pageNumber(PAGE_SORT_NUMBER)
                .pageSize(PAGE_SORT_SIZE)
                .sortColumn(ID)
                .sortDirection(Sort.Direction.ASC.name())
                .build();

        categoryList = new ArrayList<>();
        for (int i = 1; i <= TOTAL_ELEMENTS; i++) {
            categoryList.add(category);
        }

    }

    @Test
    public void findCategoryById() throws Exception {

        when(categoryService.findById(anyLong())).thenReturn(category);

        mvc.perform(get("/category/{id}", 1)
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findByIdThenThrowCategoryNotFound() throws Exception {

        when(categoryService.findById(anyLong())).thenThrow(new CategoryNotFoundException(1));

        mvc.perform(get("/category/1216")
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, is(NAME_CAN_NOT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, is(NAME_CAN_NOT_BE_EMPTY)));
    }

    @Test
    public void givenCategoryInUseDeleteCategoryExpectException() throws Exception {

        doThrow(new CategoryInUseException(category.getName())).when(categoryService).deleteById(anyLong());

        mvc.perform(delete("/category/1216")
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, is(categoryInUseErrorMessage(category.getName()))))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, is(categoryInUseErrorMessage(category.getName()))));
    }

    @Test
    public void deleteCategory() throws Exception {

        mvc.perform(delete("/category/{id}", 1))
                .andExpect(status().isOk());
    }

    @Test
    public void addCategory() throws Exception {

        when(categoryService.add(categoryDto)).thenReturn(category);

        mvc.perform(post(BASE_URL)
                .content(asJsonString(categoryDto))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(category.getId()));
    }

    @Test
    public void updateCategory() throws Exception {

        when(categoryService.findById(anyLong())).thenReturn(category);
        when(categoryService.update(categoryDto, 1L)).thenReturn(category);

        mvc.perform(put("/category/{id}", 1L)
                .content(asJsonString(categoryDto))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    public void whenNullValueReturnBadRequest() throws Exception {

        when(categoryService.add(categoryDto)).thenReturn(category);

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(null);
        mvc.perform(post(BASE_URL)
                .content(asJsonString(categoryDto))
                .contentType(APPLICATION_JSON_UTF8_VALUE)
                .accept(APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath(ERROR_CODE, is(BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, is(CATEGORY_CANT_BE_NULL)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, is(CATEGORY_CANT_BE_NULL)));
    }


    @Test
    public void checkCategoryCategoryIsNotPresentException() throws Exception {
        when(categoryService.add(categoryDto)).thenThrow(new CategoryNotFoundException(1));
        mvc.perform(post(BASE_URL)
                .content(asJsonString(categoryDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(CATEGORY_PRESENT_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(CATEGORY_PRESENT_EXCEPTION)));
    }

    @Test
    public void retrieveAllCategoryPage() throws Exception {
        Page<Category> expectedCategories = new PageImpl<>(categoryList);
        when(categoryService.list(pageDto))
                .thenReturn(expectedCategories);

        mvc.perform(get(PAGE_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_DIRECTION, Sort.Direction.ASC.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfElements", is(TOTAL_ELEMENTS)))
                .andExpect(jsonPath("$.totalElements", is(TOTAL_ELEMENTS)))
                .andExpect(jsonPath("$.totalPages", is(1)));
    }

    @Test
    public void retrieveAllCategoryPageableDefault() throws Exception {
        mvc.perform(get(PAGE_URL))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveAllCategoryCheckPageSizeBadRequest() throws Exception {
        mvc.perform(get(PAGE_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "0")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void retrieveAllCategoryCheckPageNumberBadRequest() throws Exception {
        mvc.perform(get(PAGE_URL)
                .param(PAGE_NUMBER, "-1")
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_INDEX_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(PAGE_INDEX_MUST_BE_POSITIVE)));
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        when(categoryService.list(any(PageDto.class)))
                .thenThrow(new IllegalArgumentException());
        mvc.perform(get(PAGE_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_SORT, ID)
                .param(PAGE_DIRECTION, "ASCS"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())));
    }

    @Test
    public void categoryAlreadyExistsException() throws Exception {
        Category categoryForSave = Category.builder().id(1L).name(NAME).build();

        when(categoryService.add(any())).thenThrow(new CategoryAlreadyExistsException(categoryForSave.getName()));

        mvc.perform(MockMvcRequestBuilders
                .post(BASE_URL)
                .content(objectMapper.writeValueAsString(categoryForSave))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE)
                        .value(categoryAlreadyExistsMessage(categoryForSave.getName())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE)
                        .value(categoryAlreadyExistsMessage(categoryForSave.getName())));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    private String categoryAlreadyExistsMessage(String username) {
        return String.format("Category with name: '%s' already exists", username);
    }

    private String categoryInUseErrorMessage(String name) {
        return String.format("Category with name: '%s'is in use ", name);
    }
}

