package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CourseSessionDto;
import az.ingress.edu.dto.UsernameDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.MinimumSessionDurationException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.CourseSessionStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CourseSessionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseSessionController.class)
@ActiveProfiles("test")
public class CreateCourseSessionControllerTest {

    private static final Instant START_DATE = Instant.now();
    private static final Instant END_DATE = Instant.now();
    private static final String COURSE_SCHEDULE = "test";
    private static final String CATEGORY_NAME = "Java";
    private static final String COURSE_NAME = "Java Course";
    private static final String TRAINER_NAME = "jhon";
    private static final String STATUS_CANT_BE_EMPTY = "Status can not be empty";
    private static final String TRAINERS_MUSTNT_BE_EMPTY = "At least one trainer must be included";
    private static final String LESSONS_CANT_BE_EMPTY = "Number of lessons can not be empty";
    private static final String LESSENS_MUST_BE_POSITIVE = "Number of lessons must be positive";
    private static final String STD_COUNT_MUST_BE_POSITIVE = "Required student count must be positive";
    private static final String SCHEDULE_CANT_BE_EMPTY = "Course schedule can't be empty";
    private static final String COURSE_ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String MIN_SESSION_DURATION = "Minimum allowed session duration is 1 day";
    private static final String TEACHER_DESC = "Wonderful course wonderful teacher";
    private static final String PRICE_CANT_BE_EMPTY = "Price can't be empty";
    private static final String INVALID_PRICE = "Invalid price";
    private static final String ID = "$.id";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DESCRIPTION = "Java master class";
    private static final String CREATE_COURSE_SESSION_PATH = "/course/{id}/session";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseSessionService courseSessionService;

    private CourseSessionDto courseSessionDto;

    private Category category;

    private List<User> trainerList;

    @Before
    public void setUp() {
        category = Category.builder().id(1L).name(CATEGORY_NAME).build();
        trainerList = Collections.singletonList(User.builder().username(TRAINER_NAME).build());

        courseSessionDto = CourseSessionDto.builder()
            .status(CourseSessionStatus.COMPLETED)
            .trainers(Collections.singletonList(new UsernameDto(TRAINER_NAME)))
            .teacherDescription(TEACHER_DESC)
            .price(1L)
            .numberOfLessons(5)
            .startDate(START_DATE)
            .endDate(END_DATE)
            .requiredStudentCnt(5)
            .courseSchedule(COURSE_SCHEDULE)
            .build();
    }

    @Test
    public void givenCourseSessionWithNegativePriceIdExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 10, START_DATE,
                END_DATE, 5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),-1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
                .content(objectMapper.writeValueAsString(courseSessionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(INVALID_PRICE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(INVALID_PRICE)));
    }

    @Test
    public void givenCourseSessionWithEmptyPriceIdExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 10, START_DATE,
                END_DATE, 5,COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),null,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
                .content(objectMapper.writeValueAsString(courseSessionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PRICE_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PRICE_CANT_BE_EMPTY)));
    }

    @Test
    public void givenCourseSessionWithNegativeCourseIdExpectErrorMessage() throws Exception {
        long courseId = -1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 10, START_DATE,
                END_DATE, 5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(COURSE_ID_MUST_BE_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(COURSE_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenCourseSessionWithEmptyStatusExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(null, 1, START_DATE,
                END_DATE, 5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(STATUS_CANT_BE_EMPTY)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(STATUS_CANT_BE_EMPTY)));
    }

    @Test
    public void givenCourseSessionWithInvalidTrainerIdExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 10,
                START_DATE, END_DATE, 5, COURSE_SCHEDULE,null,1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TRAINERS_MUSTNT_BE_EMPTY)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(TRAINERS_MUSTNT_BE_EMPTY)));
    }

    @Test
    public void givenCourseSessionWithNullAsNumberOfLessonsExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, null, START_DATE,
                END_DATE, 5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LESSONS_CANT_BE_EMPTY)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LESSONS_CANT_BE_EMPTY)));
    }

    @Test
    public void givenCourseSessionWithNegativeNumberOfLessonsExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, -1, START_DATE,
                END_DATE, 5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(LESSENS_MUST_BE_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(LESSENS_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenCourseSessionWithNegativeRequiredStudentCountExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, START_DATE,
                END_DATE, -5, COURSE_SCHEDULE,Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(STD_COUNT_MUST_BE_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(STD_COUNT_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenCourseSessionWithEmptyCourseScheduleExpectErrorMessage() throws Exception {
        long courseId = 1L;
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, START_DATE,
                END_DATE, 5, "",Collections.singletonList(new UsernameDto(TRAINER_NAME)),1L,TEACHER_DESC);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SCHEDULE_CANT_BE_EMPTY)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SCHEDULE_CANT_BE_EMPTY)));
    }

    @Test
    public void givenCourseSessionWithNonExistingCourseExpectErrorMessage() throws Exception {
        long courseId = 1L;
        when(courseSessionService.createCourseSession(any(CourseSessionDto.class),anyLong()))
            .thenThrow(new CourseNotFoundException(courseId));

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(courseNotFoundErrorMessage(courseId))))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(courseNotFoundErrorMessage(courseId))));

        verify(courseSessionService).createCourseSession(courseSessionDto,courseId);
    }

    @Test
    public void givenCourseSessionWithNonExistingUserIdExpectErrorMessage() throws Exception {
        long courseId = 1L;
        when(courseSessionService.createCourseSession(any(CourseSessionDto.class),anyLong()))
            .thenThrow(new UserNotFoundException(TRAINER_NAME));

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
                .content(objectMapper.writeValueAsString(courseSessionDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers
                .is(userNotFoundErrorMessage(TRAINER_NAME))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers
                .is(userNotFoundErrorMessage(TRAINER_NAME))));

        verify(courseSessionService).createCourseSession(courseSessionDto,courseId);
    }

    @Test
    public void givenCourseSessionNotExceedingMinSessionDurationExpectErrorMessage() throws Exception {
        long courseId = 1L;
        when(courseSessionService.createCourseSession(any(CourseSessionDto.class),anyLong()))
            .thenThrow(new MinimumSessionDurationException());

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(MIN_SESSION_DURATION)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(MIN_SESSION_DURATION)));

        verify(courseSessionService).createCourseSession(courseSessionDto,courseId);
    }

    @Test
    public void givenCourseSessionExpectOk() throws Exception {
        long courseId = 1L;
        CourseSession createdCourseSession = new CourseSession(1L,COURSE_NAME,courseId,CourseSessionStatus.COMPLETED,
                trainerList, 5, START_DATE, END_DATE, 0, 5,
                COURSE_SCHEDULE,DESCRIPTION,TEACHER_DESC,PHOTO,1L,category,0.0,null,false);
        when(courseSessionService.createCourseSession(any(CourseSessionDto.class),anyLong()))
            .thenReturn(createdCourseSession);

        mockMvc.perform(post(CREATE_COURSE_SESSION_PATH, courseId)
            .content(objectMapper.writeValueAsString(courseSessionDto))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath(ID, is(1)));

        verify(courseSessionService).createCourseSession(courseSessionDto,courseId);
    }

    private String courseNotFoundErrorMessage(long id) {
        return String.format("Course with id %d not found.", id);
    }

    private String userNotFoundErrorMessage(String username) {
        return String.format("User with username %s not found.",username);
    }
}
