package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.CourseSessionStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CourseSessionService;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseSessionController.class)
@ActiveProfiles("test")
public class GetCourseSessionControllerTest {

    private static final String SESSION_ID_MUST_BE_POSITIVE = "Course session id must be positive";
    private static final String ID = "$.id";
    private static final String TRAINER_NAME = "jhon";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_COURSE_SESSION_PATH = "/course/session/{id}";
    private static final String TEACHER_DESC = "Wonderful course wonderful teacher";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseSessionService courseSessionService;

    private List<User> trainerList;

    @Before
    public void setUp() {
        trainerList = Collections.singletonList(User.builder().username(TRAINER_NAME).build());
    }

    @Test
    public void givenNegativeSessionIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(get(GET_COURSE_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingSessionIdExpectErrorMessage() throws Exception {
        long id = 1L;
        when(courseSessionService.getCourseSession(anyLong())).thenThrow(new CourseSessionNotFoundException(id));

        mockMvc.perform(get(GET_COURSE_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(courseSessionNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(courseSessionNotFoundErrorMessage(id))));

        verify(courseSessionService).getCourseSession(id);
    }

    @Test
    public void givenExistingSessionIdExpectOk() throws Exception {
        long id = 1L;
        Category category = new Category(1L,"Java",false);
        CourseSession courseSession = new CourseSession(id,"Java course", 1L, CourseSessionStatus.COMPLETED,
                trainerList,1, Instant.now(), Instant.now(), 0, 5, "Schedule",
                "Short description",TEACHER_DESC,"www.dummy.com",1L,category,0.0,null,false);

        when(courseSessionService.getCourseSession(anyLong())).thenReturn(courseSession);

        mockMvc.perform(get(GET_COURSE_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(courseSession.getId()));

        verify(courseSessionService).getCourseSession(id);
    }

    private String courseSessionNotFoundErrorMessage(long id) {
        return String.format("Course session with id '%d' not found.", id);
    }
}
