package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.service.ReviewService;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
@ActiveProfiles("test")
public class GetReviewListForCourseSessionControllerTest {

    private static final String SESSION_ID_MUST_BE_POSITIVE = "Course session id must be positive";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_REVIEW_FOR_SESSION_PATH = "/review/course-session/{id}/list";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String PAGE = "page";
    private static final String SIZE = "size";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @Test
    public void givenNegativeSessionIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(get(GET_REVIEW_FOR_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SESSION_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNegativePageNumberExpectErrorMessage() throws Exception {
        long courseId = 1L;

        mockMvc.perform(get(GET_REVIEW_FOR_SESSION_PATH, courseId)
                .param(PAGE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)));
    }

    @Test
    public void givenNegativePageSizeNumberExpectErrorMessage() throws Exception {
        long courseId = 1L;

        mockMvc.perform(get(GET_REVIEW_FOR_SESSION_PATH, courseId)
                .param(SIZE, "-1")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }


    @Test
    public void givenNonExistingSessionIdExpectErrorMessage() throws Exception {
        long id = 1L;
        int page = 0;
        int size = 10;
        when(reviewService.getReviewList(any(ItemType.class), anyLong(), anyInt(), anyInt()))
                .thenThrow(new CourseSessionNotFoundException(id));

        mockMvc.perform(get(GET_REVIEW_FOR_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE,
                        CoreMatchers.is(courseSessionNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE,
                        CoreMatchers.is(courseSessionNotFoundErrorMessage(id))));

        verify(reviewService).getReviewList(ItemType.COURSE_SESSION, id, page, size);
    }

    @Test
    public void givenExistingSessionIdExpectOk() throws Exception {
        long id = 1L;
        List<Review> reviewList = Collections.singletonList(Review.builder()
                .id(1L)
                .itemId(id)
                .itemType(ItemType.COURSE_SESSION)
                .comment("Wanderfull!")
                .createdAt(Instant.now())
                .build());
        Page<Review> reviewPage = new PageImpl<>(reviewList);

        when(reviewService.getReviewList(any(ItemType.class), anyLong(), anyInt(), anyInt())).thenReturn(reviewPage);

        mockMvc.perform(get(GET_REVIEW_FOR_SESSION_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(reviewPage.getTotalPages()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfElements").value(reviewPage.getTotalElements()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());

        verify(reviewService).getReviewList(ItemType.COURSE_SESSION, id, 0, 10);
    }

    private String courseSessionNotFoundErrorMessage(long id) {
        return String.format("Course session with id '%d' not found.", id);
    }
}
