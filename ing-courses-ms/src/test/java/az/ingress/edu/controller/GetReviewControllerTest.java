package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.ReviewNotFoundException;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.service.ReviewService;
import java.time.Instant;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
@ActiveProfiles("test")
public class GetReviewControllerTest {

    private static final String ID = "$.id";
    private static final String REVIEW_ID_MUST_BE_POSITIVE = "Review id must be positive";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_REVIEW_PATH = "/review/{id}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    @Test
    public void givenNegativeReviewIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(get(GET_REVIEW_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(REVIEW_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(REVIEW_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingReviewIdExpectErrorMessage() throws Exception {
        long id = 1L;
        when(reviewService.getReview(anyLong())).thenThrow(new ReviewNotFoundException(id));

        mockMvc.perform(get(GET_REVIEW_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(reviewNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(reviewNotFoundErrorMessage(id))));

        verify(reviewService).getReview(id);
    }

    @Test
    public void givenExistingReviewIdExpectOk() throws Exception {
        long id = 1L;
        Review review = Review.builder()
                .id(1L)
                .itemId(1L)
                .itemType(ItemType.COURSE)
                .comment("Wanderfull!")
                .createdAt(Instant.now())
                .build();

        when(reviewService.getReview(anyLong())).thenReturn(review);

        mockMvc.perform(get(GET_REVIEW_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(review.getId()));

        verify(reviewService).getReview(id);
    }

    private String reviewNotFoundErrorMessage(long id) {
        return String.format("Review with id: '%d' not found", id);
    }
}
