package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.service.SyllabusService;
import java.time.LocalTime;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(SyllabusController.class)
@ActiveProfiles("test")
public class GetSyllabusControllerTest {

    private static final String ID = "$.id";
    private static final String SYLLABUS_ID_MUST_BE_POSITIVE = "Syllabus id must be positive";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String GET_SYLLABUS_PATH = "/syllabus/{id}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SyllabusService syllabusService;

    @Test
    public void givenNegativeSyllabusIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(get(GET_SYLLABUS_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(SYLLABUS_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingSyllabusIdExpectErrorMessage() throws Exception {
        long id = 1L;
        when(syllabusService.getSyllabus(anyLong())).thenThrow(new SyllabusNotFoundException(id));

        mockMvc.perform(get(GET_SYLLABUS_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(syllabusNotFoundErrorMessage(id))));

        verify(syllabusService).getSyllabus(id);
    }

    @Test
    public void givenExistingSyllabusIdExpectOk() throws Exception {
        long id = 1L;
        Syllabus syllabus = Syllabus.builder()
                .id(id)
                .courseId(1L)
                .title("Casting types")
                .description("How java handles casting operation")
                .duration(LocalTime.now())
                .parentId(1L)
                .build();

        when(syllabusService.getSyllabus(anyLong())).thenReturn(syllabus);

        mockMvc.perform(get(GET_SYLLABUS_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(syllabus.getId()));

        verify(syllabusService).getSyllabus(id);
    }

    private String syllabusNotFoundErrorMessage(long id) {
        return String.format("Syllabus with id %d not found.", id);
    }
}
