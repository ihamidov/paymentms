package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.CourseDto;
import az.ingress.edu.dto.ListCourseDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CategoryNotFoundException;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.service.CourseServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseRestController.class)
@ActiveProfiles("test")
public class CourseRestControllerTest {

    private static final String ID = "id";
    private static final String COURSE_NAME = "Ingress";
    private static final String COURSE_DESCRIPTION = "Microservice test";
    private static final String COURSE_REQUIREMENTS = "OCA";
    private static final String COURSE_STATUS = "Active";
    private static final String PAGE_URL = "/course/list";
    private static final String PAGE_NUMBER = "pageNumber";
    private static final String PAGE_SIZE = "pageSize";
    private static final String PAGE_SORT = "sort";
    private static final String PAGE_DIRECTION = "direction";
    private static final String NAME_CAN_NOT_BE_EMPITY = "Name should have at least 2 characters";
    private static final String COURSE_NOT_FOUND_EXCEPTION = "Course with id 1 not found.";
    private static final String CATEGORY_PRESENT_EXCEPTION = "Category with id 1 not found.";
    private static final String TAG_NOT_FOUND_EXCEPTION = "Tag with id 1 not found.";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_INDEX_MUST_BE_POSITIVE = "Page index must be positive";
    private static final String TRAINING_CENTER_CANT_BE_EMPTY = "Training center id can't be empty";

    private static final String API_UPDATE = "/course/{id}";
    private static final String API_CREATE = "/course/";


    private static final int PAGE_SORT_NUMBER = 1;
    private static final String PAGE_SORT_NUMBER_STRING = "1";
    private static final int PAGE_SORT_SIZE = 5;
    private static final String PAGE_SORT_SIZE_STRING = "5";
    private static final int TOTAL_ELEMENTS = 88;

    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHINICAL_MESSAGE = "technicalMessage";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CourseServiceImpl courseService;

    private Course course;

    private CourseDto courseDto;

    private final List<ListCourseDto> courseList = new ArrayList<>();

    private PageDto pageDto;

    @Before
    public void setUp() {
        courseDto = CourseDto.builder()
                .categoryId(1L)
                .name(COURSE_NAME)
                .description(COURSE_DESCRIPTION)
                .requirements(COURSE_REQUIREMENTS)
                .status(COURSE_STATUS)
                .photo(PHOTO)
                .trainingCenterId(1L)
                .shortDescription(COURSE_DESCRIPTION)
                .build();
        course = Course.builder()
                .id(1L)
                .category(Category.builder().id(1L).build())
                .courseDetails(CourseDetails.builder().name(COURSE_NAME).description(COURSE_DESCRIPTION)
                .requirements(COURSE_REQUIREMENTS)
                .shortDescription(COURSE_DESCRIPTION)
                .status(COURSE_STATUS).build()).build();
        pageDto = PageDto.builder()
                .pageNumber(PAGE_SORT_NUMBER)
                .pageSize(PAGE_SORT_SIZE)
                .sortColumn(ID)
                .sortDirection(Direction.ASC.name())
                .build();
        for (int i = 1; i <= TOTAL_ELEMENTS; i++) {
            courseList.add(new ListCourseDto(1L,PHOTO,
                    COURSE_NAME,COURSE_DESCRIPTION,null,0.0,null));
        }
    }

    @Test
    public void findCourseById() throws Exception {
        when(courseService.findById(anyLong())).thenReturn(course);

        mvc.perform(get(API_UPDATE, 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findCourseByIdReturnsNotFound() throws Exception {
        when(courseService.findById(anyLong())).thenThrow(new CourseNotFoundException(1));

        mvc.perform(get("/course/12345")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(COURSE_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(COURSE_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void deleteCourse() throws Exception {
        mvc.perform(delete(API_UPDATE, 1))
                .andExpect(status().isOk());
    }

    @Test
    public void createCourse() throws Exception {
        when(courseService.add(courseDto)).thenReturn(course);

        mvc.perform(post(API_CREATE)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(course.getId()));
    }

    @Test
    public void whenNullTrainingCenterIdCreateCourseExpectErrorMessage() throws Exception {
        courseDto.setTrainingCenterId(null);

        mvc.perform(post(API_CREATE)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TRAINING_CENTER_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(TRAINING_CENTER_CANT_BE_EMPTY)));
    }

    @Test
    public void whenNullCourseNameCreateCourseExpectErrorMessage() throws Exception {
        courseDto.setName("X");

        mvc.perform(post(API_CREATE)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(NAME_CAN_NOT_BE_EMPITY)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(NAME_CAN_NOT_BE_EMPITY)));
    }

    @Test
    public void whenNullTrainingCenterUpdateCourseExpectErrorMessage() throws Exception {
        courseDto.setTrainingCenterId(null);

        mvc.perform(put(API_UPDATE, 1L)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TRAINING_CENTER_CANT_BE_EMPTY)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(TRAINING_CENTER_CANT_BE_EMPTY)));
    }

    @Test
    public void checkCategoryNotFoundException() throws Exception {
        when(courseService.add(courseDto)).thenThrow(new CategoryNotFoundException(1));
        mvc.perform(post(API_CREATE)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(CATEGORY_PRESENT_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(CATEGORY_PRESENT_EXCEPTION)));
    }

    @Test
    public void checkTagNotFoundException() throws Exception {
        when(courseService.add(courseDto)).thenThrow(new TagNotFoundException(1));
        mvc.perform(post(API_CREATE)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TAG_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(TAG_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void updateCourse() throws Exception {
        when(courseService.findById(anyLong())).thenReturn(course);
        when(courseService.update(courseDto, 1)).thenReturn(course);

        mvc.perform(put(API_UPDATE, 1)
                .content(asJsonString(courseDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    public void retrieveAllCourcePage() throws Exception {
        ListDto listDto = new ListDto(courseList, TOTAL_ELEMENTS, 17);
        when(courseService.list(pageDto))
                .thenReturn(listDto);

        mvc.perform(get(PAGE_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_DIRECTION, Direction.ASC.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalElements", is(TOTAL_ELEMENTS)))
                .andExpect(jsonPath("$.totalPages", is(17)));
    }

    @Test
    public void retrieveAllCourcePageableDefault() throws Exception {
        mvc.perform(get(PAGE_URL))
                .andExpect(status().isOk());
    }

    @Test
    public void retrieveAllCourceCheckPageSizeBadRequest() throws Exception {
        mvc.perform(get(PAGE_URL)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, "0")
                .param(PAGE_SORT, ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void retrieveAllCourceCheckPageNumberBadRequest() throws Exception {
        mvc.perform(get(PAGE_URL)
                .param(PAGE_NUMBER, "-1")
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_INDEX_MUST_BE_POSITIVE)))
                .andExpect(jsonPath(ERROR_TECHINICAL_MESSAGE, CoreMatchers.is(PAGE_INDEX_MUST_BE_POSITIVE)));
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        when(courseService.list(any(PageDto.class)))
                .thenThrow(new IllegalArgumentException());
        mvc.perform(get(PAGE_URL)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param(PAGE_NUMBER, PAGE_SORT_NUMBER_STRING)
                .param(PAGE_SIZE, PAGE_SORT_SIZE_STRING)
                .param(PAGE_SORT, ID)
                .param(PAGE_DIRECTION, "ASCS"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
