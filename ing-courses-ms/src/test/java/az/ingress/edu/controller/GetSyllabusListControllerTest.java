package az.ingress.edu.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.service.SyllabusService;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(SyllabusController.class)
@ActiveProfiles("test")
public class GetSyllabusListControllerTest {

    private static final String SYLLABUS_BY_COURSE_ID = "/syllabus/course/{id}";
    private static final String CODE = "$.code";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String COURSE_ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String DUMMY = "dummy";
    private static final String SYLLABUS = "$.syllabus";
    private static final String SYLLABUS_CHILDREN = "$.syllabus[0].children";

    @MockBean
    SyllabusService syllabusService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenSyllabusWithNegativeCourseIdExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(SYLLABUS_BY_COURSE_ID, -1L)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(COURSE_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(COURSE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingCourseExpectErrorMessage() throws Exception {
        long courseId = 1L;
        when(syllabusService.getSyllabi(courseId))
                .thenThrow(new CourseNotFoundException(courseId));

        mockMvc.perform(MockMvcRequestBuilders
                .get(SYLLABUS_BY_COURSE_ID, courseId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(courseNotFoundMessage(courseId)))
                .andExpect(jsonPath(USER_MESSAGE).value(courseNotFoundMessage(courseId)));
    }

    @Test
    public void givenExistingCourseGetSyllabusListExpectOk() throws Exception {
        long courseId = 3L;
        List<Syllabus> syllabusList = Arrays
                .asList(new Syllabus(1L, courseId, null, DUMMY, DUMMY, 1,LocalTime.now(),
                        Arrays.asList(new Syllabus(1L, courseId, 1L, DUMMY, DUMMY,0,LocalTime.now(), null))));
        when(syllabusService.getSyllabi(courseId)).thenReturn(syllabusList);

        mockMvc.perform(MockMvcRequestBuilders
                .get(SYLLABUS_BY_COURSE_ID, courseId)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath(SYLLABUS).isArray())
                .andExpect(jsonPath(SYLLABUS, hasSize(1)))
                .andExpect(jsonPath(SYLLABUS_CHILDREN).isArray())
                .andExpect(jsonPath(SYLLABUS_CHILDREN, hasSize(1)));
    }

    private String courseNotFoundMessage(long id) {
        return String.format("Course with id %d not found.", id);
    }
}
