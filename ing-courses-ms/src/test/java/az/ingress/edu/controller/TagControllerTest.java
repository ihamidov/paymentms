package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.TagDto;
import az.ingress.edu.exception.TagAlreadyExistException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.model.Tag;
import az.ingress.edu.service.TagServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@RunWith(SpringRunner.class)
@WebMvcTest(TagController.class)
@ActiveProfiles("test")
public class TagControllerTest {

    private static final String TAG_NOT_FOUND_EXCEPTION = "Tag with id 1 not found.";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_TIMESTAMP = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String TAD_ID = "/tag/{id}";
    private static final int TOTAL_ELEMENTS = 21;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TagServiceImpl tagServiceImpl;

    private Tag tag;

    private TagDto tagDto;

    private List<Tag> tagList;

    @Before
    public void setUp() {
        tagDto = TagDto.builder()
                .name("java")
                .build();

        tag = Tag.builder()
                .id(1L)
                .name("java")
                .build();

        tagList = new ArrayList<>();
        for (int i = 1; i < TOTAL_ELEMENTS; i++) {
            tagList.add(tag);
        }
    }

    @Test
    public void findTagById() throws Exception {
        when(tagServiceImpl.findById(anyLong())).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders.get(TAD_ID, 1)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void findTagByIdReturnsNotFound() throws Exception {
        when(tagServiceImpl.findById(anyLong())).thenThrow(new TagNotFoundException(1));

        mockMvc.perform(get("/tag/12345")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR_TIMESTAMP, is(notNullValue())))
                .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(TAG_NOT_FOUND_EXCEPTION)))
                .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(TAG_NOT_FOUND_EXCEPTION)));
    }

    @Test
    public void createTagTest() throws Exception {

        when(tagServiceImpl.add(tagDto)).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/tag")
                .content(asJsonString(tagDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(tag.getId()));
    }

    @Test
    public void tagAlreadyExistMvc() throws Exception {

        when(tagServiceImpl.add(tagDto)).thenThrow(new TagAlreadyExistException());

        mockMvc.perform(MockMvcRequestBuilders
                .post("/tag")
                .content(asJsonString(tagDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.code", is(BAD_REQUEST.value())))
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.userMessage", is("Tag already exist")))
                .andExpect(jsonPath("$.technicalMessage", is("Tag already exist")));
    }

    @Test
    public void testFormSubmissionFailureDueToMissingName() throws Exception {
        Tag tagWithNullName = new Tag(1L, null,false);
        when(tagServiceImpl.add(tagDto)).thenReturn(tag);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/tag")
                .content(asJsonString(tagWithNullName))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeletingTagById() throws Exception {
        mockMvc.perform(delete(TAD_ID, 1))
                .andExpect(status().isOk());
    }


    @Test
    public void testUpdatingTagById() throws Exception {
        when(tagServiceImpl.findById(anyLong())).thenReturn(tag);
        when(tagServiceImpl.update(tagDto,1)).thenReturn(tag);

        mockMvc.perform(put(TAD_ID,1L)
                .content(asJsonString(tagDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    public void whenUpdatingIdNotFoundThrowException() throws Exception {
        when(tagServiceImpl.update(tagDto,-1)).thenThrow(TagNotFoundException.class);

        mockMvc.perform(put(TAD_ID,-1L)
                .content(asJsonString(tagDto))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound());
    }


    @Test
    public void testingListOfTags() throws Exception {
        Page<Tag> expectedTag = new PageImpl<>(tagList);
        when(tagServiceImpl.list(any(PageDto.class))).thenReturn(expectedTag);

        mockMvc.perform(get("/tag/list")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param("pageNumber","0")
                .param("pageSize","5")
                .param("direction", Sort.Direction.ASC.name()))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.numberOfElements",is(20)))
                .andExpect(jsonPath("$.totalElements",is(20)))
                .andExpect(jsonPath("$.content[0].id",is(1)))
                .andExpect(jsonPath("$.content[0].name",is("java")));
    }

    private String asJsonString(final Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

}
