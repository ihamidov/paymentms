package az.ingress.edu.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.CourseSessionStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.service.CourseSessionService;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(CourseSessionController.class)
@ActiveProfiles("test")
public class GetCourseSessionListControllerTest {

    private static final Instant START_DATE = Instant.now();
    private static final Instant END_DATE = Instant.now();
    private static final String COURSE_SCHEDULE = "test";
    private static final String COURSE_NAME = "Java Course";
    private static final String CATEGORY_NAME = "Java";
    private static final String PAGE_SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String PAGE_MUST_BE_ZERO_OR_POSITIVE = "Page number must be zero or positive number";
    private static final String COURSE_ID_MUST_BE_POSITIVE = "Course id must be positive";
    private static final String ORDER_BY = "id";
    private static final String TRAINER_NAME = "Jhon";
    private static final String DIRECTION = "asc";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String ERROR_CODE = "$.code";
    private static final String ERROR_USER_MESSAGE = "userMessage";
    private static final String ERROR_TECHNICAL_MESSAGE = "technicalMessage";
    private static final String DESCRIPTION = "Java master class";
    private static final String COURSE_SESSION_PATH = "/course/{id}/session/list";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseSessionService courseSessionService;

    private List<User> trainerList;

    @Before
    public void setUp() {
        trainerList = Collections.singletonList(User.builder().username(TRAINER_NAME).build());
    }

    @Test
    public void givenNegativeCourseIdExpectErrorMessage() throws Exception {
        long courseId = -1L;

        mockMvc.perform(get(COURSE_SESSION_PATH, courseId)
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(COURSE_ID_MUST_BE_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(COURSE_ID_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNegativePageNumberExpectErrorMessage() throws Exception {
        long courseId = 1L;

        mockMvc.perform(get(COURSE_SESSION_PATH, courseId)
            .param(PAGE, "-1")
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_MUST_BE_ZERO_OR_POSITIVE)));
    }

    @Test
    public void givenNegativePageSizeNumberExpectErrorMessage() throws Exception {
        long courseId = 1L;

        mockMvc.perform(get(COURSE_SESSION_PATH, courseId)
            .param(SIZE, "-1")
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.BAD_REQUEST.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(PAGE_SIZE_MUST_BE_POSITIVE)));
    }

    @Test
    public void givenNonExistingCourseExpectErrorMessage() throws Exception {
        long courseId = 1L;
        int page = 0;
        int size = 2;
        when(courseSessionService.getCourseSessionList(courseId, DIRECTION, ORDER_BY, page, size))
            .thenThrow(new CourseNotFoundException(courseId));

        mockMvc.perform((get(COURSE_SESSION_PATH, courseId)
            .param(PAGE, String.valueOf(page))).param(SIZE, String.valueOf(size))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath(ERROR_CODE, is(HttpStatus.NOT_FOUND.value())))
            .andExpect(jsonPath(ERROR_USER_MESSAGE, CoreMatchers.is(courseNotFoundErrorMessage(courseId))))
            .andExpect(jsonPath(ERROR_TECHNICAL_MESSAGE, CoreMatchers.is(courseNotFoundErrorMessage(courseId))));

        verify(courseSessionService).getCourseSessionList(courseId, DIRECTION, ORDER_BY, page, size);
    }

    @Test
    public void givenInvalidOrderDirectionGetListArticleExpectException() throws Exception {
        long courseId = 1L;
        when(courseSessionService.getCourseSessionList(anyLong(), anyString(), anyString(), anyInt(), anyInt()))
            .thenThrow(new IllegalArgumentException("test"));

        mockMvc.perform(
            get(COURSE_SESSION_PATH, courseId)
                .param(PAGE, "0")
                .param(SIZE, "2")
                .param("direction", "ascc"))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath(ERROR_CODE).value(HttpStatus.BAD_REQUEST.value()));
    }

    @Test
    public void givenExistingCourseExpectErrorMessage() throws Exception {
        long courseId = 1L;
        int page = 0;
        int size = 2;
        Category category = Category.builder().id(1L).name(CATEGORY_NAME).build();
        Page<CourseSession> courseSessionPage = new PageImpl<>(Arrays.asList(
                new CourseSession(1L,COURSE_NAME, courseId, CourseSessionStatus.COMPLETED, trainerList,
                5, START_DATE, END_DATE, 0, 5, COURSE_SCHEDULE, DESCRIPTION,null,PHOTO,1L,category,0.0,null,false),
                new CourseSession(2L,COURSE_NAME, courseId, CourseSessionStatus.COMPLETED, trainerList,
                5, START_DATE, END_DATE, 0, 5, COURSE_SCHEDULE,DESCRIPTION,null,PHOTO,1L,category,0.0,null,false)),
                PageRequest.of(page, size, Sort.by(DIRECTION, ORDER_BY)), 2);
        when(courseSessionService.getCourseSessionList(courseId, DIRECTION, ORDER_BY, page, size))
            .thenReturn(courseSessionPage);

        mockMvc.perform((get(COURSE_SESSION_PATH, courseId)
            .param(PAGE, String.valueOf(page))).param(SIZE, String.valueOf(size))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(courseSessionPage.getTotalPages()))
            .andExpect(MockMvcResultMatchers.jsonPath("$.numberOfElements").value(courseSessionPage.getTotalElements()))
            .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());

        verify(courseSessionService).getCourseSessionList(courseId, DIRECTION, ORDER_BY, page, size);
    }

    private String courseNotFoundErrorMessage(long id) {
        return String.format("Course with id %d not found.", id);
    }
}
