package az.ingress.edu.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.keycloak.representations.AccessToken.Access;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.config.SwaggerConfiguration;
import az.ingress.edu.dto.CreateReviewDto;
import az.ingress.edu.dto.EnrollmentStatusDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ReviewFilterDto;
import az.ingress.edu.dto.ReviewStatusDto;
import az.ingress.edu.dto.UpdateReviewDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.exception.ReviewNotFoundException;
import az.ingress.edu.exception.ReviewWithoutEnrollmentException;
import az.ingress.edu.exception.TrainingCenterNotFound;
import az.ingress.edu.exception.UnauthorizedOperationException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.EnrollmentStatusView;
import az.ingress.edu.model.ItemType;
import az.ingress.edu.model.Review;
import az.ingress.edu.model.ReviewStatus;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.ReviewRepository;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessToken;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceImplTest {

    private static final String RESOURCE_ID = "resource-id";
    private static final String COMMENT = "What a wonderful course!";
    private static final String COURSE_NAME = "Ingress";
    private static final String COURSE_DESCRIPTION = "Microservice test";
    private static final String COURSE_REQUIREMENTS = "OCA";
    private static final String COURSE_MANAGER = "COURSE_MANAGER";
    private static final String USER = "USER";
    private static final String ADMIN = "ADMIN";

    private Review review;
    private User createdBy;
    private Course course;

    @Mock
    CourseService courseService;

    @Mock
    CourseSessionService courseSessionService;

    @Mock
    TrainingCenterService trainingCenterService;

    @Mock
    UserService userService;

    @Mock
    ReviewRepository reviewRepository;

    @Mock
    SwaggerConfiguration swaggerConfiguration;

    @Mock
    AccessToken accessToken;

    @Mock
    EnrollmentService enrollmentService;

    @InjectMocks
    ReviewServiceImpl reviewService;

    @Before
    public void setUp() {
        course = Course.builder()
                .id(1L)
                .category(Category.builder().id(1L).name("Java").build())
                .courseDetails(CourseDetails.builder().name(COURSE_NAME)
                .description(COURSE_DESCRIPTION)
                .requirements(COURSE_REQUIREMENTS)
                        .build())
                .build();

        createdBy = User.builder().firstname("Admin").lastname("Admin")
                .username("admin")
                .email("admin@ingress.az").build();

        review = Review.builder()
                .id(1L)
                .itemId(1L)
                .itemType(ItemType.COURSE)
                .comment(COMMENT)
                .rating(5)
                .createdBy(createdBy)
                .status(ReviewStatus.APPROVED)
                .build();
    }

    @Test
    public void givenReviewWithNonExistingCourseItemIdCreateReviewExpectException() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.COURSE,COMMENT);
        when(courseService.findById(anyLong()))
                .thenThrow(new CourseNotFoundException(createReviewDto.getItemId()));

        assertThatThrownBy(() -> reviewService.createReview(createReviewDto))
                .isInstanceOf(CourseNotFoundException.class);
        verify(courseService).findById(createReviewDto.getItemId());
    }

    @Test
    public void givenReviewWithNonExistingCourseSessionItemIdCreateReviewExpectException() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.COURSE_SESSION,COMMENT);
        when(courseSessionService.getCourseSession(anyLong()))
                .thenThrow(new CourseSessionNotFoundException(createReviewDto.getItemId()));

        assertThatThrownBy(() -> reviewService.createReview(createReviewDto))
                .isInstanceOf(CourseSessionNotFoundException.class);
        verify(courseSessionService).getCourseSession(createReviewDto.getItemId());
    }

    @Test
    public void givenReviewWithNonExistingTrainingCenterItemIdCreateReviewExpectException() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.TRAINING_CENTER,COMMENT);
        when(trainingCenterService.findById(anyLong()))
                .thenThrow(new TrainingCenterNotFound());

        assertThatThrownBy(() -> reviewService.createReview(createReviewDto))
                .isInstanceOf(TrainingCenterNotFound.class);
        verify(trainingCenterService).findById(createReviewDto.getItemId());
    }

    @Test
    public void givenReviewWithExistingCourseIdCreateReview() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.COURSE,COMMENT);
        Review createdReview = CreateReviewDto.toReview(createReviewDto);
        createdReview.setItemName(course.getCourseDetails().getName());
        createdReview.setCreatedBy(createdBy);
        when(courseService.findById(anyLong())).thenReturn(course);
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(reviewRepository.save(any(Review.class))).thenReturn(createdReview);

        IdDto idDto = reviewService.createReview(createReviewDto);

        assertThat(idDto.getId()).isEqualTo(createdReview.getId());
        verify(courseService).findById(createReviewDto.getItemId());
        verify(userService).getCurrentUser();
        verify(reviewRepository).save(createdReview);
    }

    @Test
    public void givenReviewWithExistingSessionIdCreateReview() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.COURSE_SESSION,COMMENT);
        CourseSession courseSession = CourseSession.builder()
                .id(1L)
                .name(COURSE_NAME)
                .trainers(Collections.singletonList(User.builder().username("linus").build()))
                .courseId(1L)
                .build();
        Review createdReview = CreateReviewDto.toReview(createReviewDto);
        createdReview.setItemName(course.getCourseDetails().getName());
        createdReview.setCreatedBy(createdBy);
        when(courseSessionService.getCourseSession(anyLong())).thenReturn(courseSession);
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(reviewRepository.save(any(Review.class))).thenReturn(createdReview);
        when(enrollmentService.getEnrolmentStatus(courseSession.getId()))
                .thenReturn(EnrollmentStatusDto.builder()
                        .enrollmentStatus(EnrollmentStatusView.APPROVED).build());
        IdDto idDto = reviewService.createReview(createReviewDto);

        assertThat(idDto.getId()).isEqualTo(createdReview.getId());
        verify(courseSessionService).getCourseSession(createReviewDto.getItemId());
        verify(userService).getCurrentUser();
        verify(reviewRepository).save(createdReview);
    }

    @Test
    public void givenReviewAndUserWithoutNotEnrolledToSessionCreateReviewExpectException() {
        CreateReviewDto createReviewDto = new CreateReviewDto(1L,5,ItemType.COURSE_SESSION,COMMENT);
        CourseSession courseSession = CourseSession.builder()
                .id(1L).name(COURSE_NAME)
                .trainers(Collections.singletonList(User.builder().username("linus").build()))
                .courseId(1L).build();
        Review createdReview = CreateReviewDto.toReview(createReviewDto);
        createdReview.setCreatedBy(createdBy);
        when(courseSessionService.getCourseSession(anyLong())).thenReturn(courseSession);
        when(enrollmentService.getEnrolmentStatus(courseSession.getId()))
                .thenReturn(EnrollmentStatusDto.builder()
                        .enrollmentStatus(EnrollmentStatusView.NOT_ENROLLED).build());
        assertThatThrownBy(() -> reviewService.createReview(createReviewDto))
                .isInstanceOf(ReviewWithoutEnrollmentException.class);
        verify(courseSessionService).getCourseSession(createReviewDto.getItemId());
    }

    @Test
    public void givenNonExistingReviewUpdateReviewExpectException() {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> reviewService.updateReview(updateReviewDto,id))
                .isInstanceOf(ReviewNotFoundException.class);
        verify(reviewRepository).findById(id);
    }

    @Test
    public void givenExistingReviewWithAdminUserUpdateReview() {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        final Review updatedReview = Review.builder()
                .id(review.getId()).status(ReviewStatus.APPROVED)
                .itemId(review.getItemId()).itemType(review.getItemType()).rating(updateReviewDto.getRating())
                .comment(updateReviewDto.getComment()).createdBy(review.getCreatedBy()).build();
        Access access = new Access();
        access.roles(Collections.singleton(ADMIN));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(reviewRepository.save(any(Review.class))).thenReturn(updatedReview);

        IdDto idDto = reviewService.updateReview(updateReviewDto,id);

        assertThat(idDto.getId()).isEqualTo(review.getId());
        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).save(updatedReview);
    }

    @Test
    public void givenExistingReviewWithCourseManagerAsUserUpdateReview() {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        final Review updatedReview = Review.builder()
                .id(review.getId()).status(ReviewStatus.APPROVED)
                .itemId(review.getItemId()).itemType(review.getItemType()).rating(updateReviewDto.getRating())
                .comment(updateReviewDto.getComment()).createdBy(review.getCreatedBy()).build();
        Access access = new Access();
        access.roles(Collections.singleton("COURSE_MANAGER"));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(reviewRepository.save(any(Review.class))).thenReturn(updatedReview);

        IdDto idDto = reviewService.updateReview(updateReviewDto,id);

        assertThat(idDto.getId()).isEqualTo(review.getId());
        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).save(updatedReview);
    }

    @Test
    public void givenNonExistingReviewChangeReviewStatusException() {
        long id = 1L;
        ReviewStatusDto reviewStatusDto = ReviewStatusDto.builder()
                .reviewStatus(ReviewStatus.PENDING).build();
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> reviewService.changeStatus(id,reviewStatusDto))
                .isInstanceOf(ReviewNotFoundException.class);
        verify(reviewRepository).findById(id);
    }

    @Test
    public void givenExistingReviewChangeReviewStatus() {
        long id = 1L;
        ReviewStatusDto statusDto = ReviewStatusDto.builder().reviewStatus(ReviewStatus.PENDING).build();
        Review updatedReview = Review.builder()
                .id(review.getId()).status(statusDto.getReviewStatus())
                .itemId(review.getItemId()).itemType(review.getItemType()).rating(review.getRating())
                .comment(review.getComment()).createdBy(review.getCreatedBy()).build();
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(reviewRepository.save(any(Review.class))).thenReturn(updatedReview);

        IdDto idDto = reviewService.changeStatus(id,statusDto);

        assertThat(idDto.getId()).isEqualTo(review.getId());
        verify(reviewRepository).findById(id);
        verify(reviewRepository).save(updatedReview);
    }


    @Test
    public void givenExistingReviewWithUserAsUserUpdateReview() {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        final Review updatedReview = Review.builder()
                .id(review.getId()).status(ReviewStatus.APPROVED)
                .itemId(review.getItemId()).itemType(review.getItemType()).rating(updateReviewDto.getRating())
                .comment(updateReviewDto.getComment()).createdBy(review.getCreatedBy()).build();
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(reviewRepository.save(any(Review.class))).thenReturn(updatedReview);

        IdDto idDto = reviewService.updateReview(updateReviewDto,id);

        assertThat(idDto.getId()).isEqualTo(review.getId());
        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).save(updatedReview);
    }

    @Test
    public void givenApprovedReviewWithUnauthorizedUserReview() {
        final long id = 1L;
        final UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(userService.getCurrentUser()).thenReturn(User.builder().username("legosi").build());
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));

        assertThatThrownBy(() -> reviewService.updateReview(updateReviewDto,id))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
    }

    @Test
    public void givenNonApprovedReviewWithUserAsUserUpdateReview() {
        long id = 1L;
        UpdateReviewDto updateReviewDto = new UpdateReviewDto(4,COMMENT);
        final Review nonApprovedReview = Review.builder()
                .id(review.getId()).status(ReviewStatus.PENDING)
                .itemId(review.getItemId()).itemType(review.getItemType()).rating(updateReviewDto.getRating())
                .comment(updateReviewDto.getComment()).createdBy(review.getCreatedBy()).build();
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(nonApprovedReview));

        assertThatThrownBy(() -> reviewService.updateReview(updateReviewDto,id))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
    }

    @Test
    public void givenCourseIdGetReviewListExpectException() {
        long id = 1L;
        ItemType itemType = ItemType.COURSE;
        int page = 0;
        int size = 1;
        when(courseService.findById(anyLong()))
                .thenThrow(new CourseNotFoundException(id));
        assertThatThrownBy(() -> reviewService.getReviewList(itemType,id,page,size))
                .isInstanceOf(CourseNotFoundException.class);
        verify(courseService).findById(id);
    }

    @Test
    public void givenTrainingCenterIdGetReviewListExpectException() {
        long id = 1L;
        ItemType itemType = ItemType.TRAINING_CENTER;
        int page = 0;
        int size = 1;
        when(trainingCenterService.findById(anyLong()))
                .thenThrow(new TrainingCenterNotFound());
        assertThatThrownBy(() -> reviewService.getReviewList(itemType,id,page,size))
                .isInstanceOf(TrainingCenterNotFound.class);
        verify(trainingCenterService).findById(id);
    }

    @Test
    public void givenCourseSessionIdGetReviewListExpectException() {
        long id = 1L;
        ItemType itemType = ItemType.COURSE_SESSION;
        int page = 0;
        int size = 1;
        when(courseSessionService.getCourseSession(anyLong()))
                .thenThrow(new CourseSessionNotFoundException(id));
        assertThatThrownBy(() -> reviewService.getReviewList(itemType,id,page,size))
                .isInstanceOf(CourseSessionNotFoundException.class);
        verify(courseSessionService).getCourseSession(id);
    }

    @Test
    public void givenExistingItemIdGetReviewList() {
        long id = 1L;
        ItemType itemType = ItemType.COURSE;
        int page = 0;
        int size = 1;
        PageRequest pageRequest = PageRequest.of(page,size,Sort.by("id").ascending());
        Page<Review> reviewPage = new PageImpl<>(Collections.singletonList(review),pageRequest,1);
        when(courseService.findById(anyLong())).thenReturn(course);
        when(reviewRepository.findAllReviews(anyLong(),any(ItemType.class),any(PageRequest.class)))
                .thenReturn(reviewPage);
        Page<Review> actualPage = reviewService.getReviewList(itemType,id,page,size);

        assertThat(actualPage.getTotalPages()).isEqualTo(reviewPage.getTotalPages());
        assertThat(actualPage.getTotalElements()).isEqualTo(reviewPage.getTotalElements());
        assertThat(actualPage.getContent().size()).isEqualTo(reviewPage.getContent().size());
        verify(courseService).findById(id);
        verify(reviewRepository).findAllReviews(id,itemType,pageRequest);
    }

    @Test
    public void givenNonExistingReviewGetReviewExpectException() {
        long id = 1L;
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> reviewService.getReview(id)).isInstanceOf(ReviewNotFoundException.class);
        verify(reviewRepository).findById(id);
    }

    @Test
    public void givenReviewFilterGetReviewListForManagement() {
        int page = 0;
        int size = 10;
        ReviewFilterDto filter = ReviewFilterDto
                .builder().direction("ASC").reviewStatus(ReviewStatus.APPROVED)
                .order("id")
                .itemType(ItemType.COURSE)
                .build();
        Pageable pageable = PageRequest
                .of(page, size, Sort.Direction.fromString(filter.getDirection()), filter.getOrder());
        when(reviewRepository.findAll(filter.getItemType(), filter.getReviewStatus(),pageable))
                .thenReturn(new PageImpl<>(Collections.singletonList(review),pageable,1));

        assertThat(reviewService.getReviewListForManagement(page,size,filter));
    }

    @Test
    public void givenExistingReviewGetReview() {
        long id = 1L;
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));

        Review actual = reviewService.getReview(id);

        assertThat(actual.getId()).isEqualTo(review.getId());
        verify(reviewRepository).findById(id);
    }

    @Test
    public void givenNonExistingReviewDeleteReviewExpectException() {
        long id = 1L;
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> reviewService.deleteReview(id)).isInstanceOf(ReviewNotFoundException.class);
        verify(reviewRepository).findById(id);
    }

    @Test
    public void givenExistingReviewAndUserWithAdminRoleDeleteReview() {
        final long id = 1L;
        Access access = new Access();
        access.roles(Collections.singleton(ADMIN));
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        reviewService.deleteReview(id);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).delete(review);
    }

    @Test
    public void givenExistingReviewAndUserWithCourseManagerRoleDeleteReview() {
        final long id = 1L;
        Access access = new Access();
        access.roles(Collections.singleton(COURSE_MANAGER));
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        reviewService.deleteReview(id);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).delete(review);
    }

    @Test
    public void givenExistingNonApprovedReviewAndUserWithUserRoleDeleteReview() {
        final long id = 1L;
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        reviewService.deleteReview(id);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
        verify(reviewRepository).delete(review);
    }

    @Test
    public void givenExistingApprovedReviewAndUnauthorizedUserDeleteReview() {
        final long id = 1L;
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(review));
        when(userService.getCurrentUser()).thenReturn(User.builder().username("legosi").build());
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        assertThatThrownBy(() -> reviewService.deleteReview(id))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
    }

    @Test
    public void givenExistingNonApprovedReviewAndUnauthorizedUserDeleteReview() {
        long id = 1L;
        Access access = new Access();
        access.roles(Collections.singleton(USER));
        when(reviewRepository.findById(anyLong())).thenReturn(Optional.of(Review.builder().id(id)
                .status(ReviewStatus.PENDING).createdBy(createdBy).build()));
        when(userService.getCurrentUser()).thenReturn(createdBy);
        when(swaggerConfiguration.getClientId()).thenReturn(RESOURCE_ID);
        when(accessToken.getResourceAccess(RESOURCE_ID)).thenReturn(access);

        assertThatThrownBy(() -> reviewService.deleteReview(id))
                .isInstanceOf(UnauthorizedOperationException.class);

        verify(reviewRepository).findById(id);
        verify(userService).getCurrentUser();
    }
}
