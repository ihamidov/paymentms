package az.ingress.edu.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.TrainingCenterDto;
import az.ingress.edu.exception.TrainingCenterNotFound;
import az.ingress.edu.model.ContactInfo;
import az.ingress.edu.model.TrainingCenter;
import az.ingress.edu.repository.ContactInfoRepository;
import az.ingress.edu.repository.TrainingCenterRepository;
import java.util.Arrays;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TrainingCenterServiceImplTest {

    public static final String DESCRIPTION = "My Custom description";
    public static final String NAME = "My Training Name";
    @InjectMocks
    TrainingCenterServiceImpl tcService;

    @Mock
    TrainingCenterRepository tcRepository;

    @Mock
    ContactInfoRepository ciRepository;

    TrainingCenter trainingCenter;
    TrainingCenterDto trainingCenterDto;
    ContactInfo contactInfo;
    
    @Before
    public void setUp() {
        contactInfo = ContactInfo.builder()
            .contactNumbers(Arrays.asList("3213323","4324332"))
            .address("Masallı")
            .email("farid.ali.bey@gmail.com")
            .build();
        trainingCenter = TrainingCenter.builder()
            .description(DESCRIPTION)
            .name(NAME)
            .contactInfo(contactInfo)
            .build();
        trainingCenterDto = TrainingCenterDto.builder()
            .contactInfo(contactInfo)
            .description("My Custom description")
            .name("My Training Name")
            .build();
    }
    
    @Test
    public void findById() {
        when(tcRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(trainingCenter));
        assertThat(tcService.findById(anyLong()).getDescription()).isEqualTo("My Custom description");
        verify(tcRepository).findByIdAndDeletedFalse(anyLong());
    }
    
    @Test
    public void findByIdTrainingCenterNotFound() {
        lenient().when(tcRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.ofNullable(trainingCenter));
        when(tcRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> tcService.findById(1L))
                  .isInstanceOf(TrainingCenterNotFound.class)
                  .hasMessage("Training Center not found");
        verify(tcRepository).findByIdAndDeletedFalse(anyLong());
    }
    
    @Test
    public void findAll() {
        when(tcRepository.findAllByDeletedFalse()).thenReturn(Arrays.asList(trainingCenter));
        assertThat(tcService.findAll()).isEqualTo(Arrays.asList(trainingCenter));
        verify(tcRepository).findAllByDeletedFalse();
    }
    
    @Test
    public void add() {
        when(tcRepository.save(any(TrainingCenter.class))).thenReturn(trainingCenter);
        assertThat(tcService.add(trainingCenterDto).getDescription()).isEqualTo(trainingCenter.getDescription());
        verify(tcRepository).save(trainingCenter);
    }
    
    @Test
    public void update() {
        when(tcRepository.findByIdAndDeletedFalse(1L)).thenReturn(Optional.ofNullable(trainingCenter));
        when(tcRepository.save(any(TrainingCenter.class))).thenReturn(trainingCenter);
        assertThat(tcService.update(trainingCenterDto,1L).getName()).isEqualTo(trainingCenter.getName());
        verify(tcRepository).save(trainingCenter);
    }
    
    @Test
    public void deleteById() {
        ArgumentCaptor<TrainingCenter> captor = ArgumentCaptor.forClass(TrainingCenter.class);
        TrainingCenter trCenter = TrainingCenter.builder()
                .id(1L)
                .description(DESCRIPTION)
                .name(NAME)
                .contactInfo(contactInfo)
                .build();

        when(tcRepository.findByIdAndDeletedFalse(trCenter.getId())).thenReturn(Optional.of(trCenter));

        tcService.deleteById(trCenter.getId());

        verify(tcRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
        verify(tcRepository).findByIdAndDeletedFalse(trCenter.getId());
    }
}
