package az.ingress.edu.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.exception.UserAlreadyExistsException;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.UserRepository;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private static final String USERNAME = "username";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String EMAIL = "email";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Test
    public void givenUserCreateUser() {
        User userForSave = new User(USERNAME, FIRSTNAME, LASTNAME, EMAIL, null,PHOTO);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(any(User.class))).thenReturn(userForSave);

        User expectedUser = userServiceImpl.saveUser(userForSave);

        assertThat(expectedUser).isEqualTo(userForSave);
        verify(userRepository).findByUsername(userForSave.getUsername());
        verify(userRepository).save(userForSave);
    }

    @Test
    public void givenUserWithAlreadyExistsUsernameCreateUserExpectException() {
        User existUser = new User(USERNAME, FIRSTNAME, LASTNAME, EMAIL, null,null);
        User userForSave = new User(USERNAME, FIRSTNAME, LASTNAME, EMAIL, null,PHOTO);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(existUser));

        assertThatThrownBy(() -> userServiceImpl.saveUser(userForSave)).isInstanceOf(UserAlreadyExistsException.class);

        verify(userRepository).findByUsername(userForSave.getUsername());
    }
}
