package az.ingress.edu.service;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CategoryDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CategoryAlreadyExistsException;
import az.ingress.edu.exception.CategoryInUseException;
import az.ingress.edu.exception.CategoryNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.repository.CategoryRepository;
import az.ingress.edu.repository.CourseRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceImplTest {

    public static final String NAME = "Microservices";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final int PAGE_SORT_SIZE = 5;
    private static final String ID = "id";
    private static final String COURSE_NAME = "Ingress";
    private static final String COURSE_DESCRIPTION = "Microservice test";
    private static final String COURSE_REQUIREMENTS = "OCA";
    private static final String COURSE_STATUS = "Active";

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private CategoryServiceImpl categoryServiceImpl;

    private Category category;

    private CategoryDto categoryDto;

    private CategoryDto categoryDtoForSave;

    private PageDto pageDto;

    private Course course;

    @Before
    public void setUp() {
        categoryDto = CategoryDto.builder()
                .name(NAME)
                .build();
        category = Category.builder()
                .id(1L)
                .name(NAME)
                .build();

        categoryDtoForSave = CategoryDto.builder()
                .name(NAME)
                .build();

        pageDto = PageDto.builder()
                .pageNumber(PAGE_SORT_NUMBER)
                .pageSize(PAGE_SORT_SIZE)
                .sortColumn(ID)
                .sortDirection(Sort.Direction.ASC.name())
                .build();

        course = Course.builder()
                .id(1L)
                .category(category)
                .courseDetails(CourseDetails.builder()
                .name(COURSE_NAME)
                .description(COURSE_DESCRIPTION)
                .requirements(COURSE_REQUIREMENTS)
                .status(COURSE_STATUS).build()).build();
    }

    @Test
    public void findById() {
        when(categoryRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(of(category));

        assertThat(categoryServiceImpl.findById(anyLong()).getName()).isEqualTo(NAME);
        verify(categoryRepository).findByIdAndDeletedFalse(anyLong());
    }

    @Test
    public void findByIdThenThrowCategoryNotFound() {
        when(categoryRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(empty());

        assertThatThrownBy(() -> categoryServiceImpl.findById(1L))
                .isInstanceOf(CategoryNotFoundException.class)
                .hasMessage(String.format("Category with id %d not found.", 1L));
        verify(categoryRepository).findByIdAndDeletedFalse(1L);
    }

    @Test
    public void add() {
        Category categoryAdd = Category
                .builder()
                .name(NAME)
                .build();

        when(categoryRepository.save(categoryAdd)).thenReturn(category);
        category = categoryServiceImpl.add(categoryDto);

        assertThat(category.getName()).isEqualTo(NAME);
        verify(categoryRepository).save(categoryAdd);
    }

    @Test
    public void update() {
        when(categoryRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(of(category));
        when(categoryRepository.save(any(Category.class))).thenReturn(category);

        Category expectedCategory = categoryServiceImpl.update(categoryDto, category.getId());
        assertThat(expectedCategory.getName()).isEqualTo(categoryDto.getName());
        verify(categoryRepository).save(category);
    }


    @Test
    public void categoryBeingUsedDeleteByIdExpectException() {
        when(categoryRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(of(category));
        when(courseRepository.findCourseByCategory(category)).thenReturn(Arrays.asList(course));

        assertThatThrownBy(() -> categoryServiceImpl.deleteById(category.getId())).isInstanceOf(
                CategoryInUseException.class);
        verify(categoryRepository).findByIdAndDeletedFalse(category.getId());
        verify(courseRepository).findCourseByCategory(category);
    }

    @Test
    public void categoryNotUsedDeleteById() {
        when(categoryRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(of(category));
        when(courseRepository.findCourseByCategory(category))
                .thenReturn(Collections.emptyList());

        categoryServiceImpl.deleteById(category.getId());
        verify(categoryRepository).findByIdAndDeletedFalse(category.getId());
        verify(courseRepository).findCourseByCategory(category);
    }

    @Test
    public void findAllPaginationAsc() {
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.asc("id")));
        Page<Category> expectedCategores = new PageImpl<>(Collections.singletonList(category));
        when(categoryRepository.findAllByDeletedFalse(pageRequest)).thenReturn(expectedCategores);
        Page<Category> actualCourses = categoryServiceImpl.list(pageDto);

        assertThat(actualCourses).isEqualTo(expectedCategores);
        assertThat(actualCourses.getNumberOfElements()).isEqualTo(1);

        verify(categoryRepository).findAllByDeletedFalse(pageRequest);
    }

    @Test
    public void findAllPaginationDesc() {
        pageDto.setSortDirection(Sort.Direction.DESC.name());
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        Page<Category> expectedCourses = new PageImpl<>(Collections.singletonList(category));
        when(categoryRepository.findAllByDeletedFalse(pageRequest)).thenReturn(expectedCourses);
        Page<Category> actualCourses = categoryServiceImpl.list(pageDto);

        assertThat(actualCourses).isEqualTo(expectedCourses);
        assertThat(actualCourses.getNumberOfElements()).isEqualTo(1);

        verify(categoryRepository).findAllByDeletedFalse(pageRequest);
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        pageDto.setSortColumn(ID);
        pageDto.setSortDirection("ASCS");
        assertThatThrownBy(() -> categoryServiceImpl.list(pageDto))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void categoryAlreadyExistsException() {
        Category exixstCategory = Category.builder().id(1L).name(NAME).build();
        CategoryDto categoryForSave = CategoryDto.builder().name(NAME).build();
        when(categoryRepository.findCategoryByNameAndDeletedFalse(anyString())).thenReturn(Optional.of(exixstCategory));
        assertThatThrownBy(() -> categoryServiceImpl.add(categoryForSave))
                .isInstanceOf(CategoryAlreadyExistsException.class);

        verify(categoryRepository).findCategoryByNameAndDeletedFalse(categoryDtoForSave.getName());
    }
}
