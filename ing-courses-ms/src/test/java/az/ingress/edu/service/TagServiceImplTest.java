package az.ingress.edu.service;

import static java.util.Optional.of;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.PageDto;
import az.ingress.edu.dto.TagDto;
import az.ingress.edu.exception.TagAlreadyExistException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.model.Tag;
import az.ingress.edu.repository.TagRepository;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

    private static final String DUMMY_TXT = "java";
    private static final String TAG_NOT_FOUND_EXCEPTION = "Tag with id 1 not found.";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final int PAGE_SORT_SIZE = 5;
    private static final String ID = "id";

    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    private PageDto pageDto;

    private final Tag tag = new Tag(null, DUMMY_TXT,false);
    private Tag tag1 = new Tag(1L, DUMMY_TXT,false);
    private final TagDto tagDto = new TagDto(DUMMY_TXT);

    @Before
    public void setUp() {
        pageDto = PageDto.builder()
                .pageNumber(PAGE_SORT_NUMBER)
                .pageSize(PAGE_SORT_SIZE)
                .sortColumn(ID)
                .sortDirection(Sort.Direction.ASC.name())
                .build();
    }

    @Test
    public void findById() {
        when(tagRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(of(tag));

        assertThat(tagService.findById(anyLong()).getName()).isEqualTo(DUMMY_TXT);
        verify(tagRepository).findByIdAndDeletedFalse(anyLong());
    }

    @Test
    public void findByIdTagNotFoundException() {
        when(tagRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> tagService.findById(1))
                .isInstanceOf(TagNotFoundException.class)
                .hasMessage(TAG_NOT_FOUND_EXCEPTION);
        verify(tagRepository).findByIdAndDeletedFalse(anyLong());
    }

    @Test
    public void whenAddThenAdd() {
        when(tagRepository.save(tag)).thenReturn(tag1);

        tagService.add(tagDto);

        assertThat(tagDto.getName()).isEqualTo(tag.getName());
        verify(tagRepository).save(tag);
    }

    @Test
    public void whenTagNameIsNull() {
        Tag tag = new Tag(null, null,false);
        Tag tag1 = new Tag(1L, null,false);
        TagDto tagDto = new TagDto(null);
        when(tagRepository.save(tag)).thenReturn(tag1);

        tagService.add(tagDto);

        assertThat(tagDto.getName()).isNullOrEmpty();
        verify(tagRepository).save(tag);
    }

    @Test
    public void whenTagAlreadyExistTheException() {
        when(tagRepository.getByName(anyString())).thenReturn(of(tag));

        assertThatThrownBy(() -> tagService.add(tagDto))
                .isInstanceOf(TagAlreadyExistException.class);
    }

    @Test
    public void deleteTagById() {
        ArgumentCaptor<Tag> captor = ArgumentCaptor.forClass(Tag.class);
        when(tagRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(tag1));

        tagService.deleteById(tag1.getId());

        verify(tagRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
    }

    @Test
    public void updateTag() {
        when(tagRepository.findByIdAndDeletedFalse(tag1.getId())).thenReturn(Optional.of(tag));
        when(tagRepository.save(any(Tag.class))).thenReturn(tag1);

        tag1 = tagService.update(tagDto, tag1.getId());
        assertThat(tag1.getName()).isEqualTo(tagDto.getName());
    }

    @Test
    public void whenPaginatingInAscendingOrderThenReturn() {
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.asc(ID)));
        Page<Tag> expected = new PageImpl<>(Collections.singletonList(tag1));
        when(tagRepository.findAllByDeletedFalse(pageRequest)).thenReturn(expected);
        Page<Tag> tagList = tagService.list(pageDto);

        assertThat(tagList).isEqualTo(expected);
        assertThat(tagList.getTotalElements()).isEqualTo(1);

        verify(tagRepository).findAllByDeletedFalse(pageRequest);
    }
}
