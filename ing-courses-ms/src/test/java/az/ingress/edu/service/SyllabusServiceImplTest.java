package az.ingress.edu.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.UpdateSyllabusDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.SyllabusNotFoundException;
import az.ingress.edu.exception.SyllabusWithChildException;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.model.Syllabus;
import az.ingress.edu.repository.CourseRepository;
import az.ingress.edu.repository.SyllabusRepository;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class SyllabusServiceImplTest {

    private static final String DUMMY = "dummy";
    private static final String TITLE = "Java";
    private static final String DESCRIPTION = "Programming essetials";

    private Course course;
    private Syllabus syllabus;

    @Mock
    private SyllabusRepository syllabusRepository;

    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private SyllabusServiceImpl syllabusService;

    private UpdateSyllabusDto updateSyllabusDto;

    @Before
    public void setUp() {

        updateSyllabusDto = UpdateSyllabusDto.builder()
                .title(TITLE)
                .description(DESCRIPTION)
                .duration(LocalTime.now())
                .build();

        course = Course.builder()
                .id(1L)
                .courseDetails(CourseDetails.builder().name("Java")
                        .description("Course")
                        .requirements("money")
                        .status("available").build())
                .build();
        syllabus = new Syllabus(null, 1L, 1L, DUMMY, DUMMY, 0, LocalTime.now(), null);
    }

    @Test
    public void givenNonExistingCourseCreateSyllabusExpectException() {
        Syllabus syllabus = new Syllabus(null, 1L, 1L, DUMMY, DUMMY, 0,LocalTime.now(), null);
        when(courseRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.createSyllabus(syllabus))
                .isInstanceOf(CourseNotFoundException.class);

        verify(courseRepository).findById(syllabus.getCourseId());
    }

    @Test
    public void givenChildSyllabusWithNonExistingParentCreateSyllabusExpectException() {
        when(courseRepository.findById(anyLong())).thenReturn(Optional.of(course));
        when(syllabusRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.createSyllabus(syllabus))
                .isInstanceOf(SyllabusNotFoundException.class);
        verify(courseRepository).findById(course.getId());
        verify(syllabusRepository).findById(syllabus.getParentId());
    }

    @Test
    public void givenSyllabusCreateSyllabus() {
        LocalTime duration = LocalTime.now();
        Syllabus savedSyllabus = new Syllabus(4L, 1L, 1L, DUMMY, DUMMY, 0,duration, null);
        Syllabus parentSyllabus = new Syllabus(1L, 1L, 2L, DUMMY, DUMMY, 1,duration, null);
        Syllabus updatedParentSyllabus = new Syllabus(1L, 1L, 2L, DUMMY, DUMMY, 2,duration, null);
        when(courseRepository.findById(anyLong())).thenReturn(Optional.of(course));
        when(syllabusRepository.findById(anyLong())).thenReturn(Optional.of(parentSyllabus));
        when(syllabusRepository.save(any())).thenReturn(updatedParentSyllabus);
        when(syllabusRepository.save(any())).thenReturn(savedSyllabus);

        assertThat(syllabusService.createSyllabus(syllabus)).isEqualTo(savedSyllabus);
        verify(courseRepository).findById(course.getId());
        verify(syllabusRepository).findById(syllabus.getParentId());
        verify(syllabusRepository).save(updatedParentSyllabus);
        verify(syllabusRepository).save(syllabus);
    }

    @Test
    public void givenNonExistingSyllabusUpdateSyllabusExpectException() {
        long id = 1L;
        when(syllabusRepository.findById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.updateSyllabus(updateSyllabusDto, id))
                .isInstanceOf(SyllabusNotFoundException.class);
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenExistingSyllabusUpdateSyllabus() {
        long id = 1L;
        LocalTime duration = updateSyllabusDto.getDuration();
        Syllabus syllabusToUpdate = new Syllabus(1L, 1L, 1L, DUMMY, DUMMY, 0,duration, null);
        Syllabus updatedSyllabus = new Syllabus(1L, 1L, 1L, TITLE, DESCRIPTION, 0,duration, null);
        when(syllabusRepository.findById(id)).thenReturn(Optional.of(syllabusToUpdate));
        when(syllabusRepository.save(any(Syllabus.class))).thenReturn(updatedSyllabus);

        IdDto idDto = syllabusService.updateSyllabus(updateSyllabusDto, id);

        assertThat(idDto.getId()).isEqualTo(id);
        verify(syllabusRepository).save(updatedSyllabus);
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenNonExistingSyllabusGetSyllabusExpectException() {
        long id = 1L;
        when(syllabusRepository.findById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.getSyllabus(id))
                .isInstanceOf(SyllabusNotFoundException.class);
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenExistingSyllabusGetSyllabusExpectException() {
        long id = 1L;
        when(syllabusRepository.findById(id)).thenReturn(Optional.of(syllabus));

        Syllabus actual = syllabusService.getSyllabus(id);

        assertThat(actual.getId()).isEqualTo(syllabus.getId());
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenNonExistingSyllabusDeleteSyllabusExpectException() {
        long id = 1L;
        when(syllabusRepository.findById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.deleteSyllabus(id))
                .isInstanceOf(SyllabusNotFoundException.class);
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenSyllabusWithChildDeleteSyllabusExpectException() {
        long id = 1L;
        Syllabus syllabusWithChild = new Syllabus(1L, 1L, 1L, DUMMY, DUMMY, 1,LocalTime.now(), null);
        when(syllabusRepository.findById(id)).thenReturn(Optional.of(syllabusWithChild));

        assertThatThrownBy(() -> syllabusService.deleteSyllabus(id))
                .isInstanceOf(SyllabusWithChildException.class);
        verify(syllabusRepository).findById(id);
    }

    @Test
    public void givenParentSyllabusDeleteSyllabus() {
        long id = 1L;
        Syllabus parentSyllabus = new Syllabus(1L, 1L, null, DUMMY, DUMMY, 0,LocalTime.now(), null);
        when(syllabusRepository.findById(id)).thenReturn(Optional.of(parentSyllabus));

        syllabusService.deleteSyllabus(id);

        verify(syllabusRepository).findById(id);
        verify(syllabusRepository).delete(parentSyllabus);
        verify(syllabusRepository, never()).save(any(Syllabus.class));
    }

    @Test
    public void givenChildSyllabusDeleteSyllabus() {
        long id = 2L;
        LocalTime duration = LocalTime.now();
        Syllabus parentSyllabus = new Syllabus(1L, 1L, null, DUMMY, DUMMY, 1,duration, null);
        final Syllabus savedParentSyllabus = new Syllabus(1L, 1L, null, DUMMY, DUMMY, 0,duration, null);
        Syllabus childSyllabus = new Syllabus(id, 1L, 1L, DUMMY, DUMMY, 0,LocalTime.now(), null);

        when(syllabusRepository.findById(id)).thenReturn(Optional.of(childSyllabus));
        when(syllabusRepository.findById(childSyllabus.getParentId())).thenReturn(Optional.of(parentSyllabus));

        syllabusService.deleteSyllabus(id);

        verify(syllabusRepository).findById(id);
        verify(syllabusRepository).findById(childSyllabus.getParentId());
        verify(syllabusRepository).save(savedParentSyllabus);
        verify(syllabusRepository).delete(childSyllabus);
    }

    @Test
    public void givenNonExistingCourseIdGetSyllabusExpectException() {
        when(courseRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.getSyllabi(course.getId()))
                .isInstanceOf(CourseNotFoundException.class);
        verify(courseRepository).findById(course.getId());
    }

    @Test
    public void givenExistingCourseIdGetSyllabus() {
        Sort sort = Sort.by("id").ascending();
        List<Syllabus> rawSyllabusList = Arrays
                .asList(new Syllabus(1L, 10L, null, DUMMY, DUMMY, 1,LocalTime.now(), null),
                        new Syllabus(2L, 10L, 1L, DUMMY, DUMMY, 2,LocalTime.now(), null),
                        new Syllabus(3L, 10L, 2L, DUMMY, DUMMY, 0,LocalTime.now(), null),
                        new Syllabus(4L, 10L, 2L, DUMMY, DUMMY, 0,LocalTime.now(), null));
        when(syllabusRepository.findAllByCourseId(course.getId(), sort))
                .thenReturn(rawSyllabusList);
        when(courseRepository.findById(course.getId())).thenReturn(Optional.of(course));

        List<Syllabus> actual = syllabusService.getSyllabi(course.getId());

        assertThat(actual.get(0).getChildren().size()).isEqualTo(1);
        assertThat(actual.get(0).getChildren().get(0).getChildren().size()).isEqualTo(2);
        verify(courseRepository).findById(course.getId());
        verify(syllabusRepository).findAllByCourseId(course.getId(), sort);
    }

    @Test
    public void givenNonExistingParentGetChildSyllabiExpectException() {
        long parentId = 1L;
        when(syllabusRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> syllabusService.getChildSyllabi(parentId))
                .isInstanceOf(SyllabusNotFoundException.class);
        verify(syllabusRepository).findById(parentId);
    }

    @Test
    public void givenExistingParentGetChildSyllabi() {
        long parentId = 1L;
        Syllabus parentSyllabus = new Syllabus(parentId, 1L, null, DUMMY, DUMMY, 1, LocalTime.now(), null);
        Syllabus childSyllabus = new Syllabus(2L, 1L, 1L, DUMMY, DUMMY, 0, LocalTime.now(), null);

        List<Syllabus> childSyllabi = Collections.singletonList(childSyllabus);
        when(syllabusRepository.findById(anyLong())).thenReturn(Optional.of(parentSyllabus));
        when(syllabusRepository.findAllByParentIdOrderById(parentId)).thenReturn(childSyllabi);
        List<Syllabus> actualChildSyllabi = syllabusService.getChildSyllabi(parentId);

        assertThat(actualChildSyllabi.size()).isEqualTo(childSyllabi.size());
        assertThat(actualChildSyllabi).isEqualTo(childSyllabi);
        verify(syllabusRepository).findById(parentId);
        verify(syllabusRepository).findAllByParentIdOrderById(parentId);
    }
}
