package az.ingress.edu.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CourseDto;
import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListCourseDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.dto.PageDto;
import az.ingress.edu.exception.CategoryNotFoundException;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.TagNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.Tag;
import az.ingress.edu.model.TrainingCenter;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.CourseRepository;
import az.ingress.edu.repository.CourseSessionRepository;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceImplTest {

    private static final String ID = "id";
    private static final String NAME = "Ingress";
    private static final String DESCRIPTION = "Microservice test";
    private static final String REQUIREMENTS = "OCA";
    private static final String UPDATED_REQ = "No requirement";
    private static final String STATUS = "Active";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final int PAGE_SORT_SIZE = 5;
    private static final String COURSE_NOT_FOUND_EXCEPTION = "Course with id 1 not found.";
    private static final String CATEGORY_NOT_FOUND_EXCEPTION = "Category with id 1 not found.";
    private static final String TAG_NOT_FOUND_EXCEPTION = "Tag with id 1 not found.";
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Mock
    private CourseSessionRepository sessionRepository;

    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private CourseServiceImpl courseService;

    @Mock
    private TagServiceImpl tagService;

    @Mock
    private CategoryServiceImpl categoryService;

    @Mock
    private UserService userService;

    @Mock
    private TrainingCenterServiceImpl trainingCenterService;

    private CourseDto courseDto;

    private Course course;

    Category category;

    Tag tag;

    CourseSession courseSession;

    private PageDto pageDto;

    private User user;

    private TrainingCenter trainingCenter;

    @Before
    public void setUp() {
        category = Category.builder().id(1L).name(NAME).build();
        tag = Tag.builder().id(1L).name(NAME).build();
        courseDto = CourseDto.builder().categoryId(category.getId()).name(NAME).description(DESCRIPTION)
                .requirements(REQUIREMENTS).tags(Arrays.asList(new IdDto(tag.getId()))).status(STATUS)
                .build();
        course = Course.builder().id(1L).category(category).courseDetails(
                CourseDetails.builder().name(NAME).description(DESCRIPTION)
                        .requirements(REQUIREMENTS).status(STATUS).build())
                .tags(Arrays.asList(tag)).build();
        pageDto = PageDto.builder().pageNumber(PAGE_SORT_NUMBER).pageSize(PAGE_SORT_SIZE).sortColumn(ID)
                .sortDirection(Direction.ASC.name()).build();
        user = User.builder().firstname("Admin").lastname("Admin")
                .username("admin")
                .email("admin@ingress.az").build();
        courseSession = CourseSession.builder().id(1L).name(NAME).courseId(1L).build();
        trainingCenter = TrainingCenter
                .builder()
                .id(1L)
                .build();
    }

    @Test
    public void findById() {
        when(courseRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(course));

        assertThat(courseService.findById(anyLong()).getCourseDetails()
                .getDescription()).isEqualTo(DESCRIPTION);
        verify(courseRepository).findByIdAndDeletedFalse(anyLong());
    }

    @Test
    public void findByIdCourseNotFoundException() {
        when(courseRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> courseService.findById(1))
                .isInstanceOf(CourseNotFoundException.class)
                .hasMessage(COURSE_NOT_FOUND_EXCEPTION);
        verify(courseRepository).findByIdAndDeletedFalse(anyLong());
    }

    @Test
    public void add() {
        final TrainingCenter trainingCenter = TrainingCenter.builder()
                .id(1L)
                .name("Training Name")
                .build();
        final Course courseForSave = Course.builder()
                .category(category)
                .courseDetails(CourseDetails.builder()
                        .name(NAME)
                        .description(DESCRIPTION)
                        .requirements(REQUIREMENTS)
                        .status(STATUS).build())
                .tags(Arrays.asList())
                .trainingCenter(trainingCenter)
                .createdBy(user)
                .modifiedBy(user).build();
        when(categoryService.findById(anyLong())).thenReturn(category);
        when(courseRepository.save(any(Course.class))).thenReturn(course);
        when(trainingCenterService.findById(anyLong())).thenReturn(trainingCenter);
        when(userService.getCurrentUser()).thenReturn(user);

        courseDto.setTrainingCenterId(anyLong());
        Course expectedCourse = courseService.add(courseDto);
        assertThat(expectedCourse.getCourseDetails().getDescription())
                .isEqualTo(course.getCourseDetails().getDescription());

        verify(courseRepository).save(courseForSave);
        verify(userService).getCurrentUser();
        verify(categoryService).findById(courseDto.getCategoryId());
        verify(trainingCenterService).findById(courseDto.getTrainingCenterId());
    }

    @Test
    public void update() {
        Course courseForSave = new Course(1L,new CourseDetails(NAME,DESCRIPTION,DESCRIPTION,REQUIREMENTS,STATUS),
                category,trainingCenter,Arrays.asList(tag),null,null,user,user,PHOTO,0.0,false);
        CourseDto course = CourseDto.builder().name(NAME).categoryId(category.getId()).photo(PHOTO)
                .description(DESCRIPTION).requirements(UPDATED_REQ).shortDescription(DESCRIPTION).status(STATUS)
                .tags(Arrays.asList(new IdDto(tag.getId()))).build();
        Course updatedCourse = new Course(1L,new CourseDetails(NAME,DESCRIPTION,DESCRIPTION,UPDATED_REQ,STATUS),
                category,trainingCenter,Arrays.asList(tag),null,null,user,user,PHOTO,0.0,false);
        when(courseRepository.findByIdAndDeletedFalse(courseForSave.getId())).thenReturn(Optional.of(courseForSave));
        when(sessionRepository.findAllByCourseId(1L)).thenReturn(Arrays.asList(courseSession));
        when(categoryService.findById(anyLong())).thenReturn(category);
        when(tagService.findById(anyLong())).thenReturn(tag);
        when(courseRepository.save(any(Course.class))).thenReturn(updatedCourse);
        when(tagService.toEntityList(Arrays.asList(new IdDto(tag.getId())))).thenReturn(Arrays.asList(tag));
        when(userService.getCurrentUser()).thenReturn(user);

        Course expectedCourse = courseService.update(courseDto, 1L);
        assertThat(expectedCourse.getCourseDetails().getDescription()).isEqualTo(course.getDescription());

        verify(courseRepository).save(courseForSave);
        verify(courseRepository).findByIdAndDeletedFalse(courseForSave.getId());
        verify(userService).getCurrentUser();
        verify(categoryService).findById(courseDto.getCategoryId());
        verify(trainingCenterService).findById(courseDto.getTrainingCenterId());
        verify(sessionRepository).save(courseSession);
    }

    @Test
    public void updateCategoryNotFoundException() {
        when(categoryService.findById(anyLong())).thenThrow(new CategoryNotFoundException(1));
        when(userService.getCurrentUser()).thenReturn(user);

        assertThatThrownBy(() -> courseService.add(courseDto))
                .isInstanceOf(CategoryNotFoundException.class)
                .hasMessage(CATEGORY_NOT_FOUND_EXCEPTION);
        verify(categoryService).findById(courseDto.getCategoryId());
    }

    @Test
    public void updateTagNotFoundException() {
        when(tagService.findById(anyLong())).thenThrow(new TagNotFoundException(1));
        when(tagService.toEntityList(Arrays.asList(new IdDto(tag.getId())))).thenReturn(Arrays.asList(tag));
        when(userService.getCurrentUser()).thenReturn(user);

        assertThatThrownBy(() -> courseService.add(courseDto))
                .isInstanceOf(TagNotFoundException.class)
                .hasMessage(TAG_NOT_FOUND_EXCEPTION);
    }

    @Test
    public void deleteById() {
        when(courseRepository.findByIdAndDeletedFalse(course.getId())).thenReturn(Optional.of(course));
        ArgumentCaptor<Course> captor = ArgumentCaptor.forClass(Course.class);

        courseService.deleteById(course.getId());
        verify(courseRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
    }

    @Test
    public void findAllPaginationAsc() {
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.asc("id")));
        Page<Course> expectedCourses = new PageImpl<>(Collections.singletonList(course));
        ListDto expectedCourseListDto = new ListDto(Collections.singletonList(ListCourseDto.builder()
                .id(course.getId())
                .category(course.getCategory())
                .name(course.getCourseDetails().getName())
                .shortDescription(course.getCourseDetails().getShortDescription())
                .photo(course.getPhoto())
                .tags(course.getTags()).build()), 1, 1);
        when(courseRepository.findAllCourses(pageRequest)).thenReturn(expectedCourses);
        ListDto actualCourses = courseService.list(pageDto);

        assertThat(actualCourses.getContent()).isEqualTo(expectedCourseListDto.getContent());
        assertThat(actualCourses.getTotalElements()).isEqualTo(1);
        assertThat(actualCourses.getTotalPages()).isEqualTo(1);

        verify(courseRepository).findAllCourses(pageRequest);
    }

    @Test
    public void findAllPaginationDesc() {
        pageDto.setSortDirection(Direction.DESC.name());
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.desc("id")));
        Page<Course> expectedCourses = new PageImpl<>(Collections.singletonList(course));
        ListDto expectedCourseListDto = new ListDto(Collections.singletonList(ListCourseDto.builder()
                .id(course.getId())
                .category(course.getCategory())
                .name(course.getCourseDetails().getName())
                .shortDescription(course.getCourseDetails().getShortDescription())
                .photo(course.getPhoto())
                .tags(course.getTags()).build()), 1, 1);
        when(courseRepository.findAllCourses(pageRequest)).thenReturn(expectedCourses);
        ListDto actualCourses = courseService.list(pageDto);

        assertThat(actualCourses.getContent()).isEqualTo(expectedCourseListDto.getContent());
        assertThat(actualCourses.getTotalElements()).isEqualTo(1);

        verify(courseRepository).findAllCourses(pageRequest);
    }

    @Test
    public void sortDirectionInvalidExceptionBadRequest() throws Exception {
        pageDto.setSortColumn(ID);
        pageDto.setSortDirection("ASCS");
        assertThatThrownBy(() -> courseService.list(pageDto))
                .isInstanceOf(IllegalArgumentException.class);
    }
}
