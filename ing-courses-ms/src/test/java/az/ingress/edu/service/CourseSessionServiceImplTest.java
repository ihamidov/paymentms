package az.ingress.edu.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.dto.CourseSessionDto;
import az.ingress.edu.dto.UsernameDto;
import az.ingress.edu.exception.CourseNotFoundException;
import az.ingress.edu.exception.CourseSessionNotFoundException;
import az.ingress.edu.exception.MinimumSessionDurationException;
import az.ingress.edu.exception.UserNotFoundException;
import az.ingress.edu.model.Category;
import az.ingress.edu.model.Course;
import az.ingress.edu.model.CourseDetails;
import az.ingress.edu.model.CourseSession;
import az.ingress.edu.model.CourseSessionStatus;
import az.ingress.edu.model.Status;
import az.ingress.edu.model.Tag;
import az.ingress.edu.model.User;
import az.ingress.edu.repository.CourseSessionRepository;
import az.ingress.edu.repository.UserRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@RunWith(MockitoJUnitRunner.class)
public class CourseSessionServiceImplTest {

    private static final String COURSE_STATUS = Status.PENDING.name();
    private static final int COURSE_NUMBER_OF_LESSONS = 5;
    private static final String USERNAME = "linux";
    private static final Instant COURSE_START_DATE = Calendar.getInstance().toInstant();
    private static final Instant COURSE_END_DATE = Calendar.getInstance().toInstant().plus(2, ChronoUnit.DAYS);
    private static final String COURSE_NAME = "Ingress Microservices";
    private static final String REQUIREMENTS = "OCA certificate";
    private static final String COURSE_SCHEDULE = "test";
    private static final String CATEGORY_NAME = "java";
    private static final String DESCRIPTION = "Microservices master class";
    private static final String TEACHER_DESC = "Wonderful course wonderful teacher";
    private static final int PAGE_SORT_NUMBER = 1;
    private static final int PAGE_SORT_SIZE = 5;
    private static final String ID = "id";
    private static final User TRAINER = new User(USERNAME, "Linus",
            "Torvalds", "linus@torvalds.com", "555","www.google.com/linus.png");
    private static final String PHOTO = "https://www.dummy.com/dummy.png";

    @Mock
    private CourseSessionRepository sessionRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CourseService courseService;

    @InjectMocks
    private CourseSessionServiceImpl sessionService;

    private CourseSession courseSession;

    private Course course;

    private Category category;

    private List<User> trainerList;

    @Before
    public void setUp() {
        category = Category.builder().id(1L).name(CATEGORY_NAME).build();
        trainerList = Collections.singletonList(TRAINER);
        course = Course.builder()
                .id(1L)
                .category(category)
                .photo(PHOTO)
                .courseDetails(CourseDetails.builder().status(COURSE_STATUS)
                        .name(COURSE_NAME)
                        .description(DESCRIPTION)
                        .requirements(REQUIREMENTS)
                        .build())
                .tags(Collections.singletonList(new Tag(1L, "Programming", false)))
                .build();

        courseSession = CourseSession.builder()
                .id(1L)
                .courseId(1L)
                .photo(PHOTO)
                .name(COURSE_NAME)
                .status(CourseSessionStatus.COMPLETED)
                .trainers(Collections.singletonList(TRAINER))
                .numberOfLessons(COURSE_NUMBER_OF_LESSONS)
                .startDate(COURSE_START_DATE)
                .endDate(COURSE_END_DATE)
                .enrolledStudentCnt(1)
                .requiredStudentCnt(5)
                .courseSchedule(COURSE_SCHEDULE)
                .shortDescription(DESCRIPTION)
                .build();
    }

    @Test
    public void givenCourseSessionNotExceedingMinCourseDurationCreateCourseSessionExpectException() {
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1,
                Instant.now(), Instant.now(), 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(courseService.findById(anyLong())).thenReturn(course);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(TRAINER));

        assertThatThrownBy(() -> sessionService.createCourseSession(courseSessionDto, 1L))
                .isInstanceOf(MinimumSessionDurationException.class);
        verify(courseService).findById(courseSession.getCourseId());
        verify(userRepository).findByUsername(USERNAME);
    }

    @Test
    public void givenCourseSessionWithNonExistingUserCreateCourseSessionExpectException() {
        long courseId = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(courseService.findById(anyLong())).thenReturn(course);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> sessionService.createCourseSession(courseSessionDto, courseId))
                .isInstanceOf(UserNotFoundException.class);
        verify(courseService).findById(courseId);
        verify(userRepository).findByUsername(USERNAME);
    }

    @Test
    public void givenCourseSessionWithNonExistingCourseCreateCourseSessionExpectException() {
        long courseId = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(courseService.findById(anyLong())).thenThrow(new CourseNotFoundException(1L));

        assertThatThrownBy(() -> sessionService.createCourseSession(courseSessionDto, courseId))
                .isInstanceOf(CourseNotFoundException.class);
        verify(courseService).findById(courseSession.getCourseId());
    }

    @Test
    public void givenCourseSessionCreateCourseSession() {
        long courseId = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 5, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        CourseSession sessionToCreate = new CourseSession(null, COURSE_NAME, 1L, CourseSessionStatus.COMPLETED,
                trainerList, 1, COURSE_START_DATE, COURSE_END_DATE, 0, 5,
                COURSE_SCHEDULE, null,TEACHER_DESC, PHOTO,1L, category, null, course.getTags(), false);
        CourseSession createdCourseSession = new CourseSession(1L, COURSE_NAME, 1L, CourseSessionStatus.COMPLETED,
                trainerList, 1, COURSE_START_DATE, COURSE_END_DATE, 0, 5,
                COURSE_SCHEDULE, null,TEACHER_DESC, PHOTO,1L, category, null, course.getTags(), false);
        when(courseService.findById(anyLong())).thenReturn(course);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(TRAINER));
        when(sessionRepository.save(any(CourseSession.class))).thenReturn(createdCourseSession);

        CourseSession returnCourseSession = sessionService.createCourseSession(courseSessionDto, courseId);

        assertThat(returnCourseSession.getId()).isEqualTo(createdCourseSession.getId());
        verify(courseService).findById(courseId);
        verify(userRepository).findByUsername(USERNAME);
        verify(sessionRepository).save(sessionToCreate);
    }

    @Test
    public void givenNonExistingCourseGetCourseSessionListException() {
        when(courseService.findById(anyLong())).thenThrow(new CourseNotFoundException(1L));

        assertThatThrownBy(() -> sessionService.getCourseSessionList(1L, Sort.Direction.ASC.toString(), ID, 0, 2))
                .isInstanceOf(CourseNotFoundException.class);
        verify(courseService).findById(1L);
    }

    @Test
    public void whenPaginatingInAscendingOrderThenReturn() {
        long courseId = 1L;
        PageRequest pageRequest = PageRequest.of(PAGE_SORT_NUMBER, PAGE_SORT_SIZE, Sort.by(Sort.Order.asc(ID)));
        Page<CourseSession> expected = new PageImpl<>(Collections.singletonList(courseSession));
        when(courseService.findById(anyLong())).thenReturn(course);
        when(sessionRepository.findAllByCourseIdAndDeletedFalse(courseId, pageRequest)).thenReturn(expected);

        Page<CourseSession> sessionList = sessionService.getCourseSessionList(courseId, Sort.Direction.ASC.toString(),
                ID, PAGE_SORT_NUMBER, PAGE_SORT_SIZE);

        assertThat(sessionList).isEqualTo(expected);
        assertThat(sessionList.getTotalElements()).isEqualTo(1);
        verify(courseService).findById(courseId);
        verify(sessionRepository).findAllByCourseIdAndDeletedFalse(courseId, pageRequest);
    }

    @Test
    public void givenNonExistingSessionIdGetCourseSessionExpectException() {
        long id = 1L;
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> sessionService.getCourseSession(id))
                .isInstanceOf(CourseSessionNotFoundException.class);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenExistingSessionIdGetCourseSession() {
        long id = 1L;
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(courseSession));

        CourseSession actual = sessionService.getCourseSession(id);

        assertThat(actual.getId()).isEqualTo(id);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenNonExistingSessionIdDeleteCourseSessionExpectException() {
        long id = 1L;
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> sessionService.deleteCourseSession(id))
                .isInstanceOf(CourseSessionNotFoundException.class);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenExistingSessionIdDeleteCourseSession() {
        long id = 1L;
        ArgumentCaptor<CourseSession> captor = ArgumentCaptor.forClass(CourseSession.class);
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(courseSession));

        sessionService.deleteCourseSession(id);

        verify(sessionRepository).save(captor.capture());
        assertThat(captor.getValue().isDeleted()).isEqualTo(true);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenNonExistingCourseSessionUpdateCourseSessionExpectException() {
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.empty());
        long id = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        assertThatThrownBy(() -> sessionService.updateCourseSession(courseSessionDto, id))
                .isInstanceOf(CourseSessionNotFoundException.class);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenCourseSessionNotExceedingMinCourseDurationUpdateCourseSessionExpectException() {
        long id = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto invalidSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, Instant.now(),
                Instant.now(), 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(courseSession));

        assertThatThrownBy(() -> sessionService.updateCourseSession(invalidSessionDto, id))
                .isInstanceOf(MinimumSessionDurationException.class);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
    }

    @Test
    public void givenCourseSessionWithNonExistingTrainerUpdateCourseSessionExpectException() {
        long id = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(courseSession));
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> sessionService.updateCourseSession(courseSessionDto, id))
                .isInstanceOf(UserNotFoundException.class);
        verify(sessionRepository).findByIdAndDeletedFalse(courseSession.getId());
        verify(userRepository).findByUsername(USERNAME);
    }

    @Test
    public void givenCourseSessionWithValidDataUpdateCourseSession() {
        CourseSession updatedSession = new CourseSession(1L, COURSE_NAME, 1L, CourseSessionStatus.COMPLETED,
                trainerList, 1, COURSE_START_DATE, COURSE_END_DATE, 1, 1,
                COURSE_SCHEDULE, DESCRIPTION,TEACHER_DESC, PHOTO,1L, null, null, null, false);
        long id = 1L;
        List<UsernameDto> trainerIds = Collections.singletonList(new UsernameDto(USERNAME));
        CourseSessionDto courseSessionDto = new CourseSessionDto(CourseSessionStatus.COMPLETED, 1, COURSE_START_DATE,
                COURSE_END_DATE, 1, COURSE_SCHEDULE, trainerIds,1L,TEACHER_DESC);
        when(sessionRepository.findByIdAndDeletedFalse(anyLong())).thenReturn(Optional.of(courseSession));
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(TRAINER));
        when(sessionRepository.save(any(CourseSession.class))).thenReturn(updatedSession);

        CourseSession actual = sessionService.updateCourseSession(courseSessionDto, id);

        assertThat(actual.getId()).isEqualTo(id);
        assertThat(actual.getCourseId()).isEqualTo(id);
        verify(sessionRepository).findByIdAndDeletedFalse(id);
        verify(userRepository).findByUsername(USERNAME);
        verify(sessionRepository).save(updatedSession);
    }
}
