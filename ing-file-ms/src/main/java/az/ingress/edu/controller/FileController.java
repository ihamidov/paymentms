package az.ingress.edu.controller;

import az.ingress.edu.dto.IdDto;
import az.ingress.edu.dto.ListDto;
import az.ingress.edu.model.File;
import az.ingress.edu.service.FileService;
import java.io.IOException;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
@Validated
public class FileController {

    private static final String FILE_ID_MUST_BE_POSITIVE = "File id must be positive";
    private static final String INDEX_MUST_BE_ZERO_OR_GREATER = "Page index must be 0 or greater";
    private static final String SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping("/upload")
    public IdDto createFile(MultipartFile file) throws IOException {
        return new IdDto(fileService.createFile(file).getId());
    }

    @GetMapping("/{id}/url")
    public String getFileContent(@Positive(message = FILE_ID_MUST_BE_POSITIVE) @PathVariable Long id)
            throws IOException {
        return fileService.getFileUrl(id);
    }

    @GetMapping("/{id}")
    public File getFile(@Positive(message = FILE_ID_MUST_BE_POSITIVE) @PathVariable long id) {
        return fileService.getFile(id);
    }

    @GetMapping
    public ListDto getAllFiles(@RequestParam(name = "page", defaultValue = "0")
                               @PositiveOrZero(message = INDEX_MUST_BE_ZERO_OR_GREATER) int page,
                               @RequestParam(name = "size", defaultValue = "10")
                               @Positive(message = SIZE_MUST_BE_POSITIVE) int size,
                               @RequestParam(name = "order", defaultValue = "id") String order,
                               @RequestParam(name = "direction", defaultValue = "desc") String direction) {
        Page<File> listArticles = fileService.findAll(page, size, order, direction);
        return new ListDto(listArticles.getContent(), listArticles.getTotalPages(), listArticles.getTotalElements());
    }
}
