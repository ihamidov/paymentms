package az.ingress.edu.util.impl;

import az.ingress.edu.config.FileUtilsConfiguration;
import az.ingress.edu.exception.NotSupportedFileFormat;
import az.ingress.edu.util.FileUtils;
import java.io.IOException;
import org.springframework.stereotype.Component;

@Component
public class FileUtilsImpl implements FileUtils {

    private final FileUtilsConfiguration utilsConfiguration;

    public FileUtilsImpl(FileUtilsConfiguration utilsConfiguration) throws IOException {
        this.utilsConfiguration = utilsConfiguration;
    }

    @Override
    public void validateFileExtension(String fileName) {
        if (!fileName.contains(".") || !isSupportedFile(getExtension(fileName))) {
            throw new NotSupportedFileFormat();
        }
    }

    @Override
    public String getExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.'));
    }

    @Override
    public boolean isSupportedFile(String extension) {
        return utilsConfiguration.getSupportedFormats()
                .contains(extension);
    }
}
