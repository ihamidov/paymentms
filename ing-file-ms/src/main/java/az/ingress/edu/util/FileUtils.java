package az.ingress.edu.util;

public interface FileUtils {
    void validateFileExtension(String fileName);

    String getExtension(String fileName);

    boolean isSupportedFile(String extension);
}
