package az.ingress.edu.repository;

import az.ingress.edu.model.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<File, Long>, PagingAndSortingRepository<File,Long> {
}
