package az.ingress.edu.service;

import az.ingress.edu.model.File;
import java.io.IOException;
import java.net.MalformedURLException;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    File createFile(MultipartFile file) throws IOException;

    String getFileUrl(long id) throws MalformedURLException;

    File getFile(long id);

    Page<File> findAll(int page, int size, String order, String direction);
}
