package az.ingress.edu.service.impl;

import az.ingress.edu.config.FileUtilsConfiguration;
import az.ingress.edu.exception.FileNotFoundException;
import az.ingress.edu.exception.FileNotPresentException;
import az.ingress.edu.exception.MaxUploadSizeExceedException;
import az.ingress.edu.model.File;
import az.ingress.edu.repository.FileRepository;
import az.ingress.edu.service.FileService;
import az.ingress.edu.util.FileUtils;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@AllArgsConstructor
public class FileServiceImpl implements FileService {

    private final FileUtils fileUtils;
    private final FileRepository fileRepository;
    private final FileUtilsConfiguration utilsConfiguration;
    private final Cloudinary cloudinary;

    @Override
    public File createFile(MultipartFile multipartFile) throws IOException {

        if (!isFilePresent(multipartFile)) {
            throw new FileNotPresentException();
        }
        checkFileSize(multipartFile);
        String originalFileName = multipartFile.getOriginalFilename();
        fileUtils.validateFileExtension(originalFileName);
        String name = getFileName(multipartFile);
        Map uploadResult = cloudinary.uploader()
                .upload(convertMultiPartToFile(multipartFile, name), ObjectUtils.emptyMap());
        File file = File.builder().name(name)
                .url(uploadResult.get("url").toString()).originalName(originalFileName).build();

        return fileRepository.save(file);
    }

    @Override
    public String getFileUrl(long id) {
        return getFile(id).getUrl();
    }

    @Override
    public File getFile(long id) {
        return fileRepository.findById(id).orElseThrow(() -> new FileNotFoundException(id));
    }

    @Override
    public Page<File> findAll(int page, int size, String order, String direction) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(direction), order));
        return fileRepository.findAll(pageable);
    }

    private String getFileName(MultipartFile file) {
        Calendar calendar = Calendar.getInstance();
        String generatedName = String.valueOf(calendar.getTimeInMillis());
        return generatedName + fileUtils.getExtension(file.getOriginalFilename());
    }

    private boolean isFilePresent(MultipartFile file) {
        return file != null && !file.isEmpty() && file.getOriginalFilename() != null;
    }

    private void checkFileSize(MultipartFile multipartFile) {
        if (multipartFile.getSize() > utilsConfiguration.getMaxUploadSize()) {
            throw new MaxUploadSizeExceedException(utilsConfiguration.getMaxUploadSize());
        }
    }

    private java.io.File convertMultiPartToFile(MultipartFile file, String name) throws IOException {
        FileOutputStream fos = null;
        try {
            java.io.File convertedFile = new java.io.File(name);
            fos = new FileOutputStream(convertedFile);
            fos.write(file.getBytes());
            fos.close();
            return convertedFile;
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }
}
