package az.ingress.edu.exception;

public class MaxUploadSizeExceedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public MaxUploadSizeExceedException(Long maxSize) {
        super(String.format("File exceeds '%d' max upload size", maxSize));
    }
}
