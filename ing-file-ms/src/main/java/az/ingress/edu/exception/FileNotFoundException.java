package az.ingress.edu.exception;

public class FileNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public FileNotFoundException(long id) {
        super(String.format("File with id: '%d' not found",id));
    }
}
