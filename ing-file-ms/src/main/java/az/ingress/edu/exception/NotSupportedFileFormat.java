package az.ingress.edu.exception;

public class NotSupportedFileFormat extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public NotSupportedFileFormat() {
        super("Unsupported file format");
    }
}
