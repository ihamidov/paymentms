package az.ingress.edu.exception;

public class FileNotPresentException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public FileNotPresentException() {
        super("Attempt to upload empty file");
    }
}
