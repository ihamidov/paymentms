package az.ingress.edu.exception;

import az.ingress.edu.dto.ErrorDto;
import java.util.Calendar;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ControllerExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorDto> handleConstraintViolation(ConstraintViolationException ex) {
        ConstraintViolation violation = ex.getConstraintViolations().iterator().next();
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                violation.getMessage(), violation.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(MaxUploadSizeExceedException.class)
    public ResponseEntity<ErrorDto> handleMultipartUploadSizeExceededException(MaxUploadSizeExceedException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }

    @ExceptionHandler(NotSupportedFileFormat.class)
    public ResponseEntity<ErrorDto> handleUnsupportedFileFormatException(
            NotSupportedFileFormat ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileNotPresentException.class)
    public ResponseEntity<ErrorDto> handleFileNotPresentException(
            FileNotPresentException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<ErrorDto> handleFileNotFoundException(
            FileNotFoundException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.NOT_FOUND.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorDto> handleIllegalArgumentException(IllegalArgumentException ex) {
        ErrorDto errorDetail = new ErrorDto(HttpStatus.BAD_REQUEST.value(),
                ex.getMessage(), ex.getMessage(), Calendar.getInstance());
        return ResponseEntity.badRequest().body(errorDetail);
    }
}
