create table file
(
    id            bigint auto_increment
        primary key,
    created_at    datetime     null,
    name          varchar(255) null,
    original_name varchar(255) null
) engine = InnoDB CHARACTER SET utf8 COLLATE utf8_unicode_ci;
