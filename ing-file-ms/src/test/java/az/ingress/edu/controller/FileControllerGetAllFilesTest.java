package az.ingress.edu.controller;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.model.File;
import az.ingress.edu.service.FileService;
import java.util.Arrays;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(FileController.class)
@ActiveProfiles("test")
public class FileControllerGetAllFilesTest {

    private static final String GET_ALL_FILES_PATH = "/file";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String NAME = "1234.png";
    private static final String ORIGINAL_NAME = "test.png";
    private static final String PAGE = "page";
    private static final String SIZE = "size";
    private static final String TOTAL_PAGES = "$.totalPages";
    private static final String TOTAL_ELEMENTS = "$.totalElements";
    private static final String CONTENT = "$.content";
    private static final String INDEX_MUST_BE_ZERO_OR_GREATER = "Page index must be 0 or greater";
    private static final String SIZE_MUST_BE_POSITIVE = "Page size must be positive";
    private static final String URL = "www.cloudinary.com/dummy.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @Test
    public void givenPageAndSizeGetAllFiles() throws Exception {
        Page<File> dummyPageFiles = new PageImpl<>(Arrays.asList(
                new File(1L,NAME,ORIGINAL_NAME,URL,Calendar.getInstance().toInstant()),
                new File(2L,NAME,ORIGINAL_NAME,URL,Calendar.getInstance().toInstant()),
                new File(3L,NAME,ORIGINAL_NAME,URL,Calendar.getInstance().toInstant()),
                new File(4L,NAME,ORIGINAL_NAME,URL,Calendar.getInstance().toInstant())
               ),
                PageRequest.of(0, 4, Sort.by(Sort.Direction.DESC, "id")), 4);
        when(fileService.findAll(anyInt(),anyInt(),anyString(),anyString())).thenReturn(dummyPageFiles);

        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_ALL_FILES_PATH).param(PAGE, "0").param(SIZE, "4")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath(TOTAL_PAGES).value(dummyPageFiles.getTotalPages()))
                .andExpect(jsonPath(TOTAL_ELEMENTS).value(dummyPageFiles.getTotalElements()))
                .andExpect(jsonPath(CONTENT).isNotEmpty());
    }

    @Test
    public void givenPageIsNegativeExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_ALL_FILES_PATH).param(PAGE, "-1").param(SIZE, "2")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(INDEX_MUST_BE_ZERO_OR_GREATER))
                .andExpect(jsonPath(USER_MESSAGE).value(INDEX_MUST_BE_ZERO_OR_GREATER));
    }

    @Test
    public void givenSizeIsNegativeExpectErrorMessage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_ALL_FILES_PATH).param(PAGE, "0").param(SIZE, "-2")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(SIZE_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(SIZE_MUST_BE_POSITIVE));
    }

    @Test
    public void givenInvalidOrderDirectionGetAllFilesExpectException() throws Exception {
        when(fileService.findAll(anyInt(),anyInt(),anyString(),anyString())).thenThrow(new IllegalArgumentException(
                "test"));

        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_ALL_FILES_PATH)
                .param(PAGE, "0")
                .param(SIZE, "2")
                .param("order", "id")
                .param("direction", "ascc"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()));
    }
}

