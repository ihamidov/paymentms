package az.ingress.edu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.FileNotPresentException;
import az.ingress.edu.exception.MaxUploadSizeExceedException;
import az.ingress.edu.exception.NotSupportedFileFormat;
import az.ingress.edu.model.File;
import az.ingress.edu.service.FileService;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(FileController.class)
@ActiveProfiles("test")
public class FileControllerUploadFileTest {

    private static final String TEST_FILE_ORIGINAL_NAME = "test.png";
    private static final String NON_SUPPORTED_CONTENT_TYPE = "plain/text";
    private static final String NON_SUPPORTED_FILE = "test.txt";
    private static final String FILE_UPLOAD = "/file/upload";
    private static final String FILE_NAME = "12343.png";
    private static final String DUMMY_TEXT = "dummy text";
    private static final String ID = "$.id";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String REQUEST_PART_NAME = "file";
    private static final String UNSUPPORTED_FILE_FORMAT = "Unsupported file format";
    private static final String ATTEMPT_TO_UPLOAD_EMPTY_FILE = "Attempt to upload empty file";
    private static final String URL = "www.cloudinary.com/dummy.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @Test
    public void givenFileWithSupportedFormatExpectOk() throws Exception {
        File file = new File(1L, FILE_NAME, TEST_FILE_ORIGINAL_NAME,URL, Calendar.getInstance().toInstant());
        MockMultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, TEST_FILE_ORIGINAL_NAME,
                MediaType.IMAGE_PNG_VALUE, readResourceFile(TEST_FILE_ORIGINAL_NAME));
        when(fileService.createFile(multipartFile)).thenReturn(file);

        mockMvc.perform(multipart(FILE_UPLOAD).file(multipartFile))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(1));
    }

    @Test
    public void givenFileWithNonSupportedFormatExpectErrorMessage() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, NON_SUPPORTED_FILE,
                NON_SUPPORTED_CONTENT_TYPE, DUMMY_TEXT.getBytes(Charset.forName("UTF-8")));
        when(fileService.createFile(multipartFile)).thenThrow(new NotSupportedFileFormat());

        mockMvc.perform(multipart(FILE_UPLOAD).file(multipartFile))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(UNSUPPORTED_FILE_FORMAT))
                .andExpect(jsonPath(USER_MESSAGE).value(UNSUPPORTED_FILE_FORMAT));
    }

    @Test
    public void givenFileExceedingMaxUploadSizeExpectErrorMessage() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, NON_SUPPORTED_FILE,
                NON_SUPPORTED_CONTENT_TYPE, DUMMY_TEXT.getBytes(Charset.forName("UTF-8")));
        when(fileService.createFile(multipartFile))
                .thenThrow(new MaxUploadSizeExceedException(multipartFile.getSize()));

        mockMvc.perform(multipart(FILE_UPLOAD).file(multipartFile))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE)
                        .value(getFileExceedsMaxUploadSizeMessage(multipartFile.getSize())))
                .andExpect(jsonPath(USER_MESSAGE)
                        .value(getFileExceedsMaxUploadSizeMessage(multipartFile.getSize())));
    }

    @Test
    public void givenEmptyFileExpectErrorMessage() throws Exception {
        byte[] emptyContent = new byte[]{};
        MockMultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, "", "",
                emptyContent);
        when(fileService.createFile(multipartFile)).thenThrow(new FileNotPresentException());

        mockMvc.perform(multipart(FILE_UPLOAD).file(multipartFile))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(ATTEMPT_TO_UPLOAD_EMPTY_FILE))
                .andExpect(jsonPath(USER_MESSAGE).value(ATTEMPT_TO_UPLOAD_EMPTY_FILE));
    }

    private byte[] readResourceFile(String pathOnClassPath) throws Exception {
        return Files.readAllBytes(Paths.get(Thread.currentThread().getContextClassLoader()
                .getResource(pathOnClassPath).toURI()));
    }

    private String getFileExceedsMaxUploadSizeMessage(long maxSize) {
        return String.format("File exceeds '%d' max upload size",maxSize);
    }
}

