package az.ingress.edu.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.edu.exception.FileNotFoundException;
import az.ingress.edu.model.File;
import az.ingress.edu.service.FileService;
import java.util.Calendar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(FileController.class)
@ActiveProfiles("test")
public class FileControllerGetFileTest {

    private static final String GET_FILE_PATH = "/file/{id}";
    private static final String TECHNICAL_MESSAGE = "$.technicalMessage";
    private static final String USER_MESSAGE = "$.userMessage";
    private static final String CODE = "$.code";
    private static final String FILE_ID_MUST_BE_POSITIVE = "File id must be positive";
    private static final String NAME = "1234.png";
    private static final String ORIGINAL_NAME = "test.png";
    private static final String ID = "$.id";
    private static final String URL = "www.cloudinary.com/dummy.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @Test
    public void givenFileWithInvalidIdExpectErrorMessage() throws Exception {
        long id = -1L;

        mockMvc.perform(MockMvcRequestBuilders
                .get(GET_FILE_PATH, id)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(CODE).value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(FILE_ID_MUST_BE_POSITIVE))
                .andExpect(jsonPath(USER_MESSAGE).value(FILE_ID_MUST_BE_POSITIVE));
    }

    @Test
    public void givenNonExistingFileIdExpectErrorMessage() throws Exception {
        long id = 1L;
        when(fileService.getFile(id)).thenThrow(new FileNotFoundException(id));

        mockMvc.perform(MockMvcRequestBuilders.get(GET_FILE_PATH, id))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(CODE).value(HttpStatus.NOT_FOUND.value()))
                .andExpect(jsonPath(TECHNICAL_MESSAGE).value(fileNotFoundMessage(id)))
                .andExpect(jsonPath(USER_MESSAGE).value(fileNotFoundMessage(id)));
    }

    @Test
    public void givenExistingFileExpectOk() throws Exception {
        long id = 1L;
        File file = new File(id, NAME, ORIGINAL_NAME,URL, Calendar.getInstance().toInstant());
        when(fileService.getFile(id)).thenReturn(file);

        mockMvc.perform(MockMvcRequestBuilders.get(GET_FILE_PATH, id))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(id));

        verify(fileService).getFile(id);
    }

    private String fileNotFoundMessage(long id) {
        return String.format("File with id: '%d' not found",id);
    }
}

