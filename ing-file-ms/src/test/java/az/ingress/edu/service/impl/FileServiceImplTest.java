package az.ingress.edu.service.impl;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import az.ingress.edu.config.FileUtilsConfiguration;
import az.ingress.edu.exception.FileNotFoundException;
import az.ingress.edu.exception.FileNotPresentException;
import az.ingress.edu.exception.MaxUploadSizeExceedException;
import az.ingress.edu.exception.NotSupportedFileFormat;
import az.ingress.edu.model.File;
import az.ingress.edu.repository.FileRepository;
import az.ingress.edu.util.FileUtils;
import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import com.cloudinary.utils.ObjectUtils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceImplTest {

    private static final String SUPPORTED_FILE = "test.png";
    private static final String NON_SUPPORTED_FILE = "test.txt";
    private static final String DUMMY_TEXT = "dummy text";
    private static final String REQUEST_PART_NAME = "file";
    private static final String GENERATED_NAME = "12343.png";
    private static final String ID = "id";
    private static final String DUMMY_URL = "www.google.com/file.png";

    @Mock
    FileRepository fileRepository;

    @Mock
    FileUtils fileUtils;

    @Mock
    FileUtilsConfiguration fileUtilsConfiguration;

    @Mock
    Cloudinary cloudinary;

    @InjectMocks
    FileServiceImpl fileService;

    @Captor
    ArgumentCaptor<File> captor;

    @Test
    public void givenEmptyFileCreateFileExpectException() {
        MultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, null, null,
                new byte[]{});

        assertThatThrownBy(() -> fileService.createFile(multipartFile)).isInstanceOf(FileNotPresentException.class);
    }

    @Test
    public void givenFileExceedingMaxUploadSizeFormatExpectException() throws Exception {
        MultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, SUPPORTED_FILE,MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(Paths.get(Thread.currentThread()
                        .getContextClassLoader().getResource(SUPPORTED_FILE).toURI())));
        when(fileUtilsConfiguration.getMaxUploadSize()).thenReturn(100L);
        assertThatThrownBy(() -> fileService.createFile(multipartFile))
                .isInstanceOf(MaxUploadSizeExceedException.class);
    }

    @Test
    public void givenFileWithNotSupportedFormatCreateFileExpectException() {
        MultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, NON_SUPPORTED_FILE,
                MediaType.TEXT_PLAIN_VALUE, DUMMY_TEXT.getBytes(Charset.forName("UTF-8")));
        when(fileUtilsConfiguration.getMaxUploadSize()).thenReturn(200000L);
        doThrow(new NotSupportedFileFormat()).when(fileUtils).validateFileExtension(anyString());

        assertThatThrownBy(() -> fileService.createFile(multipartFile)).isInstanceOf(NotSupportedFileFormat.class);
        verify(fileUtils).validateFileExtension(multipartFile.getOriginalFilename());
    }

    @Test
    public void givenFileWithSupportedFileFormatCreateFile() throws Exception {
        MultipartFile multipartFile = new MockMultipartFile(REQUEST_PART_NAME, SUPPORTED_FILE,MediaType.IMAGE_PNG_VALUE,
                Files.readAllBytes(Paths.get(Thread.currentThread()
                        .getContextClassLoader().getResource(SUPPORTED_FILE).toURI())));
        doNothing().when(fileUtils).validateFileExtension(anyString());
        when(fileUtils.getExtension(multipartFile.getOriginalFilename())).thenReturn(".png");
        when(fileUtilsConfiguration.getMaxUploadSize()).thenReturn(200000L);
        Uploader uploaderMock = mock(Uploader.class, RETURNS_DEEP_STUBS);
        Map<String, String> urlMap = new HashMap<>();
        when(cloudinary.uploader()).thenReturn(uploaderMock);
        urlMap.put("url", DUMMY_URL);
        when(cloudinary.uploader().upload(multipartFile.getBytes(), ObjectUtils.emptyMap())).thenReturn(urlMap);


        fileService.createFile(multipartFile);

        verify(fileRepository).save(captor.capture());
        assertThat(captor.getValue().getOriginalName()).isEqualTo(multipartFile.getOriginalFilename());
        verify(fileUtils).getExtension(multipartFile.getOriginalFilename());
        verify(fileUtils).validateFileExtension(multipartFile.getOriginalFilename());
    }

    @Test
    public void givenFileWithNonExistingIdGetFileUrlExpectException() {
        long id = 1L;
        when(fileRepository.findById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> fileService.getFileUrl(id)).isInstanceOf(FileNotFoundException.class);
        verify(fileRepository).findById(id);
    }

    @Test
    public void givenFileWithExistingIdGetFileUrl() throws IOException {
        long id = 1L;
        File file = new File(id, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant());
        when(fileRepository.findById(id)).thenReturn(Optional.of(file));
        String url = fileService.getFileUrl(id);

        assertThat(url).isNotEmpty();
        assertThat(url).isEqualTo(file.getUrl());

        verify(fileRepository).findById(id);
    }

    @Test
    public void givenFileWithNotExistingIdGetFileExpectException() {
        long id = 1L;
        when(fileRepository.findById(id)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> fileService.getFile(id)).isInstanceOf(FileNotFoundException.class);
        verify(fileRepository).findById(id);
    }

    @Test
    public void givenFileWithExistingIdGetFile() {
        long id = 1L;
        File file = new File(id, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant());
        when(fileRepository.findById(id)).thenReturn(Optional.of(file));

        File returnedFile = fileService.getFile(id);

        assertThat(returnedFile.getId()).isEqualTo(id);
        verify(fileRepository).findById(id);
    }

    @Test
    public void givenPageAndSizeFindAll() {
        List<File> dummyListFiles = Arrays.asList(
                new File(1L, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant()),
                new File(2L, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant()),
                new File(3L, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant()),
                new File(4L, GENERATED_NAME, SUPPORTED_FILE, DUMMY_URL, Calendar.getInstance().toInstant())
        );
        Page<File> dummyPageFiles = new PageImpl<>(dummyListFiles,
                PageRequest.of(0, 4, Sort.by(Sort.Direction.DESC, ID)), 4);
        when(fileRepository.findAll(any(Pageable.class))).thenReturn(dummyPageFiles);

        Page<File> expectedPageFiles = fileService.findAll(0, 4, ID, "desc");

        Assertions.assertThat(expectedPageFiles).isEqualTo(dummyPageFiles);
        Assertions.assertThat(expectedPageFiles.getSize()).isEqualTo(dummyPageFiles.getSize());
        Assertions.assertThat(expectedPageFiles.getTotalElements()).isEqualTo(dummyPageFiles.getTotalElements());
        Assertions.assertThat(expectedPageFiles.getTotalPages()).isEqualTo(dummyPageFiles.getTotalPages());
        verify(fileRepository).findAll(PageRequest.of(0, 4, Sort.by(Sort.Direction.DESC, ID)));
    }

    @Test
    public void givenInvalidSortDirectionFindAllExpectException() {
        Assertions.assertThatThrownBy(() -> fileService.findAll(0, 2, ID, "descc"))
                .isInstanceOf(IllegalArgumentException.class);
    }
}
